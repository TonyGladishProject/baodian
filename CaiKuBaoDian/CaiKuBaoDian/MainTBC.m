//
//  MainTBC.m
//  CaiKuBaoDian
//
//  Created by mac on 08/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "MainTBC.h"
#import "NavigationBaseVC.h"
#import "HomeVC.h"
#import "PersonalVC.h"
#import "BaiBaoXiangVC.h"
#import "ChatRoomVC.h"
#import "LoginVC.h"
#import "RegisterVC.h"
#import "OpenInstallSDK.h"
#import "ViewLaunch.h"
#import <SDWebImage/UIButton+WebCache.h>

@interface MainTBC ()<UITabBarControllerDelegate>
@property ( nonatomic, copy ) NSString * timeStamp;
@property ( nonatomic, copy ) NSString * timeStampOpen;
@property ( nonatomic, copy ) NSString * qs;
@property ( nonatomic, copy ) NSString * num;
@property ( nonatomic, assign) BOOL started;
@property (strong,nonatomic)dispatch_source_t sourceTimer;
@property ( nonatomic, strong ) ViewLaunch * viewLaunch;
@property ( nonatomic, copy ) NSString * floatbtnImg;
@end

@implementation MainTBC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshImage:) name:@"refreshImage" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(noti:) name:@"lotteryInfo" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(homeDataFinished) name:@"homeDataFinished" object:nil];
    if (!UserPhone) {
        [UserDefaults setValue:@"" forKey:@"mobile"];
        [UserDefaults synchronize];
    }
    NSArray * arr = @[[HomeVC new],[PersonalVC new],[BaiBaoXiangVC new],[ChatRoomVC new]];
    NSArray * arrImgs = @[@"ic_main_tab1_normal",@"ic_main_tab2_normal",@"ic_main_tab3_normal",@"ic_main_tab4_normal"];
    NSArray * arrImgSelected = @[@"ic_main_tab1_checked",@"ic_main_tab2_checked",@"ic_main_tab3_checked",@"ic_main_tab4_checked"];
    NSArray * arrTitles = @[@"六合彩票",@"个人中心",@"百宝箱",@"聊天室"];
    NSArray * arrNavTiles = @[@"彩库宝典",@"个人中心",@"六合宝箱",@"聊天室"];
    for (int i = 0; i < arr.count; i++) {
        MainHomeVC * b = arr[i];
        b.name = arrNavTiles[i];
        [self addChildViewController:arr[i] title:arrTitles[i] defaultImg:arrImgs[i] selectedImg:arrImgSelected[i] tag:i];
    }
    self.delegate = self;
    [self loadVersion];
    [self downTime];
    [self online];
    [self addView];
}

- (void)addView
{
    self.viewLaunch = [ViewLaunch newView];
    [self.view addSubview:self.viewLaunch];
}

-(void)createFloatBtn
{
    [MNFloatBtn show];
    self.floatbtn = [MNFloatBtn sharedBtn];
    self.floatbtn.frame = CGRectMake(WIDTH - 60, HEIGHT/2 + 150, 60, 60);
    [self.floatbtn setBackgroundImage:nil forState:UIControlStateNormal];
    [self.floatbtn sd_setImageWithURL:[NSURL URLWithString:self.floatbtnImg] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"pop_caim"]];
    self.floatbtn.btnClick = ^(UIButton *sender) {
        [Tools presentPocket];
    };
}

- (void)refreshImage:(NSNotification *)noti
{
    [self.viewLaunch.img sd_setImageWithURL:[NSURL URLWithString:noti.userInfo[@"launchImg"]] placeholderImage:[UIImage imageNamed:@"img11"]];
    self.floatbtnImg = noti.userInfo[@"buttonImg"];
}

- (void)homeDataFinished
{
    if ([UserDefaults valueForKey:@"showEnterTime"]) {
        [self showHomeVC];
    } else {
        [UserDefaults setValue:@"YES" forKey:@"showEnterTime"];
        [UserDefaults synchronize];
        [self showDownTime];
    }
}

- (void)showDownTime
{
    [self showTime];
    __block NSInteger second = 5;
    //(1)
    dispatch_queue_t quene = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    //(2)
    dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, quene);
    //(3)
    dispatch_source_set_timer(timer, DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC, 0 * NSEC_PER_SEC);
    //(4)
    dispatch_source_set_event_handler(timer, ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            if (second <= 0) {
                second = 5;
                [self showHomeVC];
                //(6)
                dispatch_cancel(timer);
            } else {
                self.viewLaunch.time.text = [NSString stringWithFormat:@"%lds",second];
                second--;
            }
        });
    });
    //(5)
    dispatch_resume(timer);
}

- (void)showTime
{
    self.viewLaunch.label.hidden = YES;
    [self.viewLaunch.indicator stopAnimating];
    self.viewLaunch.indicator.hidden = YES;
    self.viewLaunch.time.hidden = NO;
}

- (void)showHomeVC
{
    [self timeCompare];
    [self.viewLaunch removeFromSuperview];
    [self getInstallParams];
    [self createFloatBtn];
}

- (void)noti:(NSNotification *)noti
{
    if (self.sourceTimer) {
        dispatch_source_cancel(self.sourceTimer);
    }
    _started = NO;
    self.timeStamp = noti.userInfo[@"timeprompt"];
    self.num = noti.userInfo[@"num"];
    self.qs = noti.userInfo[@"qs"];
    NSLog(@"----noti-%@,%@,%@,%d",self.timeStamp,self.num,self.qs,[self.num mj_isPureInt]);
    if (![self.num mj_isPureInt]) {
        self.timeStampOpen  = [Tools getNowTimeTimestamp];
        _started = YES;
        [self downTimeNum];
    }
}

#pragma mark - online
- (void)online
{
    [self timeCompare];
    NSString *UDID = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    [HTTPTool postWithURL:APIOnline parameters:@{@"machine_code":UDID,@"mobile":UserPhone,@"device":@"ios"} success:^(id  _Nonnull responseObject) {
        
    } failure:^(NSError * _Nonnull error) {
        
    }];
}

- (void)downTime
{
    __block NSInteger second = 20;
    //(1)
    dispatch_queue_t quene = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    //(2)
    dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, quene);
    //(3)
    dispatch_source_set_timer(timer, DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC, 0 * NSEC_PER_SEC);
    //(4)
    dispatch_source_set_event_handler(timer, ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            if (second <= 0) {
                second = 20;
                
                [self downTime];
                [self online];
                //(6)
                dispatch_cancel(timer);
            } else {
//                NSLog(@"-----time-%ld",(long)second);
                second--;
            }
        });
    });
    //(5)
    dispatch_resume(timer);
}

#pragma mark - refreshNum
- (void)timeCompare
{
    NSLog(@"-----online-%@---%@",[Tools getNowTimeTimestamp],self.timeStamp);
    NSString * nowtimeStamp = [Tools getNowTimeTimestamp];
    if (self.timeStamp) {
        if (self.timeStamp.intValue - nowtimeStamp.intValue < 60 * 5 &&  self.timeStamp.intValue - nowtimeStamp.intValue > -60 * 15) {
            if (!_started) {
                _started = YES;
                [self downTimeNum];
            }
        } else {
            if (self.timeStampOpen) {
                if (nowtimeStamp.intValue - self.timeStampOpen.intValue > 60 * 15) {
                    NSLog(@"停止轮训--正在开奖");
                    self.timeStampOpen = nil;
                    if (self.sourceTimer) {
                        dispatch_source_cancel(self.sourceTimer);
                    }
                }
            } else {
                if (nowtimeStamp.intValue - self.timeStamp.intValue > 60 * 15) {
                    NSLog(@"停止轮训--超时停止");
                    if (self.sourceTimer) {
                        dispatch_source_cancel(self.sourceTimer);
                    }
                }
            }
        }
    }
}

- (void)openNum
{
    [HTTPTool getWithURL:APIOpenNum parameters:@{} success:^(id  _Nonnull responseObject) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshLottery" object:nil userInfo:responseObject[@"data"]];
    } failure:^(NSError * _Nonnull error) {
    }];
}

- (void)downTimeNum
{
    __block NSInteger second = 3;
    //(1)
    dispatch_queue_t quene = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    //(2)
    dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, quene);
    self.sourceTimer = timer;
    //(3)
    dispatch_source_set_timer(timer, DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC, 0 * NSEC_PER_SEC);
    //(4)
    dispatch_source_set_event_handler(timer, ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            if (second <= 0) {
                second = 3;
                NSLog(@"-----open");
                [self downTimeNum];
                [self openNum];
                //(6)
                dispatch_cancel(timer);
            } else {
//                NSLog(@"-----time-%ld",(long)second);
                second--;
            }
        });
    });
    //(5)
    dispatch_resume(timer);
}

#pragma mark-----获取动态安装参数，示例----------(根据自身业务在所需处获取动态安装参数)

-(void)getInstallParams{
    if ([UserDefaults valueForKey:@"install_share_code"]) {
        return;
    }
    [[OpenInstallSDK defaultManager] getInstallParmsCompleted:^(OpeninstallData*_Nullable appData) {
        if (appData.data) {//(动态安装参数)
            //e.g.如免填邀请码建立邀请关系、自动加好友、自动进入某个群组或房间等
            if (appData.data[@"share_code"]) {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [UserDefaults setValue:@"YES" forKey:@"install_share_code"];
                    [UserDefaults synchronize];
                    RegisterVC * r = [RegisterVC new];
                    r.code = appData.data[@"share_code"];
                    UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:r];
                    [self presentViewController:nav animated:NO completion:nil];
                });
            }
        }
    }];
}

- (void)loadVersion
{
    NSString *versionLocal = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    [HTTPTool getWithURL:APIVersion success:^(id  _Nonnull responseObject) {
        NSLog(@"responseObject=%@",responseObject);
        NSString * versionUrl = responseObject[@"data"][@"app_version"];
        if ([Tools compareVersion:versionUrl versionLocal:versionLocal]) {
            [MBProgressHUD showMessage:@"检测到新版本，开始更新"];
            NSString * urlStr = [@"itms-services://?action=download-manifest&url=" stringByAppendingString:APIDownload];
            NSLog(@"--------url-%@",urlStr);
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStr]];
        }
    }];
}

- (void)noRead
{
    [HTTPTool postWithURL:APINoRead parameters:@{@"username":UserPhone} success:^(id  _Nonnull responseObject) {
        
        UITabBarItem * item=self.tabBar.items[1];
        if (responseObject[@"data"][@"noReadNumber"]) {
            item.badgeValue=responseObject[@"data"][@"noReadNumber"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"showDot" object:nil];
        }
    }];
}

- (void)loadUserInfo
{
    [HTTPTool postWithURL:APIUserInfo parameters:@{@"username":UserPhone}  success:^(id  _Nonnull responseObject) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"userBalance" object:nil userInfo:@{@"score":responseObject[@"data"][@"score"]}];
        [UserDefaults setValue:responseObject[@"data"][@"score"] forKey:@"score"];
        [UserDefaults synchronize];
    }];
}

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController
{
    if (tabBarController.tabBar.selectedItem.tag == 1001) {
        if ([UserDefaults boolForKey:@"isLogin"]) {
            [self noRead];
            [self loadUserInfo];
        } else {
            UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:[LoginVC new]];
            [self presentViewController:nav animated:YES completion:nil];
            return NO;
        }
    } else if (tabBarController.tabBar.selectedItem.tag == 1002) {
        [HTTPTool postWithURL:APIBox parameters:@{} success:^(id  _Nonnull responseObject) {
            NSNumber * num = responseObject[@"flag"];
            NSString * str = [[NSNumberFormatter new] stringFromNumber:num];
            if (str.intValue == 1) {
                NSDictionary * data = responseObject[@"data"];
                if (![UserDefaults valueForKey:@"qs"]) {
                    [UserDefaults setValue:data[@"desc"] forKey:@"qs"];
                } else {
                    NSString * str = [UserDefaults valueForKey:@"qs"];
                    if (![str isEqualToString:data[@"desc"]]) {
                        [UserDefaults setValue:data[@"desc"] forKey:@"qs"];
                        [UserDefaults removeObjectForKey:@"boxShake"];
                        [UserDefaults removeObjectForKey:@"boxCards"];
                        [UserDefaults removeObjectForKey:@"boxKit"];
                        [UserDefaults removeObjectForKey:@"boxCode"];
                        [UserDefaults removeObjectForKey:@"boxTable"];
                        [UserDefaults removeObjectForKey:@"boxGuess"];
                        [UserDefaults removeObjectForKey:@"boxLuck"];
                    }
                }
                [UserDefaults synchronize];
            }
        }];
    }
    return YES;
}

- (void)addChildViewController:(UIViewController *)childController title:(NSString *)title defaultImg:(NSString *)img selectedImg:(NSString *)selectedImg tag:(int)tag
{
    NavigationBaseVC * n = [[NavigationBaseVC alloc] initWithRootViewController:childController];
    n.tabBarItem.title = title;
    n.tabBarItem.image = [[UIImage imageNamed:img] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    n.tabBarItem.selectedImage = [[UIImage imageNamed:selectedImg]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [self addChildViewController:n];
    [n.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName:UIColorFromRGB(0xcc7064)} forState:UIControlStateSelected];
    n.tabBarItem.tag = 1000 + tag;
}
-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
