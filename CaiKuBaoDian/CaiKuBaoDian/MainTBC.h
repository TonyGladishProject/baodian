//
//  MainTBC.h
//  CaiKuBaoDian
//
//  Created by mac on 08/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MNFloatBtn.h"

NS_ASSUME_NONNULL_BEGIN

@interface MainTBC : UITabBarController
@property ( nonatomic, strong ) MNFloatBtn * floatbtn;
@end

NS_ASSUME_NONNULL_END
