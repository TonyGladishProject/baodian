//
//  DateVC.m
//  CaiKuBaoDian
//
//  Created by mac on 13/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "DateVC.h"
#import "IntroductionView.h"
#import "FSCalendar.h"
#import <EventKit/EventKit.h>

@interface DateVC ()<FSCalendarDataSource, FSCalendarDelegate>
@property ( nonatomic, strong ) FSCalendar *calendar;
@property ( nonatomic, strong ) NSDateFormatter * dateFormatter;
@property (strong, nonatomic) NSCalendar *chineseCalendar;
@property (strong, nonatomic) NSArray<EKEvent *>  *events;
@property (strong, nonatomic) NSArray<NSString *>  *lunarChars;
@property ( nonatomic, strong ) IntroductionView * v;
@end

@implementation DateVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadData];
    [self refreshUI];
}

- (void)loadData
{
    [MBProgressHUD viewLoading:self.view];
    [HTTPTool postWithURL:APIDate parameters:@{@"year":@"2019"} success:^(id  _Nonnull responseObject) {
        [MBProgressHUD hideHUDForView:self.view];
        
        for (NSDictionary * dic in responseObject[@"data"]) {
            NSString * month = dic[@"month"];
            if (month.length < 2) {
                month = [NSString stringWithFormat:@"0%@",month];
            }
            for (NSString * str in dic[@"date"]) {
                NSString * day = str;
                if (day.length < 2) {
                    day = [NSString stringWithFormat:@"0%@",day];
                }
                NSString * date = [NSString stringWithFormat:@"%@-%@-%@",dic[@"year"],month,day];
                [self.dataArray addObject:date];
            }
        }
        [self createCalendar];
        [self createWebView];
    }];
}

- (void)refreshUI
{
    self.titleLabel.text = @"搅珠日期";
    
    IntroductionView * v = [IntroductionView newView];
    [v labelStr:@"搅珠日期对照表，可查看当月以及下一个月的搅珠开奖日期"];
    [self.scrollView addSubview:v];
    self.v = v;

}

- (void)createCalendar
{
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    self.dateFormatter = formatter;
    
    self.calendar = [[FSCalendar alloc] initWithFrame:CGRectMake(0, self.v.bottom, WIDTH, 300)];
    self.calendar.dataSource = self;
    self.calendar.delegate = self;
    [self.scrollView addSubview:self.calendar];
    
    self.chineseCalendar = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierChinese];
    self.lunarChars = @[@"初一",@"初二",@"初三",@"初四",@"初五",@"初六",@"初七",@"初八",@"初九",@"初十",@"十一",@"十二",@"十三",@"十四",@"十五",@"十六",@"十七",@"十八",@"十九",@"二十",@"二一",@"二二",@"二三",@"二四",@"二五",@"二六",@"二七",@"二八",@"二九",@"三十"];
    
    __weak typeof(self) weakSelf = self;
    EKEventStore *store = [[EKEventStore alloc] init];
    [store requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
              if(granted) {
            NSDate *startDate = [NSDate dateWithTimeIntervalSinceNow:-3600*24*90]; // 开始日期
            NSDate *endDate =[NSDate dateWithTimeIntervalSinceNow:3600*24*90]; // 截止日期
            NSPredicate *fetchCalendarEvents = [store predicateForEventsWithStartDate:startDate endDate:endDate calendars:nil];
             NSArray*eventList = [store eventsMatchingPredicate:fetchCalendarEvents];
            NSArray*events = [eventList filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(EKEvent * _Nullable event, NSDictionary* _Nullable bindings) {
                return event.calendar.subscribed;
                
            }]];
            weakSelf.events = events;
            
        }}];
}


- (NSString *)calendar:(FSCalendar *)calendar subtitleForDate:(NSDate *)date{
     EKEvent *event = [self eventsForDate:date].firstObject;
          if (event) {
        return event.title;
        
    }
    NSInteger day = [_chineseCalendar component:NSCalendarUnitDay fromDate:date];
      return _lunarChars[day-1];
    
}
//代理添加 农历显示
- (NSArray*)eventsForDate:(NSDate *)date{
    NSArray*filteredEvents = [self.events filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(EKEvent * _Nullable evaluatedObject, NSDictionary* _Nullable bindings) {
        return [evaluatedObject.occurrenceDate isEqualToDate:date];
        
    }]];
    return filteredEvents;
    
}//返回特殊节日的方法

- (NSInteger)calendar:(FSCalendar *)calendar numberOfEventsForDate:(NSDate *)date{//要标记的日期显示圆点3个其他不显示
    for (NSString * str  in self.dataArray) {
        if ([[self.dateFormatter stringFromDate:date] isEqualToString:str]) {
             return 3;
        }
    }
    return 0;
}


-(void)calendar:(FSCalendar *)calendar didSelectDate:(NSDate *)date atMonthPosition:(FSCalendarMonthPosition)monthPosition{
    NSLog(@"--%@",[self.dateFormatter stringFromDate:date]);
    
    NSArray * arr = [[self.dateFormatter stringFromDate:date] componentsSeparatedByString:@"-"];
    NSString * l = arr.lastObject;
    if ([l substringToIndex:1].intValue ==0) {
        l = [l substringFromIndex:1];
    }
    NSString *functionString = [NSString stringWithFormat:@"selectDay('%@','%@','%@')",arr.firstObject,arr[1],l];
    [self.webView stringByEvaluatingJavaScriptFromString:functionString];
}

- (void)createWebView
{
    _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, self.calendar.bottom, WIDTH,400)];
    NSURL * webUrl = [NSURL URLWithString:APIDateWeb];
    NSURLRequest * requst = [NSURLRequest requestWithURL:webUrl];
    [_webView loadRequest:requst];
    [self.scrollView addSubview:_webView];
//    self.webView.delegate = self;

    self.scrollView.contentSize = CGSizeMake(WIDTH, self.webView.bottom);
}

- (NSMutableArray *)dataArray
{
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

@end
