//
//  ShakeVC.m
//  CaiKuBaoDian
//
//  Created by mac on 01/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "ShakeVC.h"
#import "IntroductionView.h"
#import "PromptLabel.h"
#import <AudioToolbox/AudioToolbox.h>
#import "BallView.h"

@interface ShakeVC ()
@property ( nonatomic, strong ) PromptLabel * prompt;
@property ( nonatomic, assign ) CGFloat w;
@property ( nonatomic, assign ) CGFloat h;
@property ( nonatomic, assign) BOOL showed;
@property ( nonatomic, strong ) NSArray * arrNums;
@end

@implementation ShakeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.titleLabel.text = @"摇一摇";
    [UIApplication sharedApplication].applicationSupportsShakeToEdit = YES;
    [self becomeFirstResponder];
    [self refreshUI];
}

- (void)refreshUI
{
    self.prompt = [PromptLabel newView];
    self.prompt.y = self.contentViewHeight - 80;
    [self.scrollView addSubview:self.prompt];
    self.prompt.text = @"小提示:每期只能进行一次\"摇一摇\"";
    
    IntroductionView * v = [IntroductionView newView];
    [v labelStr:@"试试您的手气到底有多准，只要摇一摇就能算出专属您的本期特码!"];
    [self.scrollView addSubview:v];
    
    float w = WIDTH - 80;
    float h = self.contentViewHeight - 80;
    self.w = w;
    self.h = h;
    
    self.imgPure = [[UIImageView alloc] initWithFrame:CGRectMake(40, 0, w, h)];
    [self.scrollView addSubview:self.imgPure];
    self.imgPure.image = [UIImage imageNamed:@"shake_light"];
    self.imgPure.contentMode = UIViewContentModeScaleAspectFit;
    
    NSArray * arr ;
    if ([UserDefaults valueForKey:@"boxShake"]) {
        arr = [UserDefaults valueForKey:@"boxShake"];
    } else {
        arr = [self randomArray];
    }
    self.arrNums = arr;
    float bw = (WIDTH - 30 - 9 * 5)/8;
    float bx = (WIDTH - bw * 3 - 20)/2;
    
    for (int i = 0; i < 3; i++) {
        BallView * view = [BallView newView];
        view.centerY = h/2;
        view.x = bx + i * (bw + 10);
        view.img.image = [UIImage imageNamed:[Tools colorFromNum:arr[i]]];
        view.label.text = arr[i];
        [self.scrollView addSubview:view];
    }
    
    if ([UserDefaults valueForKey:@"boxShake"]) {
        _showed = YES;
        return;
    }
    self.imgUp = [[UIImageView alloc] initWithFrame:CGRectMake(40, h/2 -w/2, w, w/2)];
    [self.scrollView addSubview:self.imgUp];
    self.imgUp.image = [UIImage imageNamed:@"shake_up"];
    self.imgUp.contentMode = UIViewContentModeScaleAspectFit;
    
    self.imgDown = [[UIImageView alloc] initWithFrame:CGRectMake(40, h/2, w, w/2)];
    [self.scrollView addSubview:self.imgDown];
    self.imgDown.image = [UIImage imageNamed:@"shake_down"];
    self.imgDown.contentMode = UIViewContentModeScaleAspectFit;
    
    self.img = [[UIImageView alloc] initWithFrame:CGRectMake(40, 0, w, h)];
    [self.scrollView addSubview:self.img];
    self.img.image = [UIImage imageNamed:@"shake1"];
    self.img.contentMode = UIViewContentModeScaleAspectFit;
    self.img.animationImages = @[[UIImage imageNamed:@"shake1"],[UIImage imageNamed:@"shake2"],[UIImage imageNamed:@"shake3"]];
    self.img.animationRepeatCount = 0;
    self.img.animationDuration = 1;
    [self.img startAnimating];
}

- (void)motionBegan:(UIEventSubtype)motion withEvent:(UIEvent *)event {
    NSLog(@"开始摇动");
    if (_showed) {
        return;
    }
    NSString *audioFile=[[NSBundle mainBundle] pathForResource:@"shake_sound_male" ofType:@"mp3"];
    NSURL *fileUrl=[NSURL fileURLWithPath:audioFile];
    //1.获得系统声音ID
    SystemSoundID soundID=0;
    /**
     * inFileUrl:音频文件url
     * outSystemSoundID:声音id（此函数会将音效文件加入到系统音频服务中并返回一个长整形ID）
     */
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)(fileUrl), &soundID);
    //如果需要在播放完之后执行某些操作，可以调用如下方法注册一个播放完成回调函数
//    AudioServicesAddSystemSoundCompletion(soundID, NULL, NULL, soundCompleteCallback, NULL);
    //2.播放音频
//    AudioServicesPlaySystemSound(soundID);//播放音效
        AudioServicesPlayAlertSound(soundID);//播放音效并震动
    
    return;
}

void soundCompleteCallback(SystemSoundID soundID,void * clientData){
    NSLog(@"播放完成...");
    //3.销毁声音
    AudioServicesDisposeSystemSoundID(soundID);
}

- (void)motionCancelled:(UIEventSubtype)motion withEvent:(UIEvent *)event {
    NSLog(@"取消摇动");
    return;
}

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event {
    if (event.subtype == UIEventSubtypeMotionShake) { // 判断是否是摇动结束
        NSLog(@"摇动结束");
        if (_showed) {
            return;
        }
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            NSString *audioFile=[[NSBundle mainBundle] pathForResource:@"shake_match" ofType:@"mp3"];
            NSURL *fileUrl=[NSURL fileURLWithPath:audioFile];
            //1.获得系统声音ID
            SystemSoundID soundIDd=0;
            /**
             * inFileUrl:音频文件url
             * outSystemSoundID:声音id（此函数会将音效文件加入到系统音频服务中并返回一个长整形ID）
             */
            AudioServicesCreateSystemSoundID((__bridge CFURLRef)(fileUrl), &soundIDd);
            //如果需要在播放完之后执行某些操作，可以调用如下方法注册一个播放完成回调函数
            AudioServicesAddSystemSoundCompletion(soundIDd, NULL, NULL, soundcompleteCallback, NULL);
            //2.播放音频
            AudioServicesPlaySystemSound(soundIDd);//播放音效
            [self showNumbers];
        });
        
    }
    return;
}

void soundcompleteCallback(SystemSoundID soundIDd,void * clientData){
    NSLog(@"播放完成...");
    //3.销毁声音
    AudioServicesDisposeSystemSoundID(soundIDd);
}

- (void)showNumbers
{
    [UserDefaults setValue:self.arrNums forKey:@"boxShake"];
    [UserDefaults synchronize];
    _showed = YES;
    self.img.hidden = YES;
    [UIView animateWithDuration:1 animations:^{
        self.imgUp.y = self.h/2 -self.w;
    } completion:^(BOOL finished) {
        self.imgUp.hidden = YES;
    }];
    [UIView animateWithDuration:1 animations:^{
        self.imgDown.y = self.h/2 +self.w/2;
    } completion:^(BOOL finished) {
        self.imgDown.hidden = YES;
    }];
}

-(NSArray *)randomArray
{
    //随机数从这里边产生
    NSMutableArray *startArray=[[NSMutableArray alloc] initWithObjects:
                                @"01",@"02",@"03",@"04",@"05",@"06",@"07", @"08",@"09",@"10",
                                @"11",@"12",@"13",@"14",@"15",@"16",@"17",@"18",@"19",@"20",
                                @"21",@"22",@"23",@"24",@"25",@"26",@"27",@"28",@"29",@"30",
                                @"31",@"32",@"33",@"34",@"35",@"36",@"37",@"38",@"39",@"40",
                                @"41",@"42",@"43",@"44",@"45",@"46",@"47",@"48",@"49",nil];
    //随机数产生结果
    NSMutableArray *resultArray=[[NSMutableArray alloc] initWithCapacity:0];
    //随机数个数
    NSInteger m=3;
    for (int i=0; i<m; i++) {
        int t=arc4random()%startArray.count;
        resultArray[i]=startArray[t];
        startArray[t]=[startArray lastObject]; //为更好的乱序，故交换下位置
        [startArray removeLastObject];
    }
    return resultArray;
}

@end
