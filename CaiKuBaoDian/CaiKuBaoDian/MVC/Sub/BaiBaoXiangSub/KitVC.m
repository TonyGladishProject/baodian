//
//  KitVC.m
//  CaiKuBaoDian
//
//  Created by mac on 03/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "KitVC.h"
#import "IntroductionView.h"
#import "PromptLabel.h"

@interface KitVC ()
@property ( nonatomic, strong ) UILabel * label;
@property ( nonatomic, copy ) NSString * str;
@property ( nonatomic, strong ) UIButton * button;
@end

@implementation KitVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self refreshUI];
}

- (void)refreshUI
{
    self.titleLabel.text = @"玄机锦囊";
    
    IntroductionView * v = [IntroductionView newView];
    [v labelStr:@"10万六合彩用户都说准的谜题，这一期，您猜出什么玄机？"];
    [self.scrollView addSubview:v];
    
    PromptLabel * prompt = [PromptLabel newView];
    prompt.y = self.contentViewHeight - 80;
    [prompt labelStr:@"温馨提示:打开锦囊将获取本期一道六合谜题，参透谜题将获得本期中奖号码，快来开启你的玄机之旅吧!"];
    [self.scrollView addSubview:prompt];
    
    self.img = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, WIDTH/2, WIDTH/2)];
    self.img.center = CGPointMake(WIDTH/2, v.bottom  + WIDTH/4);
    [self.scrollView addSubview:self.img];
    self.img.image = [UIImage imageNamed:@"kit1"];
    self.img.contentMode = UIViewContentModeScaleAspectFit;
    self.img.animationImages = @[[UIImage imageNamed:@"kit1"],[UIImage imageNamed:@"kit2"],[UIImage imageNamed:@"kit3"],[UIImage imageNamed:@"kit4"],[UIImage imageNamed:@"kit5"],[UIImage imageNamed:@"kit6"]];
    self.img.animationRepeatCount = 1;
    self.img.animationDuration = 2;
    
    UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0, 0, 100, 40);
    [self.scrollView addSubview:button];
    button.center = CGPointMake(WIDTH/2, self.img.bottom + 40);
    button.backgroundColor = UIColorFromRGB(0xD4B651);
    [button setTitle:@"打开锦囊" forState:UIControlStateNormal];
    [button layerMask5];
    [button addTarget:self action:@selector(butonClick:) forControlEvents:UIControlEventTouchUpInside];
    self.button = button;
    
    self.label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 40)];
    self.label.center = CGPointMake(WIDTH/2, self.img.bottom + 40);
    [self.scrollView addSubview:self.label];
    self.label.hidden = YES;
    
    if ([UserDefaults valueForKey:@"boxKit"]) {
        self.str = [UserDefaults valueForKey:@"boxKit"];
        [self showResult];
    }
}

- (void)butonClick:(UIButton *)button
{
    [self.img startAnimating];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self showResult];
    });
    
    [self doKit];
}

- (void)showResult
{
    self.img.image = [UIImage imageNamed:@"kit6"];
    self.button.hidden = YES;
    self.label.hidden = NO;
    [Tools setLabelHeight:self.label str:self.str width:200 font:17];
}

- (void)doKit
{
    [HTTPTool postWithURL:APIKit parameters:@{} success:^(id  _Nonnull responseObject) {
        self.str = responseObject[@"data"];
        [UserDefaults setValue:self.str forKey:@"boxKit"];
        [UserDefaults synchronize];
    }];
}

@end
