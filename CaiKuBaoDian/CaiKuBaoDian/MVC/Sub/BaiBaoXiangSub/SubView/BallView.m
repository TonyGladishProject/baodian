//
//  BallView.m
//  CaiKuBaoDian
//
//  Created by mac on 09/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "BallView.h"

@implementation BallView

+(instancetype)newView
{
    float w = (WIDTH - 30 - 9 * 5)/8;
    BallView * view = [[BallView alloc] initWithFrame:CGRectMake(0, 0, w,w)];
    return view;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self createUI];
    }
    return self;
}

- (void)createUI
{
    self.backgroundColor = UIColor.whiteColor;
    [self layerMask];
    
    self.img = [[UIImageView alloc] initWithFrame:self.bounds];
    [self addSubview:self.img];
    
    self.label = [[UILabel alloc] initWithFrame:self.bounds];
    self.label.x = -2;
    self.label.y = -3;
    [self addSubview:self.label];
    self.label.textAlignment = NSTextAlignmentCenter;
    
}

@end
