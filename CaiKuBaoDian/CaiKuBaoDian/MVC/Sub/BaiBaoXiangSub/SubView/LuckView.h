//
//  LuckView.h
//  CaiKuBaoDian
//
//  Created by mac on 09/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "BaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface LuckView : BaseView
@property (weak, nonatomic) IBOutlet UIImageView *img;
@property (weak, nonatomic) IBOutlet UIImageView *imgUp;
@property (weak, nonatomic) IBOutlet UIImageView *imgDown;
@property (weak, nonatomic) IBOutlet UILabel *label;
@property ( nonatomic, assign ) int i;
@property ( nonatomic, assign ) int j;
@property ( nonatomic, strong ) NSMutableArray * arrNum;
@end

NS_ASSUME_NONNULL_END
