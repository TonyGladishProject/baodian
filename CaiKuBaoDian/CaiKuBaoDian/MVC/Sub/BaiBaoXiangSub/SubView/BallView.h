//
//  BallView.h
//  CaiKuBaoDian
//
//  Created by mac on 09/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "BaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface BallView : BaseView
@property ( nonatomic, strong ) UIImageView * img;
@property ( nonatomic, strong ) UILabel * label;
@end

NS_ASSUME_NONNULL_END
