//
//  LuckView.m
//  CaiKuBaoDian
//
//  Created by mac on 09/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "LuckView.h"
#import "BallView.h"

@implementation LuckView


+(instancetype)newView
{
    LuckView * view = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil].lastObject;
    view.frame = CGRectMake(0, 0, WIDTH,view.contentViewHeight);
    return view;
}

- (void)drawRect:(CGRect)rect {
    
    self.imgUp.animationImages = @[[UIImage imageNamed:@"img_luck_bg1@3x.jpg"],[UIImage imageNamed:@"img_luck_bg2@3x.jpg"]];
    self.imgUp.animationRepeatCount = 0;
    self.imgUp.animationDuration = 1;
    [self.imgUp startAnimating];
    self.label.y = WIDTH/375*70;
    self.label.centerX = WIDTH/2;
    [self.imgDown addSubview:self.label];
//    self.img.center = CGPointMake(WIDTH/2, WIDTH/3);
    self.i = 0;
    self.j = 0;
    [self.img touchEvents:self action:@selector(imgTouch)];
}

- (void)showBall
{
    if (self.arrNum.count > 0) {
        for (BallView * view in self.arrNum) {
            [UIView animateWithDuration:1 animations:^{
                view.alpha = 0.0;
            } completion:^(BOOL finished) {
                [view removeFromSuperview];
            }];
        }
    }
    NSArray * arr = [self randomArray];
    self.arrNum = [NSMutableArray arrayWithCapacity:8];
    for ( int i = 0; i < 8; i++) {
        BallView * view = [BallView newView];
        view.center = CGPointMake(WIDTH/375*40 + (view.width + 5)*i - WIDTH, self.imgDown.height/4);
        [self.imgDown addSubview:view];
        
        if (i == 6) {
            view.img.image = [UIImage imageNamed:@"icon_luck_add"];
            view.backgroundColor = UIColor.clearColor;
        } else {
            view.img.image = [UIImage imageNamed:[Tools colorFromNum:arr[i]]];
            view.label.text = arr[i];
        }
        [self.arrNum addObject:view];
    }
}

-(NSArray *)randomArray
{
    //随机数从这里边产生
    NSMutableArray *startArray=[[NSMutableArray alloc] initWithObjects:
                                @"01",@"02",@"03",@"04",@"05",@"06",@"07", @"08",@"09",@"10",
                                @"11",@"12",@"13",@"14",@"15",@"16",@"17",@"18",@"19",@"20",
                                @"21",@"22",@"23",@"24",@"25",@"26",@"27",@"28",@"29",@"30",
                                @"31",@"32",@"33",@"34",@"35",@"36",@"37",@"38",@"39",@"40",
                                @"41",@"42",@"43",@"44",@"45",@"46",@"47",@"48",@"49",nil];
    //随机数产生结果
    NSMutableArray *resultArray=[[NSMutableArray alloc] initWithCapacity:0];
    //随机数个数
    NSInteger m=8;
    for (int i=0; i<m; i++) {
        int t=arc4random()%startArray.count;
        resultArray[i]=startArray[t];
        startArray[t]=[startArray lastObject]; //为更好的乱序，故交换下位置
        [startArray removeLastObject];
    }
    return resultArray;
}

- (void)imgTouch
{
    [self showBall];
    [self rotateImageView];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self imgMove];
    });
}

- (void)rotateImageView {
    self.i++;
//    NSLog(@"------?-%d",self.i);
    // 一秒钟旋转几圈
    CGFloat circleByOneSecond = 8.f;
    
    // 执行动画
    [UIView animateWithDuration:1.f / circleByOneSecond
                          delay:0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         self.img.transform = CGAffineTransformRotate(self.img.transform, M_PI_2);
                     }
                     completion:^(BOOL finished){
                         if (self.i<40) {
                             [self rotateImageView];
                         } else {
                             self.i=0;
                         }
                     }];
}

- (void)imgMove
{
    [UIView animateWithDuration:.6 animations:^{
        BallView * view = self.arrNum[self.j];
        view.centerX = WIDTH/375*40 + (view.width + 5)*self.j;
    } completion:^(BOOL finished) {
        self.j ++;
        if (self.j < 8) {
            [self imgMove];
        } else {
            self.j = 0;
        }
    }];
}

@end
