//
//  CodeView.h
//  CaiKuBaoDian
//
//  Created by mac on 03/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "BaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface CodeView : BaseView
@property (weak, nonatomic) IBOutlet UIButton *buttonBoy;
@property (weak, nonatomic) IBOutlet UIButton *buttonGirl;
@property (weak, nonatomic) IBOutlet UILabel *yourBirth;
@property (weak, nonatomic) IBOutlet UILabel *loverBirth;
@property (weak, nonatomic) IBOutlet UIButton *button;
@property ( nonatomic, strong ) UIView * mainView;
@end

NS_ASSUME_NONNULL_END
