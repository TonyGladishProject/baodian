//
//  CodeNumsView.h
//  CaiKuBaoDian
//
//  Created by mac on 06/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "BaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface CodeNumsView : BaseView
@property ( nonatomic, strong ) UIImageView * img;
@property ( nonatomic, strong ) NSMutableArray * arrNums;
@property ( nonatomic, strong ) NSMutableArray * arrN;
@property ( nonatomic, strong ) NSMutableArray * arrI;
@end

NS_ASSUME_NONNULL_END
