//
//  FlowView.h
//  CaiKuBaoDian
//
//  Created by mac on 17/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "BaseView.h"
#import "NewPagedFlowView.h"

NS_ASSUME_NONNULL_BEGIN
@interface FlowModel : NSObject
@property ( nonatomic, copy ) NSString * name;
@property ( nonatomic, copy ) NSString * imageName;
@end

@interface FlowView : BaseView<NewPagedFlowViewDelegate, NewPagedFlowViewDataSource>
@property ( nonatomic, strong ) NewPagedFlowView * pageView;
@property ( nonatomic, strong ) UIImageView * imgBG;
@property ( nonatomic, copy ) NSString * selectedStr;
@property (nonatomic, copy)  void(^finishedBlock) (NSString * str);
- (void)numbers:(NSMutableArray *)dataArray;
- (void)shengXiao:(NSMutableArray *)dataArray;
@end

NS_ASSUME_NONNULL_END
