//
//  GuessImgView.m
//  CaiKuBaoDian
//
//  Created by mac on 08/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "GuessImgView.h"
#import "FlowView.h"

@implementation GuessImgView

+(instancetype)newView
{
    GuessImgView * view = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil].lastObject;
    view.frame = CGRectMake(0, 0, WIDTH,view.contentViewHeight);
    return view;
}

- (void)drawRect:(CGRect)rect {
    if ([UserDefaults valueForKey:@"boxGuess"]) {
        NSDictionary * dic = [UserDefaults valueForKey:@"boxGuess"];
        [self.buttonSX setTitle:dic[@"sx"] forState:UIControlStateNormal];
        [self.buttonWeiShu setTitle:dic[@"numbers"] forState:UIControlStateNormal];
        _showedSX = _showedNumbers = YES;
    }
}

- (IBAction)buttonSXClick:(UIButton *)sender {
    if (_showedSX) {
        return;
    }
    self.sx = @"";
    kSelfWeak
    float x = (WIDTH - 80 * 3)/4;
    for (int i = 0; i < 3; i++) {
        FlowView * view = [FlowView newView];
        view.center = CGPointMake(x + 40 + (x + 80)*i, 180);
        [self addSubview:view];
        NSMutableArray * arr = [NSMutableArray arrayWithObjects:@"guess_shu",@"guess_niu",@"guess_hu",@"guess_tu",@"guess_long",@"guess_she",@"guess_ma",@"guess_yang",@"guess_hou",@"guess_ji",@"guess_gou",@"guess_zhu", nil];
        [view shengXiao:arr];
        view.finishedBlock = ^(NSString * _Nonnull str) {
            [weakSelf showSX:str];
        };
    }
}
- (IBAction)buttonWeiShuClick:(UIButton *)sender {
    if (_showedNumbers) {
        return;
    }
    self.numbers = @"";
    kSelfWeak
    float x = (WIDTH - 80 * 3)/4;
    for (int i = 0; i < 3; i++) {
        FlowView * view = [FlowView newView];
        view.center = CGPointMake(x + 40 + (x + 80)*i, 180);
        [self addSubview:view];
        NSMutableArray * arr = [NSMutableArray arrayWithCapacity:10];
        for (int i = 0; i < 10; i++) {
            [arr addObject:[NSString stringWithFormat:@"guess_%d",i]];
        }
        [view numbers:arr];
        view.finishedBlock = ^(NSString * _Nonnull str) {
            [weakSelf showNumber:str];
        };
    }
}

- (void)showNumber:(NSString *)str
{
    self.numbers = [self.numbers stringByAppendingString:str];
    if (self.numbers.length > 2) {
        [self.buttonWeiShu setTitle:self.numbers forState:UIControlStateNormal];
        _showedNumbers = YES;
    }
}

- (void)showSX:(NSString *)str
{
    self.sx = [self.sx stringByAppendingString:str];
    if (self.sx.length > 2) {
        [self.buttonSX setTitle:self.sx forState:UIControlStateNormal];
        _showedSX = YES;
    }
}
@end
