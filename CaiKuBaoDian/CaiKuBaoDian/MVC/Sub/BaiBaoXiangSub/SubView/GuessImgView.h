//
//  GuessImgView.h
//  CaiKuBaoDian
//
//  Created by mac on 08/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "BaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface GuessImgView : BaseView
@property (weak, nonatomic) IBOutlet UIButton *buttonSX;
@property (weak, nonatomic) IBOutlet UIButton *buttonWeiShu;
@property ( nonatomic, copy ) NSString * numbers;
@property ( nonatomic, copy ) NSString * sx;
@property ( nonatomic, assign) BOOL showedSX;
@property ( nonatomic, assign) BOOL showedNumbers;
@end

NS_ASSUME_NONNULL_END
