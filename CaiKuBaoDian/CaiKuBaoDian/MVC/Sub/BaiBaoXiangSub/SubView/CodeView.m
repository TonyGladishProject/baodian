//
//  CodeView.m
//  CaiKuBaoDian
//
//  Created by mac on 03/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "CodeView.h"


@implementation CodeView

+(instancetype)newView
{
    CodeView * view = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil].lastObject;
    view.frame = CGRectMake(0, 0, WIDTH,view.contentViewHeight);
    return view;
}

- (void)drawRect:(CGRect)rect {
    [self.yourBirth layerMask5];
    [self.loverBirth layerMask5];
    [self.button layerMask5];
    [self.buttonBoy setImage:[UIImage imageNamed:@"boyKuang"] forState:UIControlStateNormal];
    [self.buttonBoy setImage:[UIImage imageNamed:@"boyGou"] forState:UIControlStateSelected];
    [self.buttonGirl setImage:[UIImage imageNamed:@"girlKuang"] forState:UIControlStateNormal];
    [self.buttonGirl setImage:[UIImage imageNamed:@"girlGou"] forState:UIControlStateSelected];
    self.buttonBoy.selected = YES;
}
- (IBAction)boyButtonClick:(UIButton *)sender {
    sender.selected = !sender.selected;
    self.buttonGirl.selected = NO;
}
- (IBAction)girlButtonClick:(UIButton *)sender {
    sender.selected = !sender.selected;
    self.buttonBoy.selected = NO;
}

@end
