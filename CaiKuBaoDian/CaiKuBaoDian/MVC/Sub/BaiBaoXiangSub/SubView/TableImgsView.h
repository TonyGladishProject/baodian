//
//  TableImgsView.h
//  CaiKuBaoDian
//
//  Created by mac on 07/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "BaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface TableImgsView : BaseView
@property (weak, nonatomic) IBOutlet UIView *groundView;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *arrSX;
@property (weak, nonatomic) IBOutlet UIButton *button;
@property (weak, nonatomic) IBOutlet UILabel *labelSX;
@property (weak, nonatomic) IBOutlet UILabel *labelColor;

@property ( nonatomic, strong ) NSMutableArray * arrCles;
@property ( nonatomic, assign ) NSInteger slctedID;//12生肖旋转id
@property ( nonatomic, assign ) NSInteger slctedBall;//球球
@property ( nonatomic, strong ) NSArray * resultArray;
@property ( nonatomic, assign ) NSInteger slctedItem;//结果id
@end

NS_ASSUME_NONNULL_END
