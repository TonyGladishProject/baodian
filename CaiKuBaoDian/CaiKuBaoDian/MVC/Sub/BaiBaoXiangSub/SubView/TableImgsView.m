//
//  TableImgsView.m
//  CaiKuBaoDian
//
//  Created by mac on 07/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "TableImgsView.h"
#define degreesToRadians(degrees) ((degrees * (float)M_PI) / 180.0f)

@implementation TableImgsView

+(instancetype)newView
{
    TableImgsView * view = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil].lastObject;
    view.frame = CGRectMake(0, 0, WIDTH,view.contentViewHeight);
    return view;
}

- (void)drawRect:(CGRect)rect {
    [self.button layerMask5];
    
    for (int i = 0; i < self.arrSX.count; i++) {
        UIButton * button = self.arrSX[i];
        [button setImage:[UIImage imageNamed:@"tableKuang"] forState:UIControlStateSelected];
    }
    [self createCircle];
    
    if ([UserDefaults valueForKey:@"boxTable"]) {
        NSDictionary * dic = [UserDefaults valueForKey:@"boxTable"];
        self.resultArray = dic[@"sxArr"];
        self.labelSX.text = dic[@"sxText"];
        self.labelColor.text = dic[@"colorText"];
        NSString * str = dic[@"ballIndex"];
        self.button.hidden = YES;
        for (NSString * string in self.resultArray) {
            UIButton * button = self.arrSX[string.integerValue - 1];
            button.selected = YES;
        }
        
        UIButton * ball = self.arrCles[str.integerValue];
        ball.selected = YES;
    } else {
        self.resultArray = [self randomArray];
    }
}

- (void)createCircle
{
    NSArray * arr = @[@"tableRed",@"tableGreen",@"tableBlue",@"tableRed",@"tableGreen",@"tableBlue",@"tableRed",@"tableGreen",@"tableBlue"];
    float dist = 60;//半径
    if (iphone8Plus) {
        dist = 70;
    } else if (iphone5) {
        dist = 50;
    }
    for (int i = 0; i < 9; i++) {
        float angle = degreesToRadians((360/9) * i);
        float x = cos(angle + 30) * dist;
        float y = sin(angle + 30) * dist;
        UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(0, 0, 50, 50);
        btn.center = CGPointMake(self.groundView.centerX + x, self.groundView.centerY + y);
        [btn setBackgroundImage:[UIImage imageNamed:arr[i]] forState:UIControlStateNormal];
        [btn setImage:[UIImage imageNamed:@"tableCircle"] forState:UIControlStateSelected];
        [self.groundView addSubview:btn];
        [self.arrCles addObject:btn];
        [btn setTitle:[NSString stringWithFormat:@"%d",i] forState:UIControlStateNormal];
    }
}

- (IBAction)buttonClick:(UIButton *)sender
{
    [self countdownTime:sender];
}

- (NSArray *)randomArray
{
    //随机数从这里边产生
    NSMutableArray *startArray=[[NSMutableArray alloc] initWithObjects:
                                @"1",@"2",@"3",@"4",@"5",@"6",
                                @"7", @"8",@"9",@"10", @"11",@"12",nil];
    //随机数产生结果
    NSMutableArray *resultArray=[[NSMutableArray alloc] initWithCapacity:0];
    //随机数个数
    NSInteger m=3;
    for (int i=0; i<m; i++) {
        int t=arc4random()%startArray.count;
        resultArray[i]=startArray[t];
        startArray[t]=[startArray lastObject]; //为更好的乱序，故交换下位置
        [startArray removeLastObject];
    }
    return resultArray;
}

#pragma mark - 倒计时
- (void)countdownTime:(UIButton *)button
{
    button.hidden = YES;
    NSArray * arrSX = @[@"鼠",@"牛",@"虎",@"兔",@"龙",@"蛇",@"马",@"羊",@"猴",@"鸡",@"狗",@"猪"];
    NSArray * arrC = @[@"红波",@"绿波",@"蓝波",@"红波",@"绿波",@"蓝波",@"红波",@"绿波",@"蓝波"];
    NSString * str1 = self.resultArray.firstObject;
    NSString * str2 = self.resultArray[1];
    int f = 12 + str1.intValue;
    int s = 24 + str2.intValue;
    NSString * str = self.resultArray.lastObject;
    self.slctedID = 11;
    self.slctedBall = 8;
    __block int second = 36 + str.intValue;
    int a = second;
    //(1)
    dispatch_queue_t quene = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    //(2)
    dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, quene);
    //(3)
    dispatch_source_set_timer(timer, DISPATCH_TIME_NOW, .1 * NSEC_PER_SEC, 0 * NSEC_PER_SEC);
    //(4)
    dispatch_source_set_event_handler(timer, ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            if (second <= 0) {
                second = 6;
                self.labelSX.text = [self.labelSX.text stringByAppendingString:[NSString stringWithFormat:@" %@",arrSX[self.slctedID]]];
                self.labelColor.text = arrC[self.slctedBall];
                NSDictionary * dic = @{@"sxArr":self.resultArray,@"ballIndex":[NSString stringWithFormat:@"%lu",self.slctedBall],@"sxText":self.labelSX.text,@"colorText":self.labelColor.text};
                [UserDefaults setValue:dic forKey:@"boxTable"];
                [UserDefaults synchronize];
                //(6)
                dispatch_cancel(timer);
            } else {
                
                UIButton * btn = self.arrSX[self.slctedID];
                if ( s != (a - second) && s != (a - second -12) && f != (a - second) && f != (a - second -12) && f != (a - second -24)) {
                    btn.selected = NO;
                } else if ( f == (a - second)) {
                    self.labelSX.text = arrSX[self.slctedID];
                } else if ( s == (a - second)) {
                   self.labelSX.text = [self.labelSX.text stringByAppendingString:[NSString stringWithFormat:@" %@",arrSX[self.slctedID]]];
                }
                UIButton *ballB = self.arrCles[self.slctedBall];
                ballB.selected = NO;
                
                if (self.slctedID == 11) {
                    self.slctedID = -1;
                }
                self.slctedID ++;
                UIButton * b = self.arrSX[self.slctedID];
                b.selected = YES;
                
                if (self.slctedBall == 8) {
                    self.slctedBall = -1;
                }
                self.slctedBall ++;
                UIButton * ball = self.arrCles[self.slctedBall];
                ball.selected = YES;
                second --;
            }
        });
    });
    //(5)
    dispatch_resume(timer);
}

- (NSMutableArray *)arrCles
{
    if (!_arrCles) {
        _arrCles = [NSMutableArray array];
    }
    return _arrCles;
}
@end
