//
//  CodeNumsView.m
//  CaiKuBaoDian
//
//  Created by mac on 06/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "CodeNumsView.h"

@implementation CodeNumsView

+(instancetype)newView
{
    return [[CodeNumsView alloc] initWithFrame:CGRectMake(0, 0, WIDTH,HEIGHT/1.5)];
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self createUI];
    }
    return self;
}

- (void)createUI
{
    self.img = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, WIDTH/2, WIDTH/2)];
    self.img.center = CGPointMake(WIDTH/2, 40  + WIDTH/4);
    [self addSubview:self.img];
    self.img.image = [UIImage imageNamed:@"jb7"];
    self.img.contentMode = UIViewContentModeScaleAspectFit;
    self.img.animationImages = @[[UIImage imageNamed:@"jb1"],[UIImage imageNamed:@"jb2"],[UIImage imageNamed:@"jb3"],[UIImage imageNamed:@"jb4"],[UIImage imageNamed:@"jb5"],[UIImage imageNamed:@"jb6"],[UIImage imageNamed:@"jb7"]];
    self.img.animationRepeatCount = 1;
    self.img.animationDuration = 1;
    
    
    NSArray * arr ;
    if ([UserDefaults valueForKey:@"boxCode"]) {
        arr = [UserDefaults valueForKey:@"boxCode"];
    } else {
        arr = [self randomArray];
        self.arrNums = [NSMutableArray arrayWithArray:arr];
    }
    CGFloat f = WIDTH - 40;
    CGFloat x = 10;
    CGFloat w = (f - 70)/6;
    for (int i = 0; i < 6; i++) {
        UIImageView * img = [[UIImageView alloc] initWithFrame:CGRectMake(20 + 10 + (w+10) *i, 20, w, w)];
        [self.arrI addObject:img];
        [self addSubview:img];
        img.hidden = YES;
        img.image = [UIImage imageNamed:[Tools colorFromNum:arr[i]]];
        
        UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(16 + 10 + (w+10) *i, 16, w, w)];
        [self.arrN addObject:label];
        [self addSubview:label];
        label.hidden = YES;
        label.textAlignment = NSTextAlignmentCenter;
        label.text = arr[i];
        if (iphone5) {
            label.font = fontCell;
        } else if (iphone8Plus) {
            label.font = [UIFont systemFontOfSize:23];
        }
        
    }
}

-(NSArray *)randomArray
{
    //随机数从这里边产生
    NSMutableArray *startArray=[[NSMutableArray alloc] initWithObjects:
                                @"01",@"02",@"03",@"04",@"05",@"06",@"07", @"08",@"09",@"10",
                                @"11",@"12",@"13",@"14",@"15",@"16",@"17",@"18",@"19",@"20",
                                @"21",@"22",@"23",@"24",@"25",@"26",@"27",@"28",@"29",@"30",
                                @"31",@"32",@"33",@"34",@"35",@"36",@"37",@"38",@"39",@"40",
                                @"41",@"42",@"43",@"44",@"45",@"46",@"47",@"48",@"49",nil];
    //随机数产生结果
    NSMutableArray *resultArray=[[NSMutableArray alloc] initWithCapacity:0];
    //随机数个数
    NSInteger m=6;
    for (int i=0; i<m; i++) {
        int t=arc4random()%startArray.count;
        resultArray[i]=startArray[t];
        startArray[t]=[startArray lastObject]; //为更好的乱序，故交换下位置
        [startArray removeLastObject];
    }
    return resultArray;
}

- (NSMutableArray *)arrNums
{
    if (!_arrNums) {
        _arrNums = [NSMutableArray array];
    }
    return _arrNums;
}

- (NSMutableArray *)arrN
{
    if (!_arrN) {
        _arrN = [NSMutableArray array];
    }
    return _arrN;
}

- (NSMutableArray *)arrI
{
    if (!_arrI) {
        _arrI = [NSMutableArray array];
    }
    return _arrI;
}

@end
