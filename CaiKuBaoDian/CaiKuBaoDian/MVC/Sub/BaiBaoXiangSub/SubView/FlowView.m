//
//  FlowView.m
//  CaiKuBaoDian
//
//  Created by mac on 17/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "FlowView.h"

@implementation FlowModel
@end

@interface FlowView ()
@property ( nonatomic, strong ) NSMutableArray * array;
@end

@implementation FlowView


+(instancetype)newView
{
    return [[FlowView alloc] initWithFrame:CGRectMake(0, 0, 80, 160)];
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self createUI];
    }
    return self;
}

- (void)numbers:(NSMutableArray *)dataArray
{
    int x = arc4random() % dataArray.count;
    
    for (int i = x; i < 50; i++) {
        int j = i % dataArray.count;
        FlowModel * model = [FlowModel new];
        model.name = [NSString stringWithFormat:@"%d",j];
        model.imageName = dataArray[j];
        [self.array addObject:model];
    }
    [self.pageView reloadData];
}

- (void)shengXiao:(NSMutableArray *)dataArray
{
    int x = arc4random() % dataArray.count;
    
    for (int i = x; i < 50; i++) {
        int j = i % dataArray.count;
        FlowModel * model = [FlowModel new];
        model.imageName = dataArray[j];
        model.name = [Tools modelName:[model.imageName substringFromIndex:6]];
        [self.array addObject:model];
    }
    [self.pageView reloadData];
}

- (void)createUI
{
    self.imgBG = [[UIImageView alloc] initWithFrame:self.bounds];
    [self addSubview:self.imgBG];
    self.imgBG.image = [UIImage imageNamed:@"img_tianji_bg"];
    
    self.pageView = [[NewPagedFlowView alloc] initWithFrame:self.bounds];
    self.pageView.delegate = self;
    self.pageView.dataSource = self;
    self.pageView.minimumPageAlpha = 0.0;
    self.pageView.orientation = NewPagedFlowViewOrientationVertical;
    self.pageView.autoTime = 0.1;
    [self addSubview:self.pageView];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.pageView stopTimer];
    });
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (self.finishedBlock) {
            self.finishedBlock(self.selectedStr);
        }
    });
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self removeFromSuperview];
    });
}

#pragma mark NewPagedFlowView Delegate
- (CGSize)sizeForPageInFlowView:(NewPagedFlowView *)flowView {
    return CGSizeMake(50, 50);
}

- (void)didSelectCell:(UIView *)subView withSubViewIndex:(NSInteger)subIndex {
    NSLog(@"点击了第%ld张图",(long)subIndex + 1);
}

- (void)didScrollToPage:(NSInteger)pageNumber inFlowView:(NewPagedFlowView *)flowView {
    FlowModel * model = self.array[pageNumber];
    self.selectedStr = model.name;
//    NSLog(@"ViewController 滚动到了第%ld页",pageNumber);
}

#pragma mark NewPagedFlowView Datasource
- (NSInteger)numberOfPagesInFlowView:(NewPagedFlowView *)flowView {
    return self.array.count;
}

- (PGIndexBannerSubiew *)flowView:(NewPagedFlowView *)flowView cellForPageAtIndex:(NSInteger)index{
    PGIndexBannerSubiew *bannerView = [flowView dequeueReusableCell];
    if (!bannerView) {
        bannerView = [[PGIndexBannerSubiew alloc] init];
        bannerView.tag = index;
        bannerView.layer.cornerRadius = 4;
        bannerView.layer.masksToBounds = YES;
    }
    FlowModel * model = self.array[index];
    bannerView.mainImageView.image = [UIImage imageNamed:model.imageName];
    return bannerView;
}

- (NSMutableArray *)array
{
    if (!_array) {
        _array = [NSMutableArray array];
    }
    return _array;
}

@end

