//
//  SXCardsVC.m
//  CaiKuBaoDian
//
//  Created by mac on 01/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "SXCardsVC.h"
#import "IntroductionView.h"
#import "PromptLabel.h"

@interface SXCardsVC ()
@property ( nonatomic, strong ) PromptLabel * prompt;
@property ( nonatomic, assign ) int count;
@end

@implementation SXCardsVC

- (void)viewDidLoad {
    [super viewDidLoad];
 
    self.count = 0;
    self.titleLabel.text = @"生肖卡牌";
    self.prompt = [PromptLabel newView];
    self.prompt.y = self.contentViewHeight/1.8;
    self.prompt.text = @"小提示:您本期已选过，请下期再来";
    
    IntroductionView * v = [IntroductionView newView];
    CGFloat h = [v labelStr:@"每期开奖前，通过该工具，可以快捷的查看3个隐藏在卡牌中的生肖，测测自己的财运，每期只能看一次哦"];
    v.y = - h;
    self.collectionView.contentInset = UIEdgeInsetsMake(h, 0, 0, 0);
    [self.collectionView addSubview:v];
    
    if ([UserDefaults valueForKey:@"boxCards"]) {
        self.sltdArr = [UserDefaults valueForKey:@"boxCards"];
        [self.collectionView reloadData];
        [self.collectionView addSubview:self.prompt];
        return;
    }
    
    [self randomCards];
}

- (void)randomCards
{
    self.dataArray = [NSMutableArray arrayWithArray:@[
                                                      @"gou",@"hou",@"hu",
                                                      @"ji",@"long",@"ma",
                                                      @"niu",@"she",@"shu",
                                                      @"tu",@"yang",@"zhu"]];
    int i = (int)self.dataArray.count;
    while (--i > 0) {
        int j = rand() % (i + 1);
        [self.dataArray exchangeObjectAtIndex:i withObjectAtIndex:j];
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (self.sltdArr.count > 2) {
        return self.sltdArr.count;
    }
    return self.dataArray.count;
}

- (void)collectionView:(CollectionViewCell *)cell dataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    cell.image.frame = cell.bounds;
    cell.imgSX.frame = cell.bounds;
    if (self.sltdArr.count > 2) {
        [cell.image removeFromSuperview];
        [cell addSubview:cell.imgSX];
        cell.imgSX.image = [UIImage imageNamed:self.sltdArr[indexPath.row]];
    } else {
        cell.image.image = [UIImage imageNamed:@"cards"];
        cell.imgSX.image = [UIImage imageNamed:self.dataArray[indexPath.row]];
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return  CGSizeMake((WIDTH -40)/3,(WIDTH -30)/3 * 1.4);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(10, 10,10, 10);//（上、左、下、右）
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 9.5;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 10;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    CollectionViewCell * cell = (CollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    if (self.count >= 3) {
        return;
    }
    self.count ++;
    if (!cell.isSlcted && self.sltdArr.count < 3) {
        [self animtionView:cell indexPath:indexPath];
    }
}

- (void)animtionView:(CollectionViewCell *)cell indexPath:(NSIndexPath *)indexPath
{
    UIViewAnimationOptions option = UIViewAnimationOptionTransitionFlipFromRight;
    [UIView transitionWithView:cell                      duration:1
                       options:option
                    animations:^ {
                        [cell.image removeFromSuperview];
                        [cell addSubview:cell.imgSX];
                    }
                    completion:^(BOOL finished){
                        cell.isSlcted = YES;
                        [self.sltdArr addObject:self.dataArray[indexPath.row]];
                        if (self.sltdArr.count >2) {
                            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                                [self.collectionView reloadData];
                                [self.collectionView addSubview:self.prompt];
                                [UserDefaults setValue:self.sltdArr forKey:@"boxCards"];
                                [UserDefaults synchronize];
                            });
                        }
                    }];
}

- (NSMutableArray *)sltdArr
{
    if (!_sltdArr) {
        _sltdArr = [NSMutableArray array];
    }
    return _sltdArr;
}

@end
