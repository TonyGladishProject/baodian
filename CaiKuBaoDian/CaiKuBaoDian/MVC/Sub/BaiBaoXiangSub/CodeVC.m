
//
//  CodeVC.m
//  CaiKuBaoDian
//
//  Created by mac on 03/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "CodeVC.h"
#import "IntroductionView.h"
#import "PromptLabel.h"
#import "CodeView.h"
#import "DatePicker.h"
#import "CodeNumsView.h"

@interface CodeVC ()
@property ( nonatomic, strong ) CodeView * codeView;
@property ( nonatomic, strong ) CodeNumsView * numsView;
@property ( nonatomic, strong ) DatePicker * datePicker;
@end

@implementation CodeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self refreshUI];
}

- (void)refreshUI
{
    self.titleLabel.text = @"恋人特码";
    
    IntroductionView * v = [IntroductionView newView];
    [v labelStr:@"请输入您和他/她的生日，计算出本期特码，赶紧来试试"];
    [self.scrollView addSubview:v];
    
    PromptLabel * prompt = [PromptLabel newView];
    prompt.y = self.contentViewHeight - 80;
    prompt.text = @"小提示:每期只能进行一次匹配";
    
    self.codeView = [CodeView newView];
    self.codeView.frame = CGRectMake(0, v.bottom, WIDTH, self.contentViewHeight - v.bottom);
    
    [self.scrollView addSubview:self.codeView];
    [self.scrollView addSubview:prompt];
    self.codeView.mainView = self.view;
    
    self.datePicker = [DatePicker newView];
    [self.view addSubview:self.datePicker];
    [self.view bringSubviewToFront:self.datePicker];
    
    [self.codeView.yourBirth touchEvents:self action:@selector(yourBirthTouch)];
    [self.codeView.loverBirth touchEvents:self action:@selector(loverBirthTouch)];
    [self.codeView.button addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
    
    self.numsView = [CodeNumsView newView];
    self.numsView.y = v.bottom;
    
    if ([UserDefaults valueForKey:@"boxCode"]) {
        [self.scrollView addSubview:self.numsView];
        [self.codeView removeFromSuperview];
        [self showNums];
    }
}

- (void)yourBirthTouch
{
    self.datePicker.label = self.codeView.yourBirth;
    [self.datePicker show];
}
-(void)loverBirthTouch
{
    self.datePicker.label = self.codeView.loverBirth;
    [self.datePicker show];
}

- (IBAction)buttonClick:(UIButton *)sender {
    if ([self.codeView.yourBirth.text isEqualToString:@" 请选择您的生日"]) {
        [MBProgressHUD showMessage:@"请选择您的生日"];
        return;
    } else if ([self.codeView.loverBirth.text isEqualToString:@" 请选择恋人生日"]) {
        [MBProgressHUD showMessage:@"请选择恋人生日"];
        return;
    }
    
    [self doMake];
}

- (void)doMake
{
    [self.scrollView addSubview:self.numsView];
    [self.codeView removeFromSuperview];
    [self.numsView.img startAnimating];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self showNums];
        [UserDefaults setValue:self.numsView.arrNums forKey:@"boxCode"];
        [UserDefaults synchronize];
    });
}

- (void)showNums
{
    self.numsView.img.hidden = YES;
    for (int i = 0 ; i < 6; i++) {
        UILabel * label = self.numsView.arrN[i];
        label.hidden = NO;
        
        UIImageView * img = self.numsView.arrI[i];
        img.hidden = NO;
    }
}
@end
