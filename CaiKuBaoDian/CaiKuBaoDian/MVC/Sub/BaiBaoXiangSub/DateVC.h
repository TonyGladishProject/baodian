//
//  DateVC.h
//  CaiKuBaoDian
//
//  Created by mac on 13/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "ScrollViewVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface DateVC : ScrollViewVC<UIWebViewDelegate>
@property ( nonatomic, strong ) UIWebView * webView;
@property ( nonatomic, strong ) NSMutableArray * dataArray;
@end

NS_ASSUME_NONNULL_END
