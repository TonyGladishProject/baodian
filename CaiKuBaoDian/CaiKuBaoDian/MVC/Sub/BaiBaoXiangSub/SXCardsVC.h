//
//  SXCardsVC.h
//  CaiKuBaoDian
//
//  Created by mac on 01/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "CollectionVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface SXCardsVC : CollectionVC
@property ( nonatomic, strong ) NSMutableArray * sltdArr;
@end

NS_ASSUME_NONNULL_END
