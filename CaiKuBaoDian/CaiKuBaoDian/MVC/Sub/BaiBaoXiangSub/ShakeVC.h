//
//  ShakeVC.h
//  CaiKuBaoDian
//
//  Created by mac on 01/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "ScrollViewVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface ShakeVC : ScrollViewVC
@property ( nonatomic, strong ) UIImageView * img;
@property ( nonatomic, strong ) UIImageView * imgPure;
@property ( nonatomic, strong ) UIImageView * imgUp;
@property ( nonatomic, strong ) UIImageView * imgDown;
@end

NS_ASSUME_NONNULL_END
