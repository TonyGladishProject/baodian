
//
//  TableVC.m
//  CaiKuBaoDian
//
//  Created by mac on 07/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "TableVC.h"
#import "IntroductionView.h"
#import "PromptLabel.h"
#import "TableImgsView.h"

@interface TableVC ()
@property ( nonatomic, strong ) TableImgsView * imgsView;
@end

@implementation TableVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self refreshUI];
}

- (void)refreshUI
{
    self.titleLabel.text = @"波肖转盘";
    
    IntroductionView * v = [IntroductionView newView];
    [v labelStr:@"转转转!转出本期的生肖和波色，激动不如行动，感觉来转出您心目中的生肖和波色!"];
    [self.scrollView addSubview:v];
    
    self.imgsView = [TableImgsView newView];
    self.imgsView.frame = CGRectMake(0, v.bottom, WIDTH, self.contentHeight - v.bottom);
    [self.scrollView addSubview:self.imgsView];
    
    PromptLabel * prompt = [PromptLabel newView];
    prompt.y = self.contentViewHeight - 80;
    [prompt labelStr:@"小提示:每期只能进行一次波肖转盘"];
    [self.scrollView addSubview:prompt];
}

@end
