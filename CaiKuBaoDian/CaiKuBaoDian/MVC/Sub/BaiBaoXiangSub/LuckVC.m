//
//  LuckVC.m
//  CaiKuBaoDian
//
//  Created by mac on 09/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "LuckVC.h"
#import "PromptLabel.h"
#import "LuckView.h"

@interface LuckVC ()
@property ( nonatomic, strong ) LuckView * imgsView;
@end

@implementation LuckVC



- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self refreshUI];
}

- (void)refreshUI
{
    self.titleLabel.text = @"幸运转盘";
    
    self.imgsView = [LuckView newView];
    self.imgsView.frame = CGRectMake(0, 0, WIDTH, self.contentHeight);
    [self.scrollView addSubview:self.imgsView];
    
    PromptLabel * prompt = [PromptLabel newView];
    prompt.y = self.contentViewHeight - 80;
    [prompt labelStr:@"幸运摇奖只用于模拟开奖，点击\"开始摇号\"，系统将自动摇出一组幸运号码"];
//    [self.scrollView addSubview:prompt];
    
}

@end


