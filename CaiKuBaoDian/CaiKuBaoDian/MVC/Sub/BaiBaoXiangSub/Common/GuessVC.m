//
//  GuessVC.m
//  CaiKuBaoDian
//
//  Created by mac on 08/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "GuessVC.h"
#import "IntroductionView.h"
#import "PromptLabel.h"
#import "GuessImgView.h"

@interface GuessVC ()
@property ( nonatomic, strong ) GuessImgView * imgsView;
@end

@implementation GuessVC


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self refreshUI];
}

- (void)refreshUI
{
    self.titleLabel.text = @"天机测算";
    
    IntroductionView * v = [IntroductionView newView];
    [v labelStr:@"天机泄露啦!!!所求何事，让诸葛亮丞相为你卜一卦，祝你六合彩顺风顺水"];
    [self.scrollView addSubview:v];
    
    self.imgsView = [GuessImgView newView];
    self.imgsView.frame = CGRectMake(0, v.bottom, WIDTH, self.contentHeight - v.bottom);
    [self.scrollView addSubview:self.imgsView];
    
    PromptLabel * prompt = [PromptLabel newView];
    prompt.y = self.contentViewHeight - 80;
    if (HEIGHT < 740) {
        prompt.y = self.contentViewHeight - 40;
    }
    if (iphone5) {
        prompt.y = self.contentViewHeight - 20;
    }
    [prompt labelStr:@"小提示:每期只能进行一次\"天机测算\""];
    [self.scrollView addSubview:prompt];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    
    if (self.imgsView.numbers.length > 0 && self.imgsView.sx.length > 0) {
        NSDictionary * dic = @{@"sx":self.imgsView.sx,@"numbers":self.imgsView.numbers};
        [UserDefaults setValue:dic forKey:@"boxGuess"];
    }
}

@end

