//
//  IntroductionView.h
//  CaiKuBaoDian
//
//  Created by mac on 03/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "BaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface IntroductionView : BaseView
@property ( nonatomic, strong ) UIImageView * img;
@property ( nonatomic, strong ) UILabel * name;
@property ( nonatomic, strong ) UILabel * label;
- (CGFloat)labelStr:(NSString *)str;
@end

NS_ASSUME_NONNULL_END
