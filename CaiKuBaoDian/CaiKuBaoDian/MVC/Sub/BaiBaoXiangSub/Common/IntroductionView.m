//
//  IntroductionView.m
//  CaiKuBaoDian
//
//  Created by mac on 03/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "IntroductionView.h"

@implementation IntroductionView

+(instancetype)newView
{
    return [[IntroductionView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, WIDTH/3.75)];
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self createUI];
    }
    return self;
}

- (void)createUI
{
    self.img = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, WIDTH - 20, self.height - 20)];
    [self addSubview:self.img];
    self.img.image = [UIImage imageNamed:@"boxKuang"];
    
    self.name = [[UILabel alloc] initWithFrame:CGRectMake(20, 20, WIDTH - 40, 20)];
    [self addSubview:self.name];
    self.name.text = @"简介";
    self.name.textAlignment = NSTextAlignmentCenter;
    
    self.label = [[UILabel alloc] initWithFrame:CGRectMake(20, 40, WIDTH - 40, 40)];
    [self addSubview:self.label];
    self.label.numberOfLines = 0;
    self.label.font = fontCell;
}

- (CGFloat)labelStr:(NSString *)str
{
    self.label.text = str;
    CGFloat height = [Tools setLabelHeight:self.label str:str width:WIDTH - 40 font:13];
    self.height = self.name.bottom + height + 25;
    self.img.height = self.height - 20;
    return self.height;
}

@end
