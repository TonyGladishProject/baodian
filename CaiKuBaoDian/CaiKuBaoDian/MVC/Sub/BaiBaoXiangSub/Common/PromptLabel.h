//
//  PromptLabel.h
//  CaiKuBaoDian
//
//  Created by mac on 03/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "NoDataLabel.h"

NS_ASSUME_NONNULL_BEGIN

@interface PromptLabel : NoDataLabel
- (CGFloat)labelStr:(NSString *)str;
@end

NS_ASSUME_NONNULL_END
