//
//  PromptLabel.m
//  CaiKuBaoDian
//
//  Created by mac on 03/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "PromptLabel.h"

@implementation PromptLabel
+(instancetype)newView
{
    return [[PromptLabel alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 40)];
}
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self createUI];
    }
    return self;
}

- (void)createUI
{
    self.backgroundColor = UIColor.clearColor;
    self.textAlignment = NSTextAlignmentCenter;
    self.textColor = MainColor;
    self.font = fontCell;
}

- (CGFloat)labelStr:(NSString *)str
{
    self.numberOfLines = 0;
    self.text = str;
    self.x  = 40;
    self.width = WIDTH - 80;
    [Tools setLabelHeight:self str:str width:WIDTH - 80 font:13];
    return self.height;
}

@end
