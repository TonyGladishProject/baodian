//
//  GDImgVC.m
//  CaiKuBaoDian
//
//  Created by mac on 18/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "GDImgVC.h"

@interface GDImgVC ()

@end

@implementation GDImgVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self postHud:APIGDImg parameters:@{@"year":self.model.na,@"mid":self.idStr,@"new_id":self.model.id,@"qs":self.strQs}];
}

- (void)configure
{
    self.contentViewHeight = HEIGHT - self.navBottomHeight - 44;
    self.navView.hidden = YES;
    self.contentView.frame = CGRectMake(0, 0, WIDTH, self.contentViewHeight);
    self.imageView.frame = self.contentView.bounds;
}

- (void)dataToModel:(id)responseObject
{
    self.imgData = responseObject[@"data"][@"url"];
}


@end
