//
//  GalleryDetailVC.m
//  CaiKuBaoDian
//
//  Created by mac on 18/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "GalleryDetailVC.h"
#import "GDImgVC.h"
#import "LabelCollectionCell.h"

@interface GalleryDetailVC ()
@end

@implementation GalleryDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.magicController.view.backgroundColor = UIColor.blackColor;
    [self refreshUI];
    [self loadData];
}

- (void)loadData
{
//    [MBProgressHUD viewLoading:self.view];
    [HTTPTool postWithURL:APIQS parameters:@{@"year":self.yearStr,@"mid":self.idStr,@"new_id":self.model.id} success:^(id  _Nonnull responseObject) {
        [MBProgressHUD hideHUDForView:self.view];
        
        self.menuList = [NSMutableArray array];
        [self.yearPicker.dataArray removeAllObjects];
        [self.arrNames removeAllObjects];
        NSArray * data = responseObject[@"data"][@"data"];
        NSArray *ln = responseObject[@"data"][@"ln"];
        for (NSDictionary * dic in data) {
            GalleryDetailModel * model = [GalleryDetailModel mj_objectWithKeyValues:dic];
            NSArray * arr = [model.name componentsSeparatedByString:@" "];
            [self.menuList addObject:arr.lastObject];
            [self.arrNames addObject:arr.lastObject];
        }
        
        for (NSNumber * num in ln) {
            NSString * str = [[NSNumberFormatter new] stringFromNumber:num];
            [self.yearPicker.dataArray addObject:str];
        }
        [self.magicController.magicView reloadData];
        [self.magicController switchToPage:self.menuList.count - 1 animated:YES];
        [self.collectionView reloadData];
    }];
}

- (void)refreshUI
{
    self.titleLabel.text = self.model.name;
    self.navImage.hidden = YES;

    [self.rightButton setTitle:self.yearStr forState:UIControlStateNormal];
    [self createMoreButton];
    [self.moreBtn addTarget:self action:@selector(moreBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    self.navTintView.backgroundColor = UIColor.blackColor;
    self.magicController.magicView.navigationColor = self.navTintView.backgroundColor;
    self.magicController.magicView.sliderColor = UIColor.whiteColor;
    self.bottomView.backgroundColor = UIColor.blackColor;
    
    [self createCollectionView];
}

- (UIButton *)magicView:(VTMagicView *)magicView menuItemAtIndex:(NSUInteger)itemIndex {
    static NSString *itemIdentifier = @"itemIdentifier";
    UIButton *menuItem = [magicView dequeueReusableItemWithIdentifier:itemIdentifier];
    if (!menuItem) {
        menuItem = [UIButton buttonWithType:UIButtonTypeCustom];
        [menuItem setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
        menuItem.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:15.f];
    }
    return menuItem;
}

- (UIViewController *)magicView:(VTMagicView *)magicView viewControllerAtPage:(NSUInteger)pageIndex {
     NSString *otherId = [NSString stringWithFormat:@"GDImg%lu",pageIndex];
    GDImgVC *i = [magicView dequeueReusablePageWithIdentifier:otherId];
    if (!i) {
        i = [[GDImgVC alloc] init];
    }
    i.model = self.model;
    i.idStr = self.idStr;
    i.strQs = [NSString stringWithFormat:@"%lu",pageIndex + 1];
    return i;
}

- (void)moreBtnClick:(UIButton *)button
{
    button.selected = !button.selected;
    if (button.selected) {
        [UIView animateWithDuration:.3 animations:^{
            self.collectionView.y = self.magicNavHeight;
        }];
    } else {
        [UIView animateWithDuration:.3 animations:^{
            self.collectionView.y = self.magicNavHeight - self.contentViewHeight;
        }];
    }
    
}

- (void)confirmClick
{
    [self loadData];
}

- (CGFloat)magicNavHeight
{
    return 44;
}

#pragma mark - UICollectionView
- (void)createCollectionView
{
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0,self.magicNavHeight - self.contentViewHeight, WIDTH, self.contentViewHeight - self.magicNavHeight) collectionViewLayout:flowLayout];
    self.collectionView.backgroundColor = UIColor.blackColor;
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    [self.magicController.magicView addSubview:self.collectionView];
    self.collectionView.alwaysBounceVertical = YES;
    
    [self registerClass:[LabelCollectionCell class]];
    [self.view bringSubviewToFront:self.navView];
    [self.magicController.magicView bringSubviewToFront:self.magicController.magicView.navigationView];
}

- (void)registerClass:(Class)cell
{
    [self.collectionView registerClass:cell forCellWithReuseIdentifier:NSStringFromClass(cell)];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.arrNames.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    LabelCollectionCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LabelCollectionCell" forIndexPath:indexPath];
    cell.label.text = self.arrNames[indexPath.item];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return  CGSizeMake((WIDTH - 60)/5, 30);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(10, 10,10, 10);//（上、左、下、右）
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 9.5;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 10;
}

#pragma mark  点击CollectionView触发事件
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self moreBtnClick:self.moreBtn];
    [self.magicController switchToPage:indexPath.row animated:YES];
}

- (NSMutableArray *)arrNames
{
    if (!_arrNames) {
        _arrNames = [NSMutableArray array];
    }
    return  _arrNames;
}


@end
