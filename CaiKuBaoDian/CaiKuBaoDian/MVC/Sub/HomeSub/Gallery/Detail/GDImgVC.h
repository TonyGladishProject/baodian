//
//  GDImgVC.h
//  CaiKuBaoDian
//
//  Created by mac on 18/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "PhotoVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface GDImgVC : PhotoVC
@property ( nonatomic, strong ) GalleryModel * model;
@property ( nonatomic, copy ) NSString * strQs;
@end

NS_ASSUME_NONNULL_END
