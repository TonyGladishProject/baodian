//
//  GalleryDetailVC.h
//  CaiKuBaoDian
//
//  Created by mac on 18/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "MagicVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface GalleryDetailVC : MagicVC<UICollectionViewDataSource,UICollectionViewDelegate>
@property ( nonatomic, strong ) UICollectionView * collectionView;
@property ( nonatomic, strong ) NSMutableArray * arrNames;
@property ( nonatomic, strong ) GalleryModel * model;
@property ( nonatomic, assign ) CGFloat magicNavHeight;
@end

NS_ASSUME_NONNULL_END
