//
//  SwitchView.h
//  CaiKuBaoDian
//
//  Created by mac on 14/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "BaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface SwitchView : BaseView
@property ( nonatomic, strong ) UILabel * labelLeft;
@property ( nonatomic, strong ) UILabel * labelCenter;
@property ( nonatomic, strong ) UILabel * labelRight;
@property ( nonatomic, strong ) UIView * slideView;
+(instancetype)newView;
@end

NS_ASSUME_NONNULL_END
