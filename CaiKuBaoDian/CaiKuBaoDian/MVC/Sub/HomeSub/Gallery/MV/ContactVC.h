//
//  ContactVC.h
//  CaiKuBaoDian
//
//  Created by mac on 02/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "YearPickerVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface ContactVC : YearPickerVC<UITableViewDataSource,UITableViewDelegate>
@property ( nonatomic, strong ) UITableView * tableViewA;
@property ( nonatomic, strong ) UITableView * tableViewB;
@property ( nonatomic, strong ) UITableView * tableViewC;
@property ( nonatomic, strong ) NSMutableArray * dataArrayA;
@property ( nonatomic, strong ) NSMutableArray * dataArrayB;
@property ( nonatomic, strong ) NSMutableArray * dataArrayC;
@property ( nonatomic, copy ) NSString * idStrA;
@property ( nonatomic, copy ) NSString * idStrB;
@property ( nonatomic, copy ) NSString * idStrC;
@property ( nonatomic, strong ) NSMutableArray * resultArrayA;
@property ( nonatomic, strong ) NSMutableArray * resultArrayB;
@property ( nonatomic, strong ) NSMutableArray * resultArrayC;
@property ( nonatomic, copy ) NSString * searchText;
- (void)searchResultA:(NSString *)searchText;
- (void)searchResultB:(NSString *)searchText;
- (void)searchResultC:(NSString *)searchText;
- (void)createTableViewA:(UIScrollView *)scrollView;
- (void)createTableViewB:(UIScrollView *)scrollView;
- (void)createTableViewC:(UIScrollView *)scrollView;
- (void)registerClass:(Class)cell;
- (void)resignView;
@end

NS_ASSUME_NONNULL_END
