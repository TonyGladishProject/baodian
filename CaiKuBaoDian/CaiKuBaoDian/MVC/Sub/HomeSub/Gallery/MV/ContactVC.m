//
//  ContactVC.m
//  CaiKuBaoDian
//
//  Created by mac on 02/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "ContactVC.h"
#import "ContactsCell.h"
#import "GalleryDetailVC.h"

@interface ContactVC ()

@end

@implementation ContactVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.idStrA = @"1";
    self.idStrB = @"2";
    self.idStrC = @"3";
}

- (void)searchResultA:(NSString *)searchText
{
    self.searchText = searchText;
    [self.resultArrayA removeAllObjects];
    if (searchText.length < 1) {
        [self.tableViewA reloadData];
        return;
    }
    for (NSDictionary * dic in self.dataArrayA) {
        NSArray * arr = dic[@"szmgroup"];
        NSMutableArray * re = [NSMutableArray array];
        for (NSDictionary * model in arr) {
            NSString * str = model[@"name"];
            if ([str rangeOfString:searchText].location != NSNotFound) {
                [re addObject:model];
            }
        }
        if (re.count > 0) {
            NSDictionary * dict = @{@"szm":dic[@"szm"],@"szmgroup":re};
            [self.resultArrayA addObject:dict];
        }
    }
    [self.tableViewA reloadData];
}

- (void)searchResultB:(NSString *)searchText
{
    self.searchText = searchText;
    [self.resultArrayB removeAllObjects];
    if (searchText.length < 1) {
        [self.tableViewB reloadData];
        return;
    }
    for (NSDictionary * dic in self.dataArrayB) {
        NSArray * arr = dic[@"szmgroup"];
        NSMutableArray * re = [NSMutableArray array];
        for (NSDictionary * model in arr) {
            NSString * str = model[@"name"];
            if ([str rangeOfString:searchText].location != NSNotFound) {
                [re addObject:model];
            }
        }
        if (re.count > 0) {
            NSDictionary * dict = @{@"szm":dic[@"szm"],@"szmgroup":re};
            [self.resultArrayB addObject:dict];
        }
    }
    [self.tableViewB reloadData];
}

- (void)searchResultC:(NSString *)searchText
{
    self.searchText = searchText;
    [self.resultArrayC removeAllObjects];
    if (searchText.length < 1) {
        [self.tableViewC reloadData];
        return;
    }
    for (NSDictionary * dic in self.dataArrayC) {
        NSArray * arr = dic[@"szmgroup"];
        NSMutableArray * re = [NSMutableArray array];
        for (NSDictionary * model in arr) {
            NSString * str = model[@"name"];
            if ([str rangeOfString:searchText].location != NSNotFound) {
                [re addObject:model];
            }
        }
        if (re.count > 0) {
            NSDictionary * dict = @{@"szm":dic[@"szm"],@"szmgroup":re};
            [self.resultArrayC addObject:dict];
        }
    }
    [self.tableViewC reloadData];
}

- (void)createTableViewA:(UIScrollView *)scrollView
{
    self.tableViewA = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, self.contentViewHeight - 44) style:UITableViewStylePlain];
    self.tableViewA.dataSource = self;
    self.tableViewA.delegate = self;
    [scrollView addSubview:self.tableViewA];
    self.tableViewA.tableFooterView = [UIView new];
    self.tableViewA.separatorStyle = UITableViewCellSelectionStyleNone;
    self.tableViewA.backgroundColor = viewGaryColor;
}

- (void)createTableViewB:(UIScrollView *)scrollView
{
    self.tableViewB = [[UITableView alloc] initWithFrame:CGRectMake(WIDTH, 0, WIDTH, self.contentViewHeight - 44) style:UITableViewStylePlain];
    self.tableViewB.dataSource = self;
    self.tableViewB.delegate = self;
    [scrollView addSubview:self.tableViewB];
    self.tableViewB.tableFooterView = [UIView new];
    self.tableViewB.separatorStyle = UITableViewCellSelectionStyleNone;
    self.tableViewB.backgroundColor = viewGaryColor;
}

- (void)createTableViewC:(UIScrollView *)scrollView
{
    self.tableViewC = [[UITableView alloc] initWithFrame:CGRectMake(WIDTH*2, 0, WIDTH, self.contentViewHeight - 44) style:UITableViewStylePlain];
    self.tableViewC.dataSource = self;
    self.tableViewC.delegate = self;
    [scrollView addSubview:self.tableViewC];
    self.tableViewC.tableFooterView = [UIView new];
    self.tableViewC.separatorStyle = UITableViewCellSelectionStyleNone;
    self.tableViewC.backgroundColor = viewGaryColor;
}

- (void)registerClass:(Class)cell
{
    [self.tableViewA registerClass:cell forCellReuseIdentifier:NSStringFromClass(cell)];
    [self.tableViewB registerClass:cell forCellReuseIdentifier:NSStringFromClass(cell)];
    [self.tableViewC registerClass:cell forCellReuseIdentifier:NSStringFromClass(cell)];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (self.tableViewA == tableView) {
        if (self.searchText.length > 0) {
            return self.resultArrayA.count;
        }
        return self.dataArrayA.count;
    }
    if (self.tableViewB == tableView) {
        if (self.searchText.length > 0) {
            return self.resultArrayB.count;
        }
        return self.dataArrayB.count;
    }
    if (self.searchText.length > 0) {
        return self.resultArrayC.count;
    }
    return self.dataArrayC.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSDictionary * dic;
    if (self.tableViewA == tableView) {
         dic = self.dataArrayA[section];
        if (self.searchText.length > 0) {
            if (self.resultArrayA.count > 0) {
                dic = self.resultArrayA[section];
            } else {
                return 0;
            }
        }
    } else if (self.tableViewB == tableView) {
        dic = self.dataArrayB[section];
        if (self.searchText.length > 0) {
            if (self.resultArrayB.count > 0) {
                dic = self.resultArrayB[section];
            } else {
                return 0;
            }
        }
    } else {
        dic = self.dataArrayC[section];
        if (self.searchText.length > 0) {
            if (self.resultArrayC.count > 0) {
                dic = self.resultArrayC[section];
            } else {
                return 0;
            }
        }
    }
    NSArray * arr = dic[@"szmgroup"];
    return arr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ContactsCell * cell = [tableView dequeueReusableCellWithIdentifier:@"ContactsCell" forIndexPath:indexPath];
    NSDictionary * dic;
    if (self.tableViewA == tableView) {
        dic = self.dataArrayA[indexPath.section];
        if (self.resultArrayA.count > 0) {
            dic = self.resultArrayA[indexPath.section];
        }
    } else if (self.tableViewB == tableView) {
        dic = self.dataArrayB[indexPath.section];
        if (self.resultArrayB.count > 0) {
            dic = self.resultArrayB[indexPath.section];
        }
    } else {
        dic = self.dataArrayC[indexPath.section];
        if (self.resultArrayC.count > 0) {
            dic = self.resultArrayC[indexPath.section];
        }
    }
    NSArray * arr = dic[@"szmgroup"];
    NSDictionary * model = arr[indexPath.row];
    cell.label.text = model[@"name"];
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 44)];
    [view touchEvents:self action:@selector(resignView)];
    view.backgroundColor = cellHeaderColor;
    UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 200, 44)];
    [view addSubview:label];
    NSDictionary * dic;
    if (self.tableViewA == tableView) {
        dic = self.dataArrayA[section];
    } else if (self.tableViewB == tableView) {
        dic = self.dataArrayB[section];
    } else {
        dic = self.dataArrayC[section];
    }
    label.text = dic[@"szm"];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self resignView];
    GalleryDetailVC * g = [GalleryDetailVC new];
    NSDictionary * dic;
    if (self.tableViewA == tableView) {
        g.idStr = self.idStrA;
        dic = self.dataArrayA[indexPath.section];
        if (self.resultArrayA.count > 0) {
            dic = self.resultArrayA[indexPath.section];
        }
    } else if (self.tableViewB == tableView) {
        g.idStr = self.idStrB;
        dic = self.dataArrayB[indexPath.section];
        if (self.resultArrayB.count > 0) {
            dic = self.resultArrayB[indexPath.section];
        }
    } else {
        g.idStr = self.idStrC;
        dic = self.dataArrayC[indexPath.section];
        if (self.resultArrayC.count > 0) {
            dic = self.resultArrayC[indexPath.section];
        }
    }
    NSArray * arr = dic[@"szmgroup"];
    NSDictionary * model = arr[indexPath.row];
    g.model = [GalleryModel mj_objectWithKeyValues:model];
    g.yearStr = self.yearStr;
    [self.navigationController pushViewController:g animated:YES];
}

- (NSMutableArray *)dataArrayA
{
    if (!_dataArrayA) {
        _dataArrayA = [NSMutableArray array];
    }
    return _dataArrayA;
}

- (NSMutableArray *)dataArrayB
{
    if (!_dataArrayB) {
        _dataArrayB = [NSMutableArray array];
    }
    return _dataArrayB;
}

- (NSMutableArray *)dataArrayC
{
    if (!_dataArrayC) {
        _dataArrayC = [NSMutableArray array];
    }
    return _dataArrayC;
}

- (NSMutableArray *)resultArrayA
{
    if (!_resultArrayA) {
        _resultArrayA = [NSMutableArray array];
    }
    return _resultArrayA;
}

- (NSMutableArray *)resultArrayB
{
    if (!_resultArrayB) {
        _resultArrayB = [NSMutableArray array];
    }
    return _resultArrayB;
}

- (NSMutableArray *)resultArrayC
{
    if (!_resultArrayC) {
        _resultArrayC = [NSMutableArray array];
    }
    return _resultArrayC;
}

- (void)resignView{}
@end
