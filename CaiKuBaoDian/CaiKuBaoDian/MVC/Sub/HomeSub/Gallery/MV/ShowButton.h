//
//  ShowButton.h
//  CaiKuBaoDian
//
//  Created by mac on 19/06/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ShowButton : UIButton
@property (nonatomic, copy)  void(^highlightBlock) (BOOL highlighted,NSString * str);
@end

NS_ASSUME_NONNULL_END
