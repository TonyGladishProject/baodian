//
//  SearchBar.m
//  CaiKuBaoDian
//
//  Created by mac on 24/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "SearchBar.h"

@implementation SearchBar

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.placeholder = @"搜索";
        self.searchBarStyle = UISearchBarStyleMinimal;
    }
    return self;
}

@end
