//
//  SwitchView.m
//  CaiKuBaoDian
//
//  Created by mac on 14/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "SwitchView.h"

@implementation SwitchView

+(instancetype)newView
{
    return [[SwitchView alloc] initWithFrame:CGRectMake(0, 0, WIDTH/5.3*3, 40)];
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        [self layerBorderWidth:1 color:UIColor.whiteColor];
        
        self.slideView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.width/3, self.height)];
        [self addSubview:self.slideView];
        self.slideView.backgroundColor = UIColor.whiteColor;
        
        self.labelLeft = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.width/3, self.height)];
        [self addSubview:self.labelLeft];
        
        self.labelCenter = [[UILabel alloc] initWithFrame:CGRectMake(self.width/3, 0, self.width/3, self.height)];
        [self addSubview:self.labelCenter];
        
        self.labelRight = [[UILabel alloc] initWithFrame:CGRectMake(self.width/3 * 2, 0, self.width/3, self.height)];
        [self addSubview:self.labelRight];
        
        
        [self initUI];
    }
    return self;
}

- (void)initUI
{
    self.labelLeft.textAlignment = self.labelCenter.textAlignment = self.labelRight.textAlignment = NSTextAlignmentCenter;
    [self labelSelected:self.labelLeft];
    self.labelCenter.textColor = self.labelRight.textColor = UIColor.whiteColor;
    [self.labelCenter layerBorderWidth:1 color:UIColor.whiteColor];
}

- (void)labelSelected:(UILabel *)label
{
    label.textColor = MainColor;
}

@end
