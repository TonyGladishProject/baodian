//
//  ViewIndex.h
//  CaiKuBaoDian
//
//  Created by mac on 19/06/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "BaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface ViewIndex : BaseView
@property ( nonatomic, strong ) NSMutableArray * btnArray;
@property ( nonatomic, strong ) UITableView * tableView;
@property ( nonatomic, strong ) UILabel * label;
- (void)dataToView:(NSArray *)array;
@end

NS_ASSUME_NONNULL_END
