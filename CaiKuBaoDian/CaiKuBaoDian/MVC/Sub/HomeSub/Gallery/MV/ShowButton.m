//
//  ShowButton.m
//  CaiKuBaoDian
//
//  Created by mac on 19/06/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "ShowButton.h"

@implementation ShowButton

- (void)setHighlighted:(BOOL)highlighted
{
    if (_highlightBlock) {
        _highlightBlock(highlighted,self.titleLabel.text);
    }
}

@end
