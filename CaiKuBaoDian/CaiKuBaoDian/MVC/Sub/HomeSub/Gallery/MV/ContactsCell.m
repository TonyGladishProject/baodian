//
//  ContactsCell.m
//  CaiKuBaoDian
//
//  Created by mac on 14/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "ContactsCell.h"

@implementation ContactsCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.label.height = 44;
        self.arrow.hidden = YES;
        self.line.y = 43;
    }
    return self;
}

@end
