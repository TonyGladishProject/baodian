//
//  ViewIndex.m
//  CaiKuBaoDian
//
//  Created by mac on 19/06/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "ViewIndex.h"
#import "ShowButton.h"

@implementation ViewIndex

+(instancetype)newView
{
    ViewIndex * v = [[ViewIndex alloc] init];
    v.frame = CGRectMake(WIDTH - 30, 0, 30, v.contentViewHeight - 44);
    return v;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self createUI];
    }
    return self;
}

- (void)createUI
{
//    self.backgroundColor = UIColor.grayColor;
    CGFloat y = (self.height - 460)/28;
    for (int i = 0; i < 27; i++) {
        ShowButton * button = [ShowButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(0, y + i * (17 + y), 30, 17);
        button.titleLabel.font = [UIFont systemFontOfSize:15];
//        button.backgroundColor = MainColor;
        [button setTitleColor:UIColor.blackColor forState:UIControlStateNormal];
        button.tag = 1000 + i;
        [button addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
        button.hidden = YES;
        [self addSubview:button];
        [self.btnArray addObject:button];
    }
    self.label = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH/2 - 30, HEIGHT/2 - 30, 60, 60)];
    self.label.backgroundColor = UIColor.blackColor;
    [self.label layerMask];
    self.label.hidden = YES;
    self.label.textColor = UIColor.whiteColor;
    self.label.textAlignment = NSTextAlignmentCenter;
    self.label.font = [UIFont boldSystemFontOfSize:30];
}

- (void)dataToView:(NSArray *)array 
{
    CGFloat h = 460/array.count;
    CGFloat y = (self.height - 460)/(array.count - 1);
    for (int i = 0; i < array.count; i++) {
        ShowButton * button = self.btnArray[i];
        button.hidden = NO;
        button.frame = CGRectMake(0,  i * (h + y), 30, h);
        [button setTitle:array[i] forState:UIControlStateNormal];
        button.highlightBlock = ^(BOOL highlighted, NSString * _Nonnull str) {
            self.label.text = str;
            self.label.hidden = !highlighted;
        };
    }
    UIViewController * v = [Tools getCurrentVC];
    [v.view addSubview:self.label];
}

- (void)buttonClick:(ShowButton *)btn
{
    CGRect r = [self.tableView rectForSection:btn.tag - 1000];
    self.tableView.contentOffset = CGPointMake(0, r.origin.y);
}

- (NSMutableArray *)btnArray
{
    if (!_btnArray) {
        _btnArray = [NSMutableArray array];
    }
    return _btnArray;
}
@end
