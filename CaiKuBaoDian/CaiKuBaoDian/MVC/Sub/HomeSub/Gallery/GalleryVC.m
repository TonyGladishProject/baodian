//
//  GalleryVC.m
//  CaiKuBaoDian
//
//  Created by mac on 14/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "GalleryVC.h"
#import "ContactsCell.h"
#import "SwitchView.h"
#import "SearchBar.h"
#import "ViewIndex.h"

@interface GalleryVC ()<UIScrollViewDelegate,UISearchBarDelegate>
@property ( nonatomic, strong ) SwitchView * switchView;
@property ( nonatomic, strong ) SearchBar * searchView;
@property ( nonatomic, strong ) UIScrollView * scrollView;
@end

@implementation GalleryVC

- (void)viewDidLoad {
    [super viewDidLoad];
 
    self.bottomView.backgroundColor = viewGaryColor;
    [self createUI];
    [self loadData];
    [self loadYear];
}

- (void)loadYear
{
    [HTTPTool getWithURL:APIGalleryYear success:^(id  _Nonnull responseObject) {
        [self.yearPicker.dataArray removeAllObjects];
        for (NSNumber * num in responseObject[@"data"]) {
            NSString * str = [[NSNumberFormatter new] stringFromNumber:num];
            [self.yearPicker.dataArray addObject:str];
        }
        [self.yearPicker.pickerView reloadAllComponents];
    }];
}

- (void)loadData
{
    [MBProgressHUD dataLoading];
    [HTTPTool postWithURL:APIGallery parameters:@{@"year":self.yearStr,@"mid":@"1"} success:^(id  _Nonnull responseObject) {
        [MBProgressHUD hideHUD];
        [self loadDataB];
        NSArray * data = responseObject[@"data"];
        self.dataArrayA = [NSMutableArray arrayWithArray:data];
        [self.dataArrayA addObject:self.dataArrayA[0]];
        [self.dataArrayA removeObjectAtIndex:0];
        [self.tableViewA reloadData];
        self.bottomView.backgroundColor = UIColor.whiteColor;
        
        [self createIndexView:self.tableViewA dataArray:self.dataArrayA x:WIDTH - 30];
    }];
}

- (void)loadDataB
{
    [MBProgressHUD viewLoading:self.tableViewB];
    [HTTPTool postWithURL:APIGallery parameters:@{@"year":self.yearStr,@"mid":@"2"} success:^(id  _Nonnull responseObject) {
        [MBProgressHUD hideHUDForView:self.tableViewB];
        [self loadDataC];
        NSArray * data = responseObject[@"data"];
        self.dataArrayB = [NSMutableArray arrayWithArray:data];
        [self.dataArrayB addObject:self.dataArrayB[0]];
        [self.dataArrayB removeObjectAtIndex:0];
        [self.tableViewB reloadData];
        
        [self createIndexView:self.tableViewB dataArray:self.dataArrayB x:WIDTH*2 - 30];
    }];
}

- (void)loadDataC
{
    [MBProgressHUD viewLoading:self.tableViewC];
    [HTTPTool postWithURL:APIGallery parameters:@{@"year":self.yearStr,@"mid":@"3"} success:^(id  _Nonnull responseObject) {
        [MBProgressHUD hideHUDForView:self.tableViewC];
        NSArray * data = responseObject[@"data"];
        self.dataArrayC = [NSMutableArray arrayWithArray:data];
        [self.dataArrayC addObject:self.dataArrayC[0]];
        [self.dataArrayC removeObjectAtIndex:0];
        [self.tableViewC reloadData];

        [self createIndexView:self.tableViewC dataArray:self.dataArrayC x:WIDTH*3 - 30];
    }];
}

- (void)createIndexView:(UITableView *)tableView dataArray:(NSArray *)array x:(CGFloat)x
{
    ViewIndex * v = [ViewIndex newView];
    v.x = x;
    [self.scrollView addSubview:v];
    NSMutableArray * arr = [NSMutableArray arrayWithCapacity:26];
    for (NSDictionary * dic in array) {
        [arr addObject:dic[@"szm"]];
    }
    [v dataToView:arr];
    v.tableView = tableView;
}

- (void)createUI
{
    self.switchView = [SwitchView newView];
    self.switchView.center = CGPointMake(WIDTH/2, 22);
    [self.navMainView addSubview:self.switchView];
    self.switchView.labelLeft.text = @"彩色";
    self.switchView.labelCenter.text = @"黑白";
    self.switchView.labelRight.text = @"经书";
    [self.switchView.labelLeft touchEvents:self action:@selector(leftClick)];
    [self.switchView.labelCenter touchEvents:self action:@selector(centerClick)];
    [self.switchView.labelRight touchEvents:self action:@selector(rightClick)];
    
    //search
    self.searchView = [[SearchBar alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 44)];
    [self.contentView addSubview:self.searchView];
    self.searchView.delegate = self;
    
    //scrollView
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, self.searchView.bottom, WIDTH, self.contentViewHeight - 44)];
    [self.contentView addSubview:self.scrollView];
    self.scrollView.contentSize = CGSizeMake(WIDTH * 3, self.contentViewHeight - 44);
    self.scrollView.pagingEnabled = YES;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.delegate = self;
    
    //tableViewC
    [self createTableViewA:self.scrollView];
    [self createTableViewB:self.scrollView];
    [self createTableViewC:self.scrollView];
    [self registerClass:[ContactsCell class]];
}

#pragma mark - UISearchBarDelegate
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    CGFloat x = self.scrollView.contentOffset.x;
    if (x < WIDTH) {
        [self searchResultA:searchText];
    } else if (x > WIDTH /2 *3) {
        [self searchResultC:searchText];
    } else {
        [self searchResultB:searchText];
    }
    NSLog(@"-----x--%f",x);
}

- (void)leftClick
{
    self.switchView.labelLeft.textColor = MainColor;
    self.switchView.labelCenter.textColor = self.switchView.labelRight.textColor = UIColor.whiteColor;
    [UIView animateWithDuration:0.3 animations:^{
        self.switchView.slideView.x = 0;
        self.scrollView.contentOffset = CGPointMake(0, 0);
    }];

}

- (void)centerClick
{
    self.switchView.labelLeft.textColor = self.switchView.labelRight.textColor = UIColor.whiteColor;
    self.switchView.labelCenter.textColor = MainColor;
    [UIView animateWithDuration:0.3 animations:^{
        self.switchView.slideView.x = self.switchView.labelLeft.width;
        self.scrollView.contentOffset = CGPointMake(WIDTH, 0);
    }];
}

- (void)rightClick
{
    self.switchView.labelLeft.textColor = self.switchView.labelCenter.textColor = UIColor.whiteColor;
    self.switchView.labelRight.textColor = MainColor;
    [UIView animateWithDuration:0.3 animations:^{
        self.switchView.slideView.x = self.switchView.labelRight.x;
        self.scrollView.contentOffset = CGPointMake(WIDTH * 2, 0);
    }];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.searchView resignFirstResponder];
    CGFloat x = scrollView.contentOffset.x;
    if (x != 0) {
        CGFloat i = x / WIDTH;
        self.switchView.slideView.x = x/5.3;
        if (i < 0.3) {
            self.switchView.labelLeft.textColor = MainColor;
            self.switchView.labelCenter.textColor = self.switchView.labelRight.textColor = UIColor.whiteColor;
        } else if (i >= 1.6666) {
            self.switchView.labelRight.textColor = MainColor;
            self.switchView.labelLeft.textColor =self.switchView.labelCenter.textColor = UIColor.whiteColor;
        } else {
            self.switchView.labelCenter.textColor = MainColor;
            self.switchView.labelLeft.textColor = self.switchView.labelRight.textColor = UIColor.whiteColor;
        }
    }
}

- (void)rightButtonClick
{
    [self.searchView resignFirstResponder];
    [self.yearPicker show];
}

- (void)confirmClick
{
   [self loadData];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.searchView resignFirstResponder];
}

- (void)resignView
{
    [self.searchView resignFirstResponder];
}
@end
