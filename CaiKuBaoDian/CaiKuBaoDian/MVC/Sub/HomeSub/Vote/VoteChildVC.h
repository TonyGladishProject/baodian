//
//  VoteChildVC.h
//  CaiKuBaoDian
//
//  Created by mac on 13/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "TablePureVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface VoteChildVC : TablePureVC
@property ( nonatomic, copy ) NSString * shijian;
@property ( nonatomic, strong ) NSMutableArray * sortArray;
@end

NS_ASSUME_NONNULL_END
