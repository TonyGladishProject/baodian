//
//  VoteVC.m
//  CaiKuBaoDian
//
//  Created by mac on 11/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "VoteVC.h"
#import "VoteChildVC.h"

@interface VoteVC ()
@property ( nonatomic, strong ) NSMutableArray * dataArray;
@property ( nonatomic, copy ) NSString * shijian;
@end

@implementation VoteVC

- (void)viewDidLoad {
    [super viewDidLoad];
   
    [self loadData];
    [self refreshUI];
}

- (void)loadData
{
    [MBProgressHUD viewLoading:self.view];
    [HTTPTool postWithURL:APIDetailShengXiao parameters:@{} success:^(id  _Nonnull responseObject) {
        [MBProgressHUD hideHUDForView:self.view];
        
        self.dataArray = [NSMutableArray array];
        NSDictionary * dic = responseObject[@"data"][@"data"];
        self.shijian = responseObject[@"data"][@"shijian"];
        for (NSDictionary * d in dic.allValues) {
            NSArray * arr = [Tools modelLottery:d];
            [self.dataArray addObject:arr];
        }
        [self.dataArray exchangeObjectAtIndex:0 withObjectAtIndex:1];
        [self.magicController.magicView reloadData];
    }];
}

- (void)refreshUI
{
    self.titleLabel.text = @"彩民投票";
    
    self.magicController.magicView.separatorHidden = NO;
    self.magicController.magicView.layoutStyle = VTLayoutStyleDivide;
    self.menuList = [NSMutableArray arrayWithObjects:
                     @"波色投票",@"大小投票",@"单双投票",@"生肖投票", nil];
//    [self.magicController.magicView reloadData];
    
    self.rightButton.hidden = YES;
}

- (UIViewController *)magicView:(VTMagicView *)magicView viewControllerAtPage:(NSUInteger)pageIndex {
    if (self.dataArray.count > 1) {
        NSArray * arr = self.dataArray[pageIndex];
        NSString *otherId = [NSString stringWithFormat:@"Vote%lu",pageIndex];
        VoteChildVC *i = [magicView dequeueReusablePageWithIdentifier:otherId];
        if (!i) {
            i = [[VoteChildVC alloc] init];
        }
        i.idStr = [NSString stringWithFormat:@"%lu",pageIndex + 1];
        i.dataArray = [NSMutableArray arrayWithArray:arr];
        i.shijian = self.shijian;
        return i;
    }
    return nil;
}

@end
