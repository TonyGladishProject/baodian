//
//  VoteChildVC.m
//  CaiKuBaoDian
//
//  Created by mac on 13/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "VoteChildVC.h"
#import "VoteLabelCell.h"
#import "VoteMainCell.h"
#import "VoteButtonCell.h"

@interface VoteChildVC ()
@property ( nonatomic, strong ) VoteMainCell * cellOld;
@end
@implementation VoteChildVC
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self refreshUI];
}

- (void)refreshUI
{
    self.contentViewHeight = HEIGHT - self.navBottomHeight - 44;
    self.navView.hidden = YES;
    self.contentView.frame = CGRectMake(0, 0, WIDTH, self.contentViewHeight);
    
    self.tableView.backgroundColor = UIColor.whiteColor;
    [self registerNib:[VoteLabelCell class]];
    [self registerNib:[VoteMainCell class]];
    [self registerNib:[VoteButtonCell class]];
    self.sortArray = [self sortArray];
}

- (NSArray *)sortArray
{
    NSMutableArray * arr = [NSMutableArray arrayWithArray:self.dataArray];
    [arr sortUsingComparator:^NSComparisonResult(SXModel * obj1, SXModel * obj2)
     {
         //此处的规则含义为：若前一元素比后一元素小，则返回降序（即后一元素在前，为从大到小排列）
         if ([obj1.persent integerValue] < [obj2.persent integerValue])
         {
             return NSOrderedDescending;
         }
         else
         {
             return NSOrderedAscending;
         }
     }];
    return arr;
}

#pragma mark - tableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 1) {
        return self.dataArray.count;
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        VoteLabelCell * cell = [tableView dequeueReusableCellWithIdentifier:@"VoteLabelCell" forIndexPath:indexPath];
        cell.label.text = self.shijian;
        SXModel * model = self.sortArray[0];
        cell.labelHot.text = [NSString stringWithFormat:@"本期热门:%@",model.name];
        return cell;
    } else if (indexPath.section == 2) {
        VoteButtonCell * cell = [tableView dequeueReusableCellWithIdentifier:@"VoteButtonCell" forIndexPath:indexPath];
        [cell.button addTarget:self action:@selector(buttonClick) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }
    VoteMainCell * cell = [tableView dequeueReusableCellWithIdentifier:@"VoteMainCell" forIndexPath:indexPath];
    cell.model = self.dataArray[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.cellOld.img.backgroundColor = UIColor.clearColor;
    
    VoteMainCell * cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.img.backgroundColor = UIColorFromRGB(0xF1773A);
    self.cellOld = cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section ==0) {
        return 80;
    } else if (indexPath.section == 2) {
        return 120;
    }
    return 60;
}

- (void)buttonClick
{
    if (!self.cellOld) {
        [MBProgressHUD showMessage:@"请选择"];
        return;
    }
    
    [self doVote];
}

-(void)doVote
{
    [HTTPTool postWithURLIndicatorMSG:APIVote parameters:@{@"username":UserPhone,@"poll":self.cellOld.labelTitle.text,@"type":self.idStr} success:^(id  _Nonnull responseObject) {
        
    }];
}
@end
