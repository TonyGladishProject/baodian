//
//  VoteMainCell.h
//  CaiKuBaoDian
//
//  Created by mac on 13/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "PureCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface VoteMainCell : PureCell
@property (weak, nonatomic) IBOutlet UIImageView *img;
@property (weak, nonatomic) IBOutlet UIView *gary;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *percent;
@property (weak, nonatomic) IBOutlet UIView *progressView;
@property ( nonatomic, strong ) SXModel * model;
@property ( nonatomic, assign ) CGFloat width;
@end

NS_ASSUME_NONNULL_END
