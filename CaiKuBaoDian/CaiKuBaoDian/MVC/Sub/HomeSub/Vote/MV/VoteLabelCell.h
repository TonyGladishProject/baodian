//
//  VoteLabelCell.h
//  CaiKuBaoDian
//
//  Created by mac on 13/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "PureCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface VoteLabelCell : PureCell
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UILabel *labelHot;

@end

NS_ASSUME_NONNULL_END
