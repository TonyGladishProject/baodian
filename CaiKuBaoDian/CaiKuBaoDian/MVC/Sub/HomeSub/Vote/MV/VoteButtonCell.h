//
//  VoteButtonCell.h
//  CaiKuBaoDian
//
//  Created by mac on 13/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "PureCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface VoteButtonCell : PureCell
@property (weak, nonatomic) IBOutlet UIButton *button;

@end

NS_ASSUME_NONNULL_END
