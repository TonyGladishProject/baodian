//
//  VoteMainCell.m
//  CaiKuBaoDian
//
//  Created by mac on 13/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "VoteMainCell.h"

@implementation VoteMainCell

- (void)setModel:(SXModel *)model
{
    _model = model;
    
    self.labelTitle.text = model.name;
    self.percent.text = [NSString stringWithFormat:@"%@%%",model.persent];
    if ([model.name isEqualToString:@"蓝"]) {
        self.labelTitle.backgroundColor = self.progressView.backgroundColor = UIColorFromRGB(0x2278B3);
    } else if ([model.name isEqualToString:@"绿"] || [model.name isEqualToString:@"小"] || [model.name isEqualToString:@"双"]) {
        self.labelTitle.backgroundColor = self.progressView.backgroundColor = UIColorFromRGB(0x3AB290);
    }
    self.progressView.width = self.width/100 * model.persent.intValue;
}

- (void)awakeFromNib {
    [super awakeFromNib];
   
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [self.img layerBorderWidth:1 color:UIColor.blackColor];
    [self.img layerMask];
    
    [self.labelTitle layerMask];
    [self.gary layerMask];
    
    self.width = WIDTH - 165;
}

@end
