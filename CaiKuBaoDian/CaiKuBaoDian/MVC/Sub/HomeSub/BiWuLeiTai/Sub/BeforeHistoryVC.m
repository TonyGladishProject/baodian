//
//  BeforeHistoryVC.m
//  CaiKuBaoDian
//
//  Created by mac on 10/06/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "BeforeHistoryVC.h"
#import "BWLTDetailCell.h"
#import "DetailLTVC.h"

@interface BeforeHistoryVC ()

@end

@implementation BeforeHistoryVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.titleLabel.text = @"往期历史";
    [self registerNib:[BWLTDetailCell class]];
    [self dataHud];
    [self dataMJ];
//    [self dataMJFooter];
}

- (void)dataHud
{
    [MBProgressHUD viewLoading:self.view];
    self.page = 1;
    [self mjHeader];
}

- (void)showNoDataView:(id)responseObject
{
    
}

- (void)dataMJFooter
{
    MJRefreshAutoNormalFooter * footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        self.page ++;
        [self mjHeader];
    }];
    self.tableView.mj_footer = footer;
    footer.automaticallyRefresh = NO;
}

- (void)mjHeader
{
    [self postData:APIBeforeHistory parameters:@{@"mobile":self.idStr,@"type_id":self.type_id,@"page":[NSString stringWithFormat:@"%d",self.page]}];
}

- (void)dataToModel:(id)responseObject
{
    for (NSDictionary * dic in responseObject[@"data"]) {
        BeforeHistoryModel * model = [BeforeHistoryModel mj_objectWithKeyValues:dic];
        [self.dataArray addObject:model];
    }
    [self.tableView reloadData];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BWLTDetailCell * cell = [tableView dequeueReusableCellWithIdentifier:@"BWLTDetailCell"];
    cell.model = self.dataArray[indexPath.row];
    return  cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    DetailLTVC * d = [DetailLTVC new];
    BeforeHistoryModel * m = self.dataArray[indexPath.row];
    d.name = m.full_name;
    d.idStr = m.id;
    d.ended = YES;
    d.scene = @"lhlt_pl";
    [self pushVC:d];
    
}
@end
