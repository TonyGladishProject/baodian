//
//  BWLTDetailCell.m
//  CaiKuBaoDian
//
//  Created by mac on 22/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "BWLTDetailCell.h"

@implementation BWLTDetailCell

- (void)setModel:(BeforeHistoryModel *)model
{
    _model = model;
    
    [self.header sd_setImageWithURL:[NSURL URLWithString:model.avatar] placeholderImage:[UIImage imageNamed:@"placeHolder0"]];
    self.name.text = model.full_name;
    self.time.text = model.createtime;
    self.comment.text = [NSString stringWithFormat:@"%@个评论",model.countreply];
    self.like.text = model.dz_number;
    self.labelTitle.text = model.title;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
//    self.selectionStyle = UITableViewCellSelectionStyleNone;
    [self.viewLike touchEvents:self action:@selector(doLike)];
}

- (void)doLike
{
    [HTTPTool postWithURLLogin:APILTDLike parameters:@{@"full_name":UserPhone,@"scene":@"lhlt",@"scene_id":self.model.id} success:^(id  _Nonnull responseObject) {
        NSNumber * num = responseObject[@"data"][@"type"];
        NSString * str = [[NSNumberFormatter new] stringFromNumber:num];
        int  i = self.model.dz_number.intValue;
        if (str.intValue == 1) {
            i++;
        } else {
            i--;
        }
        self.model.dz_number = [NSString stringWithFormat:@"%d",i];
        self.like.text = self.model.dz_number;
    }];
}

@end
