//
//  PostVC.m
//  CaiKuBaoDian
//
//  Created by mac on 21/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "PostVC.h"
#import "PostView.h"

@interface PostVC ()
@property ( nonatomic, strong ) PostView * postView;
@end

@implementation PostVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.titleLabel.text = @"发布新帖";
    [self.rightButton setTitle:@"发布" forState:UIControlStateNormal];
    self.postView = [PostView newView];
    [self.scrollView addSubview:self.postView];
    self.scrollView.contentSize = CGSizeMake(WIDTH, self.postView.height + 10);
}

- (void)rightButtonClick
{
    if (self.postView.title.text.length < 1  || [Tools colorSame:self.postView.textView.textColor color:cellWordColor]) {
        [MBProgressHUD showMessage:@"请填写内容"];
        return;
    } else if (self.postView.textView.text.length < 1) {
        [MBProgressHUD showMessage:@"请填写内容"];
        return;
    }
    
    [self doPost];
}

- (void)doPost
{
    [HTTPTool postWithURLIndicatorMSG:APIPostWords parameters:@{@"full_name":UserPhone,@"type":self.type,@"type_id":self.idStr,@"title":self.postView.title.text,@"msg":self.postView.textView.text} success:^(id  _Nonnull responseObject) {
        if (self.successBlock) {
            self.successBlock();
        }
        [self leftButtonClick];
    }];
}

@end
