//
//  PostView.h
//  CaiKuBaoDian
//
//  Created by mac on 21/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "BaseView.h"
#import "TextView.h"

NS_ASSUME_NONNULL_BEGIN

@interface PostView : BaseView
@property (weak, nonatomic) IBOutlet UITextField *title;
@property (weak, nonatomic) IBOutlet UIView *groundView;
@property ( nonatomic, strong ) TextView * textView;
@end

NS_ASSUME_NONNULL_END
