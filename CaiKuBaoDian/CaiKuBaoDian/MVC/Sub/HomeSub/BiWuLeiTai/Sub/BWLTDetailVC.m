//
//  BWLTDetailVC.m
//  CaiKuBaoDian
//
//  Created by mac on 21/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "BWLTDetailVC.h"
#import "PostVC.h"
#import "DetailLTVC.h"

#import "SearchBar.h"
#import "BWLTSpecialCell.h"
#import "BWLTMainCell.h"

@interface BWLTDetailVC ()<UISearchBarDelegate>
@property ( nonatomic, strong ) SearchBar * searchView;
@property ( nonatomic, strong ) NSMutableArray * resultArray;
@property ( nonatomic, copy ) NSString * searchText;
@property ( nonatomic, assign ) NSInteger arrCount;
@property ( nonatomic, assign ) int topCount;
@end

@implementation BWLTDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.titleLabel.text = self.model.name;
    [self.rightButton setTitle:@"发帖" forState:UIControlStateNormal];
    [self registerNib:[BWLTSpecialCell class]];
    [self registerNib:[BWLTMainCell class]];
    [self refreshUI];
    [self refresh];
}

- (void)dataToModel:(id)responseObject
{
    NSLog(@"responseObject=%@",responseObject);
    
    self.arrCount = self.dataArray.count;
    if (self.page == 1) {
        self.topCount = 0;
    }
    NSArray * arr = responseObject[@"data"];
    for (int i = 0; i < arr.count; i++) {
        NSDictionary * dic = arr[i];
        LTDetailModel * model = [LTDetailModel mj_objectWithKeyValues:dic];
        if (model.is_zhiding) {
            model.num = @"";
            self.topCount ++;
        } else {
            model.num = [NSString stringWithFormat:@"%d",i + 1-self.topCount + self.arrCount];
        }
        
        [self.dataArray addObject:model];
    }
    [self.tableView reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.searchText.length > 0) {
        return self.resultArray.count;
    }
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LTDetailModel * model = self.dataArray[indexPath.row];
    if (self.searchText.length > 0) {
        model = self.resultArray[indexPath.row];
    }
    if (model.num.length < 1) {
        BWLTSpecialCell * cell = [tableView dequeueReusableCellWithIdentifier:@"BWLTSpecialCell"];
        cell.model = model;
        return  cell;
    }
    BWLTMainCell * cell = [tableView dequeueReusableCellWithIdentifier:@"BWLTMainCell"];
    cell.model = model;
    return  cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LTDetailModel * model = self.dataArray[indexPath.row];
    if (self.searchText.length > 0) {
        model = self.resultArray[indexPath.row];
    }
    if (model.num.length < 1) {
        return 75;
    }
    return 100;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    DetailLTVC * d = [DetailLTVC new];
    LTDetailModel * m = self.dataArray[indexPath.row];
    d.name = m.user_nicename;
    d.idStr = m.id;
    d.scene = @"lhlt_pl";
    [self pushVC:d];
    
}

- (void)rightButtonClick
{
    PostVC * p = [PostVC new];
    p.idStr = self.model.term_id;
    p.type = @"bwlt";
    kSelfWeak
    p.successBlock = ^{
        [weakSelf refresh];
    };
    [self pushVC:p];
}

- (void)refresh
{
    self.key = @"page";
    [self postHud:APILeiTaiDetail parameters:@{@"type_id":self.model.term_id,@"page":[NSString stringWithFormat:@"%d",self.page]}];
}

- (void)refreshUI
{
    [self createSearchView];
    self.tableView.frame = CGRectMake(0, 44, WIDTH, self.contentViewHeight - 44);
}

- (void)createSearchView
{
    //search
    self.searchView = [[SearchBar alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 44)];
    [self.contentView addSubview:self.searchView];
    self.searchView.delegate = self;
}

#pragma mark - UISearchBarDelegate
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    self.searchText = searchText;
    [self.resultArray removeAllObjects];
    if (searchText.length < 1) {
        [self.tableView reloadData];
        return;
    }
    
    for (LTDetailModel * model in self.dataArray) {
        if ([model.user_nicename rangeOfString:searchText].location != NSNotFound) {
            [self.resultArray addObject:model];
        }
    }
    [self.tableView reloadData];
}

- (NSMutableArray *)resultArray
{
    if (!_resultArray) {
        _resultArray = [NSMutableArray array];
    }
    return _resultArray;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.searchView resignFirstResponder];
}
@end
