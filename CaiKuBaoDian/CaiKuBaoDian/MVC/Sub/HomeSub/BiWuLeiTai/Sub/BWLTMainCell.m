//
//  BWLTMainCell.m
//  CaiKuBaoDian
//
//  Created by mac on 03/06/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "BWLTMainCell.h"

@implementation BWLTMainCell

- (void)setModel:(LTDetailModel *)model
{
    _model = model;
    
    [self.avatar sd_setImageWithURL:[NSURL URLWithString:model.avatar] placeholderImage:[UIImage imageNamed:@"placeHolder0"]];
    self.name.text = model.user_nicename;
    self.titleMain.text = model.title;
    
    self.zhong.text = [NSString stringWithFormat:@"%@中%@",model.article_count,model.guestbook_hit];
    self.percent.text = model.guestbook_lead;
    self.fans.text = model.myFriend;
    self.lianZhong.text = [NSString stringWithFormat:@"最大连中%@期",model.guestbook_liansheng];
    self.num.text = model.num;
    self.badge.backgroundColor = model.color;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
