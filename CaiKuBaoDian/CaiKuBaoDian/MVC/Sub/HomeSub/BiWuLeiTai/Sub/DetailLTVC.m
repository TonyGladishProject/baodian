//
//  DetailLTVC.m
//  CaiKuBaoDian
//
//  Created by mac on 17/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "DetailLTVC.h"
#import "BeforeHistoryVC.h"

@interface DetailLTVC ()

@end

@implementation DetailLTVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (!_ended) {
        [self createUIButton];
    }
}

- (void)createUIButton
{
    UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(WIDTH - 70, 0, 60, 44);
    [button setTitle:@"往期历史" forState:UIControlStateNormal];
    [self.navMainView addSubview:button];
    [button addTarget:self action:@selector(historyButtonClick) forControlEvents:UIControlEventTouchUpInside];
    button.titleLabel.font = [UIFont systemFontOfSize:13];
}

- (void)historyButtonClick
{
    BeforeHistoryVC * b = [BeforeHistoryVC new];
    b.idStr = self.idHistory;
    b.type_id = self.type_id;
    [self pushVC:b];
}

@end
