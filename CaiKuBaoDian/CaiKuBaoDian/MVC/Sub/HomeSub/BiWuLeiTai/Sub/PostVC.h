//
//  PostVC.h
//  CaiKuBaoDian
//
//  Created by mac on 21/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "ScrollViewVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface PostVC : ScrollViewVC
@property (nonatomic, copy)  void(^successBlock) (void);
@property ( nonatomic, copy ) NSString * type;
@end

NS_ASSUME_NONNULL_END
