//
//  BWLTDetailCell.h
//  CaiKuBaoDian
//
//  Created by mac on 22/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "BaseCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface BWLTDetailCell : BaseCell
@property (weak, nonatomic) IBOutlet UIImageView *header;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *time;
@property (weak, nonatomic) IBOutlet UILabel *comment;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *like;
@property (weak, nonatomic) IBOutlet UIView *viewLike;
@property ( nonatomic, strong ) BeforeHistoryModel * model;
@end

NS_ASSUME_NONNULL_END
