//
//  BWLTSpecialCell.h
//  CaiKuBaoDian
//
//  Created by mac on 03/06/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "BaseCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface BWLTSpecialCell : BaseCell
@property (weak, nonatomic) IBOutlet UIImageView *avatar;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *titleMain;

@property ( nonatomic, strong ) LTDetailModel * model;
@end

NS_ASSUME_NONNULL_END
