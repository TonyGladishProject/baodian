//
//  PostView.m
//  CaiKuBaoDian
//
//  Created by mac on 21/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "PostView.h"

@implementation PostView

+(instancetype)newView
{
    PostView * view = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil].lastObject;
    view.frame = CGRectMake(0, 0, WIDTH,view.contentViewHeight);
    return view;
}

- (void)drawRect:(CGRect)rect {
    
    self.textView = [[TextView alloc] initWithFrame:self.groundView.bounds];
    [self.groundView addSubview:self.textView];
    self.textView.placeHolder = @"请输入内容(最多1000字)";
}

@end
