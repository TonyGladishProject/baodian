//
//  BeforeHistoryVC.h
//  CaiKuBaoDian
//
//  Created by mac on 10/06/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "TableViewVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface BeforeHistoryVC : TableViewVC
@property ( nonatomic, copy ) NSString * type_id;
@end

NS_ASSUME_NONNULL_END
