//
//  BiWuLeiTaiVC.m
//  CaiKuBaoDian
//
//  Created by mac on 21/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "BiWuLeiTaiVC.h"
#import "BWLTDetailVC.h"

@interface BiWuLeiTaiVC ()

@end

@implementation BiWuLeiTaiVC


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.titleLabel.text = @"比武擂台";
    [self postHud:APIForum parameters:@{}];
}

- (void)dataToModel:(id)responseObject
{
    //    NSLog(@"responseObject=%@",responseObject);
    for (NSDictionary * dic in responseObject[@"data"]) {
        ForumModel * model = [ForumModel mj_objectWithKeyValues:dic];
        [self.dataArray addObject:model];
    }
    [self.collectionView reloadData];
}

- (void)collectionView:(CollectionViewCell *)cell dataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    cell.forumM = self.dataArray[indexPath.row];
    cell.image.frame = cell.bounds;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return CGSizeMake(WIDTH - 20, (WIDTH -30)/2/ 172.5* 65);
    }
    return  CGSizeMake((WIDTH -30)/2,(WIDTH -30)/2/ 172.5* 65);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(10, 10,10, 10);//（上、左、下、右）
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 10;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 10;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        WebVC * w = [WebVC new];
        w.titleName = @"比武擂台";
        w.urlStr = APILeiTaiRule;
        [self pushVC:w];
    } else {
        BWLTDetailVC * b = [BWLTDetailVC new];
        b.model = self.dataArray[indexPath.row];
        [self pushVC:b];
    }
}
@end
