//
//  ZiliaoCCell.m
//  CaiKuBaoDian
//
//  Created by mac on 26/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "ZiliaoCCell.h"

@implementation ZiliaoCCell
- (void)setModel:(ZiLiaoModel *)model
{
    _model = model;
    self.backgroundColor = [Tools colorWithHexString:model.back_color];
    [self layerBorderWidth:1 color:[Tools colorWithHexString:model.box_color]];
    self.label.textColor = [Tools colorWithHexString:model.font_color];
    self.imgHot.hidden = !model.show;
    [self.imgHot startAnimating];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.label.frame = CGRectMake(0, 0, self.width, self.height);
        self.label.textAlignment = NSTextAlignmentCenter;
        self.backgroundColor = UIColor.whiteColor;
        self.label.textColor = UIColor.blackColor;
        [self layerBorderWidth:1 color:UIColorFromRGB(0xF59F2C)];
        [self layerMask:5];
        [self createHot];
    }
    return self;
}

- (void)createHot
{
    self.imgHot = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH - 50 - 4, 0, 30, 30)];
    [self addSubview:self.imgHot];
    self.imgHot.animationImages = @[[UIImage imageNamed:@"re1"],[UIImage imageNamed:@"re2"],[UIImage imageNamed:@"re3"],[UIImage imageNamed:@"re4"],[UIImage imageNamed:@"re5"],[UIImage imageNamed:@"re6"]];
    self.imgHot.animationRepeatCount = 0;
    self.imgHot.animationDuration = 1;
    [self.imgHot startAnimating];
}
@end
