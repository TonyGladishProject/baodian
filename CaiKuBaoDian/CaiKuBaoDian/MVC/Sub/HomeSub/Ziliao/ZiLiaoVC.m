//
//  ZiLiaoVC.m
//  CaiKuBaoDian
//
//  Created by mac on 26/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "ZiLiaoVC.h"
#import "ZiliaoCCell.h"
#import "ZiLiaoDetailVC.h"
#import "SearchBar.h"

@interface ZiLiaoVC ()<UISearchBarDelegate>
@property ( nonatomic, strong ) SearchBar * searchView;
@property ( nonatomic, strong ) NSMutableArray * resultArray;
@property ( nonatomic, copy ) NSString * searchText;
@end

@implementation ZiLiaoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.titleLabel.text = @"资料大全";
    [self createSearchView];
    self.collectionView.frame = CGRectMake(0, 44, WIDTH, self.contentViewHeight - 44);
    [self registerClass:[ZiliaoCCell class]];
    [self.collectionView registerClass:[ZiliaoCCell class] forCellWithReuseIdentifier:@"SubCCell"];
    [self loadHud];
    [self loadMJ];
}

- (void)loadMJ
{
    MJRefreshNormalHeader * header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self loadData];
    }];
    self.collectionView.mj_header = header;
}

- (void)loadHud
{
    [MBProgressHUD viewLoading:self.view];
    [self loadData];
}

- (void)loadData
{
    
    [HTTPTool postWithURL:APIZiliao parameters:@{@"parent":@"1",@"why_add":@"2"} success:^(id  _Nonnull responseObject) {
        [self.dataArray removeAllObjects];
        NSMutableArray * arr = [NSMutableArray array];
        if ([responseObject[@"data"] isKindOfClass:[NSArray class]]) {
            NSArray * array = responseObject[@"data"];
            for (int i = 0; i < array.count; i++) {
                NSDictionary * dic = array[i];
                ZiLiaoModel * model = [ZiLiaoModel mj_objectWithKeyValues:dic];
                if (i < 3) {
                    model.show = YES;
                } else {
                    model.show = NO;
                }
                [arr addObject:model];
            }
        }
        
        [self.dataArray addObject:arr];
        [self loadSubData];
    }];
}

- (void)loadSubData
{
    [HTTPTool postWithURL:APIZiliao parameters:@{@"parent":@"1"} success:^(id  _Nonnull responseObject) {
        [MBProgressHUD hideHUDForView:self.view];
        NSMutableArray * arr = [NSMutableArray array];
        for (NSDictionary * dic in responseObject[@"data"]) {
            ZiLiaoModel * model = [ZiLiaoModel mj_objectWithKeyValues:dic];
            [arr addObject:model];
        }
        [self.dataArray addObject:arr];
        [self.collectionView reloadData];
        [self endRefreshing];
    }];
}

- (void)createSearchView
{
    //search
    self.searchView = [[SearchBar alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 44)];
    [self.contentView addSubview:self.searchView];
    self.searchView.delegate = self;
}

#pragma mark - UISearchBarDelegate
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    self.searchText = searchText;
    [self.resultArray removeAllObjects];
    if (searchText.length < 1) {
        [self.collectionView reloadData];
        return;
    }
    for (NSArray * arr in self.dataArray) {
        NSMutableArray * array = [NSMutableArray array];
        for (ZiLiaoModel * model in arr) {
            if ([model.name rangeOfString:searchText].location != NSNotFound) {
                [array addObject:model];
            }
        }
        [self.resultArray addObject:array];
    }
    [self.collectionView reloadData];
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    if (self.searchText.length > 0) {
        return self.resultArray.count;
    }
    return self.dataArray.count;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSArray * arr = self.dataArray[section];
    if (self.searchText.length > 0) {
        arr = self.resultArray[section];
    }
    return arr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ZiliaoCCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SubCCell" forIndexPath:indexPath];
    NSArray * arr = self.dataArray[indexPath.section];
    if (self.searchText.length > 0) {
       arr = self.resultArray[indexPath.section];
    }
    ZiLiaoModel * model = arr[indexPath.row];
    if (indexPath.section == 0) {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ZiliaoCCell" forIndexPath:indexPath];
        cell.model = model;
    }
    cell.label.text = model.name;
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return  CGSizeMake(WIDTH -20, 40);
    }
    return  CGSizeMake((WIDTH -40)/3, 40);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(10, 10,10, 10);//（上、左、下、右）
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 9.5;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 10;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    ZiLiaoDetailVC * d = [ZiLiaoDetailVC new];
    NSArray * arr = self.dataArray[indexPath.section];
    d.model = arr[indexPath.row];
    [self pushVC:d];
}

- (NSMutableArray *)resultArray
{
    if (!_resultArray) {
        _resultArray = [NSMutableArray array];
    }
    return _resultArray;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.searchView resignFirstResponder];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.collectionView reloadData];
}
@end
