//
//  ZLDetailCell.h
//  CaiKuBaoDian
//
//  Created by mac on 28/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "BaseCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZLDetailCell : BaseCell
@property (weak, nonatomic) IBOutlet UIImageView *header;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *time;
@property (weak, nonatomic) IBOutlet UIView *viewLike;
@property (weak, nonatomic) IBOutlet UILabel *like;
@property ( nonatomic, strong ) ZLDetailModel * modelZL;
@end

NS_ASSUME_NONNULL_END
