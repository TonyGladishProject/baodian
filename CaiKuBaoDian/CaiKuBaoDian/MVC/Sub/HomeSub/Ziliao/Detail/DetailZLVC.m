//
//  DetailZLVC.m
//  CaiKuBaoDian
//
//  Created by mac on 17/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "DetailZLVC.h"

@interface DetailZLVC ()

@end

@implementation DetailZLVC


- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (void)dataMain
{
    [HTTPTool postWithURL:APIDetailZL parameters:@{@"id":self.idStr} success:^(id  _Nonnull responseObject) {
        [self dataComment];
        [self userDataToView:responseObject];
        [self.store putObject:responseObject withId:[@"APIDetailZL" stringByAppendingString:self.idStr] intoTable:@"Detail"];
    }];
}

- (void)userDataToView:(id)responseObject
{
    NSDictionary * dic = responseObject[@"data"];
    [self.infoView dataToViewZL:dic];
    [self.webView loadHTMLString:dic[@"content"] baseURL:nil];
    self.listView.comment_id = self.commentView.idStr = dic[@"id"];
    self.shareView.shareStr = dic[@"share_url"];
}

- (void)locToUserView
{
    id responseObjectUser =
    [self.store getObjectById:[@"APIDetailZL" stringByAppendingString:self.idStr] fromTable:@"Detail"];
    [self userDataToView:responseObjectUser];
}

@end

