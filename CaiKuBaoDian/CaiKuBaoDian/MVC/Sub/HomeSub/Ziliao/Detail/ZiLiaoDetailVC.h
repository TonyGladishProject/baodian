//
//  ZiLiaoDetailVC.h
//  CaiKuBaoDian
//
//  Created by mac on 26/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "TableViewVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZiLiaoDetailVC : TableViewVC
@property ( nonatomic, strong ) ZiLiaoModel * model;
@end

NS_ASSUME_NONNULL_END
