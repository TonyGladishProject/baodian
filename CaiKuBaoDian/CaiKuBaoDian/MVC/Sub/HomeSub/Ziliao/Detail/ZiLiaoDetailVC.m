//
//  ZiLiaoDetailVC.m
//  CaiKuBaoDian
//
//  Created by mac on 26/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "ZiLiaoDetailVC.h"
#import "ZLDetailCell.h"
#import "DetailZLVC.h"

@interface ZiLiaoDetailVC ()

@end

@implementation ZiLiaoDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.titleLabel.text = self.model.name;
    [self registerNib:[ZLDetailCell class]];
    [self dataHud];
}

- (void)mjHeader
{
    [self postData:APIZiliao parameters:@{@"parent":self.model.id,@"why_add":self.model.why_add}];
}

- (void)dataToModel:(id)responseObject
{
    NSLog(@"responseObject=%@",responseObject);
    for (NSDictionary * dic  in responseObject[@"data"]) {
        ZLDetailModel * model = [ZLDetailModel mj_objectWithKeyValues:dic];
        [self.dataArray addObject:model];
    }
    [self.tableView reloadData];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZLDetailCell * cell = [tableView dequeueReusableCellWithIdentifier:@"ZLDetailCell"];
    cell.modelZL = self.dataArray[indexPath.row];
    return  cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    DetailZLVC * d = [DetailZLVC new];
    ZLDetailModel * m = self.dataArray[indexPath.row];
    d.name = m.name;
    d.idStr = m.id;
    d.scene = @"zldq";
    [self pushVC:d];
    
}

@end
