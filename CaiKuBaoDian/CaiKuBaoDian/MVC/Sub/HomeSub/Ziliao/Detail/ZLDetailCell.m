//
//  ZLDetailCell.m
//  CaiKuBaoDian
//
//  Created by mac on 28/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "ZLDetailCell.h"

@implementation ZLDetailCell

- (void)setModelZL:(ZLDetailModel *)modelZL
{
    _modelZL = modelZL;
    
    [self.header sd_setImageWithURL:[NSURL URLWithString:@""] placeholderImage:[UIImage imageNamed:@"placeHolder0"]];
    self.name.text = modelZL.name;
    self.time.text = [Tools timeFromtimeStamp:modelZL.create_time];
//    self.comment.text = [NSString stringWithFormat:@"%@个评论",modelZL.plNum];
    self.like.text = modelZL.dzNum;
//    self.labelTitle.text = modelZL.Description;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    //    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.line = [[UIView alloc] initWithFrame:CGRectMake(0, 79, WIDTH, 1)];
    [self.contentView addSubview:self.line];
    self.line.backgroundColor = cellLineColor;
    [self.viewLike touchEvents:self action:@selector(ZLModelLike)];
}

- (void)ZLModelLike
{
    [HTTPTool postWithURLLogin:APILTDLike parameters:@{@"full_name":UserPhone,@"scene":@"zldq",@"scene_id":self.modelZL.id} success:^(id  _Nonnull responseObject) {
        NSNumber * num = responseObject[@"data"][@"type"];
        NSString * str = [[NSNumberFormatter new] stringFromNumber:num];
        int  i = self.modelZL.dzNum.intValue;
        if (str.intValue == 1) {
            i++;
        } else {
            i--;
        }
        self.modelZL.dzNum = [NSString stringWithFormat:@"%d",i];
        self.like.text = self.modelZL.dzNum;
    }];
}

@end
