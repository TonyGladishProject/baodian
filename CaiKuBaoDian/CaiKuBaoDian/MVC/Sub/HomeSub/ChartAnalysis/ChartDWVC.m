//
//  ChartDWVC.m
//  CaiKuBaoDian
//
//  Created by mac on 14/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "ChartDWVC.h"
#import "CADWTitleCell.h"
#import "CADWMainCell.h"

@interface ChartDWVC ()

@end

@implementation ChartDWVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self registerClass:[CADWTitleCell class]];
    [self registerClass:[CADWMainCell class]];
}

- (void)calculateTotal
{
    self.totalArray = [NSMutableArray arrayWithArray:@[@"0",@"0",@"0",@"0",@"0",@"0",@"0"]];
    for (CASXModel * model in self.dataArray) {
        NSInteger index = model.duanwei.integerValue - 1;
        NSString * str = self.totalArray[index];
        int i = str.intValue;
        i++;
        NSString * s = [NSString stringWithFormat:@"%d",i];
        [self.totalArray replaceObjectAtIndex:index withObject:s];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        CADWTitleCell * cell = [tableView dequeueReusableCellWithIdentifier:@"CADWTitleCell" forIndexPath:indexPath];
                [cell dataToView:self.totalArray];
        return cell;
    }
    CADWMainCell * cell = [tableView dequeueReusableCellWithIdentifier:@"CADWMainCell" forIndexPath:indexPath];
        [cell dataToView:self.dataArray[indexPath.row]];
    return cell;
}

@end
