//
//  ChartWXVC.m
//  CaiKuBaoDian
//
//  Created by mac on 14/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "ChartWXVC.h"
#import "CAWXTitleCell.h"
#import "CAWXMainCell.h"

@interface ChartWXVC ()

@end

@implementation ChartWXVC


- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self registerClass:[CAWXTitleCell class]];
    [self registerClass:[CAWXMainCell class]];
}

- (void)calculateTotal
{
    self.totalArray = [NSMutableArray arrayWithArray:@[@"0",@"0",@"0",@"0",@"0"]];
    for (CASXModel * model in self.dataArray) {
        NSInteger  index = [self wxIndex:model.wuxing];
        NSString * str = self.totalArray[index];
        int i = str.intValue;
        i++;
        NSString * s = [NSString stringWithFormat:@"%d",i];
        [self.totalArray replaceObjectAtIndex:index withObject:s];
    }
}

- (NSInteger)wxIndex:(NSString *)str
{
    if ([str isEqualToString:@"金"]) {
        return 0;
    } else if ([str isEqualToString:@"木"]) {
        return 1;
    } else if ([str isEqualToString:@"水"]) {
        return 2;
    } else if ([str isEqualToString:@"火"]) {
        return 3;
    }
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        CAWXTitleCell * cell = [tableView dequeueReusableCellWithIdentifier:@"CAWXTitleCell" forIndexPath:indexPath];
                [cell dataToView:self.totalArray];
        return cell;
    }
    CAWXMainCell * cell = [tableView dequeueReusableCellWithIdentifier:@"CAWXMainCell" forIndexPath:indexPath];
        [cell dataToView:self.dataArray[indexPath.row]];
    return cell;
}

@end
