//
//  ChartVC.m
//  CaiKuBaoDian
//
//  Created by mac on 14/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "ChartVC.h"

@interface ChartVC ()

@end

@implementation ChartVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadHud];
    [self loadMJ];
    [self refreshUI];
}

- (void)loadMJ
{
    self.page = 1;
    MJRefreshNormalHeader * header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self loadData];
    }];
    self.tableView.mj_header = header;
}

- (void)loadFooter
{
    MJRefreshAutoNormalFooter * footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        if (self.count != self.dataArray.count) {
            self.page ++;
            self.count = self.dataArray.count;
        }
        [self loadData];
    }];
    if (self.dataArray.count >=7) {
        self.tableView.mj_footer = footer;
    } else {
        self.tableView.mj_footer = nil;
    }
    footer.automaticallyRefresh = NO;
}

- (void)loadHud
{
    self.page = 1;
    [MBProgressHUD viewLoading:self.view];
    [self loadData];
}

- (void)loadData
{
    [HTTPTool postWithURL:APIChart parameters:@{@"year":self.year,@"type":self.str,@"page":[NSString stringWithFormat:@"%d",self.page]} success:^(id  _Nonnull responseObject) {
        
        [self dataToModel:responseObject];
        [self loadFooter];
        [self endRefreshing];
    }];
}

- (void)dataToModel:(id)responseObject
{
    if (self.page == 1) {
        [self.dataArray removeAllObjects];
    }
    for (NSDictionary * dic in responseObject[@"data"]) {
        CASXModel * model = [CASXModel mj_objectWithKeyValues:dic];
        [self.dataArray addObject:model];
    }
    [self calculateTotal];
    [self.tableView reloadData];
}

- (void)calculateTotal{}

- (void)endRefreshing
{
    [MBProgressHUD hideHUDForView:self.view];
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}

- (void)refreshUI
{
    self.contentViewHeight = HEIGHT - self.navBottomHeight - 44;
    self.navView.hidden = YES;
    self.contentView.frame = CGRectMake(0, 0, WIDTH, self.contentViewHeight);
    self.tableView.height = self.contentViewHeight;
}

#pragma mark - tableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;
    }
    return self.dataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return 90;
    }
    return 30;
}

- (NSMutableArray *)totalArray
{
    if (!_totalArray) {
        _totalArray = [NSMutableArray array];
    }
    return _totalArray;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshYear" object:nil];
}
-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
