//
//  ChartVC.h
//  CaiKuBaoDian
//
//  Created by mac on 14/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "TablePureVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface ChartVC : TablePureVC
@property ( nonatomic, copy ) NSString * str;
@property ( nonatomic, copy ) NSString * year;
@property ( nonatomic, strong ) NSMutableArray * totalArray;
- (void)calculateTotal;
@end

NS_ASSUME_NONNULL_END
