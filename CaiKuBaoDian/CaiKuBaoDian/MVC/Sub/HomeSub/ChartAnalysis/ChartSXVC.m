//
//  ChartSXVC.m
//  CaiKuBaoDian
//
//  Created by mac on 14/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "ChartSXVC.h"
#import "CASXTitleCell.h"
#import "CASXMainCell.h"

@interface ChartSXVC ()

@end
@implementation ChartSXVC
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self registerClass:[CASXTitleCell class]];
    [self registerClass:[CASXMainCell class]];
}



- (void)calculateTotal
{
    self.totalArray = [NSMutableArray arrayWithArray:@[@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0"]];
    for (CASXModel * model in self.dataArray) {
        NSInteger  index = [CATool sxIndex:model.shengxiao];
        NSString * str = self.totalArray[index];
        int i = str.intValue;
        i++;
        NSString * s = [NSString stringWithFormat:@"%d",i];
        [self.totalArray replaceObjectAtIndex:index withObject:s];
    }
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        CASXTitleCell * cell = [tableView dequeueReusableCellWithIdentifier:@"CASXTitleCell" forIndexPath:indexPath];
        [cell dataToView:self.totalArray];
        return cell;
    }
    CASXMainCell * cell = [tableView dequeueReusableCellWithIdentifier:@"CASXMainCell" forIndexPath:indexPath];
    [cell dataToView:self.dataArray[indexPath.row]];
    return cell;
}


@end
