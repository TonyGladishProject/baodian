//
//  CATSMainCell.m
//  CaiKuBaoDian
//
//  Created by mac on 14/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "CATSMainCell.h"

@implementation CATSMainCell

- (void)dataToView:(CASXModel *)model
{
    NSArray * arr = @[model.qishu,model.tema];
    for (int i = 0; i < arr.count; i++) {
        UILabel * label = self.labelArray[i];
        label.text = arr[i];
        if (i == 1) {
            label.textColor = [CATool labelTextColor:model.yanse];
        }
    }
    
    NSInteger index = model.tou.integerValue + 2;
    for (int i = 2; i < 7; i++) {
        UILabel * label = self.labelArray[i];
        if (i == index) {
            label.text = model.tema;
            label.textColor = [CATool labelTextColor:model.yanse];
        } else {
            label.text = @"--";
            label.textColor = UIColor.darkGrayColor;
        }
    }
    
    UILabel * l8 = self.labelArray[7];
    if ([model.danshuang isEqualToString:@"单"]) {
        l8.text = model.danshuang;
        l8.textColor = [CATool labelTextColor:model.yanse];
    } else {
        l8.text = @"--";
        l8.textColor = UIColor.darkGrayColor;
    }
    UILabel * l9 = self.labelArray[8];
    if ([model.danshuang isEqualToString:@"双"]) {
        l9.text = model.danshuang;
        l9.textColor = [CATool labelTextColor:model.yanse];
    } else {
        l9.text = @"--";
        l9.textColor = UIColor.darkGrayColor;
    }
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI
{
    float w = WIDTH/9;
    for (int i = 0; i < 12; i++) {
        UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(w*i, 0, w, 30)];
        [self.contentView addSubview:label];
        [self.labelArray addObject:label];
        label.textColor = UIColor.darkGrayColor;
        label.font = fontCell;
        label.textAlignment = NSTextAlignmentCenter;
        [label layerBorderWidth:1 color:cellLineColor];
        label.text = @"--";
    }
}

@end


