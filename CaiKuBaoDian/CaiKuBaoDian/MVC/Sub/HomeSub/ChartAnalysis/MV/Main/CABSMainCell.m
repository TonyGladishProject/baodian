//
//  CABSMainCell.m
//  CaiKuBaoDian
//
//  Created by mac on 14/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "CABSMainCell.h"

@implementation CABSMainCell

- (void)dataToView:(CASXModel *)model
{
    NSArray * arr = @[model.qishu,model.tema,model.danshuang];
    for (int i = 0; i < arr.count; i++) {
        UILabel * label = self.labelArray[i];
        label.text = arr[i];
        if (i != 0) {
            label.textColor = [CATool labelTextColor:model.yanse];
        }
    }
    NSInteger index = [self bsIndex:model.yanse];
    for (int i = 3; i < 6; i++) {
        UILabel * label = self.labelArray[i];
        
        if (i == index) {
            label.text = model.color;
            label.textColor = [CATool labelTextColor:model.yanse];
        } else {
            label.text = @"--";
            label.textColor = UIColor.darkGrayColor;
        }
    }
    NSInteger inx = [self bbIndex:model];
    for (int i = 6; i < 12; i++) {
        UILabel * label = self.labelArray[i];
        
        if (i == inx) {
            label.text = model.danshuang;
            label.textColor = [CATool labelTextColor:model.yanse];
        } else {
            label.text = @"--";
            label.textColor = UIColor.darkGrayColor;
        }
    }
}

- (NSInteger)bsIndex:(NSString *)str
{
    if ([str isEqualToString:@"hong"]) {
        return 3;
    } else if ([str isEqualToString:@"lv"]) {
        return 4;
    } else if ([str isEqualToString:@"lan"]) {
        return 5;
    }
    return 3;
}

- (NSInteger)bbIndex:(CASXModel *)model
{
    if ([model.danshuang isEqualToString:@"单"]) {
        if ([model.yanse isEqualToString:@"hong"]) {
            return 6;
        } else if ([model.yanse isEqualToString:@"lv"]) {
            return 7;
        } else {
            return 8;
        }
    } else {
        if ([model.yanse isEqualToString:@"hong"]) {
            return 9;
        } else if ([model.yanse isEqualToString:@"lv"]) {
            return 10;
        } else {
            return 11;
        }
    }
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI
{
    float w = WIDTH/12;
    for (int i = 0; i < 12; i++) {
        UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(w*i, 0, w, 30)];
        [self.contentView addSubview:label];
        [self.labelArray addObject:label];
        label.textColor = UIColor.darkGrayColor;
        label.font = fontCell;
        label.textAlignment = NSTextAlignmentCenter;
        [label layerBorderWidth:1 color:cellLineColor];
        label.text = @"--";
    }
}

@end
