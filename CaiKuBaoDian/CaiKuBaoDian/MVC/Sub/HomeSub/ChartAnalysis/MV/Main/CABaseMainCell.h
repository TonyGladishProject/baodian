//
//  CABaseMainCell.h
//  CaiKuBaoDian
//
//  Created by mac on 14/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "PureCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface CABaseMainCell : PureCell
@property ( nonatomic, strong ) UILabel * qs;
@property ( nonatomic, strong ) UILabel * tema;
@property ( nonatomic, strong ) NSMutableArray * labelArray;
- (void)dataToView:(CASXModel *)model;
@end

NS_ASSUME_NONNULL_END
