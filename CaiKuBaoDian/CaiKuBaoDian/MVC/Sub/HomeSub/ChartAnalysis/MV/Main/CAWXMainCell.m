//
//  CAWXMainCell.m
//  CaiKuBaoDian
//
//  Created by mac on 14/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "CAWXMainCell.h"

@implementation CAWXMainCell

- (void)dataToView:(CASXModel *)model
{
    NSArray * arr = @[model.qishu,model.tema,model.wuxing];
    for (int i = 0; i < arr.count; i++) {
        UILabel * label = self.labelArray[i];
        label.text = arr[i];
        if (i != 0) {
            label.textColor = [CATool labelTextColor:model.yanse];
        }
    }
    NSInteger index = [self wxIndex:model.wuxing];
    for (int i = 3; i < 8; i++) {
        UILabel * label = self.labelArray[i];
        if (i == index) {
            label.text = model.wuxing;
            label.textColor = [CATool labelTextColor:model.yanse];
        } else {
            label.text = @"--";
            label.textColor = UIColor.darkGrayColor;
        }
    }
}

- (NSInteger)wxIndex:(NSString *)str
{
    if ([str isEqualToString:@"金"]) {
        return 3;
    } else if ([str isEqualToString:@"木"]) {
        return 4;
    } else if ([str isEqualToString:@"水"]) {
        return 5;
    } else if ([str isEqualToString:@"火"]) {
        return 6;
    }
    return 7;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI
{
    float w = WIDTH/8;
    for (int i = 0; i < 12; i++) {
        UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(w*i, 0, w, 30)];
        [self.contentView addSubview:label];
        [self.labelArray addObject:label];
        label.textColor = UIColor.darkGrayColor;
        label.font = fontCell;
        label.textAlignment = NSTextAlignmentCenter;
        [label layerBorderWidth:1 color:cellLineColor];
        label.text = @"--";
    }
}

@end


