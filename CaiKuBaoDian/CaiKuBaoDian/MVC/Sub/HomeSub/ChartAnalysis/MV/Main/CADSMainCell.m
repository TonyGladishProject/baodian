//
//  CADSMainCell.m
//  CaiKuBaoDian
//
//  Created by mac on 14/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "CADSMainCell.h"

@implementation CADSMainCell

- (void)dataToView:(CASXModel *)model
{
    NSArray * arr = @[model.qishu,model.tema,model.danshuang];
    for (int i = 0; i < arr.count; i++) {
        UILabel * label = self.labelArray[i];
        label.text = arr[i];
        if (i != 0) {
            label.textColor = [CATool labelTextColor:model.yanse];
        }
    }
    
    
    UILabel * l4 = self.labelArray[3];
    if ([model.danshuang isEqualToString:@"单"]) {
        l4.text = model.danshuang;
        l4.textColor = [CATool labelTextColor:model.yanse];
    } else {
        l4.text = @"--";
        l4.textColor = UIColor.darkGrayColor;
    }
    UILabel * l5 = self.labelArray[4];
    if ([model.danshuang isEqualToString:@"双"]) {
        l5.text = model.danshuang;
        l5.textColor = [CATool labelTextColor:model.yanse];
    } else {
        l5.text = @"--";
        l5.textColor = UIColor.darkGrayColor;
    }
    
    UILabel * l6 = self.labelArray[5];
    if ([model.tetoudanshuang isEqualToString:@"单"]) {
        l6.text = model.tetoudanshuang;
        l6.textColor = [CATool labelTextColor:model.yanse];
    } else {
        l6.text = @"--";
        l6.textColor = UIColor.darkGrayColor;
    }
    UILabel * l7 = self.labelArray[6];
    if ([model.tetoudanshuang isEqualToString:@"双"]) {
        l7.text = model.tetoudanshuang;
        l7.textColor = [CATool labelTextColor:model.yanse];
    } else {
        l7.text = @"--";
        l7.textColor = UIColor.darkGrayColor;
    }
    
    UILabel * l8 = self.labelArray[7];
    if ([model.heshudanshuang isEqualToString:@"单"]) {
        l8.text = model.heshudanshuang;
        l8.textColor = [CATool labelTextColor:model.yanse];
    } else {
        l8.text = @"--";
        l8.textColor = UIColor.darkGrayColor;
    }
    UILabel * l9 = self.labelArray[8];
    if ([model.heshudanshuang isEqualToString:@"双"]) {
        l9.text = model.heshudanshuang;
        l9.textColor = [CATool labelTextColor:model.yanse];
    } else {
        l9.text = @"--";
        l9.textColor = UIColor.darkGrayColor;
    }
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI
{
    float w = WIDTH/9;
    for (int i = 0; i < 12; i++) {
        UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(w*i, 0, w, 30)];
        [self.contentView addSubview:label];
        [self.labelArray addObject:label];
        label.textColor = UIColor.darkGrayColor;
        label.font = fontCell;
        label.textAlignment = NSTextAlignmentCenter;
        [label layerBorderWidth:1 color:cellLineColor];
        label.text = @"--";
    }
}

@end

