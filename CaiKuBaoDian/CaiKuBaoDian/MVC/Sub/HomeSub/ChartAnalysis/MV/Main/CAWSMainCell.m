//
//  CAWSMainCell.m
//  CaiKuBaoDian
//
//  Created by mac on 14/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "CAWSMainCell.h"

@implementation CAWSMainCell

- (void)dataToView:(CASXModel *)model
{
    self.qs.text = model.qishu;
    self.tema.text = model.tema;
    self.tema.textColor = [CATool labelTextColor:model.yanse];
    NSInteger index = model.wei.integerValue;
    for (int i = 0; i < 10; i++) {
        UILabel * label = self.labelArray[i];
        if (index == i) {
            label.text = model.tema;
            self.tema.textColor = label.textColor = [CATool labelTextColor:model.yanse];
        } else {
            label.text = @"--";
            label.textColor = UIColor.darkGrayColor;
        }
    }
    
    UILabel * l8 = self.labelArray[10];
    if ([model.daxiao isEqualToString:@"大"]) {
        l8.text = model.daxiao;
        l8.textColor = [CATool labelTextColor:model.yanse];
    } else {
        l8.text = @"--";
        l8.textColor = UIColor.darkGrayColor;
    }
    UILabel * l9 = self.labelArray[11];
    if ([model.daxiao isEqualToString:@"小"]) {
        l9.text = model.daxiao;
        l9.textColor = [CATool labelTextColor:model.yanse];
    } else {
        l9.text = @"--";
        l9.textColor = UIColor.darkGrayColor;
    }
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI
{
    NSMutableArray * arr = [NSMutableArray array];
    
    self.qs = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [self.contentView addSubview:self.qs];
    [arr addObject:self.qs];
    
    self.tema = [[UILabel alloc] initWithFrame:CGRectMake(30, 0, 30, 30)];
    [self.contentView addSubview:self.tema];
    [arr addObject:self.tema];
    
    float w = (WIDTH - 60)/12;
    for (int i = 0; i < 12; i++) {
        UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(60 + w*i, 0, w, 30)];
        [self.contentView addSubview:label];
        [self.labelArray addObject:label];
        [arr addObject:label];
        
        label.text = @"--";
    }
    
    for (UILabel * label in arr) {
        label.textColor = UIColor.darkGrayColor;
        label.font = fontCell;
        label.textAlignment = NSTextAlignmentCenter;
        [label layerBorderWidth:1 color:cellLineColor];
    }
}


@end


