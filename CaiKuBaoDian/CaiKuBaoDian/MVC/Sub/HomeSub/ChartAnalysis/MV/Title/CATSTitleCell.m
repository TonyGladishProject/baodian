//
//  CATSTitleCell.m
//  CaiKuBaoDian
//
//  Created by mac on 14/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "CATSTitleCell.h"

@implementation CATSTitleCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI
{
    float w = WIDTH/9;
    self.qs.width = w;
    self.tm.frame = CGRectMake(w, 0, w, 30);
    self.cs.width = w * 2;
    
    UILabel * tt = [[UILabel alloc] initWithFrame:CGRectMake(w*2, 0, w*5, 30)];
    [self.contentView addSubview:tt];
    [self.labelArray addObject:tt];
    tt.text = @"特头走势";
    
    UILabel * ds = [[UILabel alloc] initWithFrame:CGRectMake(w*7, 0, w*2, 30)];
    [self.contentView addSubview:ds];
    [self.labelArray addObject:ds];
    ds.text = @"特头单双";
    
    NSArray * arr = @[@"0头",@"1头",@"2头",@"3头",@"4头",@"单头",@"双头"];
    for (int i = 0; i < arr.count; i++) {
        UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(w * 2 + w*i, 30, w, 30)];
        label.text = arr[i];
        [self.contentView addSubview:label];
        [self.labelArray addObject:label];
        
        UILabel * time = [[UILabel alloc] initWithFrame:CGRectMake(w*2+ w*i, 60, w, 30)];
        [self.contentView addSubview:time];
        [self.timeArray addObject:time];
        
        time.textColor = UIColorFromRGB(0xFF1A1A);
        time.font = fontCell;
        time.textAlignment = NSTextAlignmentCenter;
        [time layerBorderWidth:1 color:cellLineColor];
    }
    
    for (UILabel * label in self.labelArray) {
        label.textColor = UIColor.darkGrayColor;
        label.font = fontCell;
        label.textAlignment = NSTextAlignmentCenter;
        [label layerBorderWidth:1 color:cellLineColor];
    }
    
}

@end
