//
//  CABSTitleCell.m
//  CaiKuBaoDian
//
//  Created by mac on 14/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "CABSTitleCell.h"

@implementation CABSTitleCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI
{
    float w = WIDTH/12;
    
    self.qs.frame = CGRectMake(0, 0, w, 30);
    self.tm.frame = CGRectMake(w, 0, w, 30);
    
    UILabel * tx = [[UILabel alloc] initWithFrame:CGRectMake(w*2, 0, w, 30)];
    [self.contentView addSubview:tx];
    [self.labelArray addObject:tx];
    tx.text = @"特肖";
    
    UILabel * bs = [[UILabel alloc] initWithFrame:CGRectMake(w * 3, 0, w * 3, 30)];
    [self.contentView addSubview:bs];
    [self.labelArray addObject:bs];
    
    UILabel * bb = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH/2, 0, WIDTH/2, 30)];
    [self.contentView addSubview:bb];
    [self.labelArray addObject:bb];
    
    NSArray * arr = @[@"红",@"绿",@"蓝",@"红单",@"绿单",@"蓝单",@"红双",@"绿双",@"蓝双"];
    for (int i = 0; i < arr.count; i++) {
        UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(w*3 + w*i, 30, w, 30)];
        label.text = arr[i];
        [self.contentView addSubview:label];
        [self.labelArray addObject:label];
        
        UILabel * time = [[UILabel alloc] initWithFrame:CGRectMake(w*3 + w*i, 60, w, 30)];
        [self.contentView addSubview:time];
        [self.timeArray addObject:time];
        
        time.textColor = UIColorFromRGB(0xFF1A1A);
        time.font = fontCell;
        time.textAlignment = NSTextAlignmentCenter;
        [time layerBorderWidth:1 color:cellLineColor];
    }
    
    for (UILabel * label in self.labelArray) {
        label.textColor = UIColor.darkGrayColor;
        label.font = fontCell;
        label.textAlignment = NSTextAlignmentCenter;
        [label layerBorderWidth:1 color:cellLineColor];
    }
    
    self.cs.width = w*3;
    
    bs.text = @"波色走势";
    bb.text = @"半波走势";
}

@end
