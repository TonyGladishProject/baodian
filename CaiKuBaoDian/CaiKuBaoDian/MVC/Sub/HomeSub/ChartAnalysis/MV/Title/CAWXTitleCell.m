//
//  CAWXTitleCell.m
//  CaiKuBaoDian
//
//  Created by mac on 14/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "CAWXTitleCell.h"

@implementation CAWXTitleCell


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI
{
    float w = WIDTH/8;
    self.qs.width = w;
    self.tm.frame = CGRectMake(w, 0, w, 30);
    self.cs.width = w * 3;
    
    UILabel * wx = [[UILabel alloc] initWithFrame:CGRectMake(w*2, 0, w, 30)];
    [self.contentView addSubview:wx];
    [self.labelArray addObject:wx];
    wx.text = @"五行";
    
    UILabel * zs = [[UILabel alloc] initWithFrame:CGRectMake(w*3, 0, w*5, 30)];
    [self.contentView addSubview:zs];
    [self.labelArray addObject:zs];
    zs.text = @"五行走势";
    
    NSArray * arr = @[@"金",@"木",@"水",@"火",@"土"];
    for (int i = 0; i < arr.count; i++) {
        UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(w * 3 + w*i, 30, w, 30)];
        label.text = arr[i];
        [self.contentView addSubview:label];
        [self.labelArray addObject:label];
        
        UILabel * time = [[UILabel alloc] initWithFrame:CGRectMake(w*3+ w*i, 60, w, 30)];
        [self.contentView addSubview:time];
        [self.timeArray addObject:time];
        
        time.textColor = UIColorFromRGB(0xFF1A1A);
        time.font = fontCell;
        time.textAlignment = NSTextAlignmentCenter;
        [time layerBorderWidth:1 color:cellLineColor];
    }
    
    for (UILabel * label in self.labelArray) {
        label.textColor = UIColor.darkGrayColor;
        label.font = fontCell;
        label.textAlignment = NSTextAlignmentCenter;
        [label layerBorderWidth:1 color:cellLineColor];
    }
    
}

@end

