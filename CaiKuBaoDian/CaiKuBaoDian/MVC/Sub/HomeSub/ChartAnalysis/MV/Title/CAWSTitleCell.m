//
//  CAWSTitleCell.m
//  CaiKuBaoDian
//
//  Created by mac on 14/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "CAWSTitleCell.h"

@implementation CAWSTitleCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI
{
    float w = (WIDTH - 60)/12;
    
    UILabel * ws = [[UILabel alloc] initWithFrame:CGRectMake(60, 0, w*10, 30)];
    [self.contentView addSubview:ws];
    [self.labelArray addObject:ws];
    ws.text = @"尾数走势";
    
    UILabel * ds = [[UILabel alloc] initWithFrame:CGRectMake(60 + w*10, 0, w*2, 30)];
    [self.contentView addSubview:ds];
    [self.labelArray addObject:ds];
    ds.text = @"大小";
    
    NSMutableArray * arrW = [NSMutableArray arrayWithCapacity:10];
    NSArray * arr = @[@"0\n尾",@"1\n尾",@"2\n尾",@"3\n尾",@"4\n尾",@"5\n尾",@"6\n尾",@"7\n尾",@"8\n尾",@"9\n尾",@"大",@"小"];
    for (int i = 0; i < arr.count; i++) {
        UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(60 + w*i, 30, w, 30)];
        label.numberOfLines = 2;
        label.text = arr[i];
        [self.contentView addSubview:label];
        [self.labelArray addObject:label];
        [arrW addObject:label];
        
        UILabel * time = [[UILabel alloc] initWithFrame:CGRectMake(60+ w*i, 60, w, 30)];
        [self.contentView addSubview:time];
        [self.timeArray addObject:time];
        
        time.textColor = UIColorFromRGB(0xFF1A1A);
        time.font = fontCell;
        time.textAlignment = NSTextAlignmentCenter;
        [time layerBorderWidth:1 color:cellLineColor];
    }
    
    for (UILabel * label in self.labelArray) {
        label.textColor = UIColor.darkGrayColor;
        label.font = fontCell;
        label.textAlignment = NSTextAlignmentCenter;
        [label layerBorderWidth:1 color:cellLineColor];
    }
    
    for (int i = 0; i < 10; i++) {
        UILabel * label = arrW[i];
        label.font = [UIFont systemFontOfSize:11];
    }
    
}

@end
