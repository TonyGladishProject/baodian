//
//  CABaseTitleCell.h
//  CaiKuBaoDian
//
//  Created by mac on 14/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "PureCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface CABaseTitleCell : PureCell
@property ( nonatomic, strong ) NSMutableArray * labelArray;
@property ( nonatomic, strong ) NSMutableArray * timeArray;
@property ( nonatomic, strong ) NSMutableArray * totalArray;
@property ( nonatomic, strong ) UILabel * qs;
@property ( nonatomic, strong ) UILabel * tm;
@property ( nonatomic, strong ) UILabel * cs;
@property ( nonatomic, assign ) CGFloat w;
- (void)dataToView:(NSArray *)totalArray;
@end

NS_ASSUME_NONNULL_END
