//
//  CASXTitleCell.m
//  CaiKuBaoDian
//
//  Created by mac on 14/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "CASXTitleCell.h"

@implementation CASXTitleCell


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI
{
    
    
    UILabel * sx = [[UILabel alloc] initWithFrame:CGRectMake(60, 0, WIDTH - 60, 30)];
    [self.contentView addSubview:sx];
    [self.labelArray addObject:sx];
    
    CGFloat w = (WIDTH - 60)/12;
    NSArray * arr = [CATool sxArray];
    for (int i = 0; i < arr.count; i++) {
        UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(60 + w*i, 30, w, 30)];
        label.text = arr[i];
        [self.contentView addSubview:label];
        [self.labelArray addObject:label];
        
        UILabel * time = [[UILabel alloc] initWithFrame:CGRectMake(60 + w*i, 60, w, 30)];
        [self.contentView addSubview:time];
        [self.timeArray addObject:time];
        
        time.textColor = UIColorFromRGB(0xFF1A1A);
        time.font = fontCell;
        time.textAlignment = NSTextAlignmentCenter;
        [time layerBorderWidth:1 color:cellLineColor];
    }
    
    for (UILabel * label in self.labelArray) {
        label.textColor = UIColor.darkGrayColor;
        label.font = fontCell;
        label.textAlignment = NSTextAlignmentCenter;
        [label layerBorderWidth:1 color:cellLineColor];
    }
    sx.text = @"生肖走势";
}

@end
