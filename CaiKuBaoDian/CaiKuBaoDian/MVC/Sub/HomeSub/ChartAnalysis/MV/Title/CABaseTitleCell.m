//
//  CABaseTitleCell.m
//  CaiKuBaoDian
//
//  Created by mac on 14/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "CABaseTitleCell.h"

@implementation CABaseTitleCell
- (void)dataToView:(NSArray *)totalArray
{
    for (int i = 0; i < totalArray.count; i++) {
        UILabel * label = self.timeArray[i];
        label.text = totalArray[i];
    }
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createCS];
    }
    return self;
}

- (void)createCS
{
    UILabel * qs = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [self.contentView addSubview:qs];
    [self.labelArray addObject:qs];
    
    UILabel * tm = [[UILabel alloc] initWithFrame:CGRectMake(30, 0, 30, 30)];
    [self.contentView addSubview:tm];
    [self.labelArray addObject:tm];
    
    UILabel * cs = [[UILabel alloc] initWithFrame:CGRectMake(0, 60, 60, 30)];
    [self.contentView addSubview:cs];
    cs.textColor = UIColorFromRGB(0xFF1A1A);
    cs.font = fontCell;
    cs.textAlignment = NSTextAlignmentCenter;
    [cs layerBorderWidth:1 color:cellLineColor];
    cs.text = @"次数";
    
    qs.text = @"期数";
    tm.text = @"特码";
    self.qs = qs;
    self.tm = tm;
    self.cs = cs;
    
}

- (NSMutableArray *)labelArray
{
    if (!_labelArray) {
        _labelArray = [NSMutableArray array];
    }
    return _labelArray;
}

- (NSMutableArray *)timeArray
{
    if (!_timeArray) {
        _timeArray = [NSMutableArray array];
    }
    return _timeArray;
}

@end
