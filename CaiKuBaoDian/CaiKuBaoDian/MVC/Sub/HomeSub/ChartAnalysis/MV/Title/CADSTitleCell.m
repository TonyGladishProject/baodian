//
//  CADSTitleCell.m
//  CaiKuBaoDian
//
//  Created by mac on 14/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "CADSTitleCell.h"

@implementation CADSTitleCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI
{
    float w = WIDTH/9;

    self.qs.frame = CGRectMake(0, 0, w, 30);
    self.tm.frame = CGRectMake(w, 0, w, 30);
    
    UILabel * ds = [[UILabel alloc] initWithFrame:CGRectMake(w*2, 0, w, 30)];
    [self.contentView addSubview:ds];
    [self.labelArray addObject:ds];
    ds.text = @"单双";
    
    NSArray * a = @[@"单双走势",@"段位走势",@"头数走势"];
    for (int i = 0; i < a.count; i++) {
        UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(w* 3 +w*2* i, 0, w*2, 30)];
        [self.contentView addSubview:label];
        [self.labelArray addObject:label];
        label.text = a[i];
    }
    
    NSArray * arr = @[@"单",@"双",@"头单",@"头双",@"和单",@"和双"];
    for (int i = 0; i < arr.count; i++) {
        UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(w*3 + w*i, 30, w, 30)];
        label.text = arr[i];
        [self.contentView addSubview:label];
        [self.labelArray addObject:label];
        
        UILabel * time = [[UILabel alloc] initWithFrame:CGRectMake(w*3 + w*i, 60, w, 30)];
        [self.contentView addSubview:time];
        [self.timeArray addObject:time];
        
        time.textColor = UIColorFromRGB(0xFF1A1A);
        time.font = fontCell;
        time.textAlignment = NSTextAlignmentCenter;
        [time layerBorderWidth:1 color:cellLineColor];
    }
    
    for (UILabel * label in self.labelArray) {
        label.textColor = UIColor.darkGrayColor;
        label.font = fontCell;
        label.textAlignment = NSTextAlignmentCenter;
        [label layerBorderWidth:1 color:cellLineColor];
    }
    
    self.cs.width = w*3;
}

@end
