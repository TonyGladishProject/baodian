//
//  ChartAnalysisVC.m
//  CaiKuBaoDian
//
//  Created by mac on 18/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "ChartAnalysisVC.h"
#import "ChartSXVC.h"
#import "ChartBSVC.h"
#import "ChartDSVC.h"
#import "ChartDWVC.h"
#import "ChartTSVC.h"
#import "ChartWSVC.h"
#import "ChartWXVC.h"

@interface ChartAnalysisVC ()
@property ( nonatomic, strong ) NSArray * array;
@property ( nonatomic, strong ) NSMutableArray * yearArray;
@end

@implementation ChartAnalysisVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(noti:) name:@"refreshYear" object:nil];
    self.titleLabel.text = @"走势分析";
    self.yearStr = @"2019";
    [self.rightButton setTitle:self.yearStr forState:UIControlStateNormal];
    
    self.menuList = [NSMutableArray arrayWithObjects:@"生肖走势",@"波色走势",@"单双走势",@"段位走势",@"头数走势",@"尾数走势",@"五行走势", nil];
    self.yearArray = [NSMutableArray arrayWithObjects:@"2019",@"2019",@"2019",@"2019",@"2019",@"2019",@"2019",  nil];
    [self.magicController.magicView reloadData];
    self.magicController.magicView.separatorHidden = NO;
}

- (UIViewController *)magicView:(VTMagicView *)magicView viewControllerAtPage:(NSUInteger)pageIndex
{
    if (self.yearArray[0]) {
        if (pageIndex == 0) {
            ChartSXVC *i = [magicView dequeueReusablePageWithIdentifier:@"ChartSXVC"];
            if (!i) {
                i = [[ChartSXVC alloc] init];
            }
            i.str = self.array[pageIndex];
            i.year = self.yearArray[pageIndex];
            return i;
        } else if (pageIndex == 1) {
            ChartBSVC *i = [magicView dequeueReusablePageWithIdentifier:@"ChartBSVC"];
            if (!i) {
                i = [[ChartBSVC alloc] init];
            }
            i.str = self.array[pageIndex];
            i.year = self.yearArray[pageIndex];
            return i;
        } else if (pageIndex == 2) {
            ChartDSVC *i = [magicView dequeueReusablePageWithIdentifier:@"ChartDSVC"];
            if (!i) {
                i = [[ChartDSVC alloc] init];
            }
            i.str = self.array[pageIndex];
            i.year = self.yearArray[pageIndex];
            return i;
        } else if (pageIndex == 3) {
            ChartDWVC *i = [magicView dequeueReusablePageWithIdentifier:@"ChartDWVC"];
            if (!i) {
                i = [[ChartDWVC alloc] init];
            }
            i.str = self.array[pageIndex];
            i.year = self.yearArray[pageIndex];
            return i;
        } else if (pageIndex == 4) {
            ChartTSVC *i = [magicView dequeueReusablePageWithIdentifier:@"ChartTSVC"];
            if (!i) {
                i = [[ChartTSVC alloc] init];
            }
            i.str = self.array[pageIndex];
            i.year = self.yearArray[pageIndex];
            return i;
        } else if (pageIndex == 5) {
            ChartWSVC *i = [magicView dequeueReusablePageWithIdentifier:@"ChartWSVC"];
            if (!i) {
                i = [[ChartWSVC alloc] init];
            }
            i.str = self.array[pageIndex];
            i.year = self.yearArray[pageIndex];
            return i;
        }
        ChartWXVC *i = [magicView dequeueReusablePageWithIdentifier:@"ChartWXVC"];
        if (!i) {
            i = [[ChartWXVC alloc] init];
        }
        i.str = self.array[pageIndex];
        i.year = self.yearArray[pageIndex];
        return i;
    }
    return nil;
}

- (void)ButtonRightClick
{
    [self.yearPicker dismiss];
    [self.rightButton setTitle:self.yearPicker.selectedStr forState:UIControlStateNormal];
    if (![self.yearArray[self.magicController.currentPage] isEqualToString:self.yearPicker.selectedStr]) {
        [self confirmClick];
    }
}

- (void)confirmClick
{
    ChartVC * c = self.magicController.currentViewController;
    [self.yearArray replaceObjectAtIndex:self.magicController.currentPage withObject:self.yearPicker.selectedStr];
    c.year = self.yearPicker.selectedStr;
    [c.tableView.mj_header beginRefreshing];
}

- (void)noti:(NSNotification *)noti
{
    [self.rightButton setTitle:self.yearArray[self.magicController.currentPage] forState:UIControlStateNormal];
}

- (NSArray *)array
{
    if (!_array) {
        _array = @[@"shengxiao",@"bose",@"danshuang",@"duanwei",@"toushu",@"weishu",@"wuxing"];
    }
    return _array;
}

@end
