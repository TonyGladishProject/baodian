//
//  ChartTSVC.m
//  CaiKuBaoDian
//
//  Created by mac on 14/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "ChartTSVC.h"
#import "CATSTitleCell.h"
#import "CATSMainCell.h"

@interface ChartTSVC ()

@end

@implementation ChartTSVC


- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self registerClass:[CATSTitleCell class]];
    [self registerClass:[CATSMainCell class]];
}

- (void)calculateTotal
{
    self.totalArray = [NSMutableArray arrayWithArray:@[@"0",@"0",@"0",@"0",@"0",@"0",@"0"]];
    for (CASXModel * model in self.dataArray) {
        NSInteger  index = model.tou.integerValue;
        NSString * str = self.totalArray[index];
        int i = str.intValue;
        i++;
        NSString * s = [NSString stringWithFormat:@"%d",i];
        [self.totalArray replaceObjectAtIndex:index withObject:s];
        
        NSInteger inx;
        if ([model.danshuang isEqualToString:@"单"]) {
            inx = 5;
        } else {
            inx = 6;
        }
        NSString * strI = self.totalArray[inx];
        int iI = strI.intValue;
        iI++;
        NSString * sI = [NSString stringWithFormat:@"%d",iI];
        [self.totalArray replaceObjectAtIndex:inx withObject:sI];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        CATSTitleCell * cell = [tableView dequeueReusableCellWithIdentifier:@"CATSTitleCell" forIndexPath:indexPath];
                [cell dataToView:self.totalArray];
        return cell;
    }
    CATSMainCell * cell = [tableView dequeueReusableCellWithIdentifier:@"CATSMainCell" forIndexPath:indexPath];
        [cell dataToView:self.dataArray[indexPath.row]];
    return cell;
}

@end
