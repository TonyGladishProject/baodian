//
//  CATool.h
//  CaiKuBaoDian
//
//  Created by mac on 14/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CATool : NSObject
+(NSArray *)sxArray;
+(NSInteger)sxIndex:(NSString *)str;
+(UIColor *)labelTextColor:(NSString *)str;
@end

NS_ASSUME_NONNULL_END
