//
//  ChartWSVC.m
//  CaiKuBaoDian
//
//  Created by mac on 14/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "ChartWSVC.h"
#import "CAWSTitleCell.h"
#import "CAWSMainCell.h"

@interface ChartWSVC ()

@end

@implementation ChartWSVC


- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self registerClass:[CAWSTitleCell class]];
    [self registerClass:[CAWSMainCell class]];
}

- (void)calculateTotal
{
    self.totalArray = [NSMutableArray arrayWithArray:@[@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0"]];
    for (CASXModel * model in self.dataArray) {
        NSInteger  index = model.wei.integerValue;
        NSString * str = self.totalArray[index];
        int i = str.intValue;
        i++;
        NSString * s = [NSString stringWithFormat:@"%d",i];
        [self.totalArray replaceObjectAtIndex:index withObject:s];
        
        NSInteger inx;
        if ([model.daxiao isEqualToString:@"大"]) {
            inx = 10;
        } else {
            inx = 11;
        }
        NSString * strI = self.totalArray[inx];
        int iI = strI.intValue;
        iI++;
        NSString * sI = [NSString stringWithFormat:@"%d",iI];
        [self.totalArray replaceObjectAtIndex:inx withObject:sI];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        CAWSTitleCell * cell = [tableView dequeueReusableCellWithIdentifier:@"CAWSTitleCell" forIndexPath:indexPath];
                [cell dataToView:self.totalArray];
        return cell;
    }
    CAWSMainCell * cell = [tableView dequeueReusableCellWithIdentifier:@"CAWSMainCell" forIndexPath:indexPath];
        [cell dataToView:self.dataArray[indexPath.row]];
    return cell;
}

@end
