//
//  ChartBSVC.m
//  CaiKuBaoDian
//
//  Created by mac on 14/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "ChartBSVC.h"
#import "CABSTitleCell.h"
#import "CABSMainCell.h"

@interface ChartBSVC ()

@end

@implementation ChartBSVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self registerClass:[CABSTitleCell class]];
    [self registerClass:[CABSMainCell class]];
}

- (void)calculateTotal
{
    self.totalArray = [NSMutableArray arrayWithArray:@[@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0"]];
    for (CASXModel * model in self.dataArray) {
        NSInteger  index = [self bsIndex:model.yanse];
        NSInteger inx = [self bbIndex:model];
        NSString * str = self.totalArray[index];
        NSString * st = self.totalArray[inx];
        int i = str.intValue;
        int j = st.intValue;
        i++;
        j++;
        NSString * s = [NSString stringWithFormat:@"%d",i];
        NSString * sj  = [NSString stringWithFormat:@"%d",j];
        [self.totalArray replaceObjectAtIndex:index withObject:s];
        [self.totalArray replaceObjectAtIndex:inx withObject:sj];
    }
}

- (NSInteger)bsIndex:(NSString *)str
{
    if ([str isEqualToString:@"hong"]) {
        return 0;
    } else if ([str isEqualToString:@"lv"]) {
        return 1;
    } else if ([str isEqualToString:@"lan"]) {
        return 2;
    }
    return 0;
}

- (NSInteger)bbIndex:(CASXModel *)model
{
    if ([model.danshuang isEqualToString:@"单"]) {
        if ([model.yanse isEqualToString:@"hong"]) {
            return 3;
        } else if ([model.yanse isEqualToString:@"lv"]) {
            return 4;
        } else {
            return 5;
        }
    } else {
        if ([model.yanse isEqualToString:@"hong"]) {
            return 6;
        } else if ([model.yanse isEqualToString:@"lv"]) {
            return 7;
        } else {
            return 8;
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        CABSTitleCell * cell = [tableView dequeueReusableCellWithIdentifier:@"CABSTitleCell" forIndexPath:indexPath];
        [cell dataToView:self.totalArray];
        return cell;
    }
    CABSMainCell * cell = [tableView dequeueReusableCellWithIdentifier:@"CABSMainCell" forIndexPath:indexPath];
    [cell dataToView:self.dataArray[indexPath.row]];
    return cell;
}


@end
