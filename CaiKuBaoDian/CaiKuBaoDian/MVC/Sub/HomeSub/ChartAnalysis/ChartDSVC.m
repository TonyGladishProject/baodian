//
//  ChartDSVC.m
//  CaiKuBaoDian
//
//  Created by mac on 14/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "ChartDSVC.h"
#import "CADSTitleCell.h"
#import "CADSMainCell.h"

@interface ChartDSVC ()

@end

@implementation ChartDSVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self registerClass:[CADSTitleCell class]];
    [self registerClass:[CADSMainCell class]];
}

- (void)calculateTotal
{
    self.totalArray = [NSMutableArray arrayWithArray:@[@"0",@"0",@"0",@"0",@"0",@"0"]];
    for (CASXModel * model in self.dataArray) {
        NSInteger id0,id1,id2;
        if ([model.danshuang isEqualToString:@"单"]) {
            id0 = 0;
        } else {
            id0 = 1;
        }
        NSString * str0 = self.totalArray[id0];
        int i0 = str0.intValue;
        i0++;
        NSString * s0 = [NSString stringWithFormat:@"%d",i0];
        [self.totalArray replaceObjectAtIndex:id0 withObject:s0];
        
        if ([model.tetoudanshuang isEqualToString:@"单"]) {
            id1 = 2;
        } else {
            id1 = 3;
        }
        NSString * str1 = self.totalArray[id1];
        int i1 = str1.intValue;
        i1++;
        NSString * s1 = [NSString stringWithFormat:@"%d",i1];
        [self.totalArray replaceObjectAtIndex:id1 withObject:s1];
        if ([model.heshudanshuang isEqualToString:@"单"]) {
            id2 = 4;
        } else {
            id2 = 5;
        }
        NSString * str2 = self.totalArray[id2];
        int i2 = str2.intValue;
        i2++;
        NSString * s2 = [NSString stringWithFormat:@"%d",i2];
        [self.totalArray replaceObjectAtIndex:id2 withObject:s2];
        
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        CADSTitleCell * cell = [tableView dequeueReusableCellWithIdentifier:@"CADSTitleCell" forIndexPath:indexPath];
        [cell dataToView:self.totalArray];
        return cell;
    }
    CADSMainCell * cell = [tableView dequeueReusableCellWithIdentifier:@"CADSMainCell" forIndexPath:indexPath];
    [cell dataToView:self.dataArray[indexPath.row]];
    return cell;
}

@end
