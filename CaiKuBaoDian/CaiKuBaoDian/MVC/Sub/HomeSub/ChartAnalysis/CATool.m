//
//  CATool.m
//  CaiKuBaoDian
//
//  Created by mac on 14/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "CATool.h"

@implementation CATool

#pragma mark - chart
+(NSArray *)sxArray
{
    return @[@"鼠",@"牛",@"虎",@"兔",@"龙",@"蛇",@"马",@"羊",@"猴",@"鸡",@"狗",@"猪"];
}

+(NSInteger)sxIndex:(NSString *)str
{
    if ([str isEqualToString:@"鼠"]) {
        return 0;
    } else if ([str isEqualToString:@"牛"]) {
        return 1;
    } else if ([str isEqualToString:@"虎"]) {
        return 2;
    } else if ([str isEqualToString:@"兔"]) {
        return 3;
    } else if ([str isEqualToString:@"龙"]) {
        return 4;
    } else if ([str isEqualToString:@"蛇"]) {
        return 5;
    } else if ([str isEqualToString:@"马"]) {
        return 6;
    } else if ([str isEqualToString:@"羊"]) {
        return 7;
    } else if ([str isEqualToString:@"猴"]) {
        return 8;
    } else if ([str isEqualToString:@"鸡"]) {
        return 9;
    } else if ([str isEqualToString:@"狗"]) {
        return 10;
    } else if ([str isEqualToString:@"猪"]) {
        return 11;
    }
    return 0;
}

+(UIColor *)labelTextColor:(NSString *)str
{
    if ([str isEqualToString:@"hong"]) {
        return viewRedColor;
    } else if ([str isEqualToString:@"lv"]) {
        return viewGreenColor;
    } else if ([str isEqualToString:@"lan"]) {
        return viewBlueColor;
    }
    return viewRedColor;
}
@end
