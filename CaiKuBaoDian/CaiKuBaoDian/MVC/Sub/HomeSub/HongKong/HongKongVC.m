//
//  HongKongVC.m
//  CaiKuBaoDian
//
//  Created by mac on 11/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "HongKongVC.h"

@interface HongKongVC ()

@end

@implementation HongKongVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (void)config
{
    self.titleLabel.text = @"香港挂牌";
    self.idQS = @"59";
    self.idImg = @"gp";
    self.scene = @"xggp";
    self.imgHeight = WIDTH/375 * 630;;
}
@end
