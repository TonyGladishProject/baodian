//
//  Share4View.h
//  CaiKuBaoDian
//
//  Created by mac on 16/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "BaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface Share4View : BaseView
@property ( nonatomic, copy ) NSString * shareStr;
@property ( nonatomic, strong ) NSMutableArray *dataArray;
@end

NS_ASSUME_NONNULL_END
