//
//  CodeShareView.m
//  CaiKuBaoDian
//
//  Created by mac on 15/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "CodeShareView.h"
#import "Share4View.h"

@implementation CodeShareView

+(instancetype)newView
{
    CodeShareView * view = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil].lastObject;
    view.frame = CGRectMake(0, 0, WIDTH,view.contentViewHeight);
    return view;
}

- (void)drawRect:(CGRect)rect {
    [self.codeView layerMask5];
    
    NSString * str = @"我的邀请码";
    if ([UserDefaults valueForKey:@"share_code"]) {
        str = [@"我的邀请码\n" stringByAppendingString:[UserDefaults valueForKey:@"share_code"]];
    }
    self.labelCode.text = str;
    float H = self.contentViewHeight - self.codeView.bottom - 40 - 80;
    float h = H/3;
    if (iphone5) {
        h = H/2;
    }
    
    float x = (WIDTH - 240)/3;
    NSArray * arr = @[@"code_save",@"code_copy"];
    for (int i = 0; i < 2; i++) {
        UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(x + i * (x + 120), self.codeView.bottom + h, 120, 40);
        [self addSubview:btn];
        [btn setImage:[UIImage imageNamed:arr[i]] forState:UIControlStateNormal];
        if (i == 0) {
            [btn addTarget:self action:@selector(savePic) forControlEvents:UIControlEventTouchUpInside];
        } else {
            [btn addTarget:self action:@selector(copyLink) forControlEvents:UIControlEventTouchUpInside];
        }
        
    }
    
    Share4View * shareView = [Share4View newView];
    shareView.y = self.codeView.bottom + h * 2+ 40 ;
    [self addSubview:shareView];
    shareView.shareStr = self.shareStr;
}

- (void)savePic
{
    [self saveImage:self.imgQR.image];
}

- (void)saveImage:(UIImage *)image
{
    UIImageWriteToSavedPhotosAlbum(image, self, @selector(image:didFinishSavingWithError:contextInfo:), (__bridge void *)self);
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    if (!error) {
        [MBProgressHUD showMessage:@"保存成功"];
    }
    NSLog(@"image = %@, error = %@, contextInfo = %@", image, error, contextInfo);
}

- (void)copyLink
{
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = self.shareStr;
}

@end
