//
//  ShareVC.m
//  CaiKuBaoDian
//
//  Created by mac on 15/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "ShareVC.h"
#import "CodeShareView.h"
#import "LBXScanNative.h"

@interface ShareVC ()
@property ( nonatomic, strong ) CodeShareView * shareView;
@end

@implementation ShareVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadHud];
    [self refreshUI];
}

- (void)loadHud
{
    [MBProgressHUD viewLoading:self.view];
    [self loadData];
}

- (void)loadData
{
    [HTTPTool postWithURL:APIShareLink parameters:@{} success:^(id  _Nonnull responseObject) {
        [self loadDes];
        
        NSString * shareStr = responseObject[@"data"][@"link"];
        if ([UserDefaults valueForKey:@"share_code"]) {
            NSString * str = [NSString stringWithFormat:@"?share_code=%@",[UserDefaults valueForKey:@"share_code"]];
            shareStr = [responseObject[@"data"][@"link"] stringByAppendingString:str];
        }
        NSLog(@"--shareStr-%@",shareStr);
        self.shareView.shareStr = shareStr;
        self.shareView.imgQR.image = [LBXScanNative createQRWithString:shareStr QRSize:self.shareView.imgQR.bounds.size];
//        self.shareView.imgQR.image = [UIImage imageNamed:@"load_001"];
    }];
}

- (void)loadDes
{
    [HTTPTool postWithURL:APIShareDes parameters:@{} success:^(id  _Nonnull responseObject) {
        [MBProgressHUD hideHUDForView:self.view];
        
        self.shareView.labelDes.text = responseObject[@"data"][@"describe"];
    }];
    
}

- (void)refreshUI
{
    
    self.titleLabel.text = @"分享有惊喜";
    self.navTintView.backgroundColor = UIColor.clearColor;
    self.navImage.hidden = YES;
    
    self.img = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    [self.view addSubview:self.img];
    self.img.image = [UIImage imageNamed:@"share_code_bg"];
    
    self.maskView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    [self.view addSubview:self.maskView];
    self.maskView.backgroundColor = UIColor.blackColor;
    self.maskView.alpha = 0.5;
    
    [self.view bringSubviewToFront:self.contentView];
    [self.view bringSubviewToFront:self.navView];
    
    self.shareView = [CodeShareView newView];
    [self.scrollView addSubview:self.shareView];
}

@end
