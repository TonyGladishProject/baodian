//
//  ShareVC.h
//  CaiKuBaoDian
//
//  Created by mac on 15/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "ScrollViewVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface ShareVC : ScrollViewVC
@property ( nonatomic, strong ) UIImageView * img;
@property ( nonatomic, strong ) UIView * maskView;
@end

NS_ASSUME_NONNULL_END
