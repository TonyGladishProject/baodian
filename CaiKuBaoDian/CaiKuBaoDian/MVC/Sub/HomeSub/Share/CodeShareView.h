//
//  CodeShareView.h
//  CaiKuBaoDian
//
//  Created by mac on 15/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "BaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface CodeShareView : BaseView
@property (weak, nonatomic) IBOutlet UIView *codeView;
@property (weak, nonatomic) IBOutlet UIImageView *imgQR;
@property (weak, nonatomic) IBOutlet UILabel *labelCode;
@property (weak, nonatomic) IBOutlet UILabel *labelDes;
@property ( nonatomic, copy ) NSString * shareStr;
@end

NS_ASSUME_NONNULL_END
