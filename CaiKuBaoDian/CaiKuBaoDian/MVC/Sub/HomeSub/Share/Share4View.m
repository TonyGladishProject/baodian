//
//  Share4View.m
//  CaiKuBaoDian
//
//  Created by mac on 16/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "Share4View.h"
#import "JSHAREService.h"

@implementation Share4View

+(instancetype)newView
{
    return [[Share4View alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 80)];
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self createUI];
    }
    return self;
}

- (void)createUI
{
    NSArray * arrI = @[@"share_code_wei",@"share_code_weif",@"share_code_qq",@"share_code_qqkong"];
    NSArray * arrL = @[@"微信",@"朋友圈",@"QQ",@"QQ空间"];
    
    CGFloat w = 45;
    CGFloat x = (WIDTH - w * 4)/5;
    for (int i = 0; i < 4; i++) {
        UIView * view = [[UIView alloc] initWithFrame:CGRectMake(x + i *(w + x), 0, w, 64)];
        [self addSubview:view];
        [view touchEvents:self action:@selector(viewTouch:)];
        view.tag = 1000 + i;
        
        UIImageView * img = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, w, w)];
        [view addSubview:img];
        
        UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(0, w, w, 64 - w)];
        [view addSubview:label];
        
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor = UIColor.whiteColor;
        label.font = [UIFont systemFontOfSize:12];
        img.image = [UIImage imageNamed:arrI[i]];
        label.text = arrL[i];
        
        [self.dataArray addObject:view];
    }
}

- (void)viewTouch:(UITapGestureRecognizer *)tap
{
    UIView *view = (UIView*) tap.view;
    [self shareLinkWithPlatform:[Tools showPlatform:view.tag]];
}

- (void)shareLinkWithPlatform:(JSHAREPlatform)platform {
    JSHAREMessage *message = [JSHAREMessage message];
    message.mediaType = JSHARELink;
    message.url = self.shareStr;
    message.platform = platform;
    [JSHAREService share:message handler:^(JSHAREState state, NSError *error) {
        [Tools showAlertWithState:state error:error];
    }];
}

- (void)turnBackKeyboard
{
    [self.textFieldBase resignFirstResponder];
    [self.textViewBase resignFirstResponder];
    self.textFieldBase.placeholder = @"说点什么吧.";
}

- (NSMutableArray *)dataArray
{
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}
@end

