//
//  SiBuXiangVC.m
//  CaiKuBaoDian
//
//  Created by mac on 09/06/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "SiBuXiangVC.h"
#import "CommentCell.h"
#import "MoreCommentCell.h"
#import "PhotoVC.h"

@interface SiBuXiangVC ()

@end

@implementation SiBuXiangVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self config];
    [self dataHud];
    [self dataMJ];
    [self refreshUI];
}

- (void)config
{
    self.titleLabel.text = @"四不像";
    self.idQS = @"2";
    self.idImg = @"sbx";
    self.scene = @"bxj";
    self.imgHeight = WIDTH;
}

#pragma mark - loadData
- (void)dataHud
{
    [MBProgressHUD viewLoading:self.view];
    [self loadQS];
}

- (void)loadQS
{
    [HTTPTool postWithURL:APIQS parameters:@{@"mid":@"1",@"year":@"2019",@"new_id":self.idQS} success:^(id  _Nonnull responseObject) {
        
        self.menuList = [NSMutableArray array];
        NSArray * data = responseObject[@"data"][@"data"];
        for (NSDictionary * dic in data) {
            GalleryDetailModel * model = [GalleryDetailModel mj_objectWithKeyValues:dic];
            [self.menuList addObject:model.qs];
        }
        self.year = @"2019";
        self.idStr = self.maxStr = [NSString stringWithFormat:@"%lu",(unsigned long)self.menuList.count];
        [self mjHeader];
    }];
}

- (void)mjHeader
{
    [self loadImg];
}

- (void)loadImg
{
    [HTTPTool postWithURL:APIGDImg parameters:@{@"mid":@"1",@"year":@"2019",@"new_id":self.idImg,@"qs":self.idStr} success:^(id  _Nonnull responseObject) {
        [self dataComment];
        
        self.shareView.shareStr = responseObject[@"share_url"];
        [self.img sd_setImageWithURL:[NSURL URLWithString:responseObject[@"data"][@"url"]] placeholderImage:[UIImage imageNamed:@"load_001"]];
        self.strImg = responseObject[@"data"][@"url"];
        [self.img touchEvents:self action:@selector(touchImg)];
    }];
}

- (void)dataComment
{
    [HTTPTool postWithURL:APIComment parameters:@{@"scene":self.scene,@"scene_id":self.idStr} success:^(id  _Nonnull responseObject) {
        [self loadSX];
        
        self.listView.scene = self.scene;
        self.listView.scene_id = self.idStr;
        self.listView.comment_id = self.commentView.idStr = self.idStr;
        [self.listView.dataArray removeAllObjects];
        for (NSDictionary * dic in responseObject[@"data"]) {
            CommentModel * model = [CommentModel mj_objectWithKeyValues:dic];
            [self.listView.dataArray addObject:model];
            for (NSDictionary * child in dic[@"child"]) {
                CommentModel * mo= [CommentModel mj_objectWithKeyValues:child];
                mo.reply = model.user_nicename;
                [self.listView.dataArray addObject:mo];
            }
        }
        self.listView.height = self.listView.tableView.height = [self tableHeight];
        [self.listView.tableView reloadData];
    }];
}

- (void)loadSX
{
    [HTTPTool postWithURL:APIDetailShengXiao parameters:@{} success:^(id  _Nonnull responseObject) {
        [MBProgressHUD hideHUDForView:self.view];
        [self endRefreshing];
        
        NSDictionary * dic = responseObject[@"data"];
        NSDictionary * data = dic[@"data"];
        NSDictionary * sx = data[@"shengxiao"];
        NSDictionary * p = sx[@"ps"];
        
        for (int i = 0; i < self.voteView.dataArray.count; i++) {
            SXModel * m = self.voteView.dataArray[i];
            m.num = p[m.id];
            m.persent = sx[m.id];
        }
        [self.voteView dataToView:dic[@"qs"]];
        [self.voteView.collectionView reloadData];
        self.label.text = [NSString stringWithFormat:@"%@年 第%@期",self.year,self.idStr];
        self.label.backgroundColor = UIColor.whiteColor;
        if (self.idStr.intValue == 1) {
            self.otherView.btnPrevious.enabled = NO;
            self.otherView.btnNext.enabled = YES;
        } else if (self.idStr.intValue == self.maxStr.intValue) {
            self.otherView.btnPrevious.enabled = YES;
            self.otherView.btnNext.enabled = NO;
        } else {
            self.otherView.btnPrevious.enabled = YES;
            self.otherView.btnNext.enabled = YES;
        }
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:self.view];
            [self endRefreshing];
            [self.voteView.collectionView reloadData];
            [self refreshHeight];
        });
    }];
}


#pragma mark - UI
- (void)refreshUI
{
    
    self.scrollView.height = self.contentHeight - 60;
    
    self.label = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, WIDTH - 20, 40)];
    [self.scrollView addSubview:self.label];
    
    self.img = [[UIImageView alloc] initWithFrame:CGRectMake(0, self.label.bottom, WIDTH, self.imgHeight)];
    [self.scrollView addSubview:self.img];
    
    kSelfWeak
    self.voteView = [VoteView newView];
    self.voteView.y = self.img.bottom;
    [self.scrollView addSubview:self.voteView];
    self.voteView.fatherView = self.view;
    self.voteView.successBlock = ^{
        [weakSelf loadSX];
    };
    
    self.shareView = [ShareView newView];
    self.shareView.y = self.voteView.bottom;
    [self.scrollView addSubview:self.shareView];
    
    self.otherView = [OtherNumView newView];
    self.otherView.y = self.shareView.bottom;
    [self.scrollView addSubview:self.otherView];
    
    //    self.label.text = [NSString stringWithFormat:@"%@年 第%@期",self.year,self.idStr];
    
    [self.otherView.btnPrevious addTarget:self action:@selector(btnPreviousClick) forControlEvents:UIControlEventTouchUpInside];
    [self.otherView.btnNext addTarget:self action:@selector(btnNextClick) forControlEvents:UIControlEventTouchUpInside];
    
    self.listView = [[CommentList alloc] initWithFrame:CGRectMake(0, self.otherView.bottom, WIDTH, 200)];
    self.listView.navigationController = self.navigationController;
    self.listView.cellBlock = ^(CommentModel * _Nonnull model) {
        weakSelf.commentView.model = model;
        [weakSelf.commentView.textField becomeFirstResponder];
        weakSelf.commentView.textField.placeholder = [NSString stringWithFormat:@"回复 %@",model.user_nicename];
    };
    self.noDataLabel = [NoDataLabel newView];
    
    self.commentView = [CommentView newView];
    [self.contentView addSubview:self.commentView];
    self.commentView.doCommentBlock = ^{
        [weakSelf doComment];
    };
    
    self.listView.textFieldBase = self.noDataLabel.textFieldBase = self.voteView.textFieldBase = self.shareView.textFieldBase = self.commentView.textField;
}

- (void)btnPreviousClick
{
    int i = self.idStr.intValue;
    if (i == 1) {
        return;
    }
    i --;
    self.idStr = [NSString stringWithFormat:@"%d",i];
    [self refreshData];
}

- (void)btnNextClick
{
    int i = self.idStr.intValue;
    if (i >= self.maxStr.intValue) {
        return;
    }
    i ++;
    self.idStr = [NSString stringWithFormat:@"%d",i];
    [self refreshData];
}

- (void)refreshData
{
    [MBProgressHUD viewLoading:self.view];
    [self loadImg];
}

- (void)doComment
{
    NSDictionary * para;
    if ([self.commentView.textField.placeholder isEqualToString:@"说点什么吧."]) {
        para = @{@"full_name":UserPhone,@"content":self.commentView.textField.text,@"scene":self.scene,@"scene_id":self.idStr};
    } else {
        para = @{@"full_name":UserPhone,@"content":self.commentView.textField.text,@"scene":self.scene,@"scene_id":self.idStr,@"parent_id":self.commentView.model.id};
    }
    [self.commentView.textField resignFirstResponder];
    self.commentView.textField.text = @"";
    self.commentView.textField.placeholder = @"说点什么吧.";
    [HTTPTool postWithURLIndicatorStatus:APIPostComment parameters:para success:^(id  _Nonnull responseObject) {
        [self dataComment];
    }];
}

- (void)touchImg
{
    PhotoVC * i = [PhotoVC new];
    i.strImg = self.strImg;
    [self pushVC:i];
}

- (void)refreshHeight
{
    if (self.listView.dataArray.count > 0) {
        [self.scrollView addSubview:self.listView];
        self.contentHeight = self.listView.bottom;
    } else {
        [self.scrollView addSubview:self.noDataLabel];
        self.contentHeight = self.noDataLabel.bottom;
    }
}

- (CGFloat)tableHeight
{
    CGFloat totalH = 0.0;
    for (int i = 0; i < self.listView.dataArray.count; i++) {
        CGFloat cellH = 90;
        CommentModel * model = self.listView.dataArray[i];
        CGFloat h = [Tools getStrHeight:model.content width:WIDTH - 74 font:14];
        if (h < 21) {
            cellH = 90;
        } else {
            cellH = 70 + h;
        }
        totalH += cellH;
    }
    return totalH + 50;
}

@end
