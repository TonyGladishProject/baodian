//
//  SiBuXiangVC.h
//  CaiKuBaoDian
//
//  Created by mac on 09/06/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "ScrollViewVC.h"
#import "VoteView.h"
#import "ShareView.h"
#import "OtherNumView.h"
#import "NoDataLabel.h"
#import "CommentView.h"
#import "CommentList.h"

NS_ASSUME_NONNULL_BEGIN

@interface SiBuXiangVC : ScrollViewVC
@property ( nonatomic, strong ) UILabel * label;
@property ( nonatomic, strong ) UIImageView * img;
@property ( nonatomic, strong ) VoteView * voteView;
@property ( nonatomic, strong ) ShareView * shareView;
@property ( nonatomic, strong ) OtherNumView * otherView;
@property ( nonatomic, strong ) NoDataLabel * noDataLabel;
@property ( nonatomic, strong ) CommentView * commentView;
@property ( nonatomic, strong ) NSMutableArray * menuList;
@property ( nonatomic, strong ) CommentList * listView;
@property ( nonatomic, strong ) NSMutableArray * dataArray;
@property ( nonatomic, assign) BOOL loaded;
@property ( nonatomic, copy ) NSString * year;
@property ( nonatomic, copy ) NSString * maxStr;
@property ( nonatomic, copy ) NSString * strImg;
@property (nonatomic, copy)  void(^buttonBlock) (int index);

#pragma mark - 子控制器实现
@property ( nonatomic, copy ) NSString * idQS;
@property ( nonatomic, copy ) NSString * idImg;
@property ( nonatomic, copy ) NSString * scene;
@property ( nonatomic, assign ) CGFloat imgHeight;
- (void)config;
@end

NS_ASSUME_NONNULL_END
