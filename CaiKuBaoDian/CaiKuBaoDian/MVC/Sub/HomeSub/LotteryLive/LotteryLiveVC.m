//
//  LotteryLiveVC.m
//  CaiKuBaoDian
//
//  Created by mac on 25/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "LotteryLiveVC.h"
#import "LLNumsCell.h"
#import "LLCell.h"

@interface LotteryLiveVC ()
@property ( nonatomic, strong ) NSDictionary * numsData;
@property ( nonatomic, strong ) NSDictionary * imageData;
@property ( nonatomic, strong ) NSDictionary * wufenData;
@property ( nonatomic, strong ) LLNumsCell * cell;
@end

@implementation LotteryLiveVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(noti:) name:@"refreshLottery" object:nil];
    [self dataHud];
    [self refreshUI];
}

- (void)noti:(NSNotification *)noti
{
    //    [self dataToLottery:noti.userInfo];
    [self.cell dataToView:noti.userInfo];
}

- (void)mjHeader
{
    [self loadNumbers];
}

- (void)loadNumbers
{
    [HTTPTool getWithURL:APILotteryNums success:^(id  _Nonnull responseObject) {
        [self loadWufen];
        
        [self.dataArray removeAllObjects];
        self.numsData = responseObject[@"data"];
        self.dataArray = [NSMutableArray arrayWithArray:responseObject[@"data"][@"recommend"]];
        NSDictionary * data = responseObject[@"data"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"lotteryInfo" object:nil userInfo:@{@"timeprompt":data[@"timeprompt"],@"num":data[@"haoma"][6],@"qs":data[@"期数"]}];
    }];
}

- (void)loadWufen
{
    [HTTPTool getWithURL:APIWufen success:^(id  _Nonnull responseObject) {
        [self loadImage];
        
        NSArray * arr = responseObject[@"data"];
        self.wufenData = arr[0];
    }];
}

- (void)loadImage
{
    [HTTPTool postWithURL:APILLImage parameters:@{@"type":@"zxkj"} success:^(id  _Nonnull responseObject) {
        [self endRefreshing];
        
        NSArray * arr = responseObject[@"data"];
        self.imageData = arr[0];
        [self.tableView reloadData];
    }];
}

- (void)refreshUI
{
    self.titleLabel.text = @"开奖直播";
    [self registerNib:[LLNumsCell class]];
    [self registerNib:[LLCell class]];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;
    }
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section ==0) {
        LLNumsCell * cell = [tableView dequeueReusableCellWithIdentifier:@"LLNumsCell"];
        cell.navigationController = self.navigationController;
        [cell dataToView:self.numsData];
        [cell dataToImage:self.imageData];
        [cell dataToWufen:self.wufenData];
        self.cell = cell;
        return cell;
    }
    LLCell * cell = [tableView dequeueReusableCellWithIdentifier:@"LLCell"];
    [cell dataToView:self.dataArray[indexPath.row]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return WIDTH + 26 + 30;
    }
    NSDictionary * dic = self.dataArray[indexPath.row];
    NSString * str = dic[@"slide_content"];
    return [Tools getStrHeight:[Tools attributedStringWithHTMLString:str] width:WIDTH - 20 font:17.f] + 40;
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
