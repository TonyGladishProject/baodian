//
//  NoticeView.m
//  CaiKuBaoDian
//
//  Created by mac on 25/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "NoticeView.h"

@implementation NoticeView

+(instancetype)newView
{
    return [[NoticeView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 26)];
}

/** 初始化方法，用于从xib文件中载入的类实例 */
- (instancetype)initWithCoder:(NSCoder *)decoder
{
    self = [super initWithCoder:decoder];
    if (self)
    {
        [self createUI];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self createUI];
    }
    return self;
}

- (void)createUI
{
    self.image = [[UIImageView alloc] initWithFrame:CGRectMake(10, 3, 16, 20)];
    [self addSubview:self.image];
    self.image.image = [UIImage imageNamed:@"icon_lingsheng"];
    
    self.scrollLabel = [TXScrollLabelView scrollWithTitle:@"" type:0 velocity:1 options:UIViewAnimationOptionTransitionFlipFromTop];
    self.scrollLabel.frame = CGRectMake(31,3,WIDTH - 31-10,20);
    [self addSubview:self.scrollLabel];
    [self.scrollLabel beginScrolling];
    self.scrollLabel.backgroundColor = UIColor.clearColor;
    self.scrollLabel.scrollTitleColor = UIColorFromRGB(0xc75038);
}

@end
