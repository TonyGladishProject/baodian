//
//  NoticeView.h
//  CaiKuBaoDian
//
//  Created by mac on 25/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "BaseView.h"
#import "TXScrollLabelView.h"

NS_ASSUME_NONNULL_BEGIN

@interface NoticeView : BaseView
@property ( nonatomic, strong ) UIImageView * image;
@property ( nonatomic, strong ) TXScrollLabelView * scrollLabel;
@end

NS_ASSUME_NONNULL_END
