//
//  LotteryLiveVC.h
//  CaiKuBaoDian
//
//  Created by mac on 25/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "TableViewVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface LotteryLiveVC : TableViewVC
@property ( nonatomic, strong ) NSDictionary * data;
@end

NS_ASSUME_NONNULL_END
