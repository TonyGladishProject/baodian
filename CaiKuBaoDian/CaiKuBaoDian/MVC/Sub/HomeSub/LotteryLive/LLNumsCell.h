//
//  LLNumsCell.h
//  CaiKuBaoDian
//
//  Created by mac on 25/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "PureCell.h"
#import "NoticeView.h"
#import "LottNumView.h"

NS_ASSUME_NONNULL_BEGIN

@interface LLNumsCell : PureCell
@property (weak, nonatomic) IBOutlet NoticeView *noticeView;
@property (weak, nonatomic) IBOutlet LottNumView *numView;
@property (weak, nonatomic) IBOutlet UILabel *labelTile;
@property (weak, nonatomic) IBOutlet UIImageView *urlImage;

@property (weak, nonatomic) IBOutlet LottNumView *wufenView;
@property ( nonatomic, copy ) NSString * urlStr;
- (void)dataToView:(NSDictionary *)data;
- (void)dataToImage:(NSDictionary *)data;
- (void)dataToWufen:(NSDictionary *)data;
@end

NS_ASSUME_NONNULL_END
