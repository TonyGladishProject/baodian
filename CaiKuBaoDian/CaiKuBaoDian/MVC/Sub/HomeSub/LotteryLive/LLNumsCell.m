//
//  LLNumsCell.m
//  CaiKuBaoDian
//
//  Created by mac on 25/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "LLNumsCell.h"

@implementation LLNumsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self.urlImage touchEvents:self action:@selector(touchImage)];
}

- (void)dataToView:(NSDictionary *)data
{
    if (data[@"notice"]) {
        self.noticeView.scrollLabel.scrollTitle = data[@"notice"];
    }
    [self.numView dataToView:data];
    self.labelTile.text = data[@"nextopentime"];
    self.numView.navigationController = self.navigationController;
}

- (void)dataToImage:(NSDictionary *)data
{
    [self.urlImage sd_setImageWithURL:[NSURL URLWithString:data[@"background_img"]] placeholderImage:nil];
    self.urlStr =data[@"out_url"];
}

- (void)dataToWufen:(NSDictionary *)data
{
    [self.wufenView dataToViewWufen:data];
}

- (void)touchImage
{
    WebVC * w = [WebVC new];
    w.titleName = @"679彩票";
    w.urlStr = self.urlStr;
    [self.navigationController pushViewController:w animated:YES];
}

@end
