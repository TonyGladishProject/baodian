//
//  LLCell.m
//  CaiKuBaoDian
//
//  Created by mac on 25/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "LLCell.h"

@implementation LLCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    
}

- (void)dataToView:(NSDictionary *)data
{
    self.labelTitle.text = [NSString stringWithFormat:@"第%@期推荐",data[@"slide_name"]];
    [self.webLabel dataToViewLLCell:data[@"slide_content"]];
}

@end
