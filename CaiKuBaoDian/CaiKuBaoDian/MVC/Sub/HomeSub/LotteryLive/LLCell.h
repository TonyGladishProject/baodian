//
//  LLCell.h
//  CaiKuBaoDian
//
//  Created by mac on 25/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "PureCell.h"
#import "WebLabel.h"

NS_ASSUME_NONNULL_BEGIN

@interface LLCell : PureCell
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet WebLabel *webLabel;
- (void)dataToView:(NSDictionary *)data;
@end

NS_ASSUME_NONNULL_END
