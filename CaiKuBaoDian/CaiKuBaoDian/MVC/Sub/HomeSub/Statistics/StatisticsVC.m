//
//  StatisticsVC.m
//  CaiKuBaoDian
//
//  Created by mac on 01/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "StatisticsVC.h"
#import "StatisticSubVC.h"
#import "LabelCollectionCell.h"

@interface StatisticsVC ()
@property ( nonatomic, strong ) NSMutableArray * dataArray;
@end

@implementation StatisticsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self refreshUI];
}

- (void)refreshUI
{
    self.titleLabel.text = @"六合统计";
    self.menuList = [NSMutableArray arrayWithObjects:
                     @"六合统计",@"属性参照",@"特码历史",@"正码历史",
                     @"尾数大小",@"生肖特码",@"生肖正码",@"波色特码",
                     @"波色正码",@"特码两面",@"特码尾数",@"正码尾数",
                     @"正码总分",@"号码波段",@"家禽野兽",@"连码走势",
                     @"连肖走势", nil];
    [self.magicController.magicView reloadData];
    
    
    [self.rightButton setTitle:@"期数:50" forState:UIControlStateNormal];
//    [self createMoreButton];
    [self.moreBtn addTarget:self action:@selector(moreBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.moreBtn setImage:nil forState:UIControlStateNormal];
    [self.moreBtn setTitle:@"更多" forState:UIControlStateNormal];
    [self.moreBtn setTitleColor:UIColor.blackColor forState:UIControlStateNormal];
}

- (void)moreBtnClick:(UIButton *)button
{
    
}

- (UIViewController *)magicView:(VTMagicView *)magicView viewControllerAtPage:(NSUInteger)pageIndex {
    NSString *otherId = [NSString stringWithFormat:@"Statistics%lu",pageIndex];
    StatisticSubVC *i = [magicView dequeueReusablePageWithIdentifier:otherId];
    if (!i) {
        i = [[StatisticSubVC alloc] init];
    }
    i.urlStr = self.dataArray[pageIndex];
    return i;
}

- (void)confirmClick
{
    
}

- (CGFloat)magicNavHeight
{
    return 44;
}

- (NSMutableArray *)dataArray
{
    if (!_dataArray) {
        _dataArray = [NSMutableArray arrayWithObjects:
                      APILHTJ,APILHSXCZ,APILHTMLS,APILHZMLS,APILHWSDX,
                      APILHSXTM,APILHSXZM,APILHBSTM,APILHBSZM,APILHTMLM,
                      APILHTMWS,APILHZMWS,APILHZMZF,APILHHMBD,APILHJQYS,
                      APILHLMZS,APILHLXZS,nil];
    }
    return _dataArray;
}

@end
