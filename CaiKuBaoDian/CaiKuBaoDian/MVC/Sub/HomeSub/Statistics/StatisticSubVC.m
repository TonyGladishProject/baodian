//
//  StatisticSubVC.m
//  CaiKuBaoDian
//
//  Created by mac on 09/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "StatisticSubVC.h"
#import "UIWebView+DKProgress.h"
#import "DKProgressLayer.h"

@interface StatisticSubVC ()

@end

@implementation StatisticSubVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self refreshUI];
}

- (void)refreshUI
{
    self.contentViewHeight = HEIGHT - self.navBottomHeight - 44;
    self.navView.hidden = YES;
    self.contentView.frame = CGRectMake(0, 0, WIDTH, self.contentViewHeight);
    
    self.webView.dk_progressLayer = [[DKProgressLayer alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 4)];
    self.webView.dk_progressLayer.progressColor = UIColorFromRGB(0xff2831);
    self.webView.dk_progressLayer.progressStyle = DKProgressStyle_Gradual;
    [self.contentView.layer addSublayer:self.webView.dk_progressLayer];
}

@end
