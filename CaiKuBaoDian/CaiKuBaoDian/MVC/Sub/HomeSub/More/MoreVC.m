//
//  MoreVC.m
//  CaiKuBaoDian
//
//  Created by mac on 12/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "MoreVC.h"
#import "MoreModel.h"

@interface MoreVC ()

@end

@implementation MoreVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.titleLabel.text = @"更多";
    [self loadData];
}

- (void)loadData
{
    [MBProgressHUD dataLoading];
    [HTTPTool getWithURL:APIMore success:^(id  _Nonnull responseObject) {
        [MBProgressHUD hideHUD];
        NSLog(@"responseObject=%@",responseObject);
        for (NSDictionary * dic in responseObject[@"data"]) {
            MoreModel * model = [MoreModel mj_objectWithKeyValues:dic];
            [self.dataArray addObject:model];
        }
        [self.collectionView reloadData];
    }];
}

- (void)collectionView:(CollectionViewCell *)cell dataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    cell.moreM = self.dataArray[indexPath.row];
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    WebVC * web = [WebVC new];
    MoreModel * more = self.dataArray[indexPath.item];
    web.urlStr = more.url;
    web.titleName= more.name;
    web.bottomColor = UIColorFromRGB(0xf0f0f0);
    if (indexPath.item == 5) {
        web.bottomColor = MainColorDeep;
    }
    [self.navigationController pushViewController:web animated:YES];
}

@end
