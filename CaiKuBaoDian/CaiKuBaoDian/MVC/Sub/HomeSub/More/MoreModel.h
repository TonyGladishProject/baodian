//
//  MoreModel.h
//  CaiKuBaoDian
//
//  Created by mac on 12/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MoreModel : NSObject
@property ( nonatomic, copy ) NSString * id;
@property ( nonatomic, copy ) NSString * image;
@property ( nonatomic, copy ) NSString * name;
@property ( nonatomic, copy ) NSString * url;
@end

NS_ASSUME_NONNULL_END
