//
//  HistoryVC.m
//  CaiKuBaoDian
//
//  Created by mac on 12/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "HistoryVC.h"

@interface HistoryVC ()

@end

@implementation HistoryVC

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)configure
{
    self.titleLabel.text = @"历史开奖";
    self.urlStr = APIHistory;
}

@end
