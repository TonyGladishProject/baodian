//
//  FeedbackView.h
//  CaiKuBaoDian
//
//  Created by mac on 25/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "BaseView.h"
#import "TextView.h"

NS_ASSUME_NONNULL_BEGIN

@interface FeedbackView : BaseView
@property (weak, nonatomic) IBOutlet UIView *groundView;
@property (weak, nonatomic) IBOutlet UIButton *button;
@property ( nonatomic, strong ) TextView * textView;
@end

NS_ASSUME_NONNULL_END
