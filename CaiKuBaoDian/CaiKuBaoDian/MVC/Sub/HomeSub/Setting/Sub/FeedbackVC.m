//
//  FeedbackVC.m
//  CaiKuBaoDian
//
//  Created by mac on 25/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "FeedbackVC.h"
#import "FeedbackView.h"

@interface FeedbackVC ()
@property ( nonatomic, strong ) FeedbackView * feedbackView;
@end

@implementation FeedbackVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.titleLabel.text = @"反馈意见";
    self.feedbackView = [FeedbackView newView];
    [self.scrollView addSubview:self.feedbackView];
    self.contentHeight = self.feedbackView.height;
    [self.feedbackView.button addTarget:self action:@selector(postClick) forControlEvents:UIControlEventTouchUpInside];
}

- (void)postClick
{
    if (self.feedbackView.textView.text.length < 1 || [Tools colorSame:self.feedbackView.textView.textColor color:cellWordColor]) {
        [MBProgressHUD showMessage:@"请填写内容"];
        return;
    }
    
    [self doPost];
}

- (void)doPost
{
    [HTTPTool postWithURLIndicatorMSG:APIFeedback parameters:@{@"username":UserPhone,@"fankui":self.feedbackView.textView.text} success:^(id  _Nonnull responseObject) {
        [self leftButtonClick];
    }];
}
@end
