//
//  DisclaimerVC.m
//  CaiKuBaoDian
//
//  Created by mac on 25/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "DisclaimerVC.h"

@interface DisclaimerVC ()
@property ( nonatomic, strong ) UILabel * label;
@end

@implementation DisclaimerVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.titleLabel.text = @"免责声明";
    [self createLabel];
    [self postHud:APIDisclaimer parameters:@{@"id":@"15"}];
}

- (void)dataToModel:(id)responseObject
{
    NSLog(@"responseObject=%@",responseObject);
    NSDictionary * dic = responseObject[@"data"];
    self.label.text = [@"\n" stringByAppendingString:[Tools attributedStringWithHTMLString:dic[@"post_content"]]];
//    [Tools setLabelHeight:self.label str:self.label.text width:WIDTH - 10  font:17.f];
    self.contentHeight=self.label.height;
}

- (void)createLabel
{
    self.label = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, WIDTH - 10, self.contentViewHeight)];
    [self.scrollView addSubview:self.label];
    self.label.numberOfLines = 0;
    self.label.textColor = cellWordColor;
}

@end
