//
//  FeedbackView.m
//  CaiKuBaoDian
//
//  Created by mac on 25/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "FeedbackView.h"

@implementation FeedbackView

+(instancetype)newView
{
    FeedbackView * view = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil].lastObject;
    view.frame = CGRectMake(0, 0, WIDTH,view.contentViewHeight);
    return view;
}

- (void)drawRect:(CGRect)rect
{
    self.textView = [[TextView alloc] initWithFrame:self.groundView.bounds];
    [self.groundView addSubview:self.textView];
    self.textView.placeHolder = @"请输入您的反馈意见，我们将努力改进";
    
    [self.button layerMask:5];
}

@end
