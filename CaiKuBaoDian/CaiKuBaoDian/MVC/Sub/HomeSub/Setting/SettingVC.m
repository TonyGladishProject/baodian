//
//  SettingVC.m
//  CaiKuBaoDian
//
//  Created by mac on 13/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "SettingVC.h"
#import "TableViewCell.h"

#import "DisclaimerVC.h"
#import "FeedbackVC.h"
#import "AppScoreView.h"

@interface SettingVC ()

@end

@implementation SettingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.titleLabel.text = @"设置";
    self.dataArray = [NSMutableArray arrayWithObjects:@"消息推送", @"清空缓存",@"免责申明",@"用户反馈",@"好评一下",nil];
    [self registerClass:[TableViewCell class]];
    self.bottomView.backgroundColor = viewGaryColor;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"TableViewCell"];
    cell.label.text = self.dataArray[indexPath.row];
    if (indexPath.row == 0) {
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell showSwitchView:YES];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row ) {
        case 1:
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [MBProgressHUD showMessage:@"清理完成"];
            });
            break;
        case 2:
            [self pushVC:[DisclaimerVC new]];
            break;
        case 3:
            [self pushVC:[FeedbackVC new]];
            break;
        case 4:
        {
            [[AppScoreView newView] popAddSuperView:self.view center:CGPointMake(WIDTH/2, HEIGHT/3) showType:KLCPopupShowTypeBounceInFromTop dismissType:KLCPopupDismissTypeBounceOutToTop];
        }
            
            
            break;
            
        default:
            break;
    }
}

@end
