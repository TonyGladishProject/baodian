//
//  AppScoreView.h
//  CaiKuBaoDian
//
//  Created by mac on 26/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "BaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface AppScoreView : BaseView
@property (weak, nonatomic) IBOutlet UIButton *button;
@property ( nonatomic, strong ) NSMutableArray * arrBtns;
@end

NS_ASSUME_NONNULL_END
