//
//  AppScoreView.m
//  CaiKuBaoDian
//
//  Created by mac on 26/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "AppScoreView.h"

@implementation AppScoreView

+(instancetype)newView
{
    AppScoreView * view = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil].lastObject;
    view.frame = CGRectMake(0, 0, 290,155);
    return view;
}

- (void)drawRect:(CGRect)rect
{
    self.arrBtns =  [NSMutableArray array];
    for (int i = 0; i < 5; i++) {
        UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(20 + i * 50, 0, 50, 50);
        button.centerY = self.height/2;
        [self.arrBtns addObject:button];
        [self addSubview:button];
        [button setImage:[UIImage imageNamed:@"star"] forState:UIControlStateNormal];
        [button setImage:[UIImage imageNamed:@"starS"] forState:UIControlStateSelected];
        [button addTarget:self action:@selector(starClick:) forControlEvents:UIControlEventTouchUpInside | UIControlEventAllTouchEvents];
        button.tag = 1000 + i;
    }
}

- (void)starClick:(UIButton *)button
{
    button.highlighted = NO;
    for (int i = 0; i < self.arrBtns.count; i++) {
        UIButton * btn = self.arrBtns[i];
        if (i <= button.tag - 1000) {
            btn.selected = YES;
        } else {
            btn.selected = NO;
        }
    }
}
- (IBAction)postClick:(UIButton *)sender {
    UIButton * btn = self.arrBtns[0];
    if (!btn.selected) {
        [MBProgressHUD showMessage:@"请评分"];
        return;
    }
    [MBProgressHUD dataLoading];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.popup dismiss:YES];
        [MBProgressHUD hideHUD];
        [MBProgressHUD showMessage:@"提交成功"];
    });
}

@end
