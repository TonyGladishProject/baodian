//
//  ExchangeRecordsVC.m
//  CaiKuBaoDian
//
//  Created by mac on 17/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "ExchangeRecordsVC.h"

@interface ExchangeRecordsVC ()

@end

@implementation ExchangeRecordsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadHud];
    [self refreshUI];
}

- (void)loadHud
{
    [MBProgressHUD viewLoading:self.view];
    [self loadData];
}

- (void)loadData
{
    [HTTPTool postWithURLLogin:APIExchangeRecords parameters:@{@"username":UserPhone} success:^(id  _Nonnull responseObject) {
        [MBProgressHUD hideHUDForView:self.view];
        [self showNoDataView:responseObject];
    }];
}

- (void)refreshUI
{
    self.titleLabel.text = @"转换记录";
}

@end
