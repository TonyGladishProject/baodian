//
//  LikeVC.m
//  CaiKuBaoDian
//
//  Created by mac on 16/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "LikeVC.h"

@interface LikeVC ()

@end

@implementation LikeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadHud];
    [self refreshUI];
}

- (void)loadHud
{
    [MBProgressHUD viewLoading:self.view];
    [self loadData];
}

- (void)loadData
{
    [HTTPTool postWithURLLogin:APILike parameters:@{@"username":UserPhone} success:^(id  _Nonnull responseObject) {
        [MBProgressHUD hideHUDForView:self.view];
        [self showNoDataView:responseObject];
    }];
}

- (void)refreshUI
{
    self.titleLabel.text = @"我的心水";
}

@end
