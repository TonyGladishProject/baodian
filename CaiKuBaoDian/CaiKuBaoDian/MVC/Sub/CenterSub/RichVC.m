//
//  RichVC.m
//  CaiKuBaoDian
//
//  Created by mac on 17/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "RichVC.h"
#import "ShareCodeVC.h"
#import "MyRichVC.h"

@interface RichVC ()

@end

@implementation RichVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self refreshUI];
}

- (void)refreshUI
{
    self.titleLabel.text = @"致富";
    
    self.magicController.magicView.separatorHidden = NO;
    self.magicController.magicView.layoutStyle = VTLayoutStyleDivide;
    self.menuList = [NSMutableArray arrayWithObjects:@"分享邀请码",@"我的财神", nil];
    [self.magicController.magicView reloadData];
    
    self.rightButton.hidden = YES;
}

- (UIViewController *)magicView:(VTMagicView *)magicView viewControllerAtPage:(NSUInteger)pageIndex {
    if (pageIndex ==0) {
        ShareCodeVC *i = [magicView dequeueReusablePageWithIdentifier:@"ShareCodeVC"];
        if (!i) {
            i = [[ShareCodeVC alloc] init];
        }
        return i;
    }
    MyRichVC *i = [magicView dequeueReusablePageWithIdentifier:@"MyRichVC"];
    if (!i) {
        i = [[MyRichVC alloc] init];
    }
    return i;
}

@end
