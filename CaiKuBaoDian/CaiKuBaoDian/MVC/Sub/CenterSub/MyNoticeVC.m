//
//  MyNoticeVC.m
//  CaiKuBaoDian
//
//  Created by mac on 08/06/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "MyNoticeVC.h"
#import "MyNoticeCell.h"

@interface MyNoticeVC ()

@end

@implementation MyNoticeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.titleLabel.text = @"我的消息";
    [self registerNib:[MyNoticeCell class]];
    [self dataHud];
}

- (void)mjHeader
{
    [self loadNotice];
}

- (void)loadNotice
{
    [HTTPTool postWithURLLogin:APIMyNotice parameters:@{@"username":UserPhone} success:^(id  _Nonnull responseObject) {
        [self endRefreshing];
        
        [self.dataArray removeAllObjects];
        NSLog(@"responseObject=%@",responseObject);
        for (NSDictionary * dic in responseObject[@"data"]) {
            MyNoticeModel * model = [MyNoticeModel mj_objectWithKeyValues:dic];
            [self.dataArray addObject:model];
        }
        [self.tableView reloadData];
    }];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MyNoticeCell * cell = [tableView dequeueReusableCellWithIdentifier:@"MyNoticeCell"];
    cell.model = self.dataArray[indexPath.row];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MyNoticeModel * model = self.dataArray[indexPath.row];
    CGFloat h = [Tools getStrHeight:model.content width:WIDTH - 75 font:17];
    return 100 + h;
}

- (void)leftButtonClick
{
    [MBProgressHUD hideHUD];
    [MBProgressHUD hideHUDForView:self.view];
    if (self.backBlock) {
        self.backBlock();
    }
    [self.navigationController popViewControllerAnimated:YES];
}
@end
