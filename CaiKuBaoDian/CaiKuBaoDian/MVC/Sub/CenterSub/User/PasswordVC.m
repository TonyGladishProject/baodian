//
//  PasswordVC.m
//  CaiKuBaoDian
//
//  Created by mac on 16/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "PasswordVC.h"
#import "PasswordView.h"

@interface PasswordVC ()
@property ( nonatomic, strong ) PasswordView * pwdView;
@end

@implementation PasswordVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self refreshUI];
}

- (void)refreshUI
{
    self.titleLabel.text = @"修改密码";
    self.scrollView.backgroundColor = viewGaryColor;
    self.pwdView = [PasswordView newView];
    [self.scrollView addSubview:self.pwdView];
    [self.pwdView.button addTarget:self action:@selector(buttonClick) forControlEvents:UIControlEventTouchUpInside];
}

- (void)buttonClick
{
    if (self.pwdView.textOld.text.length < 1) {
        [MBProgressHUD showMessage:self.pwdView.textOld.placeholder];
        return;
    } else if (self.pwdView.textNew.text.length < 1) {
        [MBProgressHUD showMessage:self.pwdView.textNew.placeholder];
        return;
    } else if (self.pwdView.textAgain.text.length < 1) {
        [MBProgressHUD showMessage:self.pwdView.textAgain.placeholder];
        return;
    } else if (![self.pwdView.textAgain.text isEqualToString:self.pwdView.textNew.text]) {
        [MBProgressHUD showMessage:@"两次密码不一致"];
        return;
    }
    
    [self doPost];
}

- (void)doPost
{
    [HTTPTool postWithURLIndicatorMSG:APIPassword parameters:@{@"username":UserPhone,@"oldpassword":self.pwdView.textOld.text,@"newpassword":self.pwdView.textNew.text} success:^(id  _Nonnull responseObject) {
        [self leftButtonClick];
    }];
}

@end

