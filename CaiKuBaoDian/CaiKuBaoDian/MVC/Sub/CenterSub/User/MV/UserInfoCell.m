//
//  UserInfoCell.m
//  CaiKuBaoDian
//
//  Created by mac on 17/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "UserInfoCell.h"

@implementation UserInfoCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.labelRight = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH - 40 - 120, 0, 120, 50)];
        [self.contentView addSubview:self.labelRight];
        self.label.height = 50;
        self.line.y = 49;
        self.labelRight.backgroundColor = self.label.backgroundColor;
        self.labelRight.textAlignment = NSTextAlignmentRight;
    }
    return self;
}
@end
