//
//  PasswordView.h
//  CaiKuBaoDian
//
//  Created by mac on 16/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "BaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface PasswordView : BaseView
@property (weak, nonatomic) IBOutlet UITextField *textOld;
@property (weak, nonatomic) IBOutlet UITextField *textNew;
@property (weak, nonatomic) IBOutlet UITextField *textAgain;
@property (weak, nonatomic) IBOutlet UIButton *button;

@end

NS_ASSUME_NONNULL_END
