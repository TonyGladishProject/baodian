//
//  LogoutCell.h
//  CaiKuBaoDian
//
//  Created by mac on 17/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LogoutCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *logoutBtn;
@property ( nonatomic, strong ) UINavigationController * navigationController;
@end

NS_ASSUME_NONNULL_END
