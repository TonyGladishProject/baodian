//
//  LogoutCell.m
//  CaiKuBaoDian
//
//  Created by mac on 17/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "LogoutCell.h"
#import "UserInfo.h"

@implementation LogoutCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    [self.logoutBtn layerBorderWidth:1 color:MainColor];
    [self.logoutBtn layerMask:8];
}

- (IBAction)logoutClick:(UIButton *)sender {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [Tools pushToHomeVC];
    });
    [[UserInfo shareInstance] cleanInfo];
    [self.navigationController popViewControllerAnimated:YES];
}

@end
