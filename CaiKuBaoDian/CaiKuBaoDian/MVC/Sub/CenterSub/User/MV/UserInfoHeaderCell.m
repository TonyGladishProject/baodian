//
//  UserInfoHeaderCell.m
//  CaiKuBaoDian
//
//  Created by mac on 17/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "UserInfoHeaderCell.h"

@implementation UserInfoHeaderCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.label.height = 65;
        self.line.y = 65;
        self.line.height = 10;
        self.line.backgroundColor = viewGaryColor;
        self.arrow.centerY = 65/2;
        
        self.image = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH - 40 - 50, 0, 50, 50)];
        self.image.centerY = 65/2;
        [self.contentView addSubview:self.image];
        [self.image layerMask];
        [self.image sd_setImageWithURL:[NSURL URLWithString:[UserDefaults valueForKey:@"avatar"]] placeholderImage:[UIImage imageNamed:@"placeHolder0"]];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(noti) name:@"refreshAvatar" object:nil];
    }
    return self;
}

- (void)noti
{
    [self.image sd_setImageWithURL:[NSURL URLWithString:[UserDefaults valueForKey:@"avatar"]] placeholderImage:[UIImage imageNamed:@"placeHolder0"]];
}
@end
