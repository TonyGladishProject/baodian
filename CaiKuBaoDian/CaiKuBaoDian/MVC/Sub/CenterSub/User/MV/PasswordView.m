//
//  PasswordView.m
//  CaiKuBaoDian
//
//  Created by mac on 16/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "PasswordView.h"

@implementation PasswordView

+(instancetype)newView
{
    PasswordView * view = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil].lastObject;
    view.frame = CGRectMake(0, 0, WIDTH,view.contentViewHeight);
    return view;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self.button layerMask5];
}

@end
