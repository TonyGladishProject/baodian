//
//  NicknameVC.m
//  CaiKuBaoDian
//
//  Created by mac on 16/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "NicknameVC.h"
#import "NicknameView.h"

@interface NicknameVC ()
@property ( nonatomic, strong ) NicknameView * nickView;
@end

@implementation NicknameVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self refreshUI];
}

- (void)refreshUI
{
    self.titleLabel.text = @"修改昵称";
    self.scrollView.backgroundColor = viewGaryColor;
    self.nickView = [NicknameView newView];
    [self.scrollView addSubview:self.nickView];
    [self.nickView.button addTarget:self action:@selector(buttonClick) forControlEvents:UIControlEventTouchUpInside];
}

- (void)buttonClick
{
    if (self.nickView.textField.text.length < 1) {
        [MBProgressHUD showMessage:self.nickView.textField.placeholder];
        return;
    }
    
    [self doPost];
}

- (void)doPost
{
    [HTTPTool postWithURLIndicatorMSG:APINickname parameters:@{@"username":UserPhone,@"nickname":self.nickView.textField.text} success:^(id  _Nonnull responseObject) {
        if (self.successBlock) {
            self.successBlock(self.nickView.textField.text);
        }
        [self leftButtonClick];
    }];
}

@end
