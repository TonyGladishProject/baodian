//
//  AvatarVC.m
//  CaiKuBaoDian
//
//  Created by mac on 16/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "AvatarVC.h"

@interface AvatarVC ()

@end

@implementation AvatarVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.titleLabel.text = @"选择头像";
    [self loadHud];
    [self loadMJ];
}

- (void)loadMJ
{
    MJRefreshNormalHeader * header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self loadData];
    }];
    self.collectionView.mj_header = header;
}

- (void)loadHud
{
    [MBProgressHUD viewLoading:self.view];
    [self loadData];
}

- (void)loadData
{
    [HTTPTool getWithURL:APIAvatar success:^(id  _Nonnull responseObject) {
        NSMutableArray * m = [NSMutableArray arrayWithCapacity:10];
        NSMutableArray * f = [NSMutableArray arrayWithCapacity:10];
        for (NSString * str in responseObject[@"data"]) {
            NSArray * arr = [str componentsSeparatedByString:@"/"];
            NSString * s = arr.lastObject;
            if ([[s substringToIndex:1] isEqualToString:@"f"]) {
                [f addObject:str];
            } else if ([[s substringToIndex:1] isEqualToString:@"m"]) {
                [m addObject:str];
            }
        }
        self.dataArray = [NSMutableArray arrayWithObjects:f,m, nil];
        [self.collectionView reloadData];
        [self endRefreshing];
    }];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return self.dataArray.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSArray * arr = self.dataArray[section];
    return arr.count;
}

- (void)collectionView:(CollectionViewCell *)cell dataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    cell.image.frame = cell.bounds;
    cell.backgroundColor = UIColor.clearColor;
    NSArray * arr = self.dataArray[indexPath.section];
    [cell.image sd_setImageWithURL:[NSURL URLWithString:arr[indexPath.row]] placeholderImage:nil];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return  CGSizeMake((WIDTH -50)/4,(WIDTH -50)/4);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(10, 10,10, 10);//（上、左、下、右）
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 10;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 10;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray * arr = self.dataArray[indexPath.section];
    NSString * str = arr[indexPath.row];
    NSArray * array = [str componentsSeparatedByString:@"/"];
    [self doPost:array.lastObject];
}

- (void)doPost:(NSString *)imageName
{
    [MBProgressHUD viewLoading:self.view];
    [HTTPTool postWithURL:APIHeaderUpdate parameters:@{@"username":UserPhone,@"avatar":imageName} success:^(id  _Nonnull responseObject) {
        NSLog(@"responseObject=%@",responseObject);
        [MBProgressHUD hideHUDForView:self.view];

        NSNumber * num = responseObject[@"flag"];
        NSString * str = [[NSNumberFormatter new] stringFromNumber:num];
        [MBProgressHUD showMessage:responseObject[@"errmsg"]];
        if (str.intValue == 1) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [UserDefaults setValue:responseObject[@"data"] forKey:@"avatar"];
                [UserDefaults synchronize];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshAvatar" object:nil];
                [self leftButtonClick];
            });
        }
    }];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
