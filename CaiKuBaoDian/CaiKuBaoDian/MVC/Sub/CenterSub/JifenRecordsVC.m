//
//  JifenRecordsVC.m
//  CaiKuBaoDian
//
//  Created by mac on 17/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "JifenRecordsVC.h"
#import "JifenRecordCell.h"

@interface JifenRecordsVC ()

@end

@implementation JifenRecordsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadHud];
    [self refreshUI];
}

- (void)dataMJ
{
    MJRefreshNormalHeader * header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self loadData];
    }];
    self.tableView.mj_header = header;
}

- (void)loadHud
{
    [MBProgressHUD viewLoading:self.view];
    [self loadData];
}

- (void)loadData
{
    [HTTPTool postWithURLLogin:APIJifenRecords parameters:@{@"username":UserPhone} success:^(id  _Nonnull responseObject) {
        [self endRefreshing];
        [self showNoDataView:responseObject];
        [self.dataArray removeAllObjects];
        for (NSDictionary * dic in responseObject[@"data"]) {
            JifenRecordModel * model = [JifenRecordModel mj_objectWithKeyValues:dic];
            [self.dataArray addObject:model];
        }
        [self.tableView reloadData];
    }];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 75;
}

- (void)refreshUI
{
    self.titleLabel.text = @"金币记录";
    [self registerNib:[JifenRecordCell class]];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    JifenRecordCell * cell = [tableView dequeueReusableCellWithIdentifier:@"JifenRecordCell"];
    cell.model = self.dataArray[indexPath.row];
    return cell;
}

@end
