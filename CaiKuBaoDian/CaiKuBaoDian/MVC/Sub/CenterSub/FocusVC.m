//
//  FocusVC.m
//  CaiKuBaoDian
//
//  Created by mac on 31/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "FocusVC.h"

@interface FocusVC ()

@end

@implementation FocusVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadHud];
    [self refreshUI];
}

- (void)loadHud
{
    [MBProgressHUD viewLoading:self.view];
    [self loadData];
}

- (void)loadData
{
    [HTTPTool postWithURLLogin:APIFocus parameters:@{@"username":UserPhone} success:^(id  _Nonnull responseObject) {
        [MBProgressHUD hideHUDForView:self.view];
        [self showNoDataView:responseObject];
    }];
}

- (void)refreshUI
{
    self.titleLabel.text = @"我的关注";
}

@end

