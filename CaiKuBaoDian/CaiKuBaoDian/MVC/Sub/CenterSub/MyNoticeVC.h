//
//  MyNoticeVC.h
//  CaiKuBaoDian
//
//  Created by mac on 08/06/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "TableViewVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface MyNoticeVC : TableViewVC
@property (nonatomic, copy)  void(^backBlock) (void);
@end

NS_ASSUME_NONNULL_END
