//
//  UserInfoVC.m
//  CaiKuBaoDian
//
//  Created by mac on 17/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "UserInfoVC.h"
#import "UserInfoHeaderCell.h"
#import "UserInfoCell.h"
#import "LogoutCell.h"

#import "AvatarVC.h"
#import "NicknameVC.h"
#import "PasswordVC.h"

@interface UserInfoVC ()
@property ( nonatomic, strong ) NSArray * arr;
@end

@implementation UserInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.titleLabel.text = @"个人中心";
    self.dataArray = [NSMutableArray arrayWithObjects:@"头像", @"昵称",@"手机号",@"修改密码",nil];
    self.arr = @[@"",[UserDefaults valueForKey:@"user_nicename"],UserPhone,@""];
    [self registerClass:[UserInfoCell class]];
    [self registerClass:[UserInfoHeaderCell class]];
    [self registerNib:[LogoutCell class]];
    self.bottomView.backgroundColor = viewGaryColor;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        UserInfoHeaderCell * cell = [tableView dequeueReusableCellWithIdentifier:@"UserInfoHeaderCell"];
        cell.label.text = self.dataArray[indexPath.row];
        return cell;
    } else if (indexPath.row == 4) {
        LogoutCell * cell = [tableView dequeueReusableCellWithIdentifier:@"LogoutCell"];
        cell.navigationController = self.navigationController;
        return cell;
    }
    UserInfoCell * cell = [tableView dequeueReusableCellWithIdentifier:@"UserInfoCell"];
    cell.label.text = self.dataArray[indexPath.row];
    cell.labelRight.text = self.arr[indexPath.row];
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return 75;
    } else if (indexPath.row == 4) {
        return 115;
    }
    return 50;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    kSelfWeak
    switch (indexPath.row) {
        case 0:
            [self pushVC:[AvatarVC new]];
            break;
        case 1:
        {
            NicknameVC *n = [NicknameVC new];
            n.successBlock = ^(NSString * _Nonnull name) {
                [weakSelf refreshNickname:name];
            };
            [self pushVC:n];
        }
            break;
        case 2:
            
            break;
        case 3:
            [self pushVC:[PasswordVC new]];
            break;
            
        default:
            break;
    }
}

- (void)refreshNickname:(NSString *)name
{

    UserInfoCell * cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    cell.labelRight.text = name;
}

- (NSMutableArray *)rightArray
{
    if (!_rightArray) {
        _rightArray = [NSMutableArray array];
    }
    return _rightArray;
}
@end
