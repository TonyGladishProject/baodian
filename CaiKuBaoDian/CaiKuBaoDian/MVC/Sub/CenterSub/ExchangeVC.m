//
//  ExchangeVC.m
//  CaiKuBaoDian
//
//  Created by mac on 16/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "ExchangeVC.h"
#import "ExchangeView.h"
#import "YearPicker.h"

@interface ExchangeVC ()
@property ( nonatomic, strong ) NSMutableArray * dataArray;
@property ( nonatomic, strong ) ExchangeView * exView;
@property ( nonatomic, strong ) YearPicker * yearPicker;
@property ( nonatomic, copy ) NSString * yearStr;
@property ( nonatomic, copy ) NSString * urlStr;
@end

@implementation ExchangeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadHud];
    [self refreshUI];
}

- (void)loadHud
{
    [MBProgressHUD viewLoading:self.view];
    [self loadData];
}

- (void)loadData
{
    [HTTPTool postWithURLLogin:APIPlatform parameters:@{@"username":UserPhone} success:^(id  _Nonnull responseObject) {
        [self loadDes];
        
        [self.dataArray removeAllObjects];
        [self.yearPicker.dataArray removeAllObjects];
        NSArray * arr = responseObject[@"data"][@"pingtai"];
        for (NSDictionary * dic in arr) {
            PlatformModel * model = [PlatformModel mj_objectWithKeyValues:dic];
            [self.dataArray addObject:model];
            [self.yearPicker.dataArray addObject:model.pingtainame];
        }
        [self.yearPicker.pickerView reloadAllComponents];
        self.yearPicker.selectedStr = self.yearPicker.dataArray[0];
        PlatformModel * model = self.dataArray[0];
        self.urlStr = model.url;
    }];
}

- (void)loadDes
{
    [HTTPTool postWithURL:APIShareDes parameters:@{} success:^(id  _Nonnull responseObject) {
        [MBProgressHUD hideHUDForView:self.view];
        
        [Tools setLabelHeight:self.exView.des str:responseObject[@"data"][@"exchange_describe"] width:WIDTH - 40 font:13];
    }];
}

- (void)refreshUI
{
    self.titleLabel.text = @"转换";
    self.scrollView.backgroundColor = viewGaryColor;
    self.exView = [ExchangeView newView];
    [self.scrollView addSubview:self.exView];
    self.exView.navigationController = self.navigationController;
    [self.exView.button addTarget:self action:@selector(buttonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.exView.platform touchEvents:self action:@selector(labelTouch)];
    [self.exView.login touchEvents:self action:@selector(pushWeb)];
    [self.exView.regis  touchEvents:self action:@selector(pushWeb)];
    
    self.yearPicker = [YearPicker newView];
    [self.view addSubview:self.yearPicker];
    [self.yearPicker.confirmBtn addTarget:self action:@selector(ButtonRightClick) forControlEvents:UIControlEventTouchUpInside];
}

- (void)labelTouch
{
    [self.yearPicker show];
}

- (void)ButtonRightClick
{
    [self.yearPicker dismiss];
    self.exView.platform.text = self.yearPicker.selectedStr;
    self.exView.platform.textColor = UIColor.blackColor;
    
    for (PlatformModel * model in self.dataArray) {
        if ([self.yearPicker.selectedStr isEqualToString:model.pingtainame]) {
            self.urlStr = model.url;
        }
    }
}

- (void)pushWeb
{
    if ([self.exView.platform.text isEqualToString:@"请选择"]) {
        [MBProgressHUD showMessage:@"请选择平台"];
        return;
    }
    WebVC * w = [WebVC new];
    w.titleName = self.yearPicker.selectedStr;
    w.urlStr = self.urlStr;
    [self pushVC:w];
}

- (void)buttonClick
{
    if ([self.exView.platform.text isEqualToString:@"请选择"]) {
        [MBProgressHUD showMessage:@"请选择平台"];
        return;
    } else if (self.exView.account.text.length < 1) {
        [MBProgressHUD showMessage:self.exView.account.placeholder];
        return;
    } else if (self.exView.jifenText.text.length < 1) {
        [MBProgressHUD showMessage:self.exView.jifenText.placeholder];
        return;
    }
    [self doExchange];
}

- (void)doExchange
{
    [HTTPTool postWithURLIndicatorMSG:APIExchangePost parameters:@{@"username":UserPhone,@"pingtainame":self.exView.platform.text,@"pingtaiaccount":self.exView.account.text,@"points":self.exView.jifenText.text} success:^(id  _Nonnull responseObject) {
        
    }];
}

- (NSMutableArray *)dataArray
{
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}
@end
