//
//  MyRichVC.m
//  CaiKuBaoDian
//
//  Created by mac on 17/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "MyRichVC.h"

@interface MyRichVC ()

@end

@implementation MyRichVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadHud];
    [self refreshUI];
}

- (void)loadHud
{
    [MBProgressHUD viewLoading:self.view];
    [self loadData];
}

- (void)loadData
{
    [HTTPTool postWithURLLogin:APIMyRich parameters:@{@"username":UserPhone} success:^(id  _Nonnull responseObject) {
        [MBProgressHUD hideHUDForView:self.view];
        
        [self showNoDataView:responseObject];
    }];
}

- (void)refreshUI
{
    self.contentViewHeight = HEIGHT - self.navBottomHeight - 44;
    self.navView.hidden = YES;
    self.contentView.frame = CGRectMake(0, 0, WIDTH, self.contentViewHeight);
}

@end
