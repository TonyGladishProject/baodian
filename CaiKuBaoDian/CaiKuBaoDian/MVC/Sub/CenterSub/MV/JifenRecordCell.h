//
//  JifenRecordCell.h
//  CaiKuBaoDian
//
//  Created by mac on 03/06/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "BaseCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface JifenRecordCell : BaseCell
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *time;
@property (weak, nonatomic) IBOutlet UILabel *score;
@property ( nonatomic, strong ) JifenRecordModel * model;
@end

NS_ASSUME_NONNULL_END
