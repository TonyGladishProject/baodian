//
//  MyNoticeCell.m
//  CaiKuBaoDian
//
//  Created by mac on 08/06/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "MyNoticeCell.h"

@implementation MyNoticeCell
- (void)setModel:(MyNoticeModel *)model
{
    _model = model;
    
    self.labelTitle.text = model.title;
    self.labelContent.text = model.content;
    NSArray *array = [model.createtime componentsSeparatedByString:@" "];
    NSArray * arr = [array[0] componentsSeparatedByString:@"-"];
    self.year.text = arr[0];
    self.day.text = [arr[1] stringByAppendingString:[@"-" stringByAppendingString:arr[2]]];
    self.time.text = array.lastObject;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.viewBack.layer.shadowColor = [UIColor grayColor].CGColor;
    self.viewBack.layer.shadowOpacity = 0.8f;
    self.viewBack.layer.shadowRadius = 4.f;
    self.viewBack.layer.shadowOffset = CGSizeMake(4,4);
    self.viewBack.layer.shadowOffset = CGSizeMake(0,0);
    self.viewBack.layer.cornerRadius = 5;
}

@end
