//
//  JifenRecordCell.m
//  CaiKuBaoDian
//
//  Created by mac on 03/06/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "JifenRecordCell.h"

@implementation JifenRecordCell

- (void)setModel:(JifenRecordModel *)model
{
    _model  = model;
    self.name.text = model.desc;
    self.time.text = model.addtime;
    self.score.text = [NSString stringWithFormat:@"%@金币",model.score_change];
}

- (void)awakeFromNib {
    [super awakeFromNib];
}

@end
