//
//  MyNoticeCell.h
//  CaiKuBaoDian
//
//  Created by mac on 08/06/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "PureCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface MyNoticeCell : PureCell
@property (weak, nonatomic) IBOutlet UILabel *year;
@property (weak, nonatomic) IBOutlet UILabel *day;
@property (weak, nonatomic) IBOutlet UILabel *time;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelContent;
@property (weak, nonatomic) IBOutlet UIView *viewBack;
@property ( nonatomic, strong ) MyNoticeModel * model;
@end

NS_ASSUME_NONNULL_END
