//
//  ShareCodeVC.m
//  CaiKuBaoDian
//
//  Created by mac on 17/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "ShareCodeVC.h"

@interface ShareCodeVC ()
@property ( nonatomic, strong ) UILabel * labelDes;
@end

@implementation ShareCodeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadHud];
    [self refreshUI];
}

- (void)loadHud
{
    [MBProgressHUD viewLoading:self.view];
    [HTTPTool postWithURL:APIShareDes parameters:@{} success:^(id  _Nonnull responseObject) {
        [MBProgressHUD hideHUDForView:self.view];
        
        [Tools setLabelHeight:self.labelDes str:responseObject[@"data"][@"description"] width:WIDTH - 40 font:13];
    }];
}

- (void)refreshUI
{
    self.contentViewHeight = HEIGHT - self.navBottomHeight - 44;
    self.navView.hidden = YES;
    self.contentView.frame = CGRectMake(0, 0, WIDTH, self.contentViewHeight);
    
    UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(20, 60, WIDTH - 40, 40)];
    [self.scrollView addSubview:label];
    label.textAlignment = NSTextAlignmentCenter;
    label.text = @"我的邀请码";
    
    self.labelDes = [[UILabel alloc] initWithFrame:CGRectMake(20, 120, WIDTH - 40, 40)];
    [self.scrollView addSubview:self.labelDes];

}

@end
