//
//  BaiBaoXiangVC.h
//  CaiKuBaoDian
//
//  Created by mac on 08/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "MainHomeVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface BaiBaoXiangVC : MainHomeVC<UICollectionViewDataSource,UICollectionViewDelegate>
@property ( nonatomic, strong ) UICollectionView * collectionView;
@property ( nonatomic, strong ) NSMutableArray * arrImages;
@property ( nonatomic, strong ) NSMutableArray * arrNames;
@end

NS_ASSUME_NONNULL_END
