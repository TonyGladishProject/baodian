//
//  BaiBaoXiangVC.m
//  CaiKuBaoDian
//
//  Created by mac on 08/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "BaiBaoXiangVC.h"
#import "CollectionViewCell.h"

#import "ShakeVC.h"
#import "SXCardsVC.h"
#import "KitVC.h"
#import "CodeVC.h"
#import "WebVC.h"
#import "TableVC.h"
#import "DateVC.h"
#import "GuessVC.h"
#import "LuckVC.h"

@interface BaiBaoXiangVC ()

@end

@implementation BaiBaoXiangVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self createCollectionView];
}

- (void)createCollectionView
{
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0,0, WIDTH, self.contentViewHeight) collectionViewLayout:flowLayout];
    self.collectionView.backgroundColor = viewGaryColor;
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    [self.contentView addSubview:self.collectionView];
    self.collectionView.alwaysBounceVertical = YES;
    
    [self registerClass:[CollectionViewCell class]];
    self.arrImages = [NSMutableArray arrayWithArray:@[
                                                                     @"item_icon_shake",@"item_icon_shengxiao",@"item_icon_xuanji",
                                                                     @"item_icon_lover",@"item_icon_turntable",
                                                                     @"item_icon_date",@"item_icon_tianji",@"item_icon_luck"]];
    self.arrNames = [NSMutableArray arrayWithArray:@[
                                                                    @"摇一摇",@"生肖卡牌",@"玄机锦囊",
                                                                    @"恋人特码",@"波肖转盘",
                                                                    @"搅珠日期",@"天机测算",@"幸运摇奖"]];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.arrImages.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CollectionViewCell" forIndexPath:indexPath];
    cell.image.image = [UIImage imageNamed:self.arrImages[indexPath.item]];
    cell.label.text = self.arrNames[indexPath.item];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return  CGSizeMake((WIDTH -2)/3,(WIDTH -2)/3 - 10 );
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(1, 0,1, 0);//（上、左、下、右）
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return .5;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 1;
}

#pragma mark  点击CollectionView触发事件
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 0:
            [self pushVC:[ShakeVC new]];
            break;
        case 1:
            [self pushVC:[SXCardsVC new]];
            break;
        case 2:
            [self pushVC:[KitVC new]];
            break;
        case 3:
            [self pushVC:[CodeVC new]];
            break;
        case 4:
            [self pushVC:[TableVC new]];
            break;
        case 5:
            [self pushVC:[DateVC new]];
            break;
        case 6:
            [self pushVC:[GuessVC new]];
            break;
        case 7:
            [self pushVC:[LuckVC new]];
            break;
        default:
            break;
    }
}

- (void)registerClass:(Class)cell
{
    [self.collectionView registerClass:cell forCellWithReuseIdentifier:NSStringFromClass(cell)];
}
@end
