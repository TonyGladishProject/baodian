//
//  HomeVC.m
//  CaiKuBaoDian
//
//  Created by mac on 08/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "HomeVC.h"
#import "SDCycleScrollView.h"
#import "TXScrollLabelView.h"
#import "HomeCollCell.h"

#import "SettingVC.h"
#import "ShareVC.h"

#import "HistoryVC.h"
#import "LotteryLiveVC.h"
#import "GalleryVC.h"
#import "BiWuLeiTaiVC.h"
#import "ZiLiaoVC.h"
#import "StatisticsVC.h"
#import "SiBuXiangVC.h"
#import "HongKongVC.h"
#import "ChartAnalysisVC.h"
#import "VoteVC.h"

#import "MoreVC.h"

@interface HomeVC ()<UICollectionViewDataSource,UICollectionViewDelegate,SDCycleScrollViewDelegate>
@property ( nonatomic, strong ) UIScrollView * scrollView;
@property ( nonatomic, strong ) SDCycleScrollView * cycleScrollView;

@property ( nonatomic, strong ) UILabel * labelNo;
@property ( nonatomic, strong ) UILabel * labelNoNext;
@property ( nonatomic, strong ) UIImageView * imgAddress;
@property ( nonatomic, strong ) TXScrollLabelView * scrollLabel;
@property ( nonatomic, strong ) UICollectionView * collectionView;
@property ( nonatomic, strong ) NSArray * arrayS;
@property ( nonatomic, assign) int count;
/**
 号码
 */
@property ( nonatomic, strong ) NSMutableArray * arrN;
/**
 图片
 */
@property ( nonatomic, strong ) NSMutableArray * arrI;
/**
 生肖
 */
@property ( nonatomic, strong ) NSMutableArray * arrL;
@property ( nonatomic, strong ) NSMutableArray * arrUrls;
@property ( nonatomic, strong ) NSArray * arrImages;
@property ( nonatomic, strong ) NSArray * arrNames;

@property ( nonatomic, strong ) NSDictionary * dataLotteryNums;

@end

@implementation HomeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(noti:) name:@"refreshLottery" object:nil];
    self.arrImages = @[@"icon_lottery_live",@"item_statistics",@"icon_index_leitai",  @"icon_index_tukv",@"icon_index_sibuxiang",@"item_tool_box",@"item_text_data",@"item_analysis",@"item_communication_forum",@"item_query",@"icon_index_toupiao",@"item_more"];
    self.arrNames = @[@"开奖直播",@"资料大全",@"比武擂台",@"六合图库",@"四不像",@"香港挂牌",@"历史开奖",         @"六合统计",@"查询助手",@"图表分析",@"彩民投票",@"查看更多"];
    self.arrayS = @[@"NO",@"YES",@"YES",@"NO",@"NO",@"NO",@"NO",@"NO",@"NO",@"NO",@"NO",@"NO"];
    self.count = 8;
    [self createNavButton];
    [self createScrollView];
}

- (void)createScrollView
{
    CGFloat height = WIDTH/3 * 2 + WIDTH/8 + 8 + 26 + ((WIDTH -2)/3 - 20) *4 + 2;
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, self.contentViewHeight)];
    [self.contentView addSubview:self.scrollView];
    self.scrollView.contentSize = CGSizeMake(WIDTH, height);
//    self.scrollView.backgroundColor = UIColor.grayColor;
    
    [self createUI];
    [self loadData];
    [self loadMJ];
}

- (void)noti:(NSNotification *)noti
{
    [self dataToLottery:noti.userInfo];
}

#pragma mark - loadData
- (void)loadMJ
{
    MJRefreshNormalHeader * header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self loadBanner];
    }];
    self.scrollView.mj_header  = header;
}


- (void)loadData
{
//    [MBProgressHUD viewLoading:self.view];
    [self loadBanner];
}
- (void)loadBanner
{
    NSLog(@"---------urlFirst-%@",APIBanner);
    [HTTPTool getWithURL:APIBanner parameters:@{} success:^(id  _Nonnull responseObject) {
        [self loadLotteryNums];
        
        self.arrUrls = [NSMutableArray arrayWithCapacity:4];
        NSMutableArray * arr = [NSMutableArray arrayWithCapacity:4];
        NSArray * data = responseObject[@"data"];
        for (NSDictionary * dic in data) {
            [arr addObject:imgHTTP(dic[@"slide_pic"])];
            [self.arrUrls addObject:dic[@"slide_url"]];
        }
        self.cycleScrollView.imageURLStringsGroup = arr;
    } failure:^(NSError * _Nonnull error) {
        NSArray * arr = @[@"http://www.weiwo8.com:8881/",
                          @"http://www.xingrenwu.com:8881/",
                          @"http://www.luodouwang.com:8881/",
                          @"http://www.076888.com:8881/",
                          @"http://www.ad.lfjsjs.com:8881/"];
        for (NSString * str in arr) {
            NSString * s = [UserDefaults valueForKey:@"APIHttp"];
            if (![s isEqualToString:str]) {
                [UserDefaults setValue:@"http://www.weiwo8.com:8881/" forKey:@"APIHttp"];
                [UserDefaults synchronize];
            }
        }
        [self changeUrl];
    }];
}

- (void)changeUrl
{
    [HTTPTool getWithURL:APIBanner parameters:@{} success:^(id  _Nonnull responseObject) {
        [self loadLotteryNums];
        
        self.arrUrls = [NSMutableArray arrayWithCapacity:4];
        NSMutableArray * arr = [NSMutableArray arrayWithCapacity:4];
        NSArray * data = responseObject[@"data"];
        for (NSDictionary * dic in data) {
            [arr addObject:imgHTTP(dic[@"slide_pic"])];
            [self.arrUrls addObject:dic[@"slide_url"]];
        }
        self.cycleScrollView.imageURLStringsGroup = arr;
    } failure:^(NSError * _Nonnull error) {
        [MBProgressHUD hideHUDForView:self.view];
        [MBProgressHUD showMessage:@"请重新下载"];
    }];
}

- (void)loadLotteryNums
{
    [HTTPTool getWithURL:APILotteryNums success:^(id  _Nonnull responseObject) {
        [self loadImage];
        
        NSDictionary * data = responseObject[@"data"];
        [self dataToLottery:data];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"lotteryInfo" object:nil userInfo:@{@"timeprompt":data[@"timeprompt"],@"num":data[@"haoma"][6],@"qs":data[@"期数"]}];
    }];
}

- (void)dataToLottery:(NSDictionary *)data
{
    self.dataLotteryNums = data;
    self.labelNo.text = [NSString stringWithFormat:@"第%@开奖结果",data[@"期数"]];
    self.labelNoNext.text = data[@"nextopentime"];
    for (int i = 0; i < 6; i++) {
        UILabel * l = self.arrN[i];
        l.text = data[@"haoma"][i];
        
        UILabel * la = self.arrL[i];
        la.text = data[@"shengxiao"][i];
        
        UIImageView * imgs = self.arrI[i];
        imgs.image = [UIImage imageNamed:[self colorFromString:data[@"yanse"][i]]];
    }
    
    
    UIImageView * imgA = self.arrI[6];
    imgA.image = [UIImage imageNamed:@"icon_live_add"];
    
    UIImageView * imgL = self.arrI.lastObject;
    imgL.image = [UIImage imageNamed:[self colorFromString:data[@"yanse"][6]]];
    
    
    UILabel * l = self.arrN.lastObject;
    l.text = data[@"haoma"][6];
    
    UILabel * la = self.arrL.lastObject;
    la.text = data[@"shengxiao"][6];
}

- (void)loadImage
{
    [HTTPTool getWithURL:APIShareImgs success:^(id  _Nonnull responseObject) {
        [self loadNotice];
        
        [self.imgAddress sd_setImageWithURL:[NSURL URLWithString:imgAPI(responseObject[@"image"])]];
    }];
}

- (void)loadNotice
{
    [HTTPTool getWithURL:APINotice success:^(id  _Nonnull responseObject) {
        [MBProgressHUD hideHUDForView:self.view];
        [self.scrollView.mj_header endRefreshing];
        
        self.scrollLabel.scrollTitle = responseObject[@"data"][@"content"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"homeDataFinished" object:nil];
    }];
}

- (NSString *)colorFromString:(NSString *)str
{
    if ([str isEqualToString:@"lan"]) {
        return @"blueball";
    } else if ([str isEqualToString:@"lv"]) {
        return @"greenball";
    }
    return @"redball";
}

#pragma mark - createUI
- (void)createUI
{
    CGFloat h = WIDTH/3;
    CGFloat w = WIDTH/2;

    self.cycleScrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, WIDTH, h) delegate:self placeholderImage:nil];
    [self.scrollView addSubview:self.cycleScrollView];
    NSArray *imagesURLStrings = @[@"load_002",@"load_002",@"load_002"];
    self.cycleScrollView.localizationImageNamesGroup = imagesURLStrings;

    //开奖号码
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0,h , WIDTH, h)];
    view.backgroundColor = UIColor.whiteColor;
    [self.scrollView addSubview:view];
    [view touchEvents:self action:@selector(numbersClick)];
    
    UIImageView * img = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, WIDTH - 20, h - 10)];
    img.center = CGPointMake(WIDTH/2, h/2);
    img.image = [UIImage imageNamed:@"menu_lottery_bg"];
    [view addSubview:img];
    
    UIImageView * imgA = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0,  20, 20)];
    imgA.center = CGPointMake(WIDTH -  25, h/2);
    imgA.image = [UIImage imageNamed:@"right"];
    [view addSubview:imgA];
    
    self.arrN = [NSMutableArray arrayWithCapacity:8];
    self.arrL = [NSMutableArray arrayWithCapacity:8];
    self.arrI  = [NSMutableArray arrayWithCapacity:8];
    CGFloat ww = 0.f;
    CGFloat xx = 0.f;
    CGFloat hh = 0.f;
    CGFloat textF = 17.f;
    CGFloat hhh = textF;
    if (iphone5) {
        ww = 28;
        xx = 2;
        hh = 5;
        textF = 13.f;
        hhh = textF;
    } else if (iphone8Plus) {
        ww = 38;
        xx = 5;
        hh = 13;
        hhh = 22;
    } else {
        ww = 33;
        xx = 4;
        hh = 10;
    }
    
    self.labelNo = [[UILabel alloc] initWithFrame:CGRectMake(0, h/2 - ww/2 - hh - 3 - 30, 300, 40)];
    self.labelNo.centerX = w;
    self.labelNo.textAlignment = NSTextAlignmentCenter;
    [view addSubview:self.labelNo];
    
    self.labelNoNext = [[UILabel alloc] initWithFrame:CGRectMake(0, h/2  + hh/2 + hhh, 300, 40)];
    self.labelNoNext.centerX = w;
    self.labelNoNext.textAlignment = NSTextAlignmentCenter;
    self.labelNoNext.textColor = UIColor.grayColor;
    self.labelNoNext.font = [UIFont systemFontOfSize:13];
    [view addSubview:self.labelNoNext];
    
    for (int i = 0; i < self.count; i++) {
        
        UILabel * labN = [[UILabel alloc] initWithFrame:CGRectMake(36 + (ww + xx) * i, h/2 - ww/2 - hh -3, ww, ww)];
        [self.arrN addObject:labN];
        [view addSubview:labN];
        
        
        UILabel * lab = [[UILabel alloc] initWithFrame:CGRectMake(38 + (ww + xx) * i, h/2  + hh/2 , ww, ww)];
        [self.arrL addObject:lab];
        [view addSubview:lab];
        
        lab.textAlignment  = labN.textAlignment  = NSTextAlignmentCenter;
        lab.font = labN.font = [UIFont systemFontOfSize:textF];
        
        UIImageView * imgs = [[UIImageView alloc] initWithFrame:CGRectMake(38 + (ww + xx) * i, h/2 - ww/2 - hh , ww, ww)];
        [self.arrI addObject:imgs];
        [view addSubview:imgs];
    }
    
    //下载地址
    UIView * vi = [[UIView alloc] initWithFrame:CGRectMake(0, h*2, WIDTH, WIDTH/8 + 8)];
    vi.backgroundColor = UIColor.whiteColor;
    [self.scrollView addSubview:vi];
    [vi touchEvents:self action:@selector(rightButtonClick)];
    
    self.imgAddress = [[UIImageView alloc] initWithFrame:CGRectMake(0, 5, WIDTH, WIDTH/8)];
    [vi addSubview:self.imgAddress];
    
    //通知
    UIView * vN = [[UIView alloc] initWithFrame:CGRectMake(0, vi.bottom, WIDTH, 26)];
    vN.backgroundColor = UIColor.whiteColor;
    [self.scrollView addSubview:vN];
    
    UIImageView * iN = [[UIImageView alloc] initWithFrame:CGRectMake(10, 3, 16, 20)];
    [vN addSubview:iN];
    iN.image = [UIImage imageNamed:@"icon_lingsheng"];
    
    self.scrollLabel = [TXScrollLabelView scrollWithTitle:@"" type:0 velocity:1 options:UIViewAnimationOptionTransitionFlipFromTop];
    self.scrollLabel.frame = CGRectMake(31,3,WIDTH - 31-10,20);
    [vN addSubview:self.scrollLabel];
    [self.scrollLabel beginScrolling];
    self.scrollLabel.backgroundColor = UIColor.clearColor;
    self.scrollLabel.scrollTitleColor = UIColorFromRGB(0xc75038);
    
    [self createCollectionView];
}

#pragma mark - collectionView
- (void)createCollectionView
{
    CGFloat cellH = (WIDTH -2)/3 - 20;
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0,WIDTH/3*2 + WIDTH/8 + 8 + 26, WIDTH, cellH *4 + 2) collectionViewLayout:flowLayout];
    self.collectionView.backgroundColor = viewGaryColor;
//    self.collectionView.backgroundColor = UIColor.lightGrayColor;
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.scrollEnabled = NO;
    [self.scrollView addSubview:self.collectionView];
    //注册Cell
    [self.collectionView registerNib:[UINib nibWithNibName:@"HomeCollCell" bundle:nil] forCellWithReuseIdentifier:@"HomeCollCell"];
    
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, self.collectionView.bottom - 1, WIDTH, 1)];
    view.backgroundColor = self.collectionView.backgroundColor;
    [self.scrollView addSubview:view];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 12;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    HomeCollCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"HomeCollCell" forIndexPath:indexPath];
    cell.image.image = [UIImage imageNamed:self.arrImages[indexPath.item]];
    cell.label.text = self.arrNames[indexPath.item];
    NSString * str = self.arrayS[indexPath.item];
    cell.show = str.boolValue;
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return  CGSizeMake((WIDTH -2)/3,(WIDTH -2)/3 - 20 );
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(1, 0,1, 0);//（上、左、下、右）
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return .5;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 1;
}

#pragma mark  点击CollectionView触发事件
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 0:
            [self pushVC:[LotteryLiveVC new]];
            break;
        case 1:
            [self pushVC:[ZiLiaoVC new]];
            break;
        case 2:
            [self pushVC:[BiWuLeiTaiVC new]];
            break;
        case 3:
            [self pushVC:[GalleryVC new]];
            break;
        case 4:
            [self pushVC:[SiBuXiangVC new]];
            break;
        case 5:
            [self pushVC:[HongKongVC new]];
            break;
        case 6:
            [self numbersClick];
            break;
        case 7:
            [self pushVC:[StatisticsVC new]];
            break;
        case 8:
        {
            WebVC * w = [WebVC new];
            w.titleName = @"查询助手";
            w.urlStr = APIAssistant;
            [self pushVC:w];
        }
            break;
        case 9:
            [self pushVC:[ChartAnalysisVC new]];
            break;
        case 10:
            [self pushVC:[VoteVC new]];
            break;
        case 11:
            [self pushVC:[MoreVC new]];
            break;
            
        default:
            break;
    }
}

- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index
{
    NSLog(@"---点击了第%ld张图片", (long)index);
    WebVC * web = [WebVC new];
    web.urlStr = self.arrUrls[index];
    web.titleName = @"679彩票-注册送19元";
    web.bottomColor = MainColorDeep;
    if (web.urlStr.length > 1) {
        [self pushHiddenTabbar:web];
    }
}

- (void)createNavButton
{
    self.leftButton.hidden = NO;
    [self.leftButton setImage:nil forState:UIControlStateNormal];
    [self.leftButton setTitle:@"设置" forState:UIControlStateNormal];
    [self.rightButton setTitle:@"分享" forState:UIControlStateNormal];
}

- (void)leftButtonClick
{
    [self pushVC:[SettingVC new]];
}

- (void)rightButtonClick
{
    [self pushVC:[ShareVC new]];
}

- (void)numbersClick
{
    [self pushVC:[HistoryVC new]];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.collectionView reloadData];
}
@end
