//
//  HomeCollCell.m
//  CaiKuBaoDian
//
//  Created by mac on 11/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "HomeCollCell.h"

@implementation HomeCollCell
- (void)setShow:(BOOL)show
{
    _show = show;
    self.imageHot.hidden = !show;
    [self.imageHot startAnimating];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    CGFloat width = (WIDTH - 2)/3;
    self.image.frame = CGRectMake(40, 20, width - 80, width - 80);
    self.label.frame = CGRectMake(0, self.image.bottom + 10, width, 20);
    
    if (iphone5) {
        self.image.frame = CGRectMake(30, 10, width - 60, width - 60);
        self.label.frame = CGRectMake(0, self.image.bottom + 5, width, 20);
        self.label.font = [UIFont systemFontOfSize:12];
    }
    
    self.imageHot = [[UIImageView alloc] initWithFrame:CGRectMake(width/2 + 10, 20, 30, 30)];
    [self addSubview:self.imageHot];
    self.imageHot.animationImages = @[[UIImage imageNamed:@"hot"],[UIImage imageNamed:@"hot2"]];
    self.imageHot.animationRepeatCount = 0;
    self.imageHot.animationDuration = 1;
    [self.imageHot startAnimating];
}

@end
