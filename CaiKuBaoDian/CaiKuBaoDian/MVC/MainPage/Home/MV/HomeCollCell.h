//
//  HomeCollCell.h
//  CaiKuBaoDian
//
//  Created by mac on 11/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HomeCollCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UILabel *label;
@property ( nonatomic, strong ) UIImageView * imageHot;
@property ( nonatomic, assign) BOOL show;
@end

NS_ASSUME_NONNULL_END
