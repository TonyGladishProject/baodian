//
//  PersonalVC.h
//  CaiKuBaoDian
//
//  Created by mac on 08/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "TablePureVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface PersonalVC : TablePureVC
@property ( nonatomic, copy ) NSString * name;
@end

NS_ASSUME_NONNULL_END
