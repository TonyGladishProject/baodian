//
//  PersonalVC.m
//  CaiKuBaoDian
//
//  Created by mac on 08/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "PersonalVC.h"
#import "UserCell.h"
#import "PCLikeCell.h"
#import "TableImgCell.h"

#import "UserInfoVC.h"
#import "LikeVC.h"
#import "FocusVC.h"
#import "FansVC.h"
#import "ExchangeVC.h"
#import "RichVC.h"
#import "JifenRecordsVC.h"
#import "ExchangeRecordsVC.h"
#import "MyNoticeVC.h"

@interface PersonalVC ()
@property ( nonatomic, strong ) NSArray * arrI;
@property ( nonatomic, strong ) UIView * dot;
@property (strong,nonatomic)dispatch_source_t timer;
@end

@implementation PersonalVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(noti) name:@"showDot" object:nil];

    self.leftButton.hidden = YES;
    self.titleLabel.text = self.name;
    
    self.arrI = @[@[@""],@[@"pcLike",@"focus",@"fans"],@[@"pcExchange",@"pcRich",@"pcScoreR",@"pcExchangeR"]];
    self.dataArray = [NSMutableArray arrayWithArray:@[@[@""],@[@"我的心水",@"我的关注",@"我的粉丝"],@[@"转换",@"致富",@"金币记录",@"转换记录"]]];
    [self registerClass:[TableImgCell class]];
    [self registerNib:[UserCell class]];
    [self registerNib:[PCLikeCell class]];
    self.bottomView.backgroundColor = viewGaryColor;
    UIImageView * img = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH - 20 - 20, 10, 20, 20)];
    [self.navMainView addSubview:img];
    img.image = [UIImage imageNamed:@"icon_msg_reply"] ;
    
    UIView * viewDot = [[UIView alloc] initWithFrame:CGRectMake(WIDTH - 20, 10, 10, 10)];
    viewDot.backgroundColor = UIColor.whiteColor;
    [viewDot layerMask];
    [self.navMainView addSubview:viewDot];
    self.dot = viewDot;
    self.dot.hidden = YES;
}

- (void)noti
{
    if (self.timer) {
        dispatch_source_cancel(self.timer);
    }
    [self downTime];
}

- (void)downTime
{
    __block NSInteger second = 60;
    //(1)
    dispatch_queue_t quene = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    //(2)
    dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, quene);
    self.timer = timer;
    //(3)
    dispatch_source_set_timer(timer, DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC, 0 * NSEC_PER_SEC);
    //(4)
    dispatch_source_set_event_handler(timer, ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            if (second <= 0) {
                second = 60;
                [self downTime];
                //(6)
                dispatch_cancel(timer);
            } else {
                self.dot.hidden = !self.dot.hidden;
                second--;
            }
        });
    });
    //(5)
    dispatch_resume(timer);
}

- (void)rightButtonClick
{
    MyNoticeVC * n = [MyNoticeVC new];
    n.backBlock = ^{
        if (self.timer) {
            dispatch_source_cancel(self.timer);
        }
        self.dot.hidden = YES;
        UITabBarItem * item=self.tabBarController.tabBar.items[1];
        item.badgeValue = nil;
        [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    };
    [self pushVC:n];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.dataArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray * arr = self.dataArray[section];
    return arr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        UserCell * cell = [tableView dequeueReusableCellWithIdentifier:@"UserCell"];
        return cell;
    }
    NSArray * arr = self.dataArray[indexPath.section];
    NSArray * arrI = self.arrI[indexPath.section];
    
    TableImgCell * cell = [tableView dequeueReusableCellWithIdentifier:@"TableImgCell"];
    cell.label.text = arr[indexPath.row];
    cell.image.image = [UIImage imageNamed:arrI[indexPath.row]];
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 10)];
    view.backgroundColor = UIColorFromRGB(0xf5f5f5);
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section ==0) {
        return 90;
    }
    return 50;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 0) {
         [self pushVC:[UserInfoVC new]];
    } else if (indexPath.section == 1) {
        switch (indexPath.row) {
            case 0:
                [self pushVC:[LikeVC new]];
                break;
            case 1:
                [self pushVC:[FocusVC new]];
                break;
            case 2:
                [self pushVC:[FansVC new]];
                break;
            default:
                break;
        }
    } else {
        switch (indexPath.row) {
            case 0:
                [self pushVC:[ExchangeVC new]];
                break;
            case 1:
                [self pushVC:[RichVC new]];
                break;
            case 2:
                [self pushVC:[JifenRecordsVC new]];
                break;
            case 3:
                [self pushVC:[ExchangeRecordsVC new]];
                break;
            default:
                break;
        }
    }
}

#pragma mark - MainHomeVC-function
- (void)pushVC:(UIViewController *)vc
{
    [self pushHiddenTabbar:vc];
}

- (void)pushHiddenTabbar:(UIViewController *)vc
{
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (CGFloat)tabbarHeight
{
    if (iphoneX) {
        return 83;
    }
    return 49;
}

- (CGFloat)navTabHeight
{
    if (iphoneX) {
        return 88 + 83;
    }
    return 64 + 49;
}

- (CGFloat)bottomHeight
{
    return [self tabbarHeight];
}

- (CGFloat)navBottomHeight
{
    return [self navTabHeight];
}
@end
