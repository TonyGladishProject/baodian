//
//  TableImgCell.m
//  CaiKuBaoDian
//
//  Created by mac on 17/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "TableImgCell.h"

@implementation TableImgCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.image = [[UIImageView alloc] initWithFrame:CGRectMake(10, 15, 20, 20)];
        [self.contentView addSubview:self.image];
        self.label.x = 35;
        self.label.height = 50;
        self.line.y = 49;
    }
    return self;
}

@end
