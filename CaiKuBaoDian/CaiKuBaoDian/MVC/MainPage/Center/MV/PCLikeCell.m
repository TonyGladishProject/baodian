//
//  PCLikeCell.m
//  CaiKuBaoDian
//
//  Created by mac on 16/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "PCLikeCell.h"

@implementation PCLikeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.image = [[UIImageView alloc] initWithFrame:CGRectMake(10, 15, 20, 20)];
    [self.groundView addSubview:self.image];
    
    self.label = [[UILabel alloc] initWithFrame:CGRectMake(35, 0, 200, 50)];
    [self.groundView addSubview:self.label];
    
    self.arrow = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH - 10 - 20, 0, 20, 20)];
    self.arrow.centerY = 25;
    [self.groundView addSubview:self.arrow];
    
    self.line = [[UIView alloc] initWithFrame:CGRectMake(0, 54, WIDTH, 1)];
//    [self.groundView addSubview:self.line];
    
    self.switchView = [[UISwitch alloc] initWithFrame:CGRectMake(WIDTH - 49 - 10, 0, 49, 31)];
    self.switchView.centerY = 25;
    [self.groundView addSubview:self.switchView];
    
    self.switchView.hidden = YES;
    self.line.backgroundColor = cellLineColor;
    self.arrow.image = [UIImage imageNamed:@"right_gray"];
    self.label.textColor = cellWordColor;
    
    self.label.text = @"我的心水";
    self.image.image = [UIImage imageNamed:@"pcLike"];
}

@end
