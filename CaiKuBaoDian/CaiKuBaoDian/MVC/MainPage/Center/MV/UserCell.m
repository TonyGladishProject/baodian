//
//  UserCell.m
//  CaiKuBaoDian
//
//  Created by mac on 16/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "UserCell.h"

@implementation UserCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.arrow.hidden = NO;
    self.arrow.centerY = 45;
    
    self.account.textColor = self.balance.textColor = UIColorFromRGB(0xE78B02);
    self.account.text = UserPhone;
    self.balance.text = @"0";
    [self.icon layerMask];
    [self.icon sd_setImageWithURL:[NSURL URLWithString:[UserDefaults valueForKey:@"avatar"]] placeholderImage:[UIImage imageNamed:@"placeHolder0"]];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(noti) name:@"refreshAvatar" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshBalance:) name:@"userBalance" object:nil];
}

- (void)noti
{
    [self.icon sd_setImageWithURL:[NSURL URLWithString:[UserDefaults valueForKey:@"avatar"]] placeholderImage:[UIImage imageNamed:@"placeHolder0"]];
}

- (void)refreshBalance:(NSNotification *)noti
{
    self.balance.text = noti.userInfo[@"score"];
}

@end
