//
//  UserCell.h
//  CaiKuBaoDian
//
//  Created by mac on 16/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "PureCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface UserCell : PureCell
@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UILabel *account;
@property (weak, nonatomic) IBOutlet UILabel *balance;

@end

NS_ASSUME_NONNULL_END
