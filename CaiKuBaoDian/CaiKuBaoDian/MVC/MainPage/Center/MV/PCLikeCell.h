//
//  PCLikeCell.h
//  CaiKuBaoDian
//
//  Created by mac on 16/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "PureCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface PCLikeCell : PureCell
@property (weak, nonatomic) IBOutlet UIView *groundView;

@end

NS_ASSUME_NONNULL_END
