//
//  ChatRoomVC.m
//  CaiKuBaoDian
//
//  Created by mac on 08/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "ChatRoomVC.h"

@interface ChatRoomVC ()
@property ( nonatomic, strong ) UIImageView * image;
@end

@implementation ChatRoomVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self createPlaceHolderImage];
}

- (void)createPlaceHolderImage
{
    self.image = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, self.contentViewHeight + self.navView.bottom)];
    [self.view addSubview:self.image];
    self.image.image = [UIImage imageNamed:@"chat_bg"];
}

@end
