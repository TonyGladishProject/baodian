//
//  PocketVC.m
//  CaiKuBaoDian
//
//  Created by mac on 15/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "PocketVC.h"
#import "AppDelegate.h"
#import "MainTBC.h"
#import "PromptLabel.h"
#import "PocketView.h"

@interface PocketVC ()
@property ( nonatomic, strong ) PromptLabel * prompt;
@property ( nonatomic, strong ) PocketView * pocketView;
@end

@implementation PocketVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadHud];
    [self refreshUI];
}

- (void)loadHud
{
    [MBProgressHUD viewLoading:self.view];
    [HTTPTool postWithURL:APIPocket parameters:@{} success:^(id  _Nonnull responseObject) {
        [MBProgressHUD hideHUDForView:self.view];
        
        NSString * str = responseObject[@"data"][@"description"];
        [Tools setLabelHeight:self.prompt str:str width:WIDTH - 40 font:13];
    }];
}

- (void)refreshUI
{
    self.titleLabel.text = @"领取红包";
    self.navTintView.backgroundColor = UIColor.clearColor;
    self.navImage.hidden = YES;
    
    self.img = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    [self.view addSubview:self.img];
    self.img.image = [UIImage imageNamed:@"red_bg"];
    
    [self.view bringSubviewToFront:self.contentView];
    [self.view bringSubviewToFront:self.navView];
    
    self.pocketView = [PocketView newView];
    [self.scrollView addSubview:self.pocketView];
    
    PromptLabel * prompt = [PromptLabel newView];
    prompt.y = self.contentViewHeight - 80;
    [self.pocketView addSubview:prompt];
    
    prompt.numberOfLines = 0;
    prompt.x  = 20;
    prompt.width = WIDTH - 40;
    prompt.textAlignment = NSTextAlignmentLeft;
    self.prompt = prompt;
}

- (void)leftButtonClick
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    MainTBC * tbc = (MainTBC *)delegate.window.rootViewController;
    tbc.floatbtn.hidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    MainTBC * tbc = (MainTBC *)delegate.window.rootViewController;
    tbc.floatbtn.hidden = NO;
}
@end
