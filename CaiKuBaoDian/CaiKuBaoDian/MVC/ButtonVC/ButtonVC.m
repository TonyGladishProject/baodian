//
//  ButtonVC.m
//  CaiKuBaoDian
//
//  Created by mac on 12/06/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "ButtonVC.h"
#import "AppDelegate.h"
#import "MainTBC.h"

@interface ButtonVC ()

@end

@implementation ButtonVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (void)leftButtonClick
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    MainTBC * tbc = (MainTBC *)delegate.window.rootViewController;
    tbc.floatbtn.hidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    MainTBC * tbc = (MainTBC *)delegate.window.rootViewController;
    tbc.floatbtn.hidden = NO;
}
@end
