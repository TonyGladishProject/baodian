//
//  PocketView.m
//  CaiKuBaoDian
//
//  Created by mac on 16/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "PocketView.h"


@implementation PocketView

+(instancetype)newView
{
    PocketView * view = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil].lastObject;
    view.frame = CGRectMake(0, 0, WIDTH,view.contentViewHeight);
    return view;
}

- (void)drawRect:(CGRect)rect {
    
    self.img.frame = CGRectMake(0, 0, WIDTH/4, WIDTH/4);
    self.img.center = CGPointMake(WIDTH/2, WIDTH/3 + 20);
    
    self.label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, WIDTH/2, 40)];
    self.label.center = CGPointMake(WIDTH/2, WIDTH/3);
    self.label.textAlignment = NSTextAlignmentCenter;
    self.label.textColor = UIColorFromRGB(0xFAC311);
    [self addSubview:self.label];
    self.label.hidden = YES;
}
- (IBAction)buttonClick:(UIButton *)sender {
    self.img.hidden = NO;
    self.label.hidden = YES;
    self.i = 0;
    [self animtionView];
    [self doPocket];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.label.hidden = NO;
        [Tools setLabelHeight:self.label str:self.str width:WIDTH/2 font:17];
    });
}

- (void)animtionView
{
    [UIView transitionWithView:self.img                      duration:1.5
                       options:UIViewAnimationOptionTransitionFlipFromRight
                    animations:^ {
                    }
                    completion:^(BOOL finished){
                        self.i++;
                        if (self.i < 2) {
                            [self animtionView];
                        } else {
                            self.img.hidden = YES;
                        }
                    }];
}

- (void)doPocket
{
    [HTTPTool postWithURLLogin:APIPocketPost parameters:@{@"username":UserPhone} success:^(id  _Nonnull responseObject) {
        self.str = responseObject[@"errmsg"];
    }];
}

@end
