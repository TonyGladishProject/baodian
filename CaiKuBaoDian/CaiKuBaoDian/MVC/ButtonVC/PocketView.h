//
//  PocketView.h
//  CaiKuBaoDian
//
//  Created by mac on 16/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "BaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface PocketView : BaseView
@property (weak, nonatomic) IBOutlet UIImageView *img;
@property (weak, nonatomic) IBOutlet UIButton *button;
@property ( nonatomic, strong ) UILabel * label;
@property ( nonatomic, assign ) int i;
@property ( nonatomic, copy ) NSString * str;
@end

NS_ASSUME_NONNULL_END
