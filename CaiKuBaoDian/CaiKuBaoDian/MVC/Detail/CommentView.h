//
//  CommentView.h
//  CaiKuBaoDian
//
//  Created by mac on 29/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "BaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface CommentView : BaseView
@property (nonatomic, copy)  void(^doCommentBlock) (void);
@property ( nonatomic, strong ) UITextField * textField;
@property ( nonatomic, strong ) UIButton * button;
@property ( nonatomic, copy ) NSString * idStr;
@property ( nonatomic, strong ) CommentModel * model;
@end

NS_ASSUME_NONNULL_END
