//
//  NoDataCell.m
//  CaiKuBaoDian
//
//  Created by mac on 10/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "NoDataCell.h"

@implementation NoDataCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.noDataLabel = [NoDataLabel newView];
        [self addSubview:self.noDataLabel];
    }
    return self;
}

@end
