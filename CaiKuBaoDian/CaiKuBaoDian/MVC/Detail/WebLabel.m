//
//  WebLabel.m
//  CaiKuBaoDian
//
//  Created by mac on 27/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "WebLabel.h"

@implementation WebLabel

+(instancetype)newView
{
    return [[WebLabel alloc] initWithFrame:CGRectMake(5, 0, WIDTH - 10, 20)];
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self createUI];
    }
    return self;
}

- (void)createUI
{
    self.numberOfLines = 0;
    self.textColor = cellWordColor;
}

- (CGFloat)dataToView:(NSString *)text
{
    self.text = [@"\n" stringByAppendingString:[Tools attributedStringWithHTMLString:text]];
    self.x = 5;
    return [Tools setLabelHeight:self str:self.text width:WIDTH - 10  font:17.f];
}

- (CGFloat)dataToView:(NSString *)text width:(CGFloat)width font:(CGFloat)font
{
    self.text = [@"\n" stringByAppendingString:[Tools attributedStringWithHTMLString:text]];
    return [Tools setLabelHeight:self str:self.text width:width  font:font];
}

- (CGFloat)dataToViewLLCell:(NSString *)text
{
    self.text = [Tools attributedStringWithHTMLString:text];
    self.x = 5;
    return [Tools setLabelHeight:self str:self.text width:WIDTH - 10  font:17.f];
}

@end
