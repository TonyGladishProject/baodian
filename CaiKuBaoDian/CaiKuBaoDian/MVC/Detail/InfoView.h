//
//  InfoView.h
//  CaiKuBaoDian
//
//  Created by mac on 30/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "BaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface InfoView : BaseView
@property ( nonatomic, strong ) UIImageView * header;
@property ( nonatomic, strong ) UILabel * name;
@property ( nonatomic, strong ) UILabel * time;
@property ( nonatomic, strong ) UILabel * comment;
@property ( nonatomic, strong ) UILabel * title;
- (void)dataToView:(NSDictionary *)data;
- (void)dataToViewZL:(NSDictionary *)data;
@end

NS_ASSUME_NONNULL_END
