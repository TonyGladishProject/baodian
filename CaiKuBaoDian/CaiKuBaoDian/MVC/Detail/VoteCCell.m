//
//  VoteCCell.m
//  CaiKuBaoDian
//
//  Created by mac on 27/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "VoteCCell.h"

@implementation VoteCCell

- (void)setModel:(SXModel *)model
{
    _model = model;
    
    self.name.text = model.name;
    self.nums.text = [NSString stringWithFormat:@"%@票",model.num];
    self.persent.text = [NSString stringWithFormat:@"%@%%",model.persent];
    self.progress.progress = (CGFloat)model.persent.integerValue/100;
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI
{
    self.name = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 20, 40)];
    [self addSubview:self.name];
    
    self.nums = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, self.width - 60, 20)];
    [self addSubview:self.nums];
    
    self.progress = [[UIProgressView alloc] initWithFrame:CGRectMake(20, 30, self.width - 60, 20)];
    [self addSubview:self.progress];
    
    self.persent = [[UILabel alloc] initWithFrame:CGRectMake(self.width - 40, 0, 40, 40)];
    [self addSubview:self.persent];
    
    self.nums.textAlignment = NSTextAlignmentCenter;
    self.name.textAlignment = NSTextAlignmentRight;
    self.progress.tintColor = MainColor;
    
//    self.name.backgroundColor = self.persent.backgroundColor = UIColor.grayColor;
}
@end
