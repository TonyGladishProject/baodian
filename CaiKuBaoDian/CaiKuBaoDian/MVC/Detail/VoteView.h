//
//  VoteView.h
//  CaiKuBaoDian
//
//  Created by mac on 27/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "CollectionView.h"
#import "SXSelectedView.h"

NS_ASSUME_NONNULL_BEGIN

@interface VoteView : CollectionView
@property ( nonatomic, strong ) UIView * fatherView;
@property ( nonatomic, strong ) UIButton * button;
@property ( nonatomic, strong ) SXSelectedView * selectedView;
@property (nonatomic, copy)  void(^successBlock) (void);
- (void)dataToView:(NSString *)str;
@end

NS_ASSUME_NONNULL_END
