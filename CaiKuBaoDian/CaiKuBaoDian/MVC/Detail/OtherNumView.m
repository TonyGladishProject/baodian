//
//  OtherNumView.m
//  CaiKuBaoDian
//
//  Created by mac on 10/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "OtherNumView.h"

@implementation OtherNumView

+(instancetype)newView
{
    return [[OtherNumView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 60)];
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self createUI];
    }
    return self;
}

- (void)createUI
{
    float x = 20;
    float w = (WIDTH - 60)/2;
    
    self.btnPrevious = [UIButton buttonWithType:UIButtonTypeCustom];
    self.btnPrevious.frame = CGRectMake(x, 10, w, 40);
    [self addSubview:self.btnPrevious];
    [self.btnPrevious setTitle:@"上一期" forState:UIControlStateNormal];
    [self.btnPrevious layerMask5];
    [self.btnPrevious setBackgroundImage:[Tools imageWithColor:UIColorFromRGB(0xFF5000) size:self.btnPrevious.size] forState:UIControlStateNormal];
    [self.btnPrevious setBackgroundImage:[Tools imageWithColor:UIColorFromRGB(0xFFDCCC) size:self.btnPrevious.size] forState:UIControlStateDisabled];
    
    self.btnNext = [UIButton buttonWithType:UIButtonTypeCustom];
    self.btnNext.frame = CGRectMake(x + w +x, 10, w, 40);
    [self addSubview:self.btnNext];
    [self.btnNext setTitle:@"下一期" forState:UIControlStateNormal];
    [self.btnNext layerMask5];
    [self.btnNext setBackgroundImage:[Tools imageWithColor:UIColorFromRGB(0xFF5000) size:self.btnPrevious.size] forState:UIControlStateNormal];
    [self.btnNext setBackgroundImage:[Tools imageWithColor:UIColorFromRGB(0xFFDCCC) size:self.btnPrevious.size] forState:UIControlStateDisabled];
    
}

@end
