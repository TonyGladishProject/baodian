//
//  MoreCommentCell.m
//  CaiKuBaoDian
//
//  Created by mac on 03/06/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "MoreCommentCell.h"
#import "CommentVC.h"

@implementation MoreCommentCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI
{
    self.moreLabel = [[UILabel alloc] initWithFrame:self.bounds];
    [self.contentView addSubview:self.moreLabel];
    self.moreLabel.textAlignment = NSTextAlignmentCenter;
    self.moreLabel.textColor = viewBlueColor;
    self.moreLabel.text = @"继续查看更多评论>>";
    [self.moreLabel touchEvents:self action:@selector(moreTouch)];
}

- (void)moreTouch
{
    CommentVC * c = [CommentVC new];
    c.scene = self.scene;
    c.scene_id = self.scene_id;
    c.comment_id = self.comment_id;
    [self.navigationController pushViewController:c animated:YES];
}

@end
