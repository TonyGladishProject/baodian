//
//  DetailVC.h
//  CaiKuBaoDian
//
//  Created by mac on 26/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "ScrollViewVC.h"
#import "LotteryNumsView.h"
#import "InfoView.h"
#import "WebLabel.h"
#import "VoteView.h"
#import "ShareView.h"
#import "NoDataLabel.h"
#import "CommentView.h"
#import "CommentList.h"
#import "YTKKeyValueStore.h"

NS_ASSUME_NONNULL_BEGIN

@interface DetailVC : ScrollViewVC
@property ( nonatomic, strong ) LotteryNumsView * numsView;
@property ( nonatomic, strong ) InfoView * infoView;
@property ( nonatomic, strong ) UIWebView * webView;
@property ( nonatomic, strong ) UIWebView * headerView;
@property ( nonatomic, strong ) UIWebView * footerView;
@property ( nonatomic, strong ) VoteView * voteView;
@property ( nonatomic, strong ) ShareView * shareView;
@property ( nonatomic, strong ) NoDataLabel * noDataLabel;
@property ( nonatomic, strong ) CommentView * commentView;
@property ( nonatomic, strong ) CommentList * listView;
@property ( nonatomic, copy ) NSString * name;
@property ( nonatomic, copy ) NSString * idHistory;
@property ( nonatomic, copy ) NSString * type_id;
@property ( nonatomic, copy ) NSString * scene;
@property ( nonatomic, strong ) YTKKeyValueStore * store;
- (void)dataMain;
- (void)dataComment;
- (void)dataHeader;
- (CGFloat)tableHeight;
@end

NS_ASSUME_NONNULL_END
