//
//  CommentVC.h
//  CaiKuBaoDian
//
//  Created by mac on 04/06/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "TableViewVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface CommentVC : TableViewVC
@property ( nonatomic, copy ) NSString * scene;
@property ( nonatomic, copy ) NSString * scene_id;
@property ( nonatomic, copy ) NSString * comment_id;
@end

NS_ASSUME_NONNULL_END
