//
//  ShareView.m
//  CaiKuBaoDian
//
//  Created by mac on 27/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "ShareView.h"

@implementation ShareView

+(instancetype)newView
{
    return [[ShareView alloc] initWithFrame:CGRectMake(0, HEIGHT, WIDTH, 130)];
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self createUI];
    }
    return self;
}

- (void)createUI
{
    NSArray * arrI = @[@"weixin",@"timeline",@"QQ"];
    NSArray * arrL = @[@"微信",@"朋友圈",@"QQ"];
    
    CGFloat w = 40;
    CGFloat x = (WIDTH - w * 3)/4;
    for (int i = 0; i < 3; i++) {
        UIView * view = [[UIView alloc] initWithFrame:CGRectMake(x + i *(w + x), 45, w, 64)];
        [self addSubview:view];
        [view touchEvents:self action:@selector(viewTouch:)];
        view.tag = 1000 + i;
        
        UIImageView * img = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, w, w)];
        [view addSubview:img];
        
        UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(0, w, w, 64 - w)];
        [view addSubview:label];
        
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [UIFont systemFontOfSize:12];
        img.image = [UIImage imageNamed:arrI[i]];
        label.text = arrL[i];
        
        [self.dataArray addObject:view];
    }
}

- (void)viewTouch:(UITapGestureRecognizer *)tap
{
    UIView *view = (UIView*) tap.view;
    [self shareLinkWithPlatform:[Tools showPlatform:view.tag]];
}

- (void)shareLinkWithPlatform:(JSHAREPlatform)platform {
    JSHAREMessage *message = [JSHAREMessage message];
    message.mediaType = JSHARELink;
    message.url = self.shareStr;
    message.platform = platform;
    [JSHAREService share:message handler:^(JSHAREState state, NSError *error) {
        [Tools showAlertWithState:state error:error];
    }];
}

- (void)turnBackKeyboard
{
    [self.textFieldBase resignFirstResponder];
    [self.textViewBase resignFirstResponder];
    self.textFieldBase.placeholder = @"说点什么吧.";
}

- (NSMutableArray *)dataArray
{
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}
@end
