//
//  VoteCCell.h
//  CaiKuBaoDian
//
//  Created by mac on 27/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "BaseCollectionCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface VoteCCell : BaseCollectionCell
@property ( nonatomic, strong ) UILabel * name;
@property ( nonatomic, strong ) UIProgressView * progress;
@property ( nonatomic, strong ) UILabel * nums;
@property ( nonatomic, strong ) UILabel * persent;
@property ( nonatomic, strong ) SXModel * model;
@end

NS_ASSUME_NONNULL_END
