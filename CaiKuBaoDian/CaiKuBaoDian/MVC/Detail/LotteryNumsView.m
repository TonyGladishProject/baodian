//
//  LotteryNumsView.m
//  CaiKuBaoDian
//
//  Created by mac on 26/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "LotteryNumsView.h"
#import "HistoryVC.h"

@implementation LotteryNumsView

+(instancetype)newView
{
    return [[LotteryNumsView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, WIDTH/3)];
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self createUI];
    }
    return self;
}

- (void)createUI
{
    CGFloat h = WIDTH/3;
    CGFloat w = WIDTH/2;
    self.count = 8;
    
    [self touchEvents:self action:@selector(numbersClick)];
    
    UIImageView * img = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, WIDTH - 20, h - 10)];
    img.center = CGPointMake(WIDTH/2, h/2);
    img.image = [UIImage imageNamed:@"menu_lottery_bg"];
    [self addSubview:img];
    
    UIImageView * imgA = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0,  20, 20)];
    imgA.center = CGPointMake(WIDTH -  25, h/2);
    imgA.image = [UIImage imageNamed:@"right"];
    [self addSubview:imgA];
    
    self.arrN = [NSMutableArray arrayWithCapacity:8];
    self.arrL = [NSMutableArray arrayWithCapacity:8];
    self.arrI  = [NSMutableArray arrayWithCapacity:8];
    CGFloat ww = 0.f;
    CGFloat xx = 0.f;
    CGFloat hh = 0.f;
    CGFloat textF = 17.f;
    CGFloat hhh = textF;
    if (iphone5) {
        ww = 28;
        xx = 2;
        hh = 5;
        textF = 13.f;
        hhh = textF;
    } else if (iphone8Plus) {
        ww = 38;
        xx = 5;
        hh = 13;
        hhh = 22;
    } else {
        ww = 33;
        xx = 4;
        hh = 10;
    }
    
    self.labelNo = [[UILabel alloc] initWithFrame:CGRectMake(0, h/2 - ww/2 - hh - 3 - 30, 300, 40)];
    self.labelNo.centerX = w;
    self.labelNo.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.labelNo];
    
    self.labelNoNext = [[UILabel alloc] initWithFrame:CGRectMake(0, h/2  + hh/2 + hhh, 300, 40)];
    self.labelNoNext.centerX = w;
    self.labelNoNext.textAlignment = NSTextAlignmentCenter;
    self.labelNoNext.textColor = UIColor.grayColor;
    self.labelNoNext.font = [UIFont systemFontOfSize:13];
    [self addSubview:self.labelNoNext];
    
    for (int i = 0; i < self.count; i++) {
        
        UILabel * labN = [[UILabel alloc] initWithFrame:CGRectMake(36 + (ww + xx) * i, h/2 - ww/2 - hh -3, ww, ww)];
        [self.arrN addObject:labN];
        [self addSubview:labN];
        
        
        UILabel * lab = [[UILabel alloc] initWithFrame:CGRectMake(38 + (ww + xx) * i, h/2  + hh/2 , ww, ww)];
        [self.arrL addObject:lab];
        [self addSubview:lab];
        
        lab.textAlignment  = labN.textAlignment  = NSTextAlignmentCenter;
        lab.font = labN.font = [UIFont systemFontOfSize:textF];
        
        UIImageView * imgs = [[UIImageView alloc] initWithFrame:CGRectMake(38 + (ww + xx) * i, h/2 - ww/2 - hh , ww, ww)];
        [self.arrI addObject:imgs];
        [self addSubview:imgs];
    }
}

- (void)numbersClick
{
    [self pushVC:[HistoryVC new]];
}

- (void)dataToView:(NSDictionary *)data
{
    self.labelNo.text = [NSString stringWithFormat:@"第%@开奖结果",data[@"期数"]];
    self.labelNoNext.text = data[@"nextopentime"];
    for (int i = 0; i < 6; i++) {
        UILabel * l = self.arrN[i];
        l.text = data[@"haoma"][i];
        
        UILabel * la = self.arrL[i];
        la.text = data[@"shengxiao"][i];
        
        UIImageView * imgs = self.arrI[i];
        imgs.image = [UIImage imageNamed:[self colorFromString:data[@"yanse"][i]]];
    }
    
    
    UIImageView * imgA = self.arrI[6];
    imgA.image = [UIImage imageNamed:@"icon_live_add"];
    
    UIImageView * imgL = self.arrI.lastObject;
    imgL.image = [UIImage imageNamed:[self colorFromString:data[@"yanse"][6]]];
    
    
    UILabel * l = self.arrN.lastObject;
    l.text = data[@"haoma"][6];
    
    UILabel * la = self.arrL.lastObject;
    la.text = data[@"shengxiao"][6];
}

- (NSString *)colorFromString:(NSString *)str
{
    if ([str isEqualToString:@"lan"]) {
        return @"blueball";
    } else if ([str isEqualToString:@"lv"]) {
        return @"greenball";
    }
    return @"redball";
}
@end
