//
//  CommentCell.m
//  CaiKuBaoDian
//
//  Created by mac on 29/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "CommentCell.h"

@implementation CommentCell

- (void)setModel:(CommentModel *)model
{
    _model = model;
    
    [self.header sd_setImageWithURL:[NSURL URLWithString:model.avatar] placeholderImage:[UIImage imageNamed:@"placeHolder0"]];
    self.name.text = model.user_nicename;
    CGFloat w = [Tools getStrWidth:self.name.text height:20 font:14];
    self.replyName.text = model.reply;
    self.replyWords.x = 74 + w;
    self.replyName.x = self.replyWords.right + 10;
    if (model.reply.length > 1) {
        self.replyWords.hidden = NO;
    } else {
        self.replyWords.hidden = YES;
    }
    self.time.text = [Tools timeFromtimeStamp:model.create_time];
    self.comment.text = model.content;
    self.like.text = model.zhan_number;
    [Tools setLabelHeight:self.comment str:model.content width:WIDTH - 74 font:14];
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.arrow.hidden = YES;
        self.line.hidden = YES;
        
        self.header = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 44, 44)];
        [self.contentView addSubview:self.header];
        
        self.name = [[UILabel alloc] initWithFrame:CGRectMake(64, 12, 200, 20)];
        [self.contentView addSubview:self.name];
        
        self.replyWords = [[UILabel alloc] initWithFrame:CGRectMake(74, 12, 27, 20)];
        [self.contentView addSubview:self.replyWords];
        
        self.replyName = [[UILabel alloc] initWithFrame:CGRectMake(120, 12, 200, 20)];
        [self.contentView addSubview:self.replyName];
        
        self.time = [[UILabel alloc] initWithFrame:CGRectMake(64, 36, 200, 20)];
        [self.contentView addSubview:self.time];
        
        self.comment = [[UILabel alloc] initWithFrame:CGRectMake(64, 60, WIDTH - 64 - 10, 30)];
        [self.contentView addSubview:self.comment];
        
        self.reply = [[UILabel alloc] initWithFrame:CGRectMake(180, 30, 60, 30)];
        [self.contentView addSubview:self.reply];
        
        self.like = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH - 10 - 20, 10, 20, 20)];
        [self.contentView addSubview:self.like];
        
        self.imgLike = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH - 40 -20, 10, 20, 20)];
        [self.contentView addSubview:self.imgLike];
        
        self.viewLike = [[UIView alloc] initWithFrame:CGRectMake(WIDTH - 10 - 80, 0, 80, 40)];
        [self.contentView addSubview:self.viewLike];
        
        [self.viewLike touchEvents:self action:@selector(doLike)];
        self.imgLike.image = [UIImage imageNamed:@"ic_zan"];
        self.like.text = @"0";
        self.comment.numberOfLines = 0;
        self.name.font = self.replyName.font =self.comment.font = self.reply.font = [UIFont systemFontOfSize:14];
        self.replyWords.textColor =self.time.textColor = cellWordColor;
        self.replyWords.font =self.time.font = [UIFont systemFontOfSize:13];
        self.reply.textAlignment = NSTextAlignmentRight;
        [self.reply touchEvents:self action:@selector(replyClick)];
        self.replyWords.text =self.reply.text = @"回复";
    }
    return self;
}

- (void)doLike
{
    [HTTPTool postWithURLLogin:APILTDLike parameters:@{@"full_name":UserPhone,@"scene":self.model.scene,@"scene_id":self.model.id} success:^(id  _Nonnull responseObject) {
        NSNumber * num = responseObject[@"data"][@"type"];
        NSString * str = [[NSNumberFormatter new] stringFromNumber:num];
        int  i = self.model.zhan_number.intValue;
        if (str.intValue == 1) {
            i++;
        } else {
            i--;
        }
        self.model.zhan_number = [NSString stringWithFormat:@"%d",i];
        self.like.text = self.model.zhan_number;
    }];
}

- (void)replyClick
{
    if (self.cellBlock) {
        self.cellBlock(self.model);
    }
}

@end
