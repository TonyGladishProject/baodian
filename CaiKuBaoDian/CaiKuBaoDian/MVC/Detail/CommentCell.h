//
//  CommentCell.h
//  CaiKuBaoDian
//
//  Created by mac on 29/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "BaseCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface CommentCell : BaseCell
@property ( nonatomic, strong ) UIImageView * header;
@property ( nonatomic, strong ) UILabel * name;
@property ( nonatomic, strong ) UILabel * replyWords;
@property ( nonatomic, strong ) UILabel * replyName;
@property ( nonatomic, strong ) UILabel * time;
@property ( nonatomic, strong ) UILabel * comment;
@property ( nonatomic, strong ) UILabel * reply;
@property ( nonatomic, strong ) UILabel * like;
@property ( nonatomic, strong ) UIImageView * imgLike;
@property ( nonatomic, strong ) UIView * viewLike;
@property ( nonatomic, strong ) CommentModel * model;
@property (nonatomic, copy)  void(^cellBlock) (CommentModel * model);
@end

NS_ASSUME_NONNULL_END
