//
//  OtherNumView.h
//  CaiKuBaoDian
//
//  Created by mac on 10/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "BaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface OtherNumView : BaseView
@property ( nonatomic, strong ) UIButton * btnPrevious;
@property ( nonatomic, strong ) UIButton * btnNext;
@end

NS_ASSUME_NONNULL_END
