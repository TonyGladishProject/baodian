//
//  NoDataCell.h
//  CaiKuBaoDian
//
//  Created by mac on 10/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "PureCell.h"
#import "NoDataLabel.h"

NS_ASSUME_NONNULL_BEGIN

@interface NoDataCell : PureCell
@property ( nonatomic, strong ) NoDataLabel * noDataLabel;
@end

NS_ASSUME_NONNULL_END
