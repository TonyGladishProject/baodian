//
//  SXSelectedView.m
//  CaiKuBaoDian
//
//  Created by mac on 30/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "SXSelectedView.h"

@implementation SXSelectedView

+(instancetype)newView
{
    return [[SXSelectedView alloc] initWithFrame:CGRectMake(0, 0, 280, 255)];
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self createUI];
    }
    return self;
}

- (void)createUI
{
    self.label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.width, 44)];
    [self addSubview:self.label];
    
    self.arrBtns = [NSMutableArray array];
    for (int i = 0; i < 12; i++) {
        int x = i%3;
        int y = i/3;
        UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(10 + 90 * x, 44 + 40 * y, 80, 35);
        [button setTitleColor:UIColor.blackColor forState:UIControlStateNormal];
        [button setTitleColor:UIColor.whiteColor forState:UIControlStateSelected];
        [button setBackgroundImage:[Tools imageWithColor:UIColor.clearColor size:button.size] forState:UIControlStateNormal];
        [button setBackgroundImage:[Tools imageWithColor:UIColorFromRGB(0xE78B02) size:button.size] forState:UIControlStateSelected];
        [button layerBorderWidth:1 color:UIColor.grayColor];
        [button layerMask5];
        [self addSubview:button];
        [self.arrBtns addObject:button];
        [button addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    self.button = [UIButton buttonWithType:UIButtonTypeCustom];
    self.button.frame = CGRectMake(self.width/2 - 60, self.height - 48, 120, 40);
    [self addSubview:self.button];
    [self.button setBackgroundColor:MainColor];
    [self.button setTitle:@"投票" forState:UIControlStateNormal];
    [self.button layerMask5];
    [self.button addTarget:self action:@selector(voteClick:) forControlEvents:UIControlEventTouchUpInside];
    
    self.label.textAlignment = NSTextAlignmentCenter;
    self.backgroundColor = UIColor.whiteColor;
}

- (void)dataToView:(NSArray *)array time:(NSString *)str
{
    self.label.text = str;
    for (int i = 0; i < array.count; i++) {
        UIButton * button = self.arrBtns[i];
        SXModel * m = array[i];
        [button setTitle:[NSString stringWithFormat:@"%@(%@票)",m.name,m.num] forState:UIControlStateNormal];
    }
}

- (void)btnClick:(UIButton *)btn
{
    NSString * str = [btn.titleLabel.text substringToIndex:1];
    if (btn.selected) {
        btn.selected = !btn.selected;
        [self.arrSelected removeObject:str];
    } else if (self.arrSelected.count < 3) {
        [self.arrSelected addObject:str];
         btn.selected = !btn.selected;
    }
}

- (void)voteClick:(UIButton *)button
{
    if (self.arrSelected.count < 3) {
        [MBProgressHUD showMessage:@"请选择3个生肖"];
        return;
    }
    [self doVote];
}

- (void)doVote
{
    NSString * string = @"";
    for (NSString * str in self.arrSelected) {
        if (string.length < 1) {
            string = str;
        } else {
            string = [string stringByAppendingString:[NSString stringWithFormat:@",%@",str]];
        }
    }
    [self.popup dismiss:YES];
    [HTTPTool postWithURLIndicatorMSG:APIDetailVote parameters:@{@"type":@"4",@"poll":string,@"username":UserPhone} success:^(id  _Nonnull responseObject) {
        if (self.successBlock) {
            self.successBlock();
        }
    }];
}

- (NSMutableArray *)arrSelected
{
    if (!_arrSelected) {
        _arrSelected = [NSMutableArray array];
    }
    return _arrSelected;
}
@end
