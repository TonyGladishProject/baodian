//
//  DetailVC.m
//  CaiKuBaoDian
//
//  Created by mac on 26/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "DetailVC.h"

@interface DetailVC ()<UIWebViewDelegate>
@property ( nonatomic, copy ) NSString * header;
@property ( nonatomic, copy ) NSString * footer;
@end

@implementation DetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
 
    self.store = [[YTKKeyValueStore alloc] initDBWithName:@"Caiku.db"];
    [self.store createTableWithName:@"Detail"];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(noti:) name:@"refreshLottery" object:nil];
    self.titleLabel.text = self.name;
    [self refreshUI];
    [self dataHud];
    [self dataMJ];
    [self showLocData];
}

- (void)showLocData
{
    id responseObject =
    [self.store getObjectById:[@"APIDetailShengXiao" stringByAppendingString:self.idStr] fromTable:@"Detail"];
    if ([responseObject isKindOfClass:[NSDictionary class]]) {
        [MBProgressHUD hideHUDForView:self.view];
        id responseObjectNums =
        [self.store getObjectById:[@"APILotteryNums" stringByAppendingString:self.idStr] fromTable:@"Detail"];
        id responseObjectCmt =
        [self.store getObjectById:[@"APIComment" stringByAppendingString:self.idStr] fromTable:@"Detail"];
        id responseObjectHdr =
        [self.store getObjectById:[@"APIDetailHeader" stringByAppendingString:self.idStr] fromTable:@"Detail"];
        
        [self.numsView dataToView:responseObjectNums[@"data"]];
        [self locToUserView];
        [self cmtDataToView:responseObjectCmt];
        [self hdrDataToView:responseObjectHdr];
        [self sxDataToView:responseObject];
    }
}

- (void)locToUserView
{
    id responseObjectUser =
    [self.store getObjectById:[@"APIDetailUserInfo" stringByAppendingString:self.idStr] fromTable:@"Detail"];
    [self userDataToView:responseObjectUser];
}

- (void)noti:(NSNotification *)noti
{
    [self.numsView dataToView:noti.userInfo];
}

#pragma mark - data
- (void)mjHeader
{
    [HTTPTool getWithURL:APILotteryNums success:^(id  _Nonnull responseObject) {
        [self dataMain];
        [self numsDataToView:responseObject];
        [self.store putObject:responseObject withId:[@"APILotteryNums" stringByAppendingString:self.idStr] intoTable:@"Detail"];
    }];
}

- (void)numsDataToView:(id)responseObject
{
    [self.numsView dataToView:responseObject[@"data"]];
    NSDictionary * data = responseObject[@"data"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"lotteryInfo" object:nil userInfo:@{@"timeprompt":data[@"timeprompt"],@"num":data[@"haoma"][6],@"qs":data[@"期数"]}];
}

- (void)dataMain
{
    [HTTPTool postWithURL:APIDetailUserInfo parameters:@{@"id":self.idStr} success:^(id  _Nonnull responseObject) {
        [self dataComment];
        [self userDataToView:responseObject];
        [self.store putObject:responseObject withId:[@"APIDetailUserInfo" stringByAppendingString:self.idStr] intoTable:@"Detail"];
    }];
}

- (void)userDataToView:(id)responseObject
{
    NSDictionary * dic = responseObject[@"data"];
    [self.infoView dataToView:dic];
    [self.webView loadHTMLString:dic[@"msg"] baseURL:nil];
    self.listView.comment_id = self.commentView.idStr = dic[@"id"];
    self.shareView.shareStr = dic[@"share_url"];
    self.idHistory = dic[@"userinfo"][@"mobile"];
    self.type_id = dic[@"type_id"];
}

- (void)dataComment
{
    [HTTPTool postWithURL:APIComment parameters:@{@"scene":self.scene,@"scene_id":self.idStr,@"page":@"1"} success:^(id  _Nonnull responseObject) {
        [self dataHeader];
        [self cmtDataToView:responseObject];
        [self.store putObject:responseObject withId:[@"APIComment" stringByAppendingString:self.idStr] intoTable:@"Detail"];
    }];
}

- (void)cmtDataToView:(id)responseObject
{
    self.listView.scene = self.scene;
    self.listView.scene_id = self.idStr;
    [self.listView.dataArray removeAllObjects];
    for (NSDictionary * dic in responseObject[@"data"]) {
        CommentModel * model = [CommentModel mj_objectWithKeyValues:dic];
        [self.listView.dataArray addObject:model];
        for (NSDictionary * child in dic[@"child"]) {
            CommentModel * mo= [CommentModel mj_objectWithKeyValues:child];
            mo.reply = model.user_nicename;
            [self.listView.dataArray addObject:mo];
        }
    }
    self.listView.height = self.listView.tableView.height = [self tableHeight];
    [self.listView.tableView reloadData];
}

- (void)dataHeader
{
    [HTTPTool postWithURL:APIDetailHeader parameters:@{} success:^(id  _Nonnull responseObject) {
        [self dataSX];
        [self hdrDataToView:responseObject];
        [self.store putObject:responseObject withId:[@"APIDetailHeader" stringByAppendingString:self.idStr] intoTable:@"Detail"];
    }];
}

- (void)hdrDataToView:(id)responseObject
{
    NSDictionary * data = responseObject[@"data"];
    NSString * header = data[@"header"];
    NSString * footer = data[@"footer"];
    
    if (header.length > 0) {
        self.header = header;
    }
    if (footer.length > 0) {
        self.footer = footer;
        [self.footerView loadHTMLString:footer baseURL:nil];
    }
}

- (void)dataSX
{
    [HTTPTool postWithURL:APIDetailShengXiao parameters:@{} success:^(id  _Nonnull responseObject) {
        [self sxDataToView:responseObject];
        [self.store putObject:responseObject withId:[@"APIDetailShengXiao" stringByAppendingString:self.idStr] intoTable:@"Detail"];
    }];
}

- (void)sxDataToView:(id)responseObject
{
    NSDictionary * dic = responseObject[@"data"];
    NSDictionary * data = dic[@"data"];
    NSDictionary * sx = data[@"shengxiao"];
    NSDictionary * p = sx[@"ps"];
    
    for (int i = 0; i < self.voteView.dataArray.count; i++) {
        SXModel * m = self.voteView.dataArray[i];
        m.num = p[m.id];
        m.persent = sx[m.id];
    }
    [self.voteView dataToView:dic[@"qs"]];
    [MBProgressHUD hideHUDForView:self.view];
    [self endRefreshing];
    [self.voteView.collectionView reloadData];
    [self refreshHeight];
}

#pragma mark - ui
- (void)refreshUI
{
    self.scrollView.height = self.contentHeight - 60;
    self.numsView = [LotteryNumsView newView];
    [self.scrollView addSubview:self.numsView];
    self.numsView.navigationController = self.navigationController;
    
    self.infoView = [InfoView newView];
    self.infoView.y = self.numsView.bottom;
    [self.scrollView addSubview:self.infoView];
    
    self.webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, self.infoView.bottom, WIDTH, WIDTH)];
    [self.scrollView addSubview:self.webView];
    [self.webView.scrollView addObserver:self forKeyPath:@"contentSize" options:NSKeyValueObservingOptionNew context:nil];
    
    self.footerView = [[UIWebView alloc] initWithFrame:CGRectMake(0, self.webView.bottom, WIDTH, 110)];
    self.footerView.scrollView.scrollEnabled = NO;
    [self.footerView.scrollView addObserver:self forKeyPath:@"footerSize" options:NSKeyValueObservingOptionNew context:nil];
    self.footerView.delegate = self;
    
    kSelfWeak
    self.voteView = [VoteView newView];
    self.voteView.y = HEIGHT;
    self.voteView.fatherView = self.view;
    [self.scrollView addSubview:self.voteView];
    self.voteView.successBlock = ^{
        [weakSelf dataSX];
    };
    
    self.shareView = [ShareView newView];
    self.shareView.backgroundColor = UIColor.whiteColor;
    [self.scrollView addSubview:self.shareView];
    
    self.listView = [[CommentList alloc] initWithFrame:CGRectMake(0, self.noDataLabel.bottom, WIDTH, 200)];
    self.listView.navigationController = self.navigationController;
    self.listView.cellBlock = ^(CommentModel * _Nonnull model) {
        weakSelf.commentView.model = model;
        [weakSelf.commentView.textField becomeFirstResponder];
        weakSelf.commentView.textField.placeholder = [NSString stringWithFormat:@"回复 %@",model.user_nicename];
    };
    self.noDataLabel = [NoDataLabel newView];
    
    self.commentView = [CommentView newView];
    [self.contentView addSubview:self.commentView];
    self.commentView.doCommentBlock = ^{
        [weakSelf doComment];
    };
    
    self.listView.textFieldBase = self.noDataLabel.textFieldBase = self.voteView.textFieldBase = self.shareView.textFieldBase = self.commentView.textField;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"contentSize"]) {
        self.webView.height = self.webView.scrollView.contentSize.height;
        [self refreshHeight];
    } else if ([keyPath isEqualToString:@"footerSize"]) {
        self.footerView.height = self.footerView.scrollView.contentSize.height;
        [self refreshHeight];
        NSLog(@"--------footerH-%f",self.footerView.height);
    }
}

- (void)doComment
{
    NSDictionary * para;
    if ([self.commentView.textField.placeholder isEqualToString:@"说点什么吧."]) {
        para = @{@"full_name":UserPhone,@"content":self.commentView.textField.text,@"scene":self.scene,@"scene_id":self.idStr};
    } else {
        para = @{@"full_name":UserPhone,@"content":self.commentView.textField.text,@"scene":self.scene,@"scene_id":self.idStr,@"parent_id":self.commentView.model.id};
    }
    [self.commentView.textField resignFirstResponder];
    self.commentView.textField.text = @"";
    self.commentView.textField.placeholder = @"说点什么吧.";
    [HTTPTool postWithURLIndicatorStatus:APIPostComment parameters:para success:^(id  _Nonnull responseObject) {
        [self dataComment];
    }];
}

- (void)refreshHeight
{
    self.voteView.y = self.webView.bottom;
    if (self.footer.length > 0) {
        [self.scrollView addSubview:self.footerView];
        self.footerView.y = self.webView.bottom;
        self.voteView.y = self.footerView.bottom;
        self.footerView.backgroundColor = UIColor.redColor;
        NSLog(@"-----footer---%f",self.footerView.height);
    }
    self.shareView.y = self.voteView.bottom;
    self.noDataLabel.y = self.shareView.bottom;
    self.listView.y = self.shareView.bottom;
    
    if (self.listView.dataArray.count > 0) {
        [self.scrollView addSubview:self.listView];
        self.contentHeight = self.listView.bottom;
    } else {
        [self.scrollView addSubview:self.noDataLabel];
        self.contentHeight = self.noDataLabel.bottom;
    }
}

- (CGFloat)tableHeight
{
    CGFloat totalH = 0.0;
    for (int i = 0; i < self.listView.dataArray.count; i++) {
        CGFloat cellH = 90;
        CommentModel * model = self.listView.dataArray[i];
        CGFloat h = [Tools getStrHeight:model.content width:WIDTH - 74 font:14];
        if (h < 21) {
            cellH = 90;
        } else {
            cellH = 70 + h;
        }
        totalH += cellH;
    }
    return totalH + 50;
}

#pragma mark - webViewDelegate
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    //判断是否是单击
    if (navigationType == UIWebViewNavigationTypeLinkClicked)
    {
        NSURL *url = [request URL];
        if([[UIApplication sharedApplication]canOpenURL:url])
        {
            WebVC * w = [WebVC new];
            w.titleName = @"679彩票";
            w.urlStr = [url absoluteString];
            [self.navigationController pushViewController:w animated:YES];
        }
        return NO;
    }
    return YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    webView.height = webView.scrollView.contentSize.height;
}
-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self.webView.scrollView removeObserver:self forKeyPath:@"contentSize" context:nil];
    [self.footerView.scrollView removeObserver:self forKeyPath:@"footerSize" context:nil];
}
@end
