//
//  CommentList.m
//  CaiKuBaoDian
//
//  Created by mac on 29/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "CommentList.h"
#import "CommentCell.h"
#import "MoreCommentCell.h"

@implementation CommentList

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self registerClass:[CommentCell class]];
        [self registerClass:[MoreCommentCell class]];
    }
    return self;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 1) {
        return 1;
    }
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1) {
        MoreCommentCell * cell = [tableView dequeueReusableCellWithIdentifier:@"MoreCommentCell" forIndexPath:indexPath];
        cell.navigationController = self.navigationController;
        cell.scene = self.scene;
        cell.scene_id = self.scene_id;
        cell.comment_id = self.comment_id;
        return cell;
    }
    CommentCell * cell = [tableView dequeueReusableCellWithIdentifier:@"CommentCell" forIndexPath:indexPath];
    cell.model = self.dataArray[indexPath.row];
    kSelfWeak
    cell.cellBlock = ^(CommentModel * _Nonnull model) {
        if (weakSelf.cellBlock) {
            weakSelf.cellBlock(model);
        }
    };
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1) {
        return 50;
    }
    CGFloat cellH = 90;
    CommentModel * model = self.dataArray[indexPath.row];
    CGFloat h = [Tools getStrHeight:model.content width:WIDTH - 74 font:14];
    if (h < 21) {
        cellH = 90;
    } else {
        cellH = 70 + h;
    }
    return cellH;
}

- (void)turnBackKeyboard
{
    [self.textFieldBase resignFirstResponder];
    [self.textViewBase resignFirstResponder];
    self.textFieldBase.placeholder = @"说点什么吧.";
}
@end
