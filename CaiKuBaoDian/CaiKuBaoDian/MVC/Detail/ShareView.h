//
//  ShareView.h
//  CaiKuBaoDian
//
//  Created by mac on 27/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "BaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface ShareView : BaseView
@property ( nonatomic, strong ) UILabel * label;
@property ( nonatomic, strong ) NSMutableArray *dataArray;
@property ( nonatomic, copy ) NSString * shareStr;
@end

NS_ASSUME_NONNULL_END
