//
//  SXSelectedView.h
//  CaiKuBaoDian
//
//  Created by mac on 30/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "BaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface SXSelectedView : BaseView
@property ( nonatomic, strong ) UILabel * label;
@property ( nonatomic, strong ) NSMutableArray * arrBtns;
@property ( nonatomic, strong ) NSMutableArray * arrSelected;
@property ( nonatomic, strong ) UIButton * button;
- (void)dataToView:(NSArray *)array time:(NSString *)str;
@property (nonatomic, copy)  void(^successBlock) (void);
@end

NS_ASSUME_NONNULL_END
