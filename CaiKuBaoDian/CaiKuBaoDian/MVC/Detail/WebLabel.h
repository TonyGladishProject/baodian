//
//  WebLabel.h
//  CaiKuBaoDian
//
//  Created by mac on 27/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "BaseLabel.h"

NS_ASSUME_NONNULL_BEGIN

@interface WebLabel : BaseLabel

- (CGFloat)dataToView:(NSString *)text;
- (CGFloat)dataToView:(NSString *)text width:(CGFloat)width font:(CGFloat)font;

- (CGFloat)dataToViewLLCell:(NSString *)text;
@end

NS_ASSUME_NONNULL_END
