//
//  VoteView.m
//  CaiKuBaoDian
//
//  Created by mac on 27/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "VoteView.h"
#import "VoteCCell.h"

@implementation VoteView

+(instancetype)newView
{
    return [[VoteView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 324)];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self registerClass:[VoteCCell class]];
        self.collectionView.backgroundColor = UIColor.whiteColor;
        NSArray * arr = @[
                                    @{@"shu":@"鼠"},@{@"niu":@"牛"},@{@"hu":@"虎"},
                                    @{@"tu":@"兔"},@{@"long":@"龙"},@{@"she":@"蛇"},
                                    @{@"ma":@"马"},@{@"yang":@"羊"},@{@"hou":@"猴"},
                                    @{@"ji":@"鸡"},@{@"gou":@"狗"},@{@"zhu":@"猪"},
                                        ];
        for (int i = 0; i < arr.count; i++) {
            SXModel * m = [SXModel new];
            NSDictionary * dic = arr[i];
            m.id = dic.allKeys[0];
            m.name = dic.allValues[0];
            m.num = @"0";
            m.persent = @"0";
            [self.dataArray addObject:m];
        }
        
        [self createButton];
        
        kSelfWeak
        self.selectedView = [SXSelectedView newView];
        self.selectedView.successBlock = ^{
            if (weakSelf.successBlock) {
                weakSelf.successBlock();
            }
        };
    }
    return self;
}

- (void)createButton
{
    self.button = [UIButton buttonWithType:UIButtonTypeCustom];
    self.button.frame = CGRectMake(WIDTH/2 - 35, 260 + 12, 70, 40);
    [self addSubview:self.button];
    self.button.backgroundColor = MainColor;
    [self.button setTitle:@"投票" forState:UIControlStateNormal];
    [self.button layerMask5];
    [self.button addTarget:self action:@selector(showPop) forControlEvents:UIControlEventTouchUpInside];
    
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, self.height - 1, WIDTH, 1)];
    view.backgroundColor = loginLineColor;
    [self addSubview:view];
}

- (void)showPop
{
    [self.selectedView popAddSuperViewCenter:self.fatherView center:centerPoint];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    VoteCCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"VoteCCell" forIndexPath:indexPath];
    cell.model = self.dataArray[indexPath.row];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return  CGSizeMake((WIDTH - 30)/2, 40);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(10, 10,10, 10);//（上、左、下、右）
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 9.5;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}

- (void)turnBackKeyboard
{
    [self.textFieldBase resignFirstResponder];
    [self.textViewBase resignFirstResponder];
    self.textFieldBase.placeholder = @"说点什么吧.";
}

- (void)dataToView:(NSString *)str
{
    [self.selectedView dataToView:self.dataArray time:str];
}

@end
