//
//  CommentView.m
//  CaiKuBaoDian
//
//  Created by mac on 29/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "CommentView.h"

@implementation CommentView

+(instancetype)newView
{
    CommentView * v = [[CommentView alloc] initWithFrame:CGRectMake(0,  0, WIDTH, 50)];
    v.y = v.contentViewHeight - 50;
    return v;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self createUI];
    }
    return self;
}

- (void)createUI
{
    self.textField = [[UITextField alloc] initWithFrame:CGRectMake(10, 8, WIDTH - 90, 34)];
    [self addSubview:self.textField];
    self.textField.borderStyle = UITextBorderStyleRoundedRect;
    self.textField.placeholder = @"说点什么吧.";
    
    self.button = [UIButton buttonWithType:UIButtonTypeCustom];
    self.button.frame = CGRectMake(WIDTH - 70, 8, 60, 34);
    [self addSubview:self.button];
    self.button.backgroundColor = MainColor;
    [self.button setTitle:@"评论" forState:UIControlStateNormal];
    [self.button layerMask5];
    [self.button addTarget:self action:@selector(buttonClick) forControlEvents:UIControlEventTouchUpInside];
    
    self.backgroundColor = UIColor.whiteColor;
}

- (void)buttonClick
{
    if (self.textField.text.length < 1) {
        [MBProgressHUD showMessage:@"请输入内容"];
        return ;
    }
    if (self.doCommentBlock) {
        self.doCommentBlock();
    }
}

@end
