//
//  InfoView.m
//  CaiKuBaoDian
//
//  Created by mac on 30/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "InfoView.h"

@implementation InfoView


+(instancetype)newView
{
    return [[InfoView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 80)];
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self createUI];
    }
    return self;
}

- (void)createUI
{
    self.header = [[UIImageView alloc] initWithFrame:CGRectMake(10, 5, 40, 40)];
    [self addSubview:self.header];
    
    self.name = [[UILabel alloc] initWithFrame:CGRectMake(60, 5, 200, 20)];
    [self addSubview:self.name];
    
    self.time = [[UILabel alloc] initWithFrame:CGRectMake(60, 30, 200, 20)];
    [self addSubview:self.time];
    
    self.title = [[UILabel alloc] initWithFrame:CGRectMake(10, 45, WIDTH - 20, 30)];
    [self addSubview:self.title];
    
    self.time.textColor = cellWordColor;
}

- (void)dataToView:(NSDictionary *)data
{
    NSDictionary * userInfo = data[@"userinfo"];
    [self.header sd_setImageWithURL:[NSURL URLWithString:userInfo[@"avatar"]] placeholderImage:[UIImage imageNamed:@"placeHolder0"]];
    self.name.text = userInfo[@"user_nicename"];
    self.time.text = data[@"createtime"];
    self.title.text = data[@"title"];
}

- (void)dataToViewZL:(NSDictionary *)data
{
    [self.header sd_setImageWithURL:[NSURL URLWithString:data[@"avatar"]] placeholderImage:[UIImage imageNamed:@"placeHolder0"]];
    self.name.text = data[@"name"];
    self.time.text = data[@"create_date"];
    self.title.text = data[@"description"];
}
@end
