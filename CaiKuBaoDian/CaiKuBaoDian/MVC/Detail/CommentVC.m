//
//  CommentVC.m
//  CaiKuBaoDian
//
//  Created by mac on 04/06/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "CommentVC.h"
#import "CommentCell.h"
#import "CommentView.h"

@interface CommentVC ()
@property ( nonatomic, strong ) CommentView * commentView;
@end

@implementation CommentVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.idStr = self.scene_id;
    [self dataHud];
    [self refreshUI];
}

- (void)refreshUI
{
    self.titleLabel.text = @"全部评论";
    self.tableView.height = self.contentViewHeight - 60;
    [self registerClass:[CommentCell class]];
    
    
    kSelfWeak
    self.commentView = [CommentView newView];
    self.commentView.idStr = self.comment_id;
    [self.contentView addSubview:self.commentView];
    self.commentView.doCommentBlock = ^{
        [weakSelf doComment];
    };
}

- (void)doComment
{
    NSDictionary * para;
    if ([self.commentView.textField.placeholder isEqualToString:@"说点什么吧."]) {
        para = @{@"full_name":UserPhone,@"content":self.commentView.textField.text,@"scene":self.scene,@"scene_id":self.idStr};
    } else {
        para = @{@"full_name":UserPhone,@"content":self.commentView.textField.text,@"scene":self.scene,@"scene_id":self.idStr,@"parent_id":self.commentView.model.id};
    }
    [self.commentView.textField resignFirstResponder];
    self.commentView.textField.text = @"";
    self.commentView.textField.placeholder = @"说点什么吧.";
    [HTTPTool postWithURLIndicatorStatus:APIPostComment parameters:para success:^(id  _Nonnull responseObject) {
        self.page = 1;
        [self.dataArray removeAllObjects];
        [self mjHeader];
    }];
}

- (void)mjHeader
{
    [self postData:APIComment parameters:@{@"scene":self.scene,@"scene_id":self.scene_id}];
}

- (void)dataMJFooter
{
    MJRefreshAutoNormalFooter * footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        self.page ++;
        [self postData:APIComment parameters:@{@"scene":self.scene,@"scene_id":self.scene_id,@"page":[NSString stringWithFormat:@"%d",self.page]}];
    }];
    self.tableView.mj_footer = footer;
    footer.automaticallyRefresh = NO;
}

- (void)dataToModel:(id)responseObject
{
    for (NSDictionary * dic in responseObject[@"data"]) {
        CommentModel * model = [CommentModel mj_objectWithKeyValues:dic];
        [self.dataArray addObject:model];
        for (NSDictionary * child in dic[@"child"]) {
            CommentModel * mo= [CommentModel mj_objectWithKeyValues:child];
            mo.reply = model.user_nicename;
            [self.dataArray addObject:mo];
        }
    }
    [self.tableView reloadData];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CommentCell * cell = [tableView dequeueReusableCellWithIdentifier:@"CommentCell" forIndexPath:indexPath];
    cell.model = self.dataArray[indexPath.row];
    kSelfWeak
    cell.cellBlock = ^(CommentModel * _Nonnull model) {
        weakSelf.commentView.model = model;
        [weakSelf.commentView.textField becomeFirstResponder];
        weakSelf.commentView.textField.placeholder = [NSString stringWithFormat:@"回复 %@",model.user_nicename];
    };
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat cellH = 90;
    CommentModel * model = self.dataArray[indexPath.row];
    CGFloat h = [Tools getStrHeight:model.content width:WIDTH - 74 font:17];
    if (h < 21) {
        cellH = 90;
    } else {
        cellH = 70 + h;
    }
    return cellH;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.commentView.textField resignFirstResponder];
}

@end
