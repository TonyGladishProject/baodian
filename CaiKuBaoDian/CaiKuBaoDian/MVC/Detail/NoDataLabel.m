//
//  NoDataLabel.m
//  CaiKuBaoDian
//
//  Created by mac on 29/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "NoDataLabel.h"

@implementation NoDataLabel

+(instancetype)newView
{
    return [[NoDataLabel alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 40)];
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self createUI];
    }
    return self;
}

- (void)createUI
{
    self.backgroundColor = viewGaryColor;
    self.textAlignment = NSTextAlignmentCenter;
    self.text = @"暂无评论内容";
}

- (void)turnBackKeyboard
{
    [self.textFieldBase resignFirstResponder];
    [self.textViewBase resignFirstResponder];
    self.textFieldBase.placeholder = @"说点什么吧.";
}
@end
