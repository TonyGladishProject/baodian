//
//  CommentList.h
//  CaiKuBaoDian
//
//  Created by mac on 29/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "TableView.h"

NS_ASSUME_NONNULL_BEGIN

@interface CommentList : TableView
@property ( nonatomic, copy ) NSString * scene;
@property ( nonatomic, copy ) NSString * scene_id;
@property ( nonatomic, copy ) NSString * comment_id;
@property (nonatomic, copy)  void(^cellBlock) (CommentModel * model);
@end

NS_ASSUME_NONNULL_END
