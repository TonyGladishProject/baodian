//
//  MoreCommentCell.h
//  CaiKuBaoDian
//
//  Created by mac on 03/06/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "PureCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface MoreCommentCell : PureCell
@property ( nonatomic, strong ) UILabel * moreLabel;
@property ( nonatomic, copy ) NSString * scene;
@property ( nonatomic, copy ) NSString * scene_id;
@property ( nonatomic, copy ) NSString * comment_id;
@end

NS_ASSUME_NONNULL_END
