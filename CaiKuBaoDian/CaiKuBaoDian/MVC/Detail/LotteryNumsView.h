//
//  LotteryNumsView.h
//  CaiKuBaoDian
//
//  Created by mac on 26/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "BaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface LotteryNumsView : BaseView
@property ( nonatomic, strong ) UILabel * labelNo;
@property ( nonatomic, strong ) UILabel * labelNoNext;
@property ( nonatomic, assign) int count;
@property ( nonatomic, strong ) NSMutableArray * arrN;
@property ( nonatomic, strong ) NSMutableArray * arrI;
@property ( nonatomic, strong ) NSMutableArray * arrL;
- (void)dataToView:(NSDictionary *)data;
@end

NS_ASSUME_NONNULL_END
