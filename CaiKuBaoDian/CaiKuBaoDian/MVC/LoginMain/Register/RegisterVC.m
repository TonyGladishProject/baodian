//
//  RegisterVC.m
//  CaiKuBaoDian
//
//  Created by mac on 15/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "RegisterVC.h"
#import "RegisterView.h"

@interface RegisterVC ()
@property ( nonatomic, strong ) RegisterView * registerView;
@property ( nonatomic, copy ) NSString * unicode;
@end
@implementation RegisterVC
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.titleLabel.text = @"注册";
    if (iphone5) {
        self.scrollView.contentSize = CGSizeMake(WIDTH, HEIGHT + 30);
    }
    self.registerView = [RegisterView newView];
    [self.contentView addSubview:self.registerView];
    
    [self.registerView.img touchEvents:self action:@selector(loadImage)];
    [self.registerView.code addTarget:self action:@selector(codeClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.registerView.btnRegister addTarget:self action:@selector(registerClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.registerView addSubview:self.coverView];
    UITextField * text5 = self.registerView.textFields[5];
    if (self.code) {
        text5.text = self.code;
    }
    [self loadImage];
}

- (void)loadImage
{
    [HTTPTool postWithURL:APIImgCode parameters:@{} success:^(id  _Nonnull responseObject) {
        [self.registerView.img sd_setImageWithURL:[NSURL URLWithString:responseObject[@"captcha"]]];
        self.unicode = responseObject[@"uniqid"];
    }];
}

- (void)codeClick:(UIButton *)button
{
    UITextField * phone = self.registerView.textFields[0];
    UITextField * textImg = self.registerView.textFields[1];
    [Tools getCode:button textField:phone textImg:textImg unicode:self.unicode];
}


- (void)registerClick:(UIButton *)button
{
    UITextField * text0 = self.registerView.textFields[0];
    UITextField * text1 = self.registerView.textFields[1];
    UITextField * text2 = self.registerView.textFields[2];
    UITextField * text3 = self.registerView.textFields[3];
    UITextField * text4 = self.registerView.textFields[4];
    UITextField * text5 = self.registerView.textFields[5];
    if (text0.text.length < 1) {
        [MBProgressHUD showMessage:text0.placeholder];
        return;
    } else if (text1.text.length < 1) {
        [MBProgressHUD showMessage:text1.placeholder];
        return;
    } else if (text2.text.length < 1) {
        [MBProgressHUD showMessage:text2.placeholder];
        return;
    } else if (text3.text.length < 1) {
        [MBProgressHUD showMessage:text3.placeholder];
        return;
    } else if (text4.text.length < 1) {
        [MBProgressHUD showMessage:text4.placeholder];
        return;
    }
    
#pragma mark - postRegister
    NSMutableDictionary * para = [[NSMutableDictionary alloc] initWithDictionary:@{@"mobile":text0.text,@"mobile_code":text2.text,@"password":text3.text,@"user_nicename":text4.text,@"pet_name":text5.text}];
    if (text5.text.length > 0) {
        [para setValue:text5.text forKey:@"invite"];
    }
    
    [HTTPTool postWithURLIndicatorPure:APIRegister parameters:para success:^(id  _Nonnull responseObject) {
        if (self.registerBlock) {
            self.registerBlock(text0.text, text3.text);
        }
        [self leftButtonClick];
    }];
}

- (void)leftButtonClick
{
    [MBProgressHUD hideHUD];
    [MBProgressHUD hideHUDForView:self.view];
    if (self.code) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    [self.navigationController popViewControllerAnimated:YES];
}

@end
