//
//  RegisterVC.h
//  CaiKuBaoDian
//
//  Created by mac on 15/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "LoginBaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface RegisterVC : LoginBaseVC
@property (nonatomic, copy)  void(^registerBlock) (NSString * phoneNumber,NSString * password);
@property ( nonatomic, copy ) NSString * code;
@end

NS_ASSUME_NONNULL_END
