//
//  RegisterView.h
//  CaiKuBaoDian
//
//  Created by mac on 15/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "BaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface RegisterView : BaseView
@property (strong, nonatomic) IBOutletCollection(UITextField) NSArray *textFields;
@property (weak, nonatomic) IBOutlet UIImageView *img;
@property (weak, nonatomic) IBOutlet UIButton *code;
@property (weak, nonatomic) IBOutlet UIButton *btnRegister;
+(instancetype)newView;
@end

NS_ASSUME_NONNULL_END
