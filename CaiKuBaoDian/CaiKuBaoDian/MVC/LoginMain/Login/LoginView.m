//
//  LoginView.m
//  CaiKuBaoDian
//
//  Created by mac on 15/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "LoginView.h"
#import "UserInfo.h"
#import "JSHAREService.h"

@implementation LoginView

+(instancetype)newView
{
    LoginView * view = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil].lastObject;
    view.frame = CGRectMake(0, 0, WIDTH,HEIGHT - WIDTH/2 - 88);
    return view;
}
- (IBAction)otherPlatformClick:(UIButton *)sender {
    JSHAREPlatform platform;
    switch (sender.tag) {
        case 1000:
            platform = JSHAREPlatformQQ;
            self.type = @"qq";
            break;
        default:
            platform = JSHAREPlatformWechatSession;
            self.type = @"wechat";
            break;
    }
    [self getUserInfoWithPlatform:platform];
}

- (void)getUserInfoWithPlatform:(JSHAREPlatform)platfrom{
    
    [JSHAREService getSocialUserInfo:platfrom handler:^(JSHARESocialUserInfo *userInfo, NSError *error) {
        NSLog(@"-------11111");
        if (error) {
            [MBProgressHUD showMessage:@"获取失败"];
        }else{
            NSLog(@"----name --%@-%@",userInfo.name,userInfo.openid);
            [self doLogin:userInfo.name openid:userInfo.openid];
        }
    }];
}

#pragma mark - postLogin
- (void)doLogin:(NSString *)name openid:(NSString *)openid
{
    [HTTPTool postWithURLIndicatorPure:APILogin parameters:@{@"type":self.type,@"username":name,@"openid":openid} success:^(id  _Nonnull responseObject) {
        
        [[UserInfo shareInstance] saveInfo:responseObject];
        [self.vc dismissViewControllerAnimated:YES completion:^{
            [Tools pushToHomeVC];
        }];
    }];
}

@end
