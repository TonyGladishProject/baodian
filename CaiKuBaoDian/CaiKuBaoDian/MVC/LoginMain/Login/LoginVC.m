//
//  LoginVC.m
//  CaiKuBaoDian
//
//  Created by mac on 15/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "LoginVC.h"
#import "LoginView.h"
#import "UserInfo.h"

#import "RegisterVC.h"
#import "ForgetPwdVC.h"

@interface LoginVC ()
@property ( nonatomic, strong ) LoginView * loginView;
@end
@implementation LoginVC
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.titleLabel.text = @"登录";

    self.loginView = [LoginView newView];
    [self.contentView addSubview:self.loginView];
    [self.loginView.forgetPwd addTarget:self action:@selector(forgetPwdClick) forControlEvents:UIControlEventTouchUpInside];
    [self.loginView.registerAccount addTarget:self action:@selector(registerClick) forControlEvents:UIControlEventTouchUpInside];
    [self.loginView.login addTarget:self action:@selector(loginClick) forControlEvents:UIControlEventTouchUpInside];
    self.loginView.vc = self;
    self.loginView.phone.text = [UserDefaults valueForKey:@"mobile"];
    [self.loginView addSubview:self.coverView];
}

#pragma mark - buttonClick
- (void)leftButtonClick
{
    [MBProgressHUD hideHUD];
    [MBProgressHUD hideHUDForView:self.view];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)forgetPwdClick
{
    ForgetPwdVC * f = [ForgetPwdVC new];
    kSelfWeak
    f.passwordBlock = ^(NSString * _Nonnull phoneNumber, NSString * _Nonnull password) {
        [weakSelf fillDataLogin:phoneNumber psd:password];
    };
    [self pushVC:f];
}

- (void)registerClick
{
    RegisterVC * r = [RegisterVC new];
    kSelfWeak
    r.registerBlock = ^(NSString * _Nonnull phoneNumber, NSString * _Nonnull password) {
        [weakSelf fillDataLogin:phoneNumber psd:password];
    };
    [self pushVC:r];
}

- (void)fillDataLogin:(NSString *)phoneNumber psd:(NSString *)password
{
    self.loginView.phone.text = phoneNumber;
    self.loginView.password.text = password;
    [self doLogin];
}

- (void)loginClick
{
    if (self.loginView.phone.text.length < 1) {
        [MBProgressHUD showMessage:self.loginView.phone.placeholder];
        return;
    } else if (self.loginView.password.text.length < 1) {
        [MBProgressHUD showMessage:self.loginView.password.placeholder];
        return;
    }

    [self doLogin];
}

#pragma mark - postLogin
- (void)doLogin
{
    [HTTPTool shareInstance].view = self.view;
    [HTTPTool postWithURLIndicatorPure:APILogin parameters:@{@"username":self.loginView.phone.text ,@"password":self.loginView.password.text} success:^(id  _Nonnull responseObject) {
        
        [[UserInfo shareInstance] saveInfo:responseObject];
        [self dismissViewControllerAnimated:YES completion:^{
            
            [Tools pushToHomeVC];
        }];
        
    }];
}

@end
