//
//  LoginView.h
//  CaiKuBaoDian
//
//  Created by mac on 15/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "BaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface LoginView : BaseView
@property (weak, nonatomic) IBOutlet UITextField *phone;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UIButton *login;
@property (weak, nonatomic) IBOutlet UIButton *forgetPwd;
@property (weak, nonatomic) IBOutlet UIButton *registerAccount;
@property ( nonatomic, copy ) NSString * type;
@property ( nonatomic, strong ) UIViewController * vc;
+(instancetype)newView;
@end

NS_ASSUME_NONNULL_END
