//
//  LoginBaseVC.m
//  CaiKuBaoDian
//
//  Created by mac on 15/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "LoginBaseVC.h"
#import "LoginHeader.h"

@interface LoginBaseVC ()

@end

@implementation LoginBaseVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.statusView.backgroundColor  = UIColorFromRGB(0xF17356);
    [self overrideScrollView];
    [self.view addSubview:self.statusView];
    self.navTintView.backgroundColor = UIColor.clearColor;
    self.navImage.hidden = YES;
    LoginHeader * header = [LoginHeader newView];
    [self.scrollView addSubview:header];
    [self createCoverView];
}

- (void)overrideScrollView
{
    [self createbackgroundView];
    self.scrollView.frame = CGRectMake(0, self.statusHeight, WIDTH, HEIGHT - 20);
    [self.view addSubview:self.scrollView];
    self.scrollView.contentSize = CGSizeMake(WIDTH, HEIGHT);
    
    [self.view bringSubviewToFront:self.navView];
    
    self.contentView.frame = CGRectMake(0, WIDTH/2, WIDTH, HEIGHT - self.statusHeight - WIDTH/2 - self.bottomHeight);
    [self.scrollView addSubview:self.contentView];
}

- (void)createbackgroundView
{
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, self.statusHeight, WIDTH, HEIGHT/2)];
    view.backgroundColor = self.statusView.backgroundColor;
    [self.view addSubview:view];
    
    UIView * viewB = [[UIView alloc] initWithFrame:CGRectMake(0, HEIGHT/2, WIDTH, HEIGHT/2)];
    viewB.backgroundColor = UIColor.whiteColor;
    [self.view addSubview:viewB];
}

- (void)createCoverView
{
    self.coverView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 2)];
    self.coverView.backgroundColor = UIColor.whiteColor;
}

@end
