//
//  LoginHeader.m
//  CaiKuBaoDian
//
//  Created by mac on 15/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "LoginHeader.h"

@implementation LoginHeader

+(instancetype)newView
{
    LoginHeader * view = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil].lastObject;
    view.frame = CGRectMake(0, 0, WIDTH,WIDTH/2);
    return view;
}

- (void)drawRect:(CGRect)rect {
    self.image.frame = CGRectMake(0, 0, WIDTH/3, WIDTH/3);
    self.image.center = CGPointMake(WIDTH/2, WIDTH/2 - WIDTH/6);
}

@end
