//
//  LoginHeader.h
//  CaiKuBaoDian
//
//  Created by mac on 15/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "BaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface LoginHeader : BaseView
@property (weak, nonatomic) IBOutlet UIImageView *image;
+(instancetype)newView;
@end

NS_ASSUME_NONNULL_END
