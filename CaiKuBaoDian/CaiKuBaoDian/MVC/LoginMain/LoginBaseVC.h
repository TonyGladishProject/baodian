//
//  LoginBaseVC.h
//  CaiKuBaoDian
//
//  Created by mac on 15/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "ScrollViewVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface LoginBaseVC : ScrollViewVC
@property ( nonatomic, strong ) UIView * coverView;
@end

NS_ASSUME_NONNULL_END
