//
//  UserInfo.h
//  CaiKuBaoDian
//
//  Created by mac on 16/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserInfo : NSObject
@property ( nonatomic, copy ) NSString * avatar;
@property ( nonatomic, copy ) NSString * id;
@property ( nonatomic, copy ) NSString * mobile;
@property ( nonatomic, copy ) NSString * pet_name;
@property ( nonatomic, copy ) NSString * share_code;
@property ( nonatomic, copy ) NSString * user_nicename;

@property ( nonatomic, copy ) NSString * AppName;
@property ( nonatomic, copy ) NSString * ClientId;
@property ( nonatomic, copy ) NSString * ClientSecret;
@property ( nonatomic, copy ) NSString * OrgName;
@property ( nonatomic, copy ) NSString * url;
+(instancetype) shareInstance;
- (void)saveInfo:(id)responseObject;
- (void)cleanInfo;
@end

NS_ASSUME_NONNULL_END
