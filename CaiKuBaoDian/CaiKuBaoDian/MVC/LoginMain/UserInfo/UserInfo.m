//
//  UserInfo.m
//  CaiKuBaoDian
//
//  Created by mac on 16/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "UserInfo.h"

@implementation UserInfo
+ (instancetype)shareInstance
{
    static UserInfo * userInfo = nil;
    static dispatch_once_t oneToken;
    dispatch_once(&oneToken,^{
        userInfo = [UserInfo new];
    });
    return userInfo;
}

- (void)saveInfo:(id)responseObject
{
    NSDictionary * info = responseObject[@"userinfo"];
    NSDictionary * easemob = responseObject[@"easemob"];
    UserInfo * user = [UserInfo shareInstance];
    user.avatar = info[@"avatar"];
    user.id = info[@"id"];
    user.mobile = info[@"mobile"];
    user.pet_name = info[@"pet_name"];
    user.share_code = info[@"share_code"];
    user.user_nicename = info[@"user_nicename"];
    
    user.AppName = easemob[@"AppName"];
    user.ClientId = easemob[@"ClientId"];
    user.ClientSecret = easemob[@"ClientSecret"];
    user.OrgName = easemob[@"OrgName"];
    user.url = easemob[@"url"];
    
    for (NSString * key in info.allKeys) {
        [UserDefaults setValue:info[key] forKey:key];
    }
    for (NSString * key in easemob) {
        [UserDefaults setValue:easemob[key] forKey:key];
    }
    [UserDefaults setBool:YES forKey:@"isLogin"];
    [UserDefaults synchronize];
}

- (void)cleanInfo
{
    UserInfo * user = [UserInfo shareInstance];
    user.avatar = @"";
    user.id = @"";
    user.mobile = @"";
    user.pet_name = @"";
    user.share_code = @"";
    user.user_nicename = @"";
    
    user.AppName = @"";
    user.ClientId = @"";
    user.ClientSecret = @"";
    user.OrgName = @"";
    user.url = @"";
    
    [UserDefaults setValue:@"" forKey:@"mobile"];
    [UserDefaults removeObjectForKey:@"avatar"];
    [UserDefaults removeObjectForKey:@"id"];
    [UserDefaults removeObjectForKey:@"pet_name"];
    [UserDefaults removeObjectForKey:@"share_code"];
    [UserDefaults removeObjectForKey:@"user_nicename"];
    
    [UserDefaults removeObjectForKey:@"AppName"];
    [UserDefaults removeObjectForKey:@"ClientId"];
    [UserDefaults removeObjectForKey:@"ClientSecret"];
    [UserDefaults removeObjectForKey:@"OrgName"];
    [UserDefaults removeObjectForKey:@"url"];
    [UserDefaults removeObjectForKey:@"isLogin"];
    [UserDefaults synchronize];
}
@end
