//
//  ForgetPwdVC.m
//  CaiKuBaoDian
//
//  Created by mac on 15/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "ForgetPwdVC.h"
#import "ForgetPwdView.h"

@interface ForgetPwdVC ()
@property ( nonatomic, strong ) ForgetPwdView * forgetPwdView;
@property ( nonatomic, copy ) NSString * unicode;
@end

@implementation ForgetPwdVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.titleLabel.text = @"忘记密码";
    
    self.forgetPwdView = [ForgetPwdView newView];
    [self.contentView addSubview:self.forgetPwdView];
    
    [self.forgetPwdView.img touchEvents:self action:@selector(loadImage)];
    [self.forgetPwdView.code addTarget:self action:@selector(codeClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.forgetPwdView.btnConfirm addTarget:self action:@selector(confirmClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.forgetPwdView addSubview:self.coverView];
    [self loadImage];
}

- (void)loadImage
{
    [HTTPTool postWithURL:APIImgCode parameters:@{} success:^(id  _Nonnull responseObject) {
        [self.forgetPwdView.img sd_setImageWithURL:[NSURL URLWithString:responseObject[@"captcha"]]];
        self.unicode = responseObject[@"uniqid"];
    }];
}

- (void)codeClick:(UIButton *)button
{
    UITextField * phone = self.forgetPwdView.textFields[0];
    UITextField * textImg = self.forgetPwdView.textFields[1];
    [Tools getCode:button textField:phone textImg:textImg unicode:self.unicode];
}


- (void)confirmClick:(UIButton *)button
{
    UITextField * text0 = self.forgetPwdView.textFields[0];
    UITextField * text1 = self.forgetPwdView.textFields[1];
    UITextField * text2 = self.forgetPwdView.textFields[2];
    UITextField * text3 = self.forgetPwdView.textFields[3];
    UITextField * text4 = self.forgetPwdView.textFields[4];
    if (text0.text.length < 1) {
        [MBProgressHUD showMessage:text0.placeholder];
        return;
    } else if (text1.text.length < 1) {
        [MBProgressHUD showMessage:text1.placeholder];
        return;
    } else if (text2.text.length < 1) {
        [MBProgressHUD showMessage:text2.placeholder];
        return;
    } else if (text3.text.length < 1) {
        [MBProgressHUD showMessage:text3.placeholder];
        return;
    } else if (text4.text.length < 1) {
        [MBProgressHUD showMessage:text4.placeholder];
        return;
    } else if (![text3.text isEqualToString:text4.text]) {
        [MBProgressHUD showMessage:@"密码不一致"];
        return;
    }
    
#pragma mark - postSubmit    
    [MBProgressHUD viewLoading:self.view];
    [HTTPTool postWithURL:APIForgetPassword parameters:@{@"mobile":text0.text,@"mobile_code":text2.text,@"password":text3.text} success:^(id  _Nonnull responseObject) {
        [MBProgressHUD hideHUDForView:self.view];
        
        NSNumber * num = responseObject[@"flag"];
        NSString * str = [[NSNumberFormatter new] stringFromNumber:num];
        [MBProgressHUD showMessage:responseObject[@"errmsg"]];
        if (str.intValue == 1) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                if (self.passwordBlock) {
                    self.passwordBlock(text0.text, text3.text);
                }
                [self leftButtonClick];
            });
        }
    }];
}

@end
