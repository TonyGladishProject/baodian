//
//  ForgetPwdVC.h
//  CaiKuBaoDian
//
//  Created by mac on 15/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "LoginBaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface ForgetPwdVC : LoginBaseVC
@property (nonatomic, copy)  void(^passwordBlock) (NSString * phoneNumber,NSString * password);
@end

NS_ASSUME_NONNULL_END
