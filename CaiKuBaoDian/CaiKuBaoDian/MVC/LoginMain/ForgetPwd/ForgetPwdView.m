//
//  ForgetPwdView.m
//  CaiKuBaoDian
//
//  Created by mac on 15/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "ForgetPwdView.h"

@implementation ForgetPwdView

+(instancetype)newView
{
    ForgetPwdView * view = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil].lastObject;
    view.frame = CGRectMake(0, 0, WIDTH,HEIGHT - WIDTH/2 - 88);
    return view;
}

- (void)drawRect:(CGRect)rect {
    UITextField * phone = self.textFields[0];
    [[Tools shareInstance] textFieldPhoneLimitedInput:phone];
    
    [self.code layerMask];
}

@end
