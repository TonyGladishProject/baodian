//
//  ViewLaunch.m
//  CaiKuBaoDian
//
//  Created by mac on 11/06/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "ViewLaunch.h"

@implementation ViewLaunch

+(instancetype)newView
{
    ViewLaunch * view = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil].lastObject;
    view.frame = CGRectMake(0, 0, WIDTH,HEIGHT);
    return view;
}

- (void)drawRect:(CGRect)rect {
    self.img.frame = CGRectMake(0, [self statusHeight], WIDTH, HEIGHT - [self statusHeight] - [self bottomHeight]);
    self.label.frame = CGRectMake(WIDTH/2 - 100, self.contentViewHeight/3 *2, 100, 20);
    self.indicator.frame = CGRectMake(self.label.right+10, self.label.y, 20, 20);
    self.time.frame = CGRectMake(WIDTH - 70, [self statusHeight] + 20, 60, 20);
    if (iphoneX) {
        self.time.y = 100;
    }
    [self.time layerMask];
    [self.indicator startAnimating];
}

- (CGFloat)bottomHeight
{
    if (iphoneX) {
        return 34;
    }
    return 0;
}

@end
