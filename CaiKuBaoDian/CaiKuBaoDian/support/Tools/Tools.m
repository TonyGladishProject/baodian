//
//  Tools.m
//  BaseFrame
//
//  Created by mac on 08/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "Tools.h"
#import "AppDelegate.h"
#import "MainTBC.h"
#import "ButtonVC.h"

@implementation Tools
+ (instancetype)shareInstance
{
    static Tools * tools = nil;
    static dispatch_once_t oneToken;
    dispatch_once(&oneToken,^{
        tools = [Tools new];
    });
    return tools;
}

//获取当前屏幕显示的viewcontroller
+ (UIViewController *)getCurrentVC
{
    UIViewController *rootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    UIViewController *currentVC = [self getCurrentVCFrom:rootViewController];
    
    return currentVC;
}

+ (UIViewController *)getCurrentVCFrom:(UIViewController *)rootVC
{
    UIViewController *currentVC;
    
    if ([rootVC presentedViewController]) {
        // 视图是被presented出来的
        
        rootVC = [rootVC presentedViewController];
    }
    
    if ([rootVC isKindOfClass:[UITabBarController class]]) {
        // 根视图为UITabBarController
        
        currentVC = [self getCurrentVCFrom:[(UITabBarController *)rootVC selectedViewController]];
        
    } else if ([rootVC isKindOfClass:[UINavigationController class]]){
        // 根视图为UINavigationController
        
        currentVC = [self getCurrentVCFrom:[(UINavigationController *)rootVC visibleViewController]];
        
    } else {
        // 根视图为非导航类
        
        currentVC = rootVC;
    }
    
    return currentVC;
}

#pragma mark - string
+ (CGSize)getStrSize:(NSString *)str width:(CGFloat)width font:(CGFloat)font
{
    return [str boundingRectWithSize:CGSizeMake(width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:font]} context:nil].size;
}

+(CGFloat)getStrWidth:(NSString *)str height:(CGFloat)height font:(CGFloat)font
{
    CGSize sizeToFit = [str sizeWithFont:[UIFont systemFontOfSize:font] constrainedToSize:CGSizeMake(CGFLOAT_MAX, height) lineBreakMode:NSLineBreakByWordWrapping];
    //此处的换行类型（lineBreakMode）可根据自己的实际情况进行设置
    return sizeToFit.width;
}

+ (CGFloat)getStrHeight:(NSString *)str width:(CGFloat)width font:(CGFloat)font
{
    CGSize size = [self getStrSize:str width:width font:font];
    return size.height;
}

+ (CGFloat)setLabelHeight:(UILabel *)label str:(NSString *)str width:(CGFloat)width font:(CGFloat)font
{
    CGFloat h = [self getStrHeight:str width:width font:font];
    label.height = h;
    label.text = str;
    label.numberOfLines = 0;
    label.font = [UIFont systemFontOfSize:font];
    return h;
}

+(NSString*)attributedStringWithHTMLString:(NSString*)htmlString
{
      NSDictionary *options = @{ NSDocumentTypeDocumentAttribute :NSHTMLTextDocumentType,
                                 
                                 NSCharacterEncodingDocumentAttribute :@(NSUTF8StringEncoding) };
    
        NSData *data = [htmlString dataUsingEncoding:NSUTF8StringEncoding];
    
        NSAttributedString *string = [[NSAttributedString alloc] initWithData:data options:options documentAttributes:nil error:nil];
    
        return string.string;
    
}

+ (void)pushToHomeVC
{
    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    MainTBC * tbc = (MainTBC *)delegate.window.rootViewController;
    tbc.selectedIndex = 0;
}

#pragma mark - textField
+ (void)getCode:(UIButton *)button textField:(UITextField *)textField textImg:(UITextField *)textImg unicode:(NSString *)unicode
{
    if (textField.text.length < 1) {
        [MBProgressHUD showMessage:@"请输入手机号"];
        return;
    }
    if (textImg.text.length < 1) {
        [MBProgressHUD showMessage:@"请输入图形验证码"];
        return;
    }
//    [self countdownTime:button];
    [HTTPTool postWithURL:APICode parameters:@{@"mobile":textField.text,@"captcha_code":textImg.text,@"captcha_uniqid":unicode} success:^(id  _Nonnull responseObject) {
        NSString * str = responseObject[@"result"];
        if ([str isEqualToString:@"成功"]) {
            [MBProgressHUD showMessage:@"已发送,请注意查收"];
        } else {
            [MBProgressHUD showMessage:@"获取失败"];
        }
    }];
}

#pragma mark - 倒计时
+ (void)countdownTime:(UIButton *)button
{
    __block NSInteger second = 60;
    //(1)
    dispatch_queue_t quene = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    //(2)
    dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, quene);
    //(3)
    dispatch_source_set_timer(timer, DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC, 0 * NSEC_PER_SEC);
    //(4)
    dispatch_source_set_event_handler(timer, ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            if (second <= 0) {
                button.userInteractionEnabled = YES;
                [button setTitle:[NSString stringWithFormat:@"获取验证码"] forState:UIControlStateNormal];
                second = 60;
                //(6)
                dispatch_cancel(timer);
            } else {
                button.userInteractionEnabled = NO;
                [button setTitle:[NSString stringWithFormat:@"%ld秒",second] forState:UIControlStateNormal];
                second--;
            }
        });
    });
    //(5)
    dispatch_resume(timer);
}

- (void)textFieldPhoneLimitedInput:(UITextField *)textField
{
    textField.keyboardType = UIKeyboardTypeNumberPad;
    [textField addTarget:self action:@selector(textFieldValueChange:) forControlEvents:UIControlEventEditingChanged];
}

- (void)textFieldValueChange:(UITextField *)textField
{
    if (textField.text.length > 11) {
        textField.text = [textField.text substringToIndex:11];
    }
}
#pragma mark - 颜色
+ (BOOL)colorSame:(UIColor*)firstColor color:(UIColor*)secondColor
{
    if (CGColorEqualToColor(firstColor.CGColor, secondColor.CGColor))
    {
        return YES;
    }
    return NO;
}

+ (UIImage *)imageWithColor:(UIColor*)color size:(CGSize)size
{
    CGRect rect =CGRectMake(0,0, size.width, size.height);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context =UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
     CGContextFillRect(context, rect);
     UIImage *image =UIGraphicsGetImageFromCurrentImageContext();
     UIGraphicsEndImageContext();
      return image;
}

+ (UIColor *)colorWithHexString:(NSString *)string
{
    if ([string hasPrefix:@"#"])
        string = [string substringFromIndex:1];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.length = 2;
    
    range.location = 0;
    NSString *rString = [string substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [string substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [string substringWithRange:range];
    
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float)r/255.0f) green:((float)g/255.0f) blue:((float)b/255.0f) alpha:1];
}

+ (NSString *)timeFromtimeStamp:(NSString *)timeString
{
    // 格式化时间
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    formatter.timeZone = [NSTimeZone timeZoneWithName:@"shanghai"];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"yyyy-MM-dd  HH:mm:ss"];
    //    2017/2/25  13:25:07
    // 毫秒值转化为秒
    NSDate* date = [NSDate dateWithTimeIntervalSince1970:[timeString doubleValue]];
    NSString* dateString = [formatter stringFromDate:date];
    return dateString;
}

//这个方法
+ (NSString *)timeStampFromString:(NSString *)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.timeZone = [NSTimeZone localTimeZone];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *dateD = [dateFormatter dateFromString:date];
    
    NSTimeZone *zone = [NSTimeZone systemTimeZone];
    NSInteger interval = [zone secondsFromGMTForDate:dateD];
    NSDate * localeDate = [dateD dateByAddingTimeInterval:interval];
    
    NSString *timeSp = [NSString stringWithFormat:@"%ld", (long)[localeDate timeIntervalSince1970]*1000];
    NSLog(@"时间戳=%@,%@",localeDate,timeSp);
    return timeSp;
}

//这个方法
+ (NSString *)timeStampFromStringSecond:(NSString *)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];// 创建一个时间格式化对象
    [dateFormatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"]; //设定时间的格式
    NSDate *tempDate = [dateFormatter dateFromString:date];//将字符串转换为时间对象
    NSString *timeStr = [NSString stringWithFormat:@"%ld", (long)[tempDate timeIntervalSince1970]];
    return timeStr;
}

+(NSString*)getCurrentTimes
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY-MM-dd"];
    NSDate *datenow = [NSDate date];
    NSString *currentTimeString = [formatter stringFromDate:datenow];
    return currentTimeString;
}

+(NSString *)getNowTimeTimestamp
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    //----------设置你想要的格式,hh与HH的区别:分别表示12小时制,24小时制
    //设置时区,这个对于时间的处理有时很重要
    NSTimeZone* timeZone = [NSTimeZone timeZoneWithName:@"Asia/Shanghai"];
    [formatter setTimeZone:timeZone];
    NSDate *datenow = [NSDate date];
    //现在时间,你可以输出来看下是什么格式
    NSString *timeSp = [NSString stringWithFormat:@"%ld", (long)[datenow timeIntervalSince1970]];
    return timeSp;
}
    

#pragma mark - colorBall
+ (NSString *)colorFromNum:(NSString *)str
{
    switch (str.intValue) {
        case 1:
        case 7:
        case 13:
        case 19:
        case 23:
        case 29:
        case 35:
        case 45:
            
        case 2:
        case 8:
        case 12:
        case 18:
        case 24:
        case 30:
        case 34:
        case 40:
        case 46:
            return @"redball";
            break;
        case 3:
        case 9:
        case 15:
        case 25:
        case 31:
        case 37:
        case 41:
        case 47:
            
        case 4:
        case 10:
        case 14:
        case 20:
        case 26:
        case 36:
        case 42:
        case 48:
            return @"blueball";
            break;
        case 5:
        case 11:
        case 17:
        case 21:
        case 27:
        case 33:
        case 39:
        case 43:
        case 49:
            
        case 6:
        case 16:
        case 22:
        case 28:
        case 32:
        case 38:
        case 44:
            return @"greenball";
            break;
            
        default:
            break;
    }
    return @"hong";
}

+ (NSString *)colorFromString:(NSString *)str
{
    if ([str isEqualToString:@"lan"]) {
        return @"blueball";
    } else if ([str isEqualToString:@"lv"]) {
        return @"greenball";
    }
    return @"redball";
}

+(NSArray *)modelLottery:(NSDictionary *)dic
{
    NSMutableArray * arr = [NSMutableArray array];
    for (NSString * str in dic) {
        if (![str isEqualToString:@"ps"]) {
            SXModel * model = [SXModel new];
            model.id = str;
            model.name = [self modelName:str];
            NSNumber * num = dic[str];
            model.persent = [[NSNumberFormatter new] stringFromNumber:num];
            [arr addObject:model];
        }
    }
    return arr;
}

+(NSString *)modelName:(NSString *)str
{
    if ([str  isEqualToString:@"hong"]) {//红蓝绿
        return @"红";
    } else if ([str  isEqualToString:@"lan"]) {
        return @"蓝";
    } else if ([str  isEqualToString:@"lv"]) {
        return @"绿";
    } else if ([str  isEqualToString:@"dan"]) {//单双
        return @"单";
    } else if ([str  isEqualToString:@"shuang"]) {
        return @"双";
    } else if ([str  isEqualToString:@"da"]) {//大小
        return @"大";
    } else if ([str  isEqualToString:@"xiao"]) {
        return @"小";
    } else if ([str  isEqualToString:@"shu"]) {//生肖
        return @"鼠";
    } else if ([str  isEqualToString:@"niu"]) {
        return @"牛";
    } else if ([str  isEqualToString:@"hu"]) {
        return @"虎";
    } else if ([str  isEqualToString:@"tu"]) {
        return @"兔";
    } else if ([str  isEqualToString:@"long"]) {
        return @"龙";
    } else if ([str  isEqualToString:@"she"]) {
        return @"蛇";
    } else if ([str  isEqualToString:@"ma"]) {
        return @"马";
    } else if ([str  isEqualToString:@"yang"]) {
        return @"羊";
    } else if ([str  isEqualToString:@"hou"]) {
        return @"猴";
    } else if ([str  isEqualToString:@"ji"]) {
        return @"鸡";
    } else if ([str  isEqualToString:@"gou"]) {
        return @"狗";
    } else if ([str  isEqualToString:@"zhu"]) {
        return @"猪";
    }
    return @"";
}

+ (void)presentPocket
{
    UIViewController *vc = [self getCurrentVC];
    ButtonVC * w = [ButtonVC new];
    w.urlStr = [UserDefaults valueForKey:@"buttonUrl"];
    [vc presentViewController:w animated:YES completion:nil];
}

#pragma mark - share
+ (JSHAREPlatform)showPlatform:(NSInteger)tag
{
    switch (tag) {
        case 1000:
            return JSHAREPlatformWechatSession;
            break;
        case 1001:
            return JSHAREPlatformWechatTimeLine;
            break;
        case 1002:
            return JSHAREPlatformQQ;
            break;
        case 1003:
            return JSHAREPlatformQzone;
            break;
            
        default:
            break;
    }
    return JSHAREPlatformQQ;
}

+ (void)showAlertWithState:(JSHAREState)state error:(NSError *)error{
    
    NSString *string = nil;
    if (error) {
        string = @"分享失败";
    }else{
        switch (state) {
            case JSHAREStateSuccess:
                string = @"分享成功";
                break;
            case JSHAREStateFail:
                string = @"分享失败";
                break;
            case JSHAREStateCancel:
                string = @"分享取消";
                break;
            case JSHAREStateUnknown:
                string = @"Unknown";
                break;
            default:
                break;
        }
    }
    
    UIAlertView *Alert = [[UIAlertView alloc] initWithTitle:nil message:string delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    dispatch_async(dispatch_get_main_queue(), ^{
        [Alert show];
    });
}

+ (BOOL)compareVersion:(NSString *)versionUrl versionLocal:(NSString *)versionLocal
{
    NSArray * arrUrl = [versionUrl componentsSeparatedByString:@"."];
    NSArray * arrLoc = [versionLocal componentsSeparatedByString:@"."];
    NSString * u0 = arrUrl[0];
    NSString * u1 = arrUrl[1];
    NSString * u2 = arrUrl[2];
    NSString * l0 = arrLoc[0];
    NSString * l1 = arrLoc[1];
    NSString * l2 = arrLoc[2];
    if (l0.intValue < u0.intValue) {
        return YES;
    } else if (l0.intValue == u0.intValue){
        if (l1.intValue < u1.intValue) {
            return YES;
        } else if (l1.intValue == u1.intValue) {
            if (l2.intValue < u2.intValue) {
                return YES;
            }
        }
    }
    return NO;
}
@end
