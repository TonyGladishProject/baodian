//
//  HTTPTool.h
//  CaiKuBaoDian
//
//  Created by mac on 09/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFHTTPSessionManager.h"

NS_ASSUME_NONNULL_BEGIN

@interface HTTPManager : AFHTTPSessionManager
/**
 创建单例get
 */
+ (instancetype)sharedManager;
@end

@interface HTTPTool : NSObject

+(instancetype) shareInstance;
@property ( nonatomic, strong ) UIView * view;
/**
 发送get网络请求
 
 @param url        网址
 @param success    成功回调
 */
+ (void)getWithURL:(NSString *)url  success:(void(^)(id responseObject))success;
/**
 发送post网络请求
 
 @param url        网址
 @param parameters 参数
 @param success    成功回调
 */
+ (void)postWithURL:(NSString *)url  parameters:(NSDictionary *)parameters success:(void(^)(id responseObject))success;
/**
 判断是否登录状态
 */
+ (void)postWithURLLogin:(NSString *)url  parameters:(NSDictionary *)parameters success:(void(^)(id responseObject))success;
/**
 post请求，带加载动画
 */
+ (void)postWithURLIndicatorPure:(NSString *)url parameters:(NSDictionary *)parameters success:(void(^)(id responseObject))success;
+ (void)postWithURLIndicator:(NSString *)url parameters:(NSDictionary *)parameters success:(void(^)(id responseObject))success;
+ (void)postWithURLIndicatorMSG:(NSString *)url parameters:(NSDictionary *)parameters success:(void(^)(id responseObject))success;
+ (void)postWithURLIndicatorStatus:(NSString *)url parameters:(NSDictionary *)parameters success:(void(^)(id responseObject))success;

+ (void)getWithURL:(NSString *)url parameters:(NSDictionary *)parameters success:(void(^)(id responseObject))success failure:(void(^)(NSError * error))failure;
+ (void)postWithURL:(NSString *)url parameters:(NSDictionary *)parameters success:(void(^)(id responseObject))success failure:(void(^)(NSError * error))failure;
/**
 上传图片
 */
+ (void)uploadImageWithURL:(NSString *)url image:(UIImage *)image  success:(void(^)(id responseObject))success failure:(void(^)(NSError * error))failure;
@end

NS_ASSUME_NONNULL_END
