//
//  Tools.h
//  BaseFrame
//
//  Created by mac on 08/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSHAREService.h"

NS_ASSUME_NONNULL_BEGIN

@interface Tools : NSObject
+(instancetype) shareInstance;

/**
 获取当前屏幕显示的viewcontroller
 */
+ (UIViewController *)getCurrentVC;
/**
 获取字符串宽度
 */
+(CGFloat)getStrWidth:(NSString *)str height:(CGFloat)height font:(CGFloat)font;
/**
 获取字符串高度
 @return float 高度
 */
+ (CGFloat)getStrHeight:(NSString *)str width:(CGFloat)width font:(CGFloat)font;

/**
 将字符串高度赋值给label
 */
+ (CGFloat)setLabelHeight:(UILabel *)label str:(NSString *)str width:(CGFloat)width font:(CGFloat)font;
+ (void)pushToHomeVC;
/**
 手机号码获取验证码
 */
+ (void)getCode:(UIButton *)button textField:(UITextField *)textField textImg:(UITextField *)textImg unicode:(NSString *)unicode;

/**
 倒计时button
 */
+ (void)countdownTime:(UIButton *)button;
/**
 限制输入手机号码
 */
- (void)textFieldPhoneLimitedInput:(UITextField *)textField;
/**
 颜色判断
 */
+ (BOOL)colorSame:(UIColor*)firstColor color:(UIColor*)secondColor;
/**
 纯色图片
 */
+ (UIImage *)imageWithColor:(UIColor*)color size:(CGSize)size;

+ (UIColor *)colorWithHexString:(NSString *)string;
/**
 带标签的字符串转富文本
 */
+(NSString*)attributedStringWithHTMLString:(NSString*)htmlString;


/**
 时间由时间戳转换--10位字符串
 */
+ (NSString *)timeFromtimeStamp:(NSString *)timeString;

/**
 时间戳由时间转换
 */
+ (NSString *)timeStampFromString:(NSString *)date;
+ (NSString *)timeStampFromStringSecond:(NSString *)date;
/**
 当前时间
 */
+(NSString*)getCurrentTimes;

+(NSString *)getNowTimeTimestamp;
/**
 球图背景色
 */
+ (NSString *)colorFromNum:(NSString *)str;

/**
 球图背景色
 */
+ (NSString *)colorFromString:(NSString *)str;

/**
 生肖相关模型
 */
+(NSArray *)modelLottery:(NSDictionary *)dic;
+(NSString *)modelName:(NSString *)str;

+ (void)presentPocket;


/**
 对应分享平台
 */
+ (JSHAREPlatform)showPlatform:(NSInteger)tag;
/**
 share 回调
 */
+ (void)showAlertWithState:(JSHAREState)state error:(NSError *)error;
+ (BOOL)compareVersion:(NSString *)versionUrl versionLocal:(NSString *)versionLocal;
@end

NS_ASSUME_NONNULL_END
