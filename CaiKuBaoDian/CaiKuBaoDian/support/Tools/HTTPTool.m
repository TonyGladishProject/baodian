//
//  HTTPTool.m
//  CaiKuBaoDian
//
//  Created by mac on 09/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "HTTPTool.h"
#import <AFNetworking.h>
#import "LoginVC.h"

@implementation HTTPManager
+ (instancetype)sharedManager
{
    static HTTPManager * manager = nil;
    static dispatch_once_t oneToken;
    dispatch_once(&oneToken,^{
        manager = [HTTPManager new];
        manager.operationQueue.maxConcurrentOperationCount = 5;
        manager.requestSerializer.timeoutInterval = 10;
        manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/html", @"text/json", @"text/javascript",@"text/plain", nil];
        
        [manager.requestSerializer setValue:@"application/x-www-form-urlencoded; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        [manager.requestSerializer setValue:@"iOS" forHTTPHeaderField:@"platform"];
        NSStringEncoding encoding = CFStringConvertEncodingToNSStringEncoding(NSUTF8StringEncoding);
        manager.requestSerializer.stringEncoding = encoding;
    });
    return manager;
}
@end

@implementation HTTPTool

+ (instancetype)shareInstance
{
    static HTTPTool * httpTool = nil;
    static dispatch_once_t oneToken;
    dispatch_once(&oneToken,^{
        httpTool = [HTTPTool new];
    });
    return httpTool;
}

+ (void)getWithURL:(NSString *)url  success:(void(^)(id responseObject))success
{
    [self getWithURL:url parameters:@{} success:^(id  _Nonnull responseObject) {
        if (responseObject) {
            success(responseObject);
        }
    } failure:^(NSError * _Nonnull error) {
        [self errorShowMessage:url];
    }];
}

+ (void)postWithURL:(NSString *)url  parameters:(NSDictionary *)parameters success:(void(^)(id responseObject))success
{
    [self postWithURL:url parameters:parameters success:^(id  _Nonnull responseObject) {
        if (responseObject) {
            success(responseObject);
        }
    } failure:^(NSError * _Nonnull error) {
        [self errorShowMessage:url];
    }];
}

+ (void)errorShowMessage:(NSString *)url
{
    NSLog(@"---------------errorUrl-%@",url);
    [MBProgressHUD hideHUDForView:[HTTPTool shareInstance].view];
    [MBProgressHUD hideHUD];
    [MBProgressHUD showMessage:NSLocalizedString(@"网络问题，请稍后再试", nil)];
}

+ (void)postWithURLIndicatorPure:(NSString *)url parameters:(NSDictionary *)parameters success:(void(^)(id responseObject))success
{
    [MBProgressHUD viewLoading:[HTTPTool shareInstance].view];
    [self postWithURL:url parameters:parameters success:^(id  _Nonnull responseObject) {
        [MBProgressHUD hideHUDForView:[HTTPTool shareInstance].view];
        [MBProgressHUD showMessage:responseObject[@"data"]];
        
        NSString * str = responseObject[@"result"];
        if ([str isEqualToString:@"成功"]) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                if (responseObject) {
                    success(responseObject);
                }
            });
        }
    }];
}

+ (void)postWithURLLogin:(NSString *)url  parameters:(NSDictionary *)parameters success:(void(^)(id responseObject))success
{
    NSString * str = UserPhone;
    if (str.length < 1) {
        [self isNotLogin];
        return;
    }
    [self postWithURL:url parameters:parameters success:^(id  _Nonnull responseObject) {
        if (responseObject) {
            success(responseObject);
        }
    }];
}

+ (void)isNotLogin
{
    [MBProgressHUD hideHUDForView:[HTTPTool shareInstance].view];
    [MBProgressHUD showMessage:@"请先登录"];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:[LoginVC new]];
        [[Tools getCurrentVC] presentViewController:nav animated:YES completion:nil];
    });
}

+ (void)postWithURLIndicator:(NSString *)url parameters:(NSDictionary *)parameters success:(void(^)(id responseObject))success
{
    [MBProgressHUD viewLoading:[HTTPTool shareInstance].view];
    [self postWithURLLogin:url parameters:parameters success:^(id  _Nonnull responseObject) {
        [MBProgressHUD hideHUDForView:[HTTPTool shareInstance].view];
        [MBProgressHUD showMessage:responseObject[@"data"]];
        
        NSString * str = responseObject[@"result"];
        if ([str isEqualToString:@"成功"]) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                if (responseObject) {
                    success(responseObject);
                }
            });
        }
    }];
}

+ (void)postWithURLIndicatorMSG:(NSString *)url parameters:(NSDictionary *)parameters success:(void(^)(id responseObject))success
{
    [MBProgressHUD viewLoading:[HTTPTool shareInstance].view];
    [self postWithURLLogin:url parameters:parameters success:^(id  _Nonnull responseObject) {
        [MBProgressHUD hideHUDForView:[HTTPTool shareInstance].view];
        
        NSNumber * num = responseObject[@"flag"];
        NSString * str = [[NSNumberFormatter new] stringFromNumber:num];
        [MBProgressHUD showMessage:responseObject[@"errmsg"]];
        if (str.intValue == 1) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                if (responseObject) {
                    success(responseObject);
                }
            });
        }
    }];
}

+ (void)postWithURLIndicatorStatus:(NSString *)url parameters:(NSDictionary *)parameters success:(void(^)(id responseObject))success
{
    [MBProgressHUD viewLoading:[HTTPTool shareInstance].view];
    [self postWithURLLogin:url parameters:parameters success:^(id  _Nonnull responseObject) {
        [MBProgressHUD hideHUDForView:[HTTPTool shareInstance].view];
        [MBProgressHUD showMessage:responseObject[@"message"]];
        
        NSString * str = responseObject[@"status"];
        if ([str isEqualToString:@"success"]) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                if (responseObject) {
                    success(responseObject);
                }
            });
        }
    }];
}


#pragma mark - original
+ (void)getWithURL:(NSString *)url parameters:(NSDictionary *)parameters success:(void(^)(id responseObject))success failure:(void(^)(NSError * error))failure
{
    HTTPManager * manager = [HTTPManager sharedManager];
    [manager GET:url parameters:parameters progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (responseObject) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error = %@",error.description);
        if (error) {
            failure(error);
        }
    }];
}

+ (void)postWithURL:(NSString *)url parameters:(NSDictionary *)parameters success:(void(^)(id responseObject))success failure:(void(^)(NSError * error))failure
{
    HTTPManager * manager = [HTTPManager sharedManager];
    [manager POST:url parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        for (NSString * key in parameters.allKeys) {
            [formData appendPartWithFormData:[parameters[key] dataUsingEncoding:NSUTF8StringEncoding] name:key];
        }
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *responseDic ;
        if ( [responseObject isKindOfClass:[NSData class]]) {
          responseDic  = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil];
        } else {
            responseDic = responseObject;
        }
        if (responseObject) {
            success(responseDic);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (error) {
            NSLog(@"error = %@",error.description);
            failure(error);
        }
    }];
}

+ (void)uploadImageWithURL:(NSString *)url image:(UIImage *)image  success:(void(^)(id responseObject))success failure:(void(^)(NSError * error))failure
{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    //接收类型不一致请替换一致text/html或别的
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",
                                                         @"text/html",
                                                         @"image/jpeg",
                                                         @"image/png",
                                                         @"application/octet-stream",
                                                         @"text/json",
                                                         nil];
    [manager POST:url parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> _Nonnull formData) {
        
        NSData *imageData =UIImageJPEGRepresentation(image,1);
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat =@"yyyyMMddHHmmss";
        NSString *str = [formatter stringFromDate:[NSDate date]];
        NSString *fileName = [NSString stringWithFormat:@"%@.png", str];
        
        //上传的参数(上传图片，以文件流的格式)
        [formData appendPartWithFileData:imageData
                                    name:@"file"
                                fileName:fileName
                                mimeType:@"image/png"];
        
    } progress:^(NSProgress *_Nonnull uploadProgress) {
        //打印下上传进度
    } success:^(NSURLSessionDataTask *_Nonnull task, id _Nullable responseObject) {
        //上传成功
        if (responseObject) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask *_Nullable task, NSError * _Nonnull error) {
        //上传失败
        if (error) {
            failure(error);
        }
    }];
    
}


@end
