//
//  YearPickerVC.h
//  CaiKuBaoDian
//
//  Created by mac on 19/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "BaseVC.h"
#import "YearPicker.h"

NS_ASSUME_NONNULL_BEGIN

@interface YearPickerVC : BaseVC
@property ( nonatomic, strong ) YearPicker * yearPicker;
@property ( nonatomic, copy ) NSString * yearStr;
- (void)confirmClick;
@end

NS_ASSUME_NONNULL_END
