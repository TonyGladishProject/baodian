//
//  MagicVC.m
//  CaiKuBaoDian
//
//  Created by mac on 18/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "MagicVC.h"

@interface MagicVC ()

@end

@implementation MagicVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self addChildViewController:self.magicController];
    [self.contentView addSubview:_magicController.view];
    
   self.menuList = [NSMutableArray arrayWithObjects:@"", @"", @"", @"", @"", @"", nil];
    [_magicController.magicView reloadData];
}

- (void)updateViewConstraints {
    UIView *magicView = _magicController.view;
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[magicView]-0-|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:NSDictionaryOfVariableBindings(magicView)]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[magicView]-0-|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:NSDictionaryOfVariableBindings(magicView)]];
    
    [super updateViewConstraints];
}

#pragma mark - VTMagicViewDataSource
- (NSArray<NSString *> *)menuTitlesForMagicView:(VTMagicView *)magicView {
    return self.menuList;
}

- (UIButton *)magicView:(VTMagicView *)magicView menuItemAtIndex:(NSUInteger)itemIndex {
    static NSString *itemIdentifier = @"itemIdentifier";
    UIButton *menuItem = [magicView dequeueReusableItemWithIdentifier:itemIdentifier];
    if (!menuItem) {
        menuItem = [UIButton buttonWithType:UIButtonTypeCustom];
        [menuItem setTitleColor:RGBCOLOR(50, 50, 50) forState:UIControlStateNormal];
        [menuItem setTitleColor:UIColorFromRGB(0x66a7b8) forState:UIControlStateSelected];
        menuItem.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:15.f];
    }
    return menuItem;
}

- (UIViewController *)magicView:(VTMagicView *)magicView viewControllerAtPage:(NSUInteger)pageIndex {
    static NSString *otherId = @"other.identifier";
    UIViewController *i = [magicView dequeueReusablePageWithIdentifier:otherId];
    if (!i) {
        i = [[UIViewController alloc] init];
    }
    return i;
}

- (void)createMoreButton
{
    self.moreBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [self.moreBtn setImage:[UIImage imageNamed:@"downArrow"] forState:UIControlStateNormal];
    self.moreBtn.center = self.view.center;
    self.magicController.magicView.rightNavigatoinItem = self.moreBtn;
}

#pragma mark - accessor methods
- (VTMagicController *)magicController {
    if (!_magicController) {
        _magicController = [[VTMagicController alloc] init];
        _magicController.view.translatesAutoresizingMaskIntoConstraints = NO;
        _magicController.magicView.navigationColor = [UIColor whiteColor];
        _magicController.magicView.sliderColor = RGBCOLOR(169, 37, 37);
        _magicController.magicView.switchStyle = VTSwitchStyleDefault;
//        _magicController.magicView.layoutStyle = VTLayoutStyleDivide;
        _magicController.magicView.navigationHeight = 44;
        _magicController.magicView.againstStatusBar = YES;
        _magicController.magicView.sliderExtension = 10.0;
        _magicController.magicView.dataSource = self;
        _magicController.magicView.delegate = self;
        _magicController.magicView.againstStatusBar = NO;
        _magicController.magicView.separatorHidden = YES;
        _magicController.view.frame = CGRectMake(0, 0, WIDTH, self.contentViewHeight);
    }
    return _magicController;
}

@end

