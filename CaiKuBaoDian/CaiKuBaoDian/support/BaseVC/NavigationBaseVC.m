//
//  NavigationBaseVC.m
//  CaiKuBaoDian
//
//  Created by mac on 23/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "NavigationBaseVC.h"

@interface NavigationBaseVC ()

@end

@implementation NavigationBaseVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end
