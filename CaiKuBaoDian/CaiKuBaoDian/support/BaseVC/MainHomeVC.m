//
//  MainHomeVC.m
//  BaseFrame
//
//  Created by mac on 08/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "MainHomeVC.h"

@interface MainHomeVC ()

@end

@implementation MainHomeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.leftButton.hidden = YES;
    self.titleLabel.text = self.name;
}

- (void)pushVC:(UIViewController *)vc
{
    [self pushHiddenTabbar:vc];
}

- (void)pushHiddenTabbar:(UIViewController *)vc
{
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (CGFloat)tabbarHeight
{
    if (iphoneX) {
        return 83;
    }
    return 49;
}

- (CGFloat)navTabHeight
{
    if (iphoneX) {
        return 88 + 83;
    }
    return 64 + 49;
}

- (CGFloat)bottomHeight
{
    return [self tabbarHeight];
}

- (CGFloat)navBottomHeight
{
    return [self navTabHeight];
}

@end
