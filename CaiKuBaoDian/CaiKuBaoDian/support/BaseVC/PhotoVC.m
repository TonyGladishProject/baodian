//
//  PhotoVC.m
//  CaiKuBaoDian
//
//  Created by mac on 23/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "PhotoVC.h"

@interface PhotoVC ()

@end

@implementation PhotoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.bottomView.backgroundColor = self.view.backgroundColor = UIColor.blackColor;
    self.titleLabel.text = @"查看大图";
    self.navImage.hidden = YES;
    [self configure];
    [self createNewUI];
}

- (void)configure{}

- (void)createNewUI
{
    //scrollView
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, self.contentViewHeight)];
    self.scrollView.bouncesZoom = YES;
    self.scrollView.maximumZoomScale = 2.5;//放大比例
    self.scrollView.minimumZoomScale = 1.0;//缩小比例
    self.scrollView.multipleTouchEnabled = YES;
    self.scrollView.delegate = self;
    self.scrollView.scrollsToTop = NO;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.showsVerticalScrollIndicator = NO;
    self.scrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.scrollView.delaysContentTouches = NO;
    self.scrollView.canCancelContentTouches = YES;
    self.scrollView.alwaysBounceVertical = NO;
    [self.contentView addSubview:self.scrollView];
    
    self.imageContainerView = [[UIView alloc] init];
    self.imageContainerView.clipsToBounds = YES;
    [self.scrollView addSubview:self.imageContainerView];
    
    self.imageView = [[UIImageView alloc] init];
    [self.imageContainerView addSubview:self.imageView];
    
    //单击
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTap:)];
    [self.contentView addGestureRecognizer:singleTap];
    //双击
    UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTap:)];
    doubleTap.numberOfTapsRequired = 2;
    [singleTap requireGestureRecognizerToFail:doubleTap];
    [self.contentView addGestureRecognizer:doubleTap];
    //长按
//    UILongPressGestureRecognizer *longPressGes = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressToSavePhoto:)];
//    [self.contentView addGestureRecognizer:longPressGes];
    self.imgData = self.strImg;
}

- (void)setImgData:(id)imgData
{
    _imgData = imgData;
    [self.scrollView setZoomScale:1.0 animated:NO];
    if ([imgData isKindOfClass:[UIImage class]]){
        UIImage *aImage = (UIImage *)imgData;
        self.imageView.image = aImage;
    }else if ([imgData isKindOfClass:[NSString class]]){
        NSString *aString = (NSString *)imgData;
        if ([aString rangeOfString:@"http"].location!=NSNotFound) {
            [self.imageView sd_setImageWithURL:[NSURL URLWithString:aString] placeholderImage:[UIImage imageNamed:@"load_001"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                [self resizeSubviews];
            }];
        }else{
            self.imageView.image = [UIImage imageNamed:aString];
        }
    }else if ([imgData isKindOfClass:[NSURL class]]){
        NSURL *aURL = (NSURL *)imgData;
        [self.imageView sd_setImageWithURL:aURL placeholderImage:[UIImage imageNamed:@"load_001"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            [self resizeSubviews];
        }];
    }
    [self resizeSubviews];
}

#pragma mark --- 核心代码，图片放大后的边界
//原理是，获取图片宽高，赋值给scrollview的contentSize
- (void)resizeSubviews {
    self.imageContainerView.origin = CGPointZero;
    self.imageContainerView.width = self.contentView.width;
    UIImage *image = self.imageView.image;
    if (image.size.height/image.size.width >self.contentView.height/self.contentView.width) {
        self.imageContainerView.height = floor(image.size.height / (image.size.width / self.contentView.width));
    } else {
        CGFloat height = image.size.height / image.size.width * self.contentView.width;
        if (height < 1 || isnan(height)) height = self.contentView.height;
        height = floor(height);
        self.imageContainerView.height = height;
        self.imageContainerView.centerY = self.contentView.height / 2;
    }
    if (self.imageContainerView.height > self.contentView.height && self.imageContainerView.height - self.contentView.height <= 1) {
        self.imageContainerView.height = self.contentView.height;
    }
    self.scrollView.contentSize = CGSizeMake(self.contentView.width, MAX(self.imageContainerView.height, self.contentView.height));
    [self.scrollView scrollRectToVisible:self.contentView.bounds animated:NO];
    self.scrollView.alwaysBounceVertical = self.imageContainerView.height <= self.contentView.height ? NO : YES;
    self.imageView.frame = self.imageContainerView.bounds;
}

#pragma mark - UITapGestureRecognizer Event
- (void)doubleTap:(UITapGestureRecognizer *)tap {
    if (self.scrollView.zoomScale > 1.0) {
        [self.scrollView setZoomScale:1.0 animated:YES];
    } else {
        CGPoint touchPoint = [tap locationInView:self.imageView];
        CGFloat newZoomScale = self.scrollView.maximumZoomScale;
        CGFloat xsize = WIDTH / newZoomScale;
        CGFloat ysize = self.contentViewHeight / newZoomScale;
        [self.scrollView zoomToRect:CGRectMake(touchPoint.x - xsize/2, touchPoint.y - ysize/2, xsize, ysize) animated:YES];
    }
}
- (void)singleTap:(UITapGestureRecognizer *)tap {
//    if (self.singleTapGestureBlock) {
//        self.singleTapGestureBlock();
//    }
}

#pragma mark - UIScrollViewDelegate
- (nullable UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return self.imageContainerView;
}
- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
    CGFloat offsetX = (scrollView.width > scrollView.contentSize.width) ? (scrollView.width - scrollView.contentSize.width) * 0.5 : 0.0;
    CGFloat offsetY = (scrollView.height > scrollView.contentSize.height) ? (scrollView.height - scrollView.contentSize.height) * 0.5 : 0.0;
    self.imageContainerView.center = CGPointMake(scrollView.contentSize.width * 0.5 + offsetX, scrollView.contentSize.height * 0.5 + offsetY);
}
-(BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    return YES;
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    if (self.scrollView.contentOffset.x <= 0) {
        if ([otherGestureRecognizer.delegate isKindOfClass:NSClassFromString(@"_FDFullscreenPopGestureRecognizerDelegate")]) {
            return YES;
        }
    }
    return NO;
}

@end
