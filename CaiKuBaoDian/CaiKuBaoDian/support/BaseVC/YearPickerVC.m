//
//  YearPickerVC.m
//  CaiKuBaoDian
//
//  Created by mac on 19/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "YearPickerVC.h"

@interface YearPickerVC ()

@end

@implementation YearPickerVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.yearStr.length < 1) {
        self.yearStr = @"2019";
    }
    [self.rightButton setTitle:@"年份" forState:UIControlStateNormal];
    self.yearPicker = [YearPicker newView];
    [self.view addSubview:self.yearPicker];
    [self.yearPicker.confirmBtn addTarget:self action:@selector(ButtonRightClick) forControlEvents:UIControlEventTouchUpInside];
}

- (void)rightButtonClick
{
    [self.yearPicker show];
}

- (void)ButtonRightClick
{
    [self.yearPicker dismiss];
    [self.rightButton setTitle:self.yearPicker.selectedStr forState:UIControlStateNormal];
    if (![self.yearStr isEqualToString:self.yearPicker.selectedStr]) {
        self.yearStr = self.yearPicker.selectedStr;
        [self confirmClick];
    }
}

- (void)confirmClick{}

@end
