//
//  MagicVC.h
//  CaiKuBaoDian
//
//  Created by mac on 18/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "YearPickerVC.h"
#import "VTMagic.h"

NS_ASSUME_NONNULL_BEGIN

@interface MagicVC : YearPickerVC<VTMagicViewDataSource, VTMagicViewDelegate>
@property (nonatomic, strong) VTMagicController *magicController;
@property ( nonatomic, strong ) UIButton * moreBtn;
@property ( nonatomic, strong ) NSMutableArray * menuList;
- (void)createMoreButton;
@end

NS_ASSUME_NONNULL_END
