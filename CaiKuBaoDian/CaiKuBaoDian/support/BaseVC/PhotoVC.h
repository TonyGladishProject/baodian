//
//  PhotoVC.h
//  CaiKuBaoDian
//
//  Created by mac on 23/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "BaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface PhotoVC : BaseVC<UIGestureRecognizerDelegate,UIScrollViewDelegate>
@property ( nonatomic, strong ) UIImageView * imageView;
@property ( nonatomic, assign ) CGFloat aspectRatio;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIView *imageContainerView;
@property ( nonatomic, copy ) NSString * strImg;
@property (nonatomic, retain)id imgData;
- (void)configure;
@end

NS_ASSUME_NONNULL_END
