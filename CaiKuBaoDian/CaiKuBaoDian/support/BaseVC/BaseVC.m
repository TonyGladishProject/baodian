//
//  BaseVC.m
//  BaseFrame
//
//  Created by mac on 08/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "BaseVC.h"

@interface BaseVC ()

@end

@implementation BaseVC

- (void)viewDidLoad {
    [super viewDidLoad];
 
    [HTTPTool shareInstance].view = self.view;
    self.view.backgroundColor = UIColor.whiteColor;
    self.navigationController.navigationBar.hidden = YES;

    [self creatStatusView];
    [self createNavView];
    [self createContentView];
    [self createBottomView];
    [self createNoDataView];
}

- (void)createNoDataView
{
    self.noDataView = [[UIView alloc] initWithFrame:self.contentView.bounds];
    
    self.noDataImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, WIDTH/2, WIDTH/2)];
    self.noDataImg.center = CGPointMake(WIDTH/2, HEIGHT/3);
    [self.noDataView addSubview:self.noDataImg];
    self.noDataImg.image = [UIImage imageNamed:@"noData"];
    self.noDataImg.contentMode = UIViewContentModeScaleAspectFit;
    
    self.noDataBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.noDataBtn.frame = CGRectMake(0, 0, WIDTH - 100, 40);
    self.noDataBtn.center = self.noDataView.center;
    [self.noDataView addSubview:self.noDataBtn];
}

- (void)createContentView
{
    self.contentViewHeight = HEIGHT - [self navBottomHeight];
    self.contentView = [[UIView alloc] initWithFrame:CGRectMake(0, [self  navBarHeight], WIDTH, self.contentViewHeight)];
    [self.view addSubview:self.contentView];
//    self.contentView.backgroundColor = UIColor.orangeColor;
    
}

- (void)createBottomView
{
    if (iphoneX) {
        self.bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, HEIGHT - 34, WIDTH, 34)];
        self.bottomView.backgroundColor = UIColor.whiteColor;
        [self.view addSubview:self.bottomView];
    }
}

- (void)creatStatusView
{
    self.statusView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, [self statusHeight])];
}

- (void)createNavView
{
    self.navView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, [self navBarHeight])];
//    self.navView.backgroundColor = UIColor.whiteColor;
    [self.view addSubview:self.navView];
    
    self.navTintView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, [self navBarHeight])];
    self.navTintView.backgroundColor =UIColor.blackColor;
    [self.navView addSubview:self.navTintView];
//    self.navTintView.alpha = 0.75;
    
    self.navImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, [self statusHeight], WIDTH, 44)];
    [self.navView addSubview:self.navImage];
    
    self.navMainView = [[UIView alloc] initWithFrame:CGRectMake(0, [self statusHeight], WIDTH, 44)];
    [self.navView addSubview:self.navMainView];
    
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, WIDTH - 200, 44)];
    self.titleLabel.centerX = WIDTH/2;
    self.titleLabel.textColor = UIColor.whiteColor;
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    self.titleLabel.text = self.titleName;
    [self.navMainView addSubview:self.titleLabel];
    
    self.leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.leftButton.frame = CGRectMake(0, 0, 80, 44);
    [self.navMainView addSubview:self.leftButton];
    [self.leftButton setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [self.leftButton addTarget:self action:@selector(leftButtonClick) forControlEvents:UIControlEventTouchUpInside];
    self.leftButton.imageEdgeInsets = UIEdgeInsetsMake(0, -45, 0, 0);
    
    self.rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.rightButton.frame = CGRectMake(WIDTH - 80, 0, 80, 44);
    [self.navMainView addSubview:self.rightButton];
    [self.rightButton addTarget:self action:@selector(rightButtonClick) forControlEvents:UIControlEventTouchUpInside];
    
    self.navImage.image = [UIImage imageNamed:@"navImage"];
}

- (void)leftButtonClick
{
    [MBProgressHUD hideHUD];
    [MBProgressHUD hideHUDForView:self.view];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)rightButtonClick{}

- (void)pushVC:(UIViewController *)vc
{
    [self.navigationController pushViewController:vc animated:YES];
}

- (CGFloat)statusHeight
{
    if (iphoneX) {
        return 44;
    }
    return 20;
}

- (CGFloat)navBarHeight
{
    if (iphoneX) {
        return 88;
    }
    return 64;
}

- (CGFloat)bottomHeight
{
    if (iphoneX) {
        return 34;
    }
    return 0;
}

- (CGFloat)navBottomHeight
{
    if (iphoneX) {
        return 88 + 34;
    }
    return 64;
}

#pragma mark - data
- (void)postData:(NSString *)api parameters:(NSDictionary *)parameters
{
    [HTTPTool postWithURL:api parameters:parameters success:^(id  _Nonnull responseObject) {
        [MBProgressHUD hideHUDForView:self.view];
        
        [self showNoDataView:responseObject];
        [self dataToModel:responseObject];
    }];
}

//子类实现
- (void)dataToModel:(id)responseObject{}

- (void)postHud:(NSString *)api parameters:(NSDictionary *)parameters
{
    self.url = api;
    self.para = [NSMutableDictionary dictionaryWithDictionary:parameters];
    [MBProgressHUD viewLoading:self.view];
    [self postData:api parameters:parameters];
}

- (void)dataHud
{
    [MBProgressHUD viewLoading:self.view];
    [self mjHeader];
}

- (void)showNoDataView:(id)responseObject
{
    NSArray * arr = responseObject[@"data"];
    if (arr.count < 1) {
        [self.contentView addSubview:self.noDataView];
    } else {
        [self.noDataView removeFromSuperview];
    }
}

@end
