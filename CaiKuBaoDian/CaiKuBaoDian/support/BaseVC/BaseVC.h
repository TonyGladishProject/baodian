//
//  BaseVC.h
//  BaseFrame
//
//  Created by mac on 08/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BaseVC : UIViewController
@property ( nonatomic, strong ) UIView * statusView;
@property ( nonatomic, strong ) UIView * navView;
@property ( nonatomic, strong ) UIView * navTintView;
@property ( nonatomic, strong ) UIView * navMainView;
@property ( nonatomic, strong ) UIImageView * navImage;
@property ( nonatomic, strong ) UILabel * titleLabel;
@property ( nonatomic, strong ) UIButton * leftButton;
@property ( nonatomic, strong ) UIButton * rightButton;
@property ( nonatomic, strong ) UIView * bottomView;
@property ( nonatomic, strong ) UIView * noDataView;
@property ( nonatomic, strong ) UIImageView * noDataImg;
@property ( nonatomic, strong ) UIButton * noDataBtn;
@property ( nonatomic, copy ) NSString * url;
@property ( nonatomic, strong ) NSMutableDictionary * para;
- (void)showNoDataView:(id)responseObject;
/**
 内容主视图
 */
@property ( nonatomic, strong ) UIView * contentView;
/**
 内容主视图高度
 */
@property ( nonatomic, assign) CGFloat   contentViewHeight;

@property ( nonatomic, strong ) UIColor * tintColor;
@property ( nonatomic, strong ) UIColor * navBackgroundColor;
@property( nonatomic, assign )  CGFloat    tintViewAlpha;
@property ( nonatomic, copy ) NSString * titleName;
@property ( nonatomic, copy ) NSString * idStr;
/**
 状态栏高度
 */
- (CGFloat)statusHeight;
/**
 导航栏高度
 */
- (CGFloat)navBarHeight;

/**
 底部留白高度
 */
- (CGFloat)bottomHeight;

/**
 导航栏，底部栏高度和
 */
- (CGFloat)navBottomHeight;
- (void)leftButtonClick;
- (void)rightButtonClick;
- (void)pushVC:(UIViewController *)vc;


/**
 加载数据，带动画
 */
- (void)postHud:(NSString *)api parameters:(NSDictionary *)parameters;
/**
 纯加载数据
 */
- (void)postData:(NSString *)api parameters:(NSDictionary *)parameters;

/**
 数据转模型，子类必须实现
 */
- (void)dataToModel:(id)responseObject;
- (void)dataMJ;
/**
 重写header方法
 */
- (void)mjHeader;
- (void)dataHud;
- (void)dataMJFooter;
- (void)endRefreshing;
@end

NS_ASSUME_NONNULL_END
