//
//  MainHomeVC.h
//  BaseFrame
//
//  Created by mac on 08/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "BaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface MainHomeVC : BaseVC
@property ( nonatomic, copy ) NSString * name;
- (void)pushHiddenTabbar:(UIViewController *)vc;
- (CGFloat)tabbarHeight;
- (CGFloat)navTabHeight;
@end

NS_ASSUME_NONNULL_END
