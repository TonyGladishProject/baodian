//
//  BaseView.h
//  CaiKuBaoDian
//
//  Created by mac on 12/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BaseView : UIView
@property ( nonatomic, strong ) UINavigationController * navigationController;
@property ( nonatomic, strong ) UITextField * textFieldBase;
@property ( nonatomic, strong ) UITextView * textViewBase;
- (void)turnBackKeyboard;
- (void)pushVC:(UIViewController *)vc;
@property ( nonatomic, assign) CGFloat   contentViewHeight;
@property ( nonatomic, weak ) KLCPopup * popup;
+(instancetype)newView;
- (CGFloat)statusHeight;
- (CGFloat)navBarHeight;
- (CGFloat)navBottomHeight;
- (KLCPopup *)popAddSuperView:(UIView *)superView center:(CGPoint)center;
- (KLCPopup *)popAddSuperViewCenter:(UIView *)superView center:(CGPoint)center;
- (KLCPopup *)popAddSuperView:(UIView *)superView center:(CGPoint)center showType:(KLCPopupShowType)showType dismissType:(KLCPopupDismissType)dismissType;
@end

NS_ASSUME_NONNULL_END
