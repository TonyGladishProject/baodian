//
//  WebVC.h
//  CaiKuBaoDian
//
//  Created by mac on 11/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "BaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface WebVC : BaseVC<UIWebViewDelegate>
@property (nonatomic, copy) NSString * urlStr;
@property ( nonatomic, strong ) UIWebView * webView;
@property ( nonatomic, strong ) UIColor * bottomColor;
- (void)configure;
@end

NS_ASSUME_NONNULL_END
