//
//  CollectionViewCell.m
//  CaiKuBaoDian
//
//  Created by mac on 12/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "CollectionViewCell.h"

@implementation CollectionViewCell
- (void)setMoreM:(MoreModel *)moreM
{
    _moreM = moreM;
    [self.image sd_setImageWithURL:[NSURL URLWithString:imgAPI(moreM.image)] placeholderImage:[UIImage imageNamed:@"placeHolder0"]];
    self.label.text = moreM.name;
    
    CGFloat h = [Tools getStrHeight:moreM.name width:self.width - 10 font:17];
    if (iphone5) {
        h = [Tools getStrHeight:moreM.name width:self.width - 10  font:12];
    }
    if (h > 22) {
        self.label.height = h;
        self.label.centerY = self.image.bottom + 24;
        if (iphone5) {
            self.label.centerY = self.image.bottom + 17;
        }
        self.label.numberOfLines = 0;
    }
}

- (void)setForumM:(ForumModel *)forumM
{
    _forumM = forumM;
    [self.image sd_setImageWithURL:[NSURL URLWithString:forumM.background_img] placeholderImage:[UIImage imageNamed:@"placeHolder0"]];
}

- (void)setLeiTaiM:(LeiTaiModel *)leiTaiM
{
    _leiTaiM = leiTaiM;
    [self.image sd_setImageWithURL:[NSURL URLWithString:leiTaiM.background_img] placeholderImage:[UIImage imageNamed:@"placeHolder0"]];
}
@end
