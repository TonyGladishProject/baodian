//
//  CollectionViewCell.h
//  CaiKuBaoDian
//
//  Created by mac on 12/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "BaseCollectionCell.h"
#import "MoreModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CollectionViewCell : BaseCollectionCell
@property ( nonatomic, assign) BOOL isSlcted;
@property ( nonatomic, strong ) MoreModel * moreM;
@property ( nonatomic, strong ) ForumModel * forumM;
@property ( nonatomic, strong ) LeiTaiModel * leiTaiM;
@end

NS_ASSUME_NONNULL_END
