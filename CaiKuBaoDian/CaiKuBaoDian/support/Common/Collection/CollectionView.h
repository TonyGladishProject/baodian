//
//  CollectionView.h
//  CaiKuBaoDian
//
//  Created by mac on 12/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "BaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface CollectionView : BaseView<UICollectionViewDataSource,UICollectionViewDelegate>
@property ( nonatomic, strong ) UICollectionView * collectionView;
@property ( nonatomic, strong ) NSMutableArray * arrImages;
@property ( nonatomic, strong ) NSMutableArray * arrNames;
@property ( nonatomic, strong ) NSMutableArray * dataArray;
@property ( nonatomic, assign ) CGSize cellSize;
- (void)registerNib:(Class)cell;
- (void)registerClass:(Class)cell;

/**
 同时设置背景view和collection的高度
 */
- (void)viewCollectionHeight:(CGFloat)h;
@property (nonatomic, copy)  void(^cellSelectedBlock) (NSIndexPath *indexPath);
@end

NS_ASSUME_NONNULL_END
