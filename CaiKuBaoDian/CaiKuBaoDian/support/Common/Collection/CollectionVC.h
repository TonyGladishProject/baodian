//
//  CollectionVC.h
//  CaiKuBaoDian
//
//  Created by mac on 12/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "BaseVC.h"
#import "CollectionViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface CollectionVC : BaseVC<UICollectionViewDataSource,UICollectionViewDelegate>
@property ( nonatomic, strong ) UICollectionView * collectionView;
@property ( nonatomic, strong ) NSMutableArray * dataArray;
- (void)registerNib:(Class)cell;
- (void)registerClass:(Class)cell;
- (void)endRefreshing;
/**
 填充model数据,子类实现
 */
- (void)collectionView:(CollectionViewCell *)cell dataForItemAtIndexPath:(NSIndexPath *)indexPath;
@end

NS_ASSUME_NONNULL_END
