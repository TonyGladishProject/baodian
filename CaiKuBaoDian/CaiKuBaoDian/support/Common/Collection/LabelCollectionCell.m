//
//  LabelCollectionCell.m
//  CaiKuBaoDian
//
//  Created by mac on 24/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "LabelCollectionCell.h"

@implementation LabelCollectionCell
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = UIColorFromRGB(0x585656);
        self.label = [UILabel new];
        [self addSubview:self.label];
        self.label.frame = CGRectMake(0, 0, self.width, self.height);
        self.label.textAlignment = NSTextAlignmentCenter;
        self.label.textColor = UIColor.whiteColor;
    }
    return self;
}
@end
