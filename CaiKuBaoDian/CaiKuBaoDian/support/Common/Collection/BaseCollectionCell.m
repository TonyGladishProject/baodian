//
//  BaseCollectionCell.m
//  CaiKuBaoDian
//
//  Created by mac on 12/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "BaseCollectionCell.h"

@implementation BaseCollectionCell
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = UIColor.whiteColor;
        CGFloat width = (WIDTH - 2)/3;
        self.image = [UIImageView new];
        [self addSubview:self.image];
        self.label = [UILabel new];
        [self addSubview:self.label];
        self.image.frame = CGRectMake(40, 20, width - 80, width - 80);
        self.label.frame = CGRectMake(5, self.image.bottom + 10, width - 10, 20);
        self.label.textAlignment = NSTextAlignmentCenter;
        
        if (iphone5) {
            self.image.frame = CGRectMake(30, 10, width - 60, width - 60);
            self.label.frame = CGRectMake(0, self.image.bottom + 5, width, 20);
            self.label.font = [UIFont systemFontOfSize:12];
        }
        
        self.imgSX = [UIImageView new];
    }
    return self;
}
@end
