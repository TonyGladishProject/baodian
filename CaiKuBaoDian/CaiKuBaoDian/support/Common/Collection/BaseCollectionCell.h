//
//  BaseCollectionCell.h
//  CaiKuBaoDian
//
//  Created by mac on 12/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BaseCollectionCell : UICollectionViewCell
@property ( nonatomic, strong ) UIImageView * image;
@property ( nonatomic, strong ) UILabel * label;
@property ( nonatomic, strong ) UIImageView * imgSX;
@end

NS_ASSUME_NONNULL_END
