//
//  CollectionVC.m
//  CaiKuBaoDian
//
//  Created by mac on 12/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "CollectionVC.h"

@interface CollectionVC ()

@end

@implementation CollectionVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self createCollectionView];
}

- (void)dataMJ
{
    MJRefreshNormalHeader * header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self postData:self.url parameters:self.para];
    }];
    self.collectionView.mj_header = header;
}

#pragma mark - data
- (void)postData:(NSString *)api parameters:(NSDictionary *)parameters
{
    [HTTPTool postWithURL:api parameters:parameters success:^(id  _Nonnull responseObject) {
        [MBProgressHUD hideHUDForView:self.view];
        [self.dataArray removeAllObjects];
        [self showNoDataView:responseObject];
        [self dataToModel:responseObject];
        [self endRefreshing];
    } failure:^(NSError * _Nonnull error) {
        [MBProgressHUD hideHUD];
        [MBProgressHUD hideHUDForView:self.view];
        [self endRefreshing];
    }];
}

- (void)endRefreshing
{
    [MBProgressHUD hideHUD];
    [MBProgressHUD hideHUDForView:self.view];
    [self.collectionView.mj_header endRefreshing];
    [self.collectionView.mj_footer endRefreshing];
}

- (void)registerNib:(Class)cell
{
    NSString * str= NSStringFromClass(cell);
    [self.collectionView registerNib:[UINib nibWithNibName:str bundle:nil] forCellWithReuseIdentifier:str];
}

- (void)registerClass:(Class)cell
{
    [self.collectionView registerClass:cell forCellWithReuseIdentifier:NSStringFromClass(cell)];
}

#pragma mark - collectionView
- (void)createCollectionView
{
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0,0, WIDTH, self.contentViewHeight) collectionViewLayout:flowLayout];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    [self.contentView addSubview:self.collectionView];
    self.collectionView.backgroundColor = viewGaryColor;
    self.collectionView.alwaysBounceVertical = YES;
    //注册Cell
    [self registerClass:[CollectionViewCell class]];
    self.bottomView.backgroundColor = viewGaryColor;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
   return self.dataArray.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CollectionViewCell" forIndexPath:indexPath];
    [self collectionView:cell dataForItemAtIndexPath:indexPath];
    return cell;
}

- (void)collectionView:(CollectionViewCell *)cell dataForItemAtIndexPath:(NSIndexPath *)indexPath{}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return  CGSizeMake((WIDTH -2)/3,(WIDTH -2)/3 - 10 );
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(1, 0,1, 0);//（上、左、下、右）
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return .5;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 1;
}

#pragma mark  点击CollectionView触发事件
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{}

- (NSMutableArray *)dataArray
{
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}
@end
