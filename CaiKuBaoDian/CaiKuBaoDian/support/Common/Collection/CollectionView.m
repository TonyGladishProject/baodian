//
//  CollectionView.m
//  CaiKuBaoDian
//
//  Created by mac on 12/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "CollectionView.h"
#import "CollectionViewCell.h"

@implementation CollectionView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createCollectionView];
    }
    return self;
}

- (void)registerNib:(Class)cell
{
    NSString * str= NSStringFromClass(cell);
    [self.collectionView registerNib:[UINib nibWithNibName:str bundle:nil] forCellWithReuseIdentifier:str];
}

- (void)registerClass:(Class)cell
{
    [self.collectionView registerClass:cell forCellWithReuseIdentifier:NSStringFromClass(cell)];
}

- (void)viewCollectionHeight:(CGFloat)h
{
    self.collectionView.height = self.height = h;
}

#pragma mark - collectionView
- (void)createCollectionView
{
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0,0, WIDTH, self.height) collectionViewLayout:flowLayout];
    self.collectionView.backgroundColor = viewGaryColor;
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    [self addSubview:self.collectionView];
    self.collectionView.alwaysBounceVertical = YES;

    [self registerClass:[CollectionViewCell class]];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (self.dataArray.count > 0) {
        return self.dataArray.count;
    }
    return self.arrImages.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CollectionViewCell" forIndexPath:indexPath];
    cell.image.image = [UIImage imageNamed:self.arrImages[indexPath.item]];
    cell.label.text = self.arrNames[indexPath.item];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.cellSize.height > 1) {
        return  self.cellSize;
    }
    return  CGSizeMake((WIDTH -2)/3,(WIDTH -2)/3 - 20 );
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(1, 0,1, 0);//（上、左、下、右）
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return .5;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 1;
}

#pragma mark  点击CollectionView触发事件
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.cellSelectedBlock) {
        self.cellSelectedBlock(indexPath);
    }
}

- (NSMutableArray *)dataArray
{
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

- (NSMutableArray *)arrNames
{
    if (!_arrNames) {
        _arrNames = [NSMutableArray array];
    }
    return  _arrNames;
}

- (NSMutableArray *)arrImages
{
    if (!_arrImages) {
        _arrImages = [NSMutableArray array];
    }
    return _arrImages;
}
@end
