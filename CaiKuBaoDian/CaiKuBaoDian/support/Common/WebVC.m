//
//  WebVC.m
//  CaiKuBaoDian
//
//  Created by mac on 11/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "WebVC.h"
#import "UIWebView+DKProgress.h"
#import "DKProgressLayer.h"

@implementation WebVC
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configure];
    [self createWebView];
    NSLog(@"url = %@",self.urlStr);
    if (_bottomColor) {
        [UIView animateWithDuration:3 animations:^{
            self.bottomView.backgroundColor = self.bottomColor;
        }];
    }
}

- (void)configure{}

- (void)createWebView
{
    _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, self.contentViewHeight)];
    if (!_urlStr) {
        _urlStr = @"https://www.baidu.com";
    }
    
    NSURL * webUrl = [NSURL URLWithString:_urlStr];
    
    NSURLRequest * requst = [NSURLRequest requestWithURL:webUrl];
    [_webView loadRequest:requst];
    [self.contentView addSubview:_webView];

    self.webView.dk_progressLayer = [[DKProgressLayer alloc] initWithFrame:CGRectMake(0, 40, WIDTH, 4)];
    self.webView.dk_progressLayer.progressColor = UIColorFromRGB(0xf0f0f0);
    self.webView.dk_progressLayer.progressStyle = DKProgressStyle_Gradual;
    [self.navMainView.layer addSublayer:self.webView.dk_progressLayer];
}
@end
