//
//  BaseView.m
//  CaiKuBaoDian
//
//  Created by mac on 12/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "BaseView.h"

@implementation BaseView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self touchEvents:self action:@selector(turnBackKeyboard)];
    }
    return self;
}

- (CGFloat)statusHeight
{
    if (iphoneX) {
        return 44;
    }
    return 20;
}

- (CGFloat)navBarHeight
{
    if (iphoneX) {
        return 88;
    }
    return 64;
}

- (CGFloat)navBottomHeight
{
    if (iphoneX) {
        return 88 + 34;
    }
    return 64;
}

- (CGFloat)contentViewHeight
{
    return HEIGHT - [self navBottomHeight];
}

- (KLCPopup *)popAddSuperView:(UIView *)superView center:(CGPoint)center showType:(KLCPopupShowType)showType dismissType:(KLCPopupDismissType)dismissType
{
    [self layerMask:5];
    KLCPopup * popup = [KLCPopup popupWithContentView:self];
    popup.showType = showType;
    popup.dismissType = dismissType;
    [popup showAtCenter:center inView:superView];
    [popup show];
    self.popup = popup;
    return popup;
}

- (KLCPopup *)popAddSuperView:(UIView *)superView center:(CGPoint)center
{
    return [self popAddSuperView:superView center:center showType:KLCPopupShowTypeSlideInFromBottom dismissType:KLCPopupDismissTypeSlideOutToBottom];
}

- (KLCPopup *)popAddSuperViewCenter:(UIView *)superView center:(CGPoint)center
{
    return [self popAddSuperView:superView center:center showType:KLCPopupShowTypeGrowIn dismissType:KLCPopupDismissTypeGrowOut];
}

- (void)pushVC:(UIViewController *)vc
{
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)turnBackKeyboard
{
    [self.textFieldBase resignFirstResponder];
    [self.textViewBase resignFirstResponder];
}
@end
