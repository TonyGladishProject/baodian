//
//  ScrollViewVC.m
//  CaiKuBaoDian
//
//  Created by mac on 14/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "ScrollViewVC.h"

@interface ScrollViewVC ()

@end

@implementation ScrollViewVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self createScrollView];
}

- (void)createScrollView
{
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, self.contentViewHeight)];
    [self.contentView addSubview:self.scrollView];
    self.scrollView.contentSize = CGSizeMake(WIDTH, self.contentViewHeight + 10);
}

- (void)dataMJ
{
    MJRefreshNormalHeader * header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self mjHeader];
    }];
    self.scrollView.mj_header  = header;
}

- (void)mjHeader{}

- (void)endRefreshing
{
    [self.scrollView.mj_header endRefreshing];
}

- (void)setContentHeight:(CGFloat)contentHeight
{
    CGSize size = self.scrollView.contentSize;
    if (contentHeight < self.scrollView.height) {
        contentHeight += 10;
    }
    size.height = contentHeight;
    self.scrollView.contentSize = size;
}

- (CGFloat)contentHeight
{
    return self.scrollView.contentSize.height;
}
@end
