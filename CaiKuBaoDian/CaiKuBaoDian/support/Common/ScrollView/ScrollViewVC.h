//
//  ScrollViewVC.h
//  CaiKuBaoDian
//
//  Created by mac on 14/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "BaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface ScrollViewVC : BaseVC
@property ( nonatomic, strong ) UIScrollView * scrollView;
/**
 内容高度
 */
@property ( nonatomic, assign ) CGFloat contentHeight;

@end

NS_ASSUME_NONNULL_END
