//
//  CommonModel.h
//  CaiKuBaoDian
//
//  Created by mac on 19/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CommonModel : NSObject

@end

@interface GalleryModel : NSObject
@property ( nonatomic, copy ) NSString * id;
@property ( nonatomic, copy ) NSString * na;
@property ( nonatomic, copy ) NSString * name;
@property ( nonatomic, copy ) NSString * szm;
@end

@interface GalleryDetailModel : NSObject
@property ( nonatomic, copy ) NSString * id;
@property ( nonatomic, copy ) NSString * na;
@property ( nonatomic, copy ) NSString * name;
@property ( nonatomic, copy ) NSString * qs;
@end

@interface ForumModel : NSObject
@property ( nonatomic, copy ) NSString * background_img;
@property ( nonatomic, copy ) NSString * parent;
@property ( nonatomic, copy ) NSString * name;
@property ( nonatomic, copy ) NSString * term_id;
@end

@interface LeiTaiModel : NSObject
@property ( nonatomic, copy ) NSString * background_img;
@property ( nonatomic, copy ) NSString * Description;
@property ( nonatomic, copy ) NSString * is_out;
@property ( nonatomic, copy ) NSString * name;
@property ( nonatomic, copy ) NSString * out_url;

@property ( nonatomic, copy ) NSString * parent;
@property ( nonatomic, copy ) NSString * term_id;
@end

@interface LTDetailModel : NSObject
@property ( nonatomic, copy ) NSString * article_count;
@property ( nonatomic, copy ) NSString * avatar;
@property ( nonatomic, copy ) NSString * createtime;
@property ( nonatomic, copy ) NSString * guestbook_hit;
@property ( nonatomic, copy ) NSString * guestbook_lead;

@property ( nonatomic, copy ) NSString * guestbook_liansheng;
@property ( nonatomic, copy ) NSString * id;
@property ( nonatomic, assign) BOOL is_zhiding;
@property ( nonatomic, copy ) NSString * myFriend;
@property ( nonatomic, copy ) NSString * title;

@property ( nonatomic, copy ) NSString * user_nicename;
@property ( nonatomic, copy ) NSString * plNum;
@property ( nonatomic, copy ) NSString * dzNum;
@property ( nonatomic, copy ) NSString * num;
@property ( nonatomic, strong ) UIColor * color;
@end

@interface ZLDetailModel : NSObject
@property ( nonatomic, copy ) NSString * name;
@property ( nonatomic, copy ) NSString * create_time;
@property ( nonatomic, copy ) NSString * dzNum;
@property ( nonatomic, copy ) NSString * id;
@property ( nonatomic, copy ) NSString * plNum;

@property ( nonatomic, copy ) NSString * Description;
@property ( nonatomic, copy ) NSString * parent;
@property ( nonatomic, copy ) NSString * qs;
@property ( nonatomic, copy ) NSString * status;
@property ( nonatomic, copy ) NSString * type;
@property ( nonatomic, copy ) NSString * why_add;
@end

@interface ZiLiaoModel : NSObject
@property ( nonatomic, copy ) NSString * name;
@property ( nonatomic, copy ) NSString * id;
@property ( nonatomic, copy ) NSString * parent;
@property ( nonatomic, copy ) NSString * status;
@property ( nonatomic, copy ) NSString * type;

@property ( nonatomic, copy ) NSString * why_add;
@property ( nonatomic, copy ) NSString * back_color;
@property ( nonatomic, copy ) NSString * box_color;
@property ( nonatomic, copy ) NSString * font_color;
@property ( nonatomic, assign) BOOL show;
@end

@interface SXModel : NSObject
@property ( nonatomic, copy ) NSString * num;
@property ( nonatomic, copy ) NSString * persent;
@property ( nonatomic, copy ) NSString * name;
@property ( nonatomic, copy ) NSString * id;
@end

@interface CommentModel : NSObject
@property ( nonatomic, copy ) NSString * avatar;
@property ( nonatomic, copy ) NSString * content;
@property ( nonatomic, copy ) NSString * create_time;
@property ( nonatomic, copy ) NSString * email;
@property ( nonatomic, copy ) NSString * full_name;

@property ( nonatomic, copy ) NSString * huifu_count;
@property ( nonatomic, copy ) NSString * id;
@property ( nonatomic, copy ) NSString * parent_id;
@property ( nonatomic, copy ) NSString * scene;
@property ( nonatomic, copy ) NSString * scene_id;

@property ( nonatomic, copy ) NSString * status;
@property ( nonatomic, copy ) NSString * tworeply;
@property ( nonatomic, copy ) NSString * update_time;
@property ( nonatomic, copy ) NSString * user_nicename;
@property ( nonatomic, copy ) NSString * zhan_number;

@property ( nonatomic, copy ) NSString * reply;
@end

@interface CASXModel : NSObject
@property ( nonatomic, copy ) NSString * qishu;
@property ( nonatomic, copy ) NSString * date;
@property ( nonatomic, copy ) NSString * tema;
@property ( nonatomic, copy ) NSString * shengxiao;
@property ( nonatomic, copy ) NSString * yanse;
@property ( nonatomic, copy ) NSString * color;

@property ( nonatomic, copy ) NSString * daxiao;
@property ( nonatomic, copy ) NSString * danshuang;
@property ( nonatomic, copy ) NSString * heshudanshuang;
@property ( nonatomic, copy ) NSString * tetoudanshuang;
@property ( nonatomic, copy ) NSString * duanwei;
@property ( nonatomic, copy ) NSString * tou;
@property ( nonatomic, copy ) NSString * wei;
@property ( nonatomic, copy ) NSString * wuxing;
@end

@interface PlatformModel : NSObject
@property ( nonatomic, copy ) NSString * id;
@property ( nonatomic, copy ) NSString * jiancheng;
@property ( nonatomic, copy ) NSString * pingtainame;
@property ( nonatomic, copy ) NSString * url;
@end

@interface JifenRecordModel : NSObject
@property ( nonatomic, copy ) NSString * addtime;
@property ( nonatomic, copy ) NSString * desc;
@property ( nonatomic, copy ) NSString * id;
@property ( nonatomic, copy ) NSString * score_change;
@property ( nonatomic, copy ) NSString * share_code;

@property ( nonatomic, copy ) NSString * stage;
@property ( nonatomic, copy ) NSString * username;
@end

@interface MyNoticeModel : NSObject
@property ( nonatomic, copy ) NSString * content;
@property ( nonatomic, copy ) NSString * createtime;
@property ( nonatomic, copy ) NSString * id;
@property ( nonatomic, copy ) NSString * is_read;
@property ( nonatomic, copy ) NSString * read_createtime;

@property ( nonatomic, copy ) NSString * title;
@property ( nonatomic, copy ) NSString * type;
@property ( nonatomic, copy ) NSString * type_name;
@property ( nonatomic, copy ) NSString * username;
@end

@interface BeforeHistoryModel : NSObject
@property ( nonatomic, copy ) NSString * avatar;
@property ( nonatomic, copy ) NSString * countreply;
@property ( nonatomic, copy ) NSString * createtime;
@property ( nonatomic, copy ) NSString * dz_number;
@property ( nonatomic, copy ) NSString * full_name;

@property ( nonatomic, copy ) NSString * id;
@property ( nonatomic, copy ) NSString * readnum;
@property ( nonatomic, copy ) NSString * title;
@end
NS_ASSUME_NONNULL_END
