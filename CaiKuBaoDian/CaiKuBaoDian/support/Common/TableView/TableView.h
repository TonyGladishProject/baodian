//
//  TableView.h
//  CaiKuBaoDian
//
//  Created by mac on 14/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "BaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface TableView : BaseView<UITableViewDataSource,UITableViewDelegate>
@property ( nonatomic, strong ) UITableView * tableView;
@property ( nonatomic, strong ) NSMutableArray * dataArray;
-(void)registerNib:(NSString *)cellName;
- (void)registerClass:(Class)cell;
@end

NS_ASSUME_NONNULL_END
