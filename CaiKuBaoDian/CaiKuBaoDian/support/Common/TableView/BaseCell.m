//
//  BaseCell.m
//  CaiKuBaoDian
//
//  Created by mac on 13/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "BaseCell.h"

@implementation BaseCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        _label = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 200, 55)];
        [self.contentView addSubview:_label];
        
        _arrow = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH - 10 - 20, 0, 20, 20)];
        _arrow.centerY = 25;
        [self.contentView addSubview:_arrow];
        
        _line = [[UIView alloc] initWithFrame:CGRectMake(0, 54, WIDTH, 1)];
        [self.contentView addSubview:_line];

        _switchView = [[UISwitch alloc] initWithFrame:CGRectMake(WIDTH - 49 - 10, 0, 49, 31)];
        _switchView.centerY = 25;
        [self.contentView addSubview:_switchView];
        _switchView.on = YES;
        
        _switchView.hidden = YES;
        _line.backgroundColor = cellLineColor;
        _arrow.image = [UIImage imageNamed:@"right_gray"];
        _label.textColor = cellWordColor;
    }
    return self;
}

- (void)showSwitchView:(BOOL)show
{
    self.arrow.hidden = show;
    self.switchView.hidden = !show;
}
@end
