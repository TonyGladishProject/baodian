//
//  TableViewVC.h
//  CaiKuBaoDian
//
//  Created by mac on 13/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "BaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface TableViewVC : BaseVC<UITableViewDataSource,UITableViewDelegate>
@property ( nonatomic, strong ) UITableView * tableView;
@property ( nonatomic, strong ) NSMutableArray * dataArray;
@property ( nonatomic, assign ) int page;
@property (nonatomic, assign) NSUInteger count;
@property ( nonatomic, copy ) NSString * key;
-(void)registerNib:(Class)cell;
- (void)registerClass:(Class)cell;
@end

NS_ASSUME_NONNULL_END
