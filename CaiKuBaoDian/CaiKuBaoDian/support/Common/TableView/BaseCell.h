//
//  BaseCell.h
//  CaiKuBaoDian
//
//  Created by mac on 13/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BaseCell : UITableViewCell
@property ( nonatomic, strong ) UIImageView * image;
@property ( nonatomic, strong ) UILabel * label;
@property ( nonatomic, strong ) UIImageView * arrow;
@property ( nonatomic, strong ) UIView * line;
@property ( nonatomic, strong ) UINavigationController * navigationController;
@property ( nonatomic, strong ) UISwitch * switchView;
- (void)showSwitchView:(BOOL)show;
@end

NS_ASSUME_NONNULL_END
