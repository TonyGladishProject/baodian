//
//  TableViewVC.m
//  CaiKuBaoDian
//
//  Created by mac on 13/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "TableViewVC.h"

@interface TableViewVC ()

@end

@implementation TableViewVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.page = 1;
    [self createTableView];
    [self dataMJ];
}

- (void)dataMJ
{
    MJRefreshNormalHeader * header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        self.page = 1;
        [self mjHeader];
    }];
    self.tableView.mj_header = header;
}

- (void)mjHeader
{
    [self keyValue];
    [self.para setObject:[NSString stringWithFormat:@"%d",self.page] forKey:self.key];
    [self postData:self.url parameters:self.para];
}

- (void)dataMJFooter
{
    MJRefreshAutoNormalFooter * footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        if (self.count != self.dataArray.count) {
            self.page ++;
        }
        [self.para setObject:[NSString stringWithFormat:@"%d",self.page] forKey:self.key];
        [self postData:self.url parameters:self.para];
    }];
    if (self.dataArray.count >=7) {
        self.tableView.mj_footer = footer;
    } else {
        self.tableView.mj_footer = nil;
    }
    footer.automaticallyRefresh = NO;
}

#pragma mark - data
- (void)postData:(NSString *)api parameters:(NSDictionary *)parameters
{
    [HTTPTool postWithURL:api parameters:parameters success:^(id  _Nonnull responseObject) {
        [MBProgressHUD hideHUDForView:self.view];
        if (self.page == 1) {
            [self.dataArray removeAllObjects];
        }
        [self showNoDataView:responseObject];
        [self dataToModel:responseObject];
        [self dataMJFooter];
        [self endRefreshing];
    } failure:^(NSError * _Nonnull error) {
        [MBProgressHUD hideHUD];
        [MBProgressHUD hideHUDForView:self.view];
        [self endRefreshing];
    }];
}

- (void)endRefreshing
{
    [MBProgressHUD hideHUDForView:self.view];
    [MBProgressHUD hideHUD];
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}

- (void)keyValue
{
    if (self.key.length < 1) {
        self.key = @"page";
    }
}

-(void)registerNib:(Class)cell
{
    NSString * str= NSStringFromClass(cell);
    [self.tableView registerNib:[UINib nibWithNibName:str bundle:nil] forCellReuseIdentifier:str];
}

- (void)registerClass:(Class)cell
{
    [self.tableView registerClass:cell forCellReuseIdentifier:NSStringFromClass(cell)];
}

- (void)createTableView
{
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, self.contentViewHeight) style:UITableViewStylePlain];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    [self.contentView addSubview:self.tableView];
    _tableView.tableFooterView = [UIView new];
    _tableView.separatorStyle = UITableViewCellSelectionStyleNone;
    _tableView.backgroundColor = viewGaryColor;
    [self registerClass:[UITableViewCell class]];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"UITableViewCell" forIndexPath:indexPath];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (NSMutableArray *)dataArray
{
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

@end
