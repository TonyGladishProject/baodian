//
//  PureCell.m
//  CaiKuBaoDian
//
//  Created by mac on 10/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "PureCell.h"

@implementation PureCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.label.hidden = YES;
        self.arrow.hidden = YES;
        self.line.hidden = YES;
        self.switchView.hidden = YES;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

@end
