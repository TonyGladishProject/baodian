//
//  BaseLabel.h
//  CaiKuBaoDian
//
//  Created by mac on 29/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BaseLabel : UILabel
@property ( nonatomic, strong ) UITextField * textFieldBase;
@property ( nonatomic, strong ) UITextView * textViewBase;
+(instancetype)newView;
@end

NS_ASSUME_NONNULL_END
