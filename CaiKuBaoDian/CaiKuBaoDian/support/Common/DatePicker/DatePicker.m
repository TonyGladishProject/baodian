//
//  DatePicker.m
//  CaiKuBaoDian
//
//  Created by mac on 04/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "DatePicker.h"

@interface DatePicker ()
@property ( nonatomic, strong ) UIView * titleView;
@property ( nonatomic, strong ) UIView * dateView;
@property ( nonatomic, strong ) UILabel * titleLabel;
@property ( nonatomic, strong ) UIView * maskView;
@property ( nonatomic, strong ) UIView * mainView;
@end
@implementation DatePicker

+(instancetype)newView
{
    return [[DatePicker alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self initUI];
    }
    return self;
}

- (void)initUI
{
    self.selectedStr = [Tools getCurrentTimes];
    self.hidden = YES;
    self.maskView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    [self addSubview:self.maskView];
    
    self.mainView = [[UIView alloc] initWithFrame:CGRectMake(0, HEIGHT, WIDTH, 200)];
    [self addSubview:self.mainView];
    
    self.titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 40)];
    [self.mainView addSubview:self.titleView];
    self.titleView.backgroundColor = viewGaryColor;
    
    self.dateView = [[UIView alloc] initWithFrame:CGRectMake(0, self.titleView.bottom, WIDTH, self.mainView.height - self.titleView.height)];
    [self.mainView addSubview:self.dateView];
    self.dateView.backgroundColor = UIColor.whiteColor;
    
    self.cancelBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    self.cancelBtn.frame = CGRectMake(0, 0, 80, self.titleView.height);
    [self.cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
    [self.cancelBtn addTarget:self action:@selector(cancelClick) forControlEvents:UIControlEventTouchUpInside];
    [self.titleView addSubview:self.cancelBtn];
    
    self.confirmBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    self.confirmBtn.frame = CGRectMake(WIDTH - 80, 0,80, self.titleView.height);
    [self.confirmBtn setTitle:@"确定" forState:UIControlStateNormal];
    [self.confirmBtn addTarget:self action:@selector(confirmClick) forControlEvents:UIControlEventTouchUpInside];
    [self.titleView addSubview:self.confirmBtn];
    
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(80, 0, WIDTH - 160, self.titleView.height)];
    [self.titleView addSubview:self.titleLabel];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    
    self.titleLabel.text = @"选择时间";
    [self.maskView touchEvents:self action:@selector(cancelClick)];
    [self createDatePicker];
}

#pragma mark - datePicker
- (void)createDatePicker
{
    _datePicker = [[UIDatePicker alloc]initWithFrame:self.dateView.bounds];
    [self.dateView addSubview:_datePicker];
    _datePicker.backgroundColor = UIColorFromRGB(0xd1d5db);
    _datePicker.locale = [[NSLocale alloc]initWithLocaleIdentifier:@"zh"];
    _datePicker.datePickerMode = UIDatePickerModeDate;
    [_datePicker addTarget:self action:@selector(rollAction:) forControlEvents:UIControlEventValueChanged];
}

- (void)rollAction:(UIDatePicker *)sender
{
    NSDateFormatter * formatter = [NSDateFormatter new];
    
    [formatter setDateFormat:@"YYYY-MM-dd"];
    
    self.selectedStr = [formatter stringFromDate:sender.date];
}

- (void)setName:(NSString *)name
{
    _name = name;
    self.titleLabel.text = name;
}

- (void)cancelClick
{
    [self dismiss];
}

- (void)confirmClick{
    self.label.text = self.selectedStr;
    [self cancelClick];
}

- (void)show
{
    self.hidden = NO;
    [UIView animateWithDuration:.3 animations:^{
        self.mainView.y = HEIGHT - 200 - self.bottomHeight;
    }];
}

- (void)dismiss
{
    [UIView animateWithDuration:.3 animations:^{
        self.mainView.y = HEIGHT;
    } completion:^(BOOL finished) {
        self.hidden = YES;
    }];
    
}

- (CGFloat)bottomHeight
{
    if (iphoneX) {
        return 34;
    }
    return 0;
}

- (NSMutableArray *)dataArray
{
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}
@end

