//
//  DatePicker.m
//  CaiKuBaoDian
//
//  Created by mac on 17/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "YearPicker.h"

@interface YearPicker ()
@property ( nonatomic, strong ) UIView * titleView;
@property ( nonatomic, strong ) UIView * dateView;
@property ( nonatomic, strong ) UILabel * label;
@property ( nonatomic, strong ) UIView * maskView;
@property ( nonatomic, strong ) UIView * mainView;
@end

@implementation YearPicker

+(instancetype)newView
{
    return [[YearPicker alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self initUI];
    }
    return self;
}

- (void)initUI
{
    self.hidden = YES;
    self.maskView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    [self addSubview:self.maskView];
    
    self.mainView = [[UIView alloc] initWithFrame:CGRectMake(0, HEIGHT, WIDTH, 200)];
    [self addSubview:self.mainView];
    
    self.titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 40)];
    [self.mainView addSubview:self.titleView];
    self.titleView.backgroundColor = viewGaryColor;
    
    self.dateView = [[UIView alloc] initWithFrame:CGRectMake(0, self.titleView.bottom, WIDTH, self.mainView.height - self.titleView.height)];
    [self.mainView addSubview:self.dateView];
    self.dateView.backgroundColor = UIColor.whiteColor;
    
    self.cancelBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    self.cancelBtn.frame = CGRectMake(0, 0, 80, self.titleView.height);
    [self.cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
    [self.cancelBtn addTarget:self action:@selector(cancelClick) forControlEvents:UIControlEventTouchUpInside];
    [self.titleView addSubview:self.cancelBtn];

    self.confirmBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    self.confirmBtn.frame = CGRectMake(WIDTH - 80, 0,80, self.titleView.height);
    [self.confirmBtn setTitle:@"确定" forState:UIControlStateNormal];
//    [self.confirmBtn addTarget:self action:@selector(confirmClick) forControlEvents:UIControlEventTouchUpInside];
    [self.titleView addSubview:self.confirmBtn];
    
    self.label = [[UILabel alloc] initWithFrame:CGRectMake(80, 0, WIDTH - 160, self.titleView.height)];
    [self.titleView addSubview:self.label];
    self.label.textAlignment = NSTextAlignmentCenter;
    
    self.label.text = @"年份";
    [self.maskView touchEvents:self action:@selector(cancelClick)];
    [self createYearPicker];
}

- (void)createYearPicker
{
    self.pickerView = [[UIPickerView alloc] initWithFrame:self.dateView.bounds];
    [self.dateView addSubview:self.pickerView];
    self.pickerView.dataSource = self;
    self.pickerView.delegate = self;
    
    if (self.dataArray.count < 1) {
        self.dataArray = [NSMutableArray arrayWithObjects:@"2013",@"2014", @"2015", @"2016", @"2017", @"2018", @"2019", nil];
        self.selectedStr = self.dataArray[6];
        [self.pickerView selectRow:self.dataArray.count - 1 inComponent:0 animated:YES];
    }
    
}

#pragma mark UIPickerViewDataSource delegate
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return self.dataArray.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return self.dataArray[row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    self.selectedStr = self.dataArray[row];
}

- (void)setName:(NSString *)name
{
    _name = name;
    self.label.text = name;
}

- (void)cancelClick
{
    [self dismiss];
}

- (void)confirmClick
{
    
}

- (void)show
{
    self.hidden = NO;
    [UIView animateWithDuration:.3 animations:^{
        self.mainView.y = HEIGHT - 200 - self.bottomHeight;
    }];
}

- (void)dismiss
{
    [UIView animateWithDuration:.3 animations:^{
        self.mainView.y = HEIGHT;
    } completion:^(BOOL finished) {
        self.hidden = YES;
    }];

}

- (CGFloat)bottomHeight
{
    if (iphoneX) {
        return 34;
    }
    return 0;
}

- (NSMutableArray *)dataArray
{
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}
@end
