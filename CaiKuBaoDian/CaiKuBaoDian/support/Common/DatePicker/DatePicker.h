//
//  DatePicker.h
//  CaiKuBaoDian
//
//  Created by mac on 04/05/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DatePicker : UIView
@property ( nonatomic, copy ) NSString * name;
@property ( nonatomic, strong ) UIButton * cancelBtn;
@property ( nonatomic, strong ) UIButton * confirmBtn;
@property (nonatomic, strong) UIDatePicker *datePicker;
@property ( nonatomic, strong ) NSMutableArray * dataArray;
@property ( nonatomic, copy ) NSString * selectedStr;
@property ( nonatomic, strong ) UILabel * label;
+(instancetype)newView;
- (void)show;
- (void)dismiss;
@end

NS_ASSUME_NONNULL_END
