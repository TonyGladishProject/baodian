//
//  DatePicker.h
//  CaiKuBaoDian
//
//  Created by mac on 17/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YearPicker : UIView<UIPickerViewDataSource,UIPickerViewDelegate>
@property ( nonatomic, copy ) NSString * name;
@property ( nonatomic, strong ) UIButton * cancelBtn;
@property ( nonatomic, strong ) UIButton * confirmBtn;
@property (nonatomic, strong) UIPickerView *pickerView;
@property ( nonatomic, strong ) NSMutableArray * dataArray;
@property ( nonatomic, copy ) NSString * selectedStr;
+(instancetype)newView;
- (void)show;
- (void)dismiss;
@end

NS_ASSUME_NONNULL_END
