//
//  TextView.h
//  CaiKuBaoDian
//
//  Created by mac on 22/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TextView : UITextView<UITextViewDelegate>
@property ( nonatomic, copy ) NSString * placeHolder;
+(instancetype)newView;
@end

NS_ASSUME_NONNULL_END
