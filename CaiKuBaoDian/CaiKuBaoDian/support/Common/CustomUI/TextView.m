//
//  TextView.m
//  CaiKuBaoDian
//
//  Created by mac on 22/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "TextView.h"

@implementation TextView

- (void)setPlaceHolder:(NSString *)placeHolder
{
    _placeHolder = placeHolder;
    self.text = placeHolder;
    self.textColor = cellWordColor;
}

+(instancetype)newView
{
    return [[TextView alloc] initWithFrame:CGRectMake(0, 0, WIDTH/5.3*2, 40)];
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self createUI];
    }
    return self;
}

- (void)createUI
{
    [self layerBorderWidth:1 color:viewGaryColor];
    [self layerMask:10];
    self.font = [UIFont systemFontOfSize:17];
    self.delegate = self;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if (CGColorEqualToColor(textView.textColor.CGColor, cellWordColor.CGColor)) {
        textView.text = @"";
        textView.textColor = UIColor.blackColor;
    }
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if (textView.text.length < 1) {
        textView.text = self.placeHolder;
        self.textColor = cellWordColor;
    }
}

@end
