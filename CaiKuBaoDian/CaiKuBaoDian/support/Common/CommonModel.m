//
//  CommonModel.m
//  CaiKuBaoDian
//
//  Created by mac on 19/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "CommonModel.h"

@implementation CommonModel

@end

@implementation GalleryModel

@end

@implementation GalleryDetailModel

@end

@implementation ForumModel

@end

@implementation LeiTaiModel
+ (NSDictionary *)mj_replacedKeyFromPropertyName
{
    return @{@"Description":@"description"};
}
@end

@implementation LTDetailModel
- (UIColor *)color
{
    if (self.num.intValue == 1) {
        return UIColorFromRGB(0xFC2431);
    } else if (self.num.intValue == 2) {
        return UIColorFromRGB(0xFC931C);
    } else if (self.num.intValue == 3) {
        return UIColorFromRGB(0xFCD00B);
    } else {
        return UIColorFromRGB(0xB5C4C3);
    }
    return UIColorFromRGB(0xFC2431);
}
@end

@implementation ZLDetailModel
+ (NSDictionary *)mj_replacedKeyFromPropertyName
{
    return @{@"Description":@"description"};
}
@end

@implementation ZiLiaoModel
- (void)setBack_color:(NSString *)back_color
{
    _back_color = back_color;
    if (back_color.length < 1) {
        _back_color = @"#f0f0f0";
    }
}

- (void)setBox_color:(NSString *)box_color
{
    _box_color = box_color;
    if (box_color.length < 1) {
        _box_color = @"#cd0000";
    }
}

- (void)setFont_color:(NSString *)font_color
{
    _font_color = font_color;
    if (font_color.length < 1) {
        _font_color = @"#2278B3";
    }
}
@end

@implementation SXModel

@end

@implementation CommentModel

@end

@implementation CASXModel
+ (NSDictionary *)mj_replacedKeyFromPropertyName
{
    return @{@"qishu":@"期数",@"date":@"日期",@"tema":@"特码"};
}

- (NSString *)color
{
    if ([self.yanse isEqualToString:@"hong"]) {
        return @"红";
    } else if ([self.yanse isEqualToString:@"lv"]) {
        return @"绿";
    }
    return @"蓝";
}
@end

@implementation PlatformModel
@end

@implementation JifenRecordModel
@end
@implementation MyNoticeModel
@end
@implementation BeforeHistoryModel
@end

