//
//  API.h
//  BaseFrame
//
//  Created by mac on 08/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#ifndef API_h
#define API_h

#define UserDefaults [NSUserDefaults standardUserDefaults]
#define UserPhone [UserDefaults valueForKey:@"mobile"]
//"http://www.076888.com:8881/",
//"http://www.xingrenwu.com:8881/",
//"http://www.weiwo8.com:8881/",
//"http://www.luodouwang.com:8881/",
// "http://www.ad.lfjsjs.com:8881/"};
//
/**
 正式环境
 */
#define APIHttp [UserDefaults valueForKey:@"APIHttp"]
/**
 测试环境
 */
//#define APIHttp @"http://www.xingrenwu.com:8881/"
/**
 虚拟环境
 */
//#define APIHttp @"http://lh.cdyso.com:8888/"
#define APIHTTP [APIHttp stringByAppendingString:@"index.php?"]

//minTBC
#define APIImg  [APIHTTP stringByAppendingString:@"g=portal&m=article&a=app_start"]
#define APIVersion  [APIHTTP stringByAppendingString:@"g=portal&m=article&a=app_version&device=ios"]
#define APIOnline [APIHTTP stringByAppendingString:@"g=user&m=login&a=online"]
#define APIDownload  [@"https://www.xingrenwu.com/" stringByAppendingString:@"app.plist"]
#define APINoRead [APIHTTP stringByAppendingString:@"g=api&m=Notice&a=no_read_num"]
#define APIUserInfo [APIHTTP stringByAppendingString:@"m=article&a=getuserinfo"]
#define imgAPI(name) [APIHttp stringByAppendingString:name]
//home
#define APIOpenNum  [APIHTTP stringByAppendingString:@"g=Portal&m=Article&a=open_kj"]
#define APIBanner  [APIHTTP stringByAppendingString:@"g=portal&m=Index&a=faxian&slide_cid=2"]
#define APILotteryNums  [APIHTTP stringByAppendingString:@"g=portal&m=article&a=kjzb"]
#define APIShareImgs  [APIHTTP stringByAppendingString:@"g=User&m=Lottery&a=TopBanner"]
#define APINotice  [APIHTTP stringByAppendingString:@"g=user&m=Easemob&a=Hornmsg"]

#define APIHistory [APIHTTP stringByAppendingString:@"g=portal&m=Index&a=kaijianglishi"]
#define APIShare [APIHTTP stringByAppendingString:@"g=Portal&m=Article&a=share"]

//直播
#define APIWufen [APIHTTP stringByAppendingString:@"g=portal&m=Index&a=get_wf_kj"]
#define APILLImage [APIHTTP stringByAppendingString:@"g=portal&m=article&a=ltgg"]
#define APILotteryLive [APIHTTP stringByAppendingString:@"g=user&m=Lottery&a=KJHtml"]//废弃
#define APIGallery [APIHTTP stringByAppendingString:@"g=portal&m=article&a=get_zm"]//图库
#define APIGalleryYear [APIHTTP stringByAppendingString:@"g=Portal&m=Article&a=get_ln"]
#define APIQS [APIHTTP stringByAppendingString:@"g=portal&m=article&a=get_qs"]
#define APIGDImg [APIHTTP stringByAppendingString:@"g=portal&m=article&a=get_image_url"]
#define APIForum [APIHTTP stringByAppendingString:@"g=portal&m=article&a=get_msg_type"]//论坛
#define APIZone [APIHTTP stringByAppendingString:@"g=portal&m=article&a=forum"]
#define APILeiTai [APIHTTP stringByAppendingString:@"g=portal&m=article&a=bwlt"]//擂台
#define APILeiTaiRule @"http://ad.lfjsjs.com/index.php/portal/article/get_bwlt"//擂台规则
#define APIBeforeHistory [APIHTTP stringByAppendingString:@"g=portal&m=article&a=get_history_list"]
#define APILeiTaiDetail [APIHTTP stringByAppendingString:@"g=portal&m=article&a=get_lt_user_list"]
#define APILTDLike [APIHTTP stringByAppendingString:@"g=Api&m=PingLun&a=dian_zhan"]
#define APIPostWords [APIHTTP stringByAppendingString:@"g=portal&m=article&a=add_guest"]
#define APIZiliao [APIHTTP stringByAppendingString:@"g=api&m=ZiLiao&a=get_list"]//资料
//六合统计
#define APILHTJ [APIHTTP stringByAppendingString:@"g=portal&m=article&a=liuhetongji&type=50"]
#define APILHSXCZ [APIHTTP stringByAppendingString:@"g=portal&m=article&a=shuxingcanzhao"]
#define APILHTMLS [APIHTTP stringByAppendingString:@"g=portal&m=article&a=temalishi&type=50"]
#define APILHZMLS [APIHTTP stringByAppendingString:@"g=portal&m=article&a=zhengmalishi&type=50"]
#define APILHWSDX [APIHTTP stringByAppendingString:@"g=portal&m=article&a=weishudaxiao&type=50"]

#define APILHSXTM [APIHTTP stringByAppendingString:@"g=portal&m=article&a=shengxiaotemalengretu&type=50"]
#define APILHSXZM [APIHTTP stringByAppendingString:@"g=portal&m=article&a=shengxiaozhengmalengretu&type=50"]
#define APILHBSTM [APIHTTP stringByAppendingString:@"g=portal&m=article&a=bosetema&type=50"]
#define APILHBSZM [APIHTTP stringByAppendingString:@"g=portal&m=article&a=bosezhengma&type=50"]
#define APILHTMLM [APIHTTP stringByAppendingString:@"g=portal&m=article&a=temaliangmian&type=50"]

#define APILHTMWS [APIHTTP stringByAppendingString:@"g=portal&m=article&a=temaweishu&type=50"]
#define APILHZMWS [APIHTTP stringByAppendingString:@"g=portal&m=article&a=zhengmaweishu&type=50"]
#define APILHZMZF [APIHTTP stringByAppendingString:@"g=portal&m=article&a=zhengmazongfen&type=50"]
#define APILHHMBD [APIHTTP stringByAppendingString:@"g=portal&m=article&a=haomaboduan&type=50"]
#define APILHJQYS [APIHTTP stringByAppendingString:@"g=portal&m=article&a=jiaqinyeshou&type=50"]

#define APILHLMZS [APIHTTP stringByAppendingString:@"g=portal&m=article&a=lianmazoushi&type=50"]
#define APILHLXZS [APIHTTP stringByAppendingString:@"g=portal&m=article&a=lianxiaozoushi&type=50"]

//四不像
#define APIComment [APIHTTP stringByAppendingString:@"g=Api&m=PingLun&a=get_ping_lun"]
#define APIPostComment [APIHTTP stringByAppendingString:@"g=Api&m=PingLun&a=ping_lun"]

//图表
#define APIChart [APIHTTP stringByAppendingString:@"g=portal&m=article&a=zoushifenxi"]

//投票
#define APIVote [APIHTTP stringByAppendingString:@"g=portal&m=article&a=caimin_vote"]

//详情
#define APIDetailUserInfo [APIHTTP stringByAppendingString:@"g=portal&m=article&a=postdetail"]
#define APIDetailHeader [APIHTTP stringByAppendingString:@"g=portal&m=article&a=get_article_ad"]
#define APIDetailShengXiao [APIHTTP stringByAppendingString:@"g=portal&m=article&a=vote_proportion"]
#define APIDetailComment [APIHTTP stringByAppendingString:@"g=portal&m=article&a=add_guest_reply"]
#define APIDetailVote [APIHTTP stringByAppendingString:@"g=portal&m=article&a=caimin_vote"]

#define APIDetailLT [APIHTTP stringByAppendingString:@"g=portal&m=article&a=bw_info"]
#define APIDetailZL [APIHTTP stringByAppendingString:@"g=api&m=ZiLiao&a=get_one"]

#define APIMore [APIHTTP stringByAppendingString:@"g=User&m=Lottery&a=LBImage"]

//setting
#define APIDisclaimer [APIHTTP stringByAppendingString:@"g=portal&m=Index&a=posts"]
#define APIFeedback [APIHTTP stringByAppendingString:@"g=portal&m=article&a=fankui"]

//share
#define APIShareDes [APIHTTP stringByAppendingString:@"g=portal&m=article&a=get_integral_describe"]
#define APIShareLink [APIHTTP stringByAppendingString:@"g=Portal&m=Article&a=share"]

//六合宝箱
#define APIBox [APIHTTP stringByAppendingString:@"g=portal&m=article&a=bxzp_xckj"]
#define APIKit [APIHTTP stringByAppendingString:@"g=portal&m=article&a=xunjijinnang"]
#define APIAssistant [APIHTTP stringByAppendingString:@"g=portal&m=Index&a=chaxunzhushou"]
#define APIDate [APIHTTP stringByAppendingString:@"g=portal&m=article&a=get_kj_date"]
#define APIDateWeb [APIHttp stringByAppendingString:@"getRL.html"]

//login
#define APICode [APIHTTP stringByAppendingString:@"g=user&m=register&a=send_sms"]
#define APILogin [APIHTTP stringByAppendingString:@"g=user&m=login&a=dologin"]
#define APIForgetPassword [APIHTTP stringByAppendingString:@"m=article&a=findpassword"]
#define APIRegister [APIHTTP stringByAppendingString:@"g=user&m=register&a=doregister"]
#define APIImgCode [APIHTTP stringByAppendingString:@"g=user&m=register&a=get_captcha"]

//个人中心
#define APINickname [APIHTTP stringByAppendingString:@"m=article&a=editnickname"]
#define APIPassword [APIHTTP stringByAppendingString:@"m=article&a=editpassword"]
#define APILike [APIHTTP stringByAppendingString:@"g=portal&m=article&a=mypost"]
#define APIFocus [APIHTTP stringByAppendingString:@"g=portal&m=article&a=myfriends"]
#define APIFans [APIHTTP stringByAppendingString:@"g=portal&m=article&a=myfensi"]
#define APIPlatform [APIHTTP stringByAppendingString:@"g=portal&m=article&a=get_pingtai"]
#define APIExchangePost [APIHTTP stringByAppendingString:@"g=portal&m=article&a=add_duihuan"]
#define APIMyRich [APIHTTP stringByAppendingString:@"g=portal&m=article&a=mymammon"]
#define APIJifenRecords [APIHTTP stringByAppendingString:@"g=portal&m=article&a=integral_log"]
#define APIExchangeRecords [APIHTTP stringByAppendingString:@"g=portal&m=article&a=duihuan_log"]
#define APIMyNotice [APIHTTP stringByAppendingString:@"g=api&m=Notice&a=notice_list"]

//更换头像
#define APIHeaderUpdate [APIHTTP stringByAppendingString:@"g=user&m=profile&a=avatar_upload"]
#define APIAvatar [APIHTTP stringByAppendingString:@"g=Portal&m=Article&a=get_avatar"]

//红包
#define APIPocket [APIHTTP stringByAppendingString:@"g=portal&m=article&a=get_score_rule"]
#define APIPocketPost [APIHTTP stringByAppendingString:@"g=portal&m=article&a=get_redpacket"]

#endif /* API_h */
