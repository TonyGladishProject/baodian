//
//  UIView+TGExtension.m
//  BaseFrame
//
//  Created by mac on 08/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import "UIView+TGExtension.h"

@implementation UIView (TGExtension)
- (void)setX:(CGFloat)x
{
    CGRect frame = self.frame;
    frame.origin.x = x;
    self.frame = frame;
}

- (CGFloat)x
{
    return self.frame.origin.x;
}

- (void)setY:(CGFloat)y
{
    CGRect frame = self.frame;
    frame.origin.y = y;
    self.frame = frame;
}

- (CGFloat)y
{
    return self.frame.origin.y;
}

- (void)setWidth:(CGFloat)width
{
    CGRect frame = self.frame;
    frame.size.width = width;
    self.frame = frame;
}

- (CGFloat)width
{
    return self.frame.size.width;
}

- (void)setHeight:(CGFloat)height
{
    CGRect frame = self.frame;
    frame.size.height = height;
    self.frame = frame;
}

- (CGFloat)height
{
    return self.frame.size.height;
}

- (void)setCenterX:(CGFloat)centerX
{
    CGPoint center = self.center;
    center.x = centerX;
    self.center = center;
}

- (CGFloat)centerX
{
    return self.center.x;
}
- (void)setCenterY:(CGFloat)centerY
{
    CGPoint center = self.center;
    center.y = centerY;
    self.center = center;
}

- (CGFloat)centerY
{
    return self.center.y;
}

- (void)setOrigin:(CGPoint)origin {
    CGRect frame = self.frame;
    frame.origin = origin;
    self.frame = frame;
}

- (CGPoint)origin {
    return self.frame.origin;
}

- (void)setSize:(CGSize)size
{
    CGRect frame = self.frame;
    frame.size = size;
    self.frame = frame;
}

- (CGSize)size
{
    return self.frame.size;
}

- (void)setBottom:(CGFloat)bottom
{
    self.bottom = bottom;
}

- (CGFloat)bottom
{
    return self.frame.origin.y + self.frame.size.height;
}

- (void)setRight:(CGFloat)right
{
    self.right = right;
}

- (CGFloat)right
{
    return self.frame.origin.x + self.frame.size.width;
}

- (void)touchEvents:(id)target action:(SEL)sel
{
    self.userInteractionEnabled = YES;
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:target action:sel];
    tap.numberOfTapsRequired = 1;
    tap.numberOfTouchesRequired = 1;
    [self addGestureRecognizer:tap];
}

- (void)layerMask
{
    self.layer.cornerRadius = self.height/2;
    self.layer.masksToBounds = YES;
}
- (void)layerMask:(CGFloat)radius
{
    self.layer.cornerRadius = radius;
    self.layer.masksToBounds = YES;
}

- (void)layerMask5
{
    self.layer.cornerRadius = 5;
    self.layer.masksToBounds = YES;
}

- (void)layerBorderWidth:(CGFloat)width color:(UIColor *)color
{
    self.layer.borderWidth = width;
    self.layer.borderColor = color.CGColor;
}

- (KLCPopup *)popupShowView:(UIView *)view center:(CGPoint)center
{
    return [self popupShowView:view center:center showType:KLCPopupShowTypeSlideInFromBottom dismissType:KLCPopupDismissTypeSlideOutToBottom];
}

- (KLCPopup *)popupShowCenterView:(UIView *)view center:(CGPoint)center
{
    return [self popupShowView:view center:center showType:KLCPopupShowTypeGrowIn dismissType:KLCPopupDismissTypeGrowOut];
}

- (KLCPopup *)popupShowView:(UIView *)view center:(CGPoint)center showType:(KLCPopupShowType)showType dismissType:(KLCPopupDismissType)dismissType
{
    [view layerMask:5];
    KLCPopup * popup = [KLCPopup popupWithContentView:view];
    popup.showType = showType;
    popup.dismissType = dismissType;
    [popup showAtCenter:center inView:self];
    [popup show];
    return popup;
}

- (void)showAlertWithVC:(UIViewController *)vc title:(NSString *)title message:(NSString *)message  completion:(void (^)(void))completion
{
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [controller addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"确定", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if (completion) {
            completion();
        }
    }]];
    [controller addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"取消", nil) style:UIAlertActionStyleCancel handler:nil]];
    [vc presentViewController:controller animated:YES completion:nil];
}

- (void)showSheetWithVC:(UIViewController *)vc title:(NSString *)title message:(NSString *)message text:(NSString *)text string:(NSString *)string execution:(void (^)(void))execution completion:(void (^)(void))completion
{
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleActionSheet];
    [controller addAction:[UIAlertAction actionWithTitle:text style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if (execution) {
            execution();
        }
    }]];
    [controller addAction:[UIAlertAction actionWithTitle:string style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if (completion) {
            completion();
        }
    }]];
    [controller addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"取消", nil) style:UIAlertActionStyleCancel handler:nil]];
    [vc presentViewController:controller animated:YES completion:nil];
}
@end
