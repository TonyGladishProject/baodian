//
//  UIView+TGExtension.h
//  BaseFrame
//
//  Created by mac on 08/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KLCPopup.h"

NS_ASSUME_NONNULL_BEGIN

@interface UIView (TGExtension)
@property (nonatomic, assign) CGFloat x;
@property (nonatomic, assign) CGFloat y;
@property (nonatomic, assign) CGFloat width;
@property (nonatomic, assign) CGFloat height;
@property (nonatomic, assign) CGPoint origin;
@property (nonatomic, assign) CGSize  size;
@property (nonatomic, assign) CGFloat centerX;
@property (nonatomic, assign) CGFloat centerY;
@property (nonatomic, assign) CGFloat bottom;
@property ( nonatomic, assign ) CGFloat right;

- (void)touchEvents:(id)target action:(SEL)sel;
- (void)layerMask;
- (void)layerMask5;
- (void)layerMask:(CGFloat)radius;
- (void)layerBorderWidth:(CGFloat)width color:(UIColor *)color;

/**
 自定义弹框,父视图方法，添加提示视图并显示
 */
- (KLCPopup *)popupShowView:(UIView *)view center:(CGPoint)center;
- (KLCPopup *)popupShowCenterView:(UIView *)view center:(CGPoint)center;
- (KLCPopup *)popupShowView:(UIView *)view center:(CGPoint)center showType:(KLCPopupShowType)showType dismissType:(KLCPopupDismissType)dismissType;
/**
 显示警示框-居住框
 */
- (void)showAlertWithVC:(UIViewController *)vc title:(NSString *)title message:(NSString *)message  completion:(void (^)(void))completion;
/**
 显示列表框-底部框
 */
- (void)showSheetWithVC:(UIViewController *)vc title:(NSString *)title message:(NSString *)message text:(NSString *)text string:(NSString *)string execution:(void (^)(void))execution completion:(void (^)(void))completion;
@end

NS_ASSUME_NONNULL_END
