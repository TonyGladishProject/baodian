//
//  Configure.h
//  BaseFrame
//
//  Created by mac on 08/04/2019.
//  Copyright © 2019 mac. All rights reserved.
//

#ifndef Configure_h
#define Configure_h


#define WIDTH   [UIScreen mainScreen].bounds.size.width
#define HEIGHT [UIScreen mainScreen].bounds.size.height
#define centerPoint CGPointMake(WIDTH/2, HEIGHT/3)

// block self 弱化 强化
#define kSelfWeak __weak typeof(self) weakSelf = self;
#define kSelfStrong __strong __typeof__(self) strongSelf = weakSelf;

#define iphoneX HEIGHT > 736
#define iphone8Plus WIDTH > 375
#define iphone5 WIDTH < 375

#define imgHTTP(name) [@"http://" stringByAppendingString:name]


#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
#define random(r, g, b, a) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:(a)/255.0]
#define randomColor random(arc4random_uniform(256), arc4random_uniform(256), arc4random_uniform(256), arc4random_uniform(256))

#define MainColor UIColorFromRGB(0xcd0000)
#define MainColorDeep UIColorFromRGB(0x930000)
#define viewGaryColor  UIColorFromRGB(0xf4f4f4)
#define cellLineColor  UIColorFromRGB(0xebebeb)
#define cellHeaderColor  UIColorFromRGB(0xf0f0f0)
#define cellWordColor  UIColorFromRGB(0x939393)
#define loginLineColor  UIColorFromRGB(0xe7e7e7)
#define grassColor UIColorFromRGB(0x79BE01)
#define viewRedColor UIColorFromRGB(0xFF1A1A)
#define viewGreenColor UIColorFromRGB(0x79BE01)
#define viewBlueColor UIColorFromRGB(0x2278B3)

#define fontCell [UIFont systemFontOfSize:13]

#ifdef DEBUG
#define LRLog(...) NSLog(@"%s 第%d行 \n %@\n\n",__func__,__LINE__,[NSString stringWithFormat:__VA_ARGS__])
#else
#define LRLog(...)

#endif
#endif /* Configure_h */
